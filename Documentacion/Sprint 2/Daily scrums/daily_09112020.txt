

###########################################################################################
###########################################################################################
-------------------------------------------------------------------------------------------
                                    REUNION 09-11-2020 
-------------------------------------------------------------------------------------------
###########################################################################################
###########################################################################################



###########################################################################################
                                    - 1. ASISTENTES -                                                
###########################################################################################
###########################################################################################
   
    - Adrián 
    - Laura M.
    - Laura P.
    - Pablo
    - Rong Rong
    - Carlos



###########################################################################################
                                    - 2. TRABAJO SPRINT 2 -                                                
###########################################################################################
###########################################################################################
    
  - Tareas pendientes Sprint anterior:
    * 1.10 Calibración de los sensores en base a una medida conocida.
    * 2.3 Información adicional --> incorporar otros contaminantes, no solo O3 e integrar.
    * 3.4 Autocorrección de datos --> viabilidad del scrapping en una web pública de la estación de referencia de Gandía.
    * 3.5 Informe del estado de los nodos --> Integrar, y agregar lecturas erroneas Carlos (medidas de API).
    * 3.6 BBDD Usuarios --> falta estadística con distancia recorrida por los usuarios.
      + Incorporar historia nueva solo para el registro de usuarios.
    * 3.9 y 3.10 Revisar y consensuar wireframes --> navegabilidad corregir.
    * 3.11 Modo juego -->  Incorporar a la app de forma visual para poder demostrar el funcionamiento 
                      y agregar mecanicas básicas.

  - Nuevas tareas a agregar:
    * 3.3 Manual de usuario.
    * 3.2 Autonomía de la batería --> pedir batería.




###########################################################################################
                                    - 3. DESARROLLO DE LA REUNIÓN -                                                
###########################################################################################
###########################################################################################

  Empieza a las 19:00, y se comentan las tareas pendientes del sprint anterior, y las nuevas posibles
  a incorporar a este sprint. Carlos propone actualizar el repositorio (todas las ramas de cada miembro),
  después de cada daily scrum para estar todos actualizados. Se ha acordado realizar los daily cada 2 días,
  de forma que sean los martes, jueves y sábados. Se ha hablado del rediseño de los wireframes, aclarando como 
  sería la navegación en el apartado web, y añadiendo un splash screen en la app movil, a la vez que permitir
  acceder como invitado directamente y dar la opción de acceder desde dicha página, reduciendo el número de 
  acciones de los usuarios. Se ha hablado de la posibilidad de obtener los datos públicos de las estaciones de medida
  a través de la opción de descarga de CSV y leyendo de este de forma secuencial los datos necesarios. 
  En cuanto al resto de tareas del sprint anterior, queda aparentemente claro como trabajar para solventar los problemas.
  Las nuevas tareas para este sprint, quedan pendientes de debatir en función de la reunión de mañana con el 
  product owner, provisionalmente se proponen las del punto anterior (3.2 y 3.3).
  
  Por otro lado se ha llegado a un acuerdo para solventar debates en equipo, se tratará de llegar a un consenso en función del problema, 
  en caso de que este se demore demasiado, sea demasiado polarizado entre los miembros del equipo, se someterá a votación para 
  no extender más de 1 o 2 días la toma de decisiones.

  Con esto finaliza la reunión a las 20:00 (hora aproximada).


