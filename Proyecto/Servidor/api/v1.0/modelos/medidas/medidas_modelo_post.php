<?php

/**
 * Inserta una nueva medida en la BBDD
 * @author Adrian Garcia Andreu
 * @version 1.1
 */


$sql = "SELECT * FROM zonas WHERE (".$form_params["latitud"]." > zonas.latitudMinima && ".$form_params["latitud"]." < zonas.latitudMaxima) ";
$sql .= "&& (".$form_params["longitud"]." > zonas.longitudMinima && ".$form_params["longitud"]." < zonas.longitudMaxima) ";

$resultado = mysqli_query($conexion, $sql);

// Almacena la respuesta en un array asociativo

$fila = mysqli_fetch_assoc($resultado);


$zona="Gandia"; // zona por defecto para las medidas en caso de no encontrar ninguna compatible
if($fila != NULL){
    $zona = $fila["zona"]; // Zona de la medida
}



$sql = "INSERT INTO medidas(valor, latitud, longitud, momento, tipoMedida, correo, zona) ";
$sql .= "VALUES(".$form_params["valor"].", ".$form_params["latitud"].", ".$form_params["longitud"].", ";
$sql .= "NOW() , '".$form_params["tipoMedida"]."', '".$form_params["correo"]."', '".$zona."')";

$respuesta = mysqli_query($conexion, $sql);
