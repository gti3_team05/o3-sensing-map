<?php
/**
 * Obtiene todas las medidas de la BBDD
 * @author Adrian Garcia Andreu
 * @version 1.2
 */

$sql = "SELECT * FROM medidas ORDER BY Momento DESC";

// En caso de que se reciba la id de un usuario, la consulta filtra por usuario
if (isset($query_params["correo"])){
    $sql = "SELECT * FROM medidas WHERE correo LIKE \"".$query_params["correo"]."\" ORDER BY Momento DESC";
}
$resultado = mysqli_query($conexion, $sql);

// Almacena la respuesta en un array asociativo
$respuesta = array();
while ($fila = mysqli_fetch_assoc($resultado)) {
    array_push($respuesta, $fila);
};

