<?php
/**
 * Se cierra la sesion iniciada en el servidor web
 * @author Adrian Garcia Andreu
 * @version 1.2
 */

session_unset();
session_destroy();


$respuesta = true;