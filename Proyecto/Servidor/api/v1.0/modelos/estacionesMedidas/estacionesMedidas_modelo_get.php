<?php
/**
 * Consulta estaciones oficiales en la BBDD
 * @author Carlos Ibanez Lamas
 * @version 1.0
 */

$sql = "SELECT * FROM estacionesoficiales";

$resultado = mysqli_query($conexion, $sql);

// Almacena la respuesta en un array asociativo
$respuesta = array();
while ($fila = mysqli_fetch_assoc($resultado)) {
    array_push($respuesta, $fila);
};