<?php

$ruta = $_SERVER['DOCUMENT_ROOT'];
$ruta .= "/proyecto3a/app/servidor/funciones/";

$rutaRecursos = $_SERVER['DOCUMENT_ROOT']."/proyecto3a/app/servidor/recursos/";

require_once($ruta."interpolacion.php");


// Se tratan de leer los datos si se han pasado parametros

$respuesta = array();

// Valores por defecto
$tipo = "O3";
$fecha = date("Y_m_d"); // Fecha actual

if(isset($query_params["tipo"])){
    $tipo = $query_params["tipo"];
}

if(isset($query_params["fecha"])){
    $fecha = $query_params["fecha"];
}


$datos = leerInterpolacion($tipo, $fecha);

if(count($datos)>0){
    $respuesta = $datos;
    $recursoExtra = $rutaRecursos."/interpolacion_".$fecha."_".$tipo.".png";

} else {
    $respuesta = NULL;
}


?>