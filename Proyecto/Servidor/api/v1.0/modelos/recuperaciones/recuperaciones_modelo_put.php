<?php

/**
 * Inserta un nuevo token de recuperacion de contraseña en la BBDD
 * @author Adrián García Andreu
 * @version 1.1
 */
$correo = $query_params['correo'];

// Generacion de clave de recuperacion
$token = getToken(crypto_rand_secure(15,20));

// Comprobacion de token ya existente
// Mientras no se halle un token unico, se seguira buscando uno.
$tokenUnico = false; 
$intentos = 5;
while($tokenUnico==false && $intentos>0){

    $sql = "SELECT token FROM recuperacionesclave WHERE token LIKE '".$token."'";
    $resultado = mysqli_query($conexion, $sql);
    $fila = mysqli_fetch_assoc($resultado);
    if($fila ==NULL){
        $tokenUnico = true;
    }

    $intentos--;
}



$sql = "INSERT INTO recuperacionesclave(token,correo, momento, usado) ";
$sql .= "VALUES('$token', '$correo', NOW(), false) ";

$respuesta = mysqli_query($conexion, $sql);



$mensaje = "Se ha solicitado un cambio de clave para su usuario de la plataforma AtmO3.\n";
$mensaje .= "Su nueva clave para acceder es la siguiente: \n\n". $token."\n\n";
$mensaje .= "Sugerimos que al acceder a la app se cambie a una clave nueva del propio usuario \n\n";
$mensaje .= "Un saludo, el equipo de AtmO3 ";


$to_email = $correo;
$subject = 'AtmO3 Recuperacion de clave';
$message = $mensaje;
$headers = 'From: atmozonito3@gmail.com' . "\r\n";
$headers .= 'Bcc: atmozonito3@gmail.com' . "\r\n";

mail($to_email,$subject,$message,$headers);



// Se actualiza la clave del usuario
$sql = "UPDATE usuarios SET clave='".$token."' WHERE correo LIKE '".$correo."'";
$respuesta = mysqli_query($conexion, $sql);





// Genera un numero aleatorio de forma criptográfica
function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 1) return $min; // not so random...
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1; // length in bytes
    $bits = (int) $log + 1; // length in bits
    $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter; // discard irrelevant bits
    } while ($rnd > $range);
    return $min + $rnd;
}

// Genera un token aleatorio con caracteres alfanumericos
function getToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet.= "0123456789";
    $max = strlen($codeAlphabet); // edited

    for ($i=0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, $max-1)];
    }

    return $token;
}