<?php

/**
 * Obtiene un listado de usuarios y su actividad
 * @author Adrian Garcia Andreu
 * @version 1.2
 */

 // Actividad de todos los usuarios
$sql = "SELECT u.*, MAX(medidas.momento) AS ultimaActividad, MIN(medidas.momento) AS primeraActividadHoy FROM usuarios as u ";
$sql .= "INNER JOIN medidas ON u.correo = medidas.correo ";
$sql .= "WHERE (DATE(medidas.momento)=(SELECT DATE(MAX(medidas.momento)) "; 
$sql .= "FROM medidas WHERE correo LIKE u.correo) ) GROUP BY u.correo ";

$resultado = mysqli_query($conexion, $sql);


// Almacena la respuesta en un array asociativo
$respuesta = array();
$indice = 0;
while ($fila = mysqli_fetch_assoc($resultado)) {
    array_push($respuesta, $fila);

    // Por cada usuario, se calcula su actividad del ultimo dia
    //$sql = "SELECT medidas.momento FROM medidas WHERE ( DATE(medidas.momento) = ";
    //$sql .= "( SELECT MAX(DATE(medidas.momento)) as ultimaActividad FROM medidas ";
    //$sql .= "WHERE ((correo LIKE '".$fila["correo"]."')) ) AND correo LIKE '".$fila["correo"]."') ";
    //$sql .= " ORDER BY medidas.momento ASC";

    // Agregada la distancia recorrida
    $sql = "SELECT medidas.momento, medidas.latitud, medidas.longitud FROM medidas WHERE ( DATE(medidas.momento) = ";
    $sql .= "( SELECT MAX(DATE(medidas.momento)) as ultimaActividad FROM medidas ";
    $sql .= "WHERE ((correo LIKE '".$fila["correo"]."')) ) AND correo LIKE '".$fila["correo"]."') ";
    $sql .= " ORDER BY medidas.momento ASC";


    $resultadoSubConsulta = mysqli_query($conexion, $sql);

    // -------------------------------------------------
    // Contador de actividad (horas)
    // --------------------------------------------------

    $umbralActividadSegundos = 30 * 60; // Cada 30 minutos de inactividad se considera que el usuario ha dejado de estar activo
    $minutosActividad = 0;
    $acumuladorSegundos = 0;
    
    $distanciaRecorrida = 0;

    $respuestaSubConsulta = array();
    while ($filaSubConsulta = mysqli_fetch_assoc($resultadoSubConsulta)) {
        array_push($respuestaSubConsulta, $filaSubConsulta);

    };

    // Si el usuario ha tomado al menos, 2 medidas
    if(count($respuestaSubConsulta)>1){ 
        for($h = 1; $h<count($respuestaSubConsulta); $h++){

            // SUMA DE TIEMPO ACTIVO 

            $datetimeActual = new DateTime($respuestaSubConsulta[$h]["momento"]);
            $datetimePrevio = new DateTime($respuestaSubConsulta[$h-1]["momento"]);

            $intervalo = date_diff($datetimeActual, $datetimePrevio); // Intervalo en segundos
            if($intervalo->format("%S") < $umbralActividadSegundos){ //Si el intervalor esta dentro de un umbral de segundos se considera tiempo activo
                
                $acumuladorSegundos += $intervalo->format("%s"); // Se agrega la diferencia en segundos

                // Differencia en minutos
                $diffMinutos = $intervalo->format("%i"); 
                if($diffMinutos > 0){$acumuladorSegundos +=$diffMinutos*60;}
                 
            } // Si no es asi, ese periodo no se considera de actividad
            
            
            // SUMMA DE DISTANCIAS
            
            $latA = $respuestaSubConsulta[$h]["latitud"];
            $lonA = $respuestaSubConsulta[$h]["longitud"];

            $latP = $respuestaSubConsulta[$h-1]["latitud"];
            $lonP = $respuestaSubConsulta[$h-1]["longitud"];
            
            // distancia acumulada
            $distanciaRecorrida += getDistancia($latP, $lonP, $latA, $lonA); // Distancia recorrida en Km


        };

        //echo "\n";
    };



    // Se agrega el tiempo de actividad convirtiendo todos los segundos en minutos
    if($acumuladorSegundos>59){$minutosActividad = round($acumuladorSegundos/60, 0, PHP_ROUND_HALF_DOWN);}

    $respuesta[$indice]["distancia"] = (number_format($distanciaRecorrida, 3, ".","")); // distancia
    $respuesta[$indice]["actividadMinutos"] = ($minutosActividad); // tiempo de actividad

    $indice++; // posicion 
};



/**
 * Funcion que obtiene la distancia en linea recta
 * entre 2 puntos, usado para calcular la distancia aproximada
 * que ha recorrida cada usuario con el nodo sensor
 */
function getDistancia($lat1, $lon1, $lat2, $lon2){
    
    $R = 6378.137; //Radio de la tierra en km
    $dLat = deg2rad( $lat2 - $lat1 );
    $dLong = deg2rad( $lon2 - $lon1 );
    $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * sin($dLong/2) * sin($dLong/2);
    $c = 2 * atan2(sqrt($a), sqrt(1-$a));
    $d = $R * $c;

    return $d; //Retorna tres decimales

};
