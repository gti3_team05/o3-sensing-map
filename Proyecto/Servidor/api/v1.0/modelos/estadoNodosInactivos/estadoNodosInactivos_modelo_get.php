<?php
/**
 * Obtiene las últimas fechas en las que el cada nodo sensor ha estado activo
 * @author Laura Pons Castelló
 * @version 1.0
 */

$sql = "SELECT usuarios.*, MAX(medidas.momento) AS ultimaMedida FROM medidas, usuarios WHERE usuarios.correo = medidas.correo GROUP BY usuarios.correo";
$resultado = mysqli_query($conexion, $sql);

/* Guardamos la respuesta en un array */
$respuesta = array();
$indice = 0;

while ($fila = mysqli_fetch_assoc($resultado)) {
    array_push($respuesta, $fila);
    
    $fecha = date_create();
    
    $fechaActual1 = date_format($fecha, 'Y-m-d H:i:s');
    
    $fecha2 = date_create_from_format('Y-m-d H:i:s', $fechaActual1);
    
    $ultimaMedida = new DateTime($respuesta[$indice]["ultimaMedida"]);
    
    $interval = date_diff($ultimaMedida, $fecha2);
    $tiempoInactivo = $interval->format('%a días y %H:%I:%S horas');
    $tiempoHoras = $interval->format('%H');
    
    if($tiempoHoras > 4){
        $respuesta[$indice]["alerta"] = "roja";
    }else{
        $respuesta[$indice]["alerta"] = "verde";
    }
    
    $respuesta[$indice]["tiempoInactividad"] = ($tiempoInactivo);

    $indice++; // posicion 
}