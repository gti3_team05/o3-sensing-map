<?php

/**
 * Inserta una nueva incidencia en la BBDD
 * @author Laura Pons Castello
 * @version 1.1
 */

// Primero obtenemos el valor del id del sensor según su usuario
$sql1 = "SELECT idNodo FROM usuarios WHERE correo ='".$query_params["correo"]."'";
$resultado = mysqli_query($conexion, $sql1);

// Almacena la respuesta en un array asociativo
$respuesta = array();
while ($fila = mysqli_fetch_assoc($resultado)) {
    array_push($respuesta, $fila);
};

// A continuación subirmos los datos a la bbdd
$sql = "INSERT INTO incidencias(usuario, idNodo, momento, tipoIncidencia, estado, comentarios) ";
$sql .= "VALUES('".$query_params["correo"]."', '".$respuesta[0]["idNodo"]."', NOW(), ";
$sql .= "'".$query_params["tipoIncidencia"]."', '".$query_params["estado"]."', NULL)";

$respuesta = mysqli_query($conexion, $sql);

