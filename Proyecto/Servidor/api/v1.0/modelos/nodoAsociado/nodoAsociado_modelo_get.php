<?php
/**
 * Obtiene si un nodo está asociado a un usuario
 * @author Adrian Garcia Andreu
 * @version 1.2
 */


$sql = "SELECT * FROM nodos WHERE idNodo=".$query_params["idNodo"];
$resultado = mysqli_query($conexion, $sql);
// Almacena la respuesta en un array asociativo
$fila = mysqli_fetch_assoc($resultado);


$respuesta = -1; 

// Si existe el nodo en la BBDD ... se comprueba que no esté asociado a un usuario
if(!is_null($fila)){
    $sql = "SELECT * FROM usuarios WHERE idNodo=".$query_params["idNodo"];
    $resultado = mysqli_query($conexion, $sql);


    $fila = mysqli_fetch_assoc($resultado); // Capturo la respuesta
    if(!is_null($fila)){
        $respuesta = 1; // Nodo ya asociado a un usuario
    } else {
        $respuesta = 0; // Nodo sin asociar a nadie
    }

}  // end if


