<?php

/**
 * Vista de para el login via web, el cual crea variables de sesion del usuario
 * @author Adrian Garcia Andreu
 * @version 1.2
 */


 // si el usuario existe...
 if(count($respuesta)>0){
    $_SESSION["correo"]=$respuesta[0]["correo"];
    $_SESSION["clave"]=$respuesta[0]["clave"];
 }

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");
header("Content-Type: application/json; charset=utf-8");

// Nada especifico al margen de los headers