<?php
/**
 * Controlador del recurso nodoAsociado, enlaza el modelo correspondiente al metodo de la peticion realizada
 * y la vista general de la API
 * @author Pablo Miret
 * @version 1.2
 */


// Se incluye el modelo del recurso para el metodo de la peticion realizada (GET, POST, PUT o DELETE)
require('modelos/mascota/mascota_modelo_'.$metodo.'.php');

// Se incluhye la vista del recurso
require('vistas/vista_principal.php');