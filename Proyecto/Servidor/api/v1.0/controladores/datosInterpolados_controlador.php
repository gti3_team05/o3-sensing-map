<?php
/**
 * Controlador del recurso datosInterpolados, enlaza el modelo correspondiente al metodo de la peticion realizada
 * y la vista general de la API
 * @author Adrian Garcia Andreu
 * @version 1.2
 */


require('modelos/datosInterpolados/datosInterpolados_modelo_'.$metodo.'.php');

require('vistas/vista_principal.php');