<?php
/**
 * Controlador del recurso incidencias, enlaza el modelo correspondiente al metodo de la peticion realizada
 * y la vista general de la API
 * @author Adrián García Andreu
 * @version 1.1
 */



// Se incluye el modelo del recurso para el metodo de la peticion realizada (GET, POST, PUT o DELETE)
require('modelos/recuperaciones/recuperaciones_modelo_'.$metodo.'.php');

// Se incluhye la vista del recurso
require('vistas/vista_principal.php');

