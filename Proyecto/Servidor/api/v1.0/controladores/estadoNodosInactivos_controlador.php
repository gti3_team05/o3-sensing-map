<?php
/**
 * Controlador del recurso estadoNodosInactivos, enlaza el modelo correspondiente al metodo de la peticion realizada
 * y la vista general de la API
 * @author Laura Pons Castelló
 * @version 1.0
 */


// Se incluye el modelo del recurso para el metodo de la peticion realizada (GET, POST, PUT o DELETE)
require('modelos/estadoNodosInactivos/estadoNodosInactivos_modelo_'.$metodo.'.php');

// Se incluhye la vista del recurso
require('vistas/vista_principal.php');