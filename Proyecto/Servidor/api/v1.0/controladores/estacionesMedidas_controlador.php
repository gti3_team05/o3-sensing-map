<?php
/**
 * Controlador del recurso estacionMedidas, enlaza el modelo correspondiente al metodo de la peticion realizada
 * y la vista general de la API
 * @author Carlos Ibanez Lamas
 * @version 1.0
 */


// Se incluye el modelo del recurso para el metodo de la peticion realizada (GET, POST, PUT o DELETE)
require('modelos/estacionesMedidas/estacionesMedidas_modelo_'.$metodo.'.php');

// Se incluhye la vista del recurso
require('vistas/vista_principal.php');