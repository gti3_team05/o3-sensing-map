<?php

/**
 * Recibe las peticiones a la API, obteniendo el recurso, 
 * metodo y parametros de estas, y las procesa
 * @author Adrian Garcia Andreu
 * @version 1.1
 */


require_once('includes/conexion.php'); //Conexion con la BBD


$version = basename(__DIR__); //Version de la API

session_start();

// --------------------------------------------------------------
// --------------------------------------------------------------
// Se obtienen los recursos de la peticion


// Recuperar el método de la petición
$metodo = strtolower($_SERVER['REQUEST_METHOD']);

// Recuperar el recurso solicitado
$uri = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);

//divido el string en trozos a partier de la carpeta de la version de la API 
$uri = explode($version."/",$uri)[1]; 
$uri_array = explode("/",$uri); // Obtenemos los recursos de la peticion
$recurso = array_shift($uri_array);



// --------------------------------------------------------------
// --------------------------------------------------------------
// Se recogen los parametros de la peticion


// Recuperar parámetros de la query (GET)
$query_params = $_GET;

// Obtengo los parametros de la peticion GET y sus valores
$uri_params = array();
for($i =0; $i < count($uri_array); $i++){
    if($uri_array[$i]!= "") $uri_params[$uri_array[$i]] = $uri_array[++$i]; 
};

// Se recuperan los parametros enviados a traves del cuerpo de la peticion recibida (PUT o POST)
$body_params = (array) json_decode(file_get_contents('php://input'));

// Se recuperan los parametros recibidos a traves de un formulario (POST)
$form_params = $_POST;


// --------------------------------------------------------------
// Se incluye el controlador del recurso llamado por el usuario
require('controladores/'.$recurso."_controlador.php");



// --------------------------------------------------------------
// --------------------------------------------------------------
// Se crea la respuesta de la API a la peticion

$output = array();

$output['metodo'] = $metodo;
$output['recurso'] = $recurso;
$output['uri_params'] = $uri_params;



// Se agrega al array de salida, en el campo de 'datos', la respuesta obtenida de la peticion realizada
$output['datos'] = $respuesta;
if(isset($recursoExtra)){
    $output['recursoExtra'] = $recursoExtra;
}

// Se codifica como JSON el array de salida para mostrarlo por pantalla
echo json_encode($output);

