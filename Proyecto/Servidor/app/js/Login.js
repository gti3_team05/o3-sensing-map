
// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que obtiene todas las medidas realizadas
// solicitandolas a la API
// --------------------------------------------------------------
// getIniciarSesion()
// --------------------------------------------------------------


function getInicioSesion(correo, clave){
  
    var parametros = "?correo="+correo+"&clave="+clave;
    
    var recurso = servidorAPI+"inicioSesionWeb/"+parametros; //Recurso para acceder a todas los usuarios
    
    fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){        
        // Se obtienen los datos retornados y se pasan a la funcion de procesado
        iniciarSesion(datos["datos"]);
        console.log(datos);
    });

}




// funcion paara iniciar sesión

function iniciarSesion(datos){
    var clave = document.getElementById("inicioSesion_clave").value;
    
    if(datos.length>0){
        var usuario = datos[0];
        console.log(usuario);
        var esAdmin = usuario["esAdministrador"];
        
        //comprobar si es admin
       
            if((esAdmin)==1){
            console.log("inicio sesion!");
                //establecer cookies
           setCookie("proyecto3A_usuario",
                usuario["correo"],2);
            
            setCookie("proyecto3A_clave",
                usuario["clave"],2);
            
            
            irPanelAdmin();

        } else{
            console.log("No es admin el usuario");
             document.getElementById("nocorrecto").style.display="block";
   
        }
        
       
        
    } else {
        console.log("No existe el usuario/clave incorrecta");
       
        document.getElementById("nocorrecto2").style.display="block";
   
       
    }
      
    
}



//funcion para ir al panel Admin

function irPanelAdmin(){
    window.location.href="panelAdmin.html";
}



// ----------------------------------------------------




// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que muestra el dialogo de inicio de sesion en la pagina
// web
// --------------------------------------------------------------
// mostrarDialogoInicioSesion()
// --------------------------------------------------------------
function peticion_inicioSesion(){

    // Obtengo los datos de los campos del formulario de inicio de sesion
    var correo = document.getElementById("inicioSesion_correo").value;
    var clave = document.getElementById("inicioSesion_clave").value;

    if(correo.length>0 && clave.length>0){
    
        getInicioSesion(correo, clave);

    } else {
        console.log("Campos vacios");
         document.getElementById("nocorrecto3").style.display="block";
        
    }
    


}




