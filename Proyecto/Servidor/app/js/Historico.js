var ultimoTipoSeleccionado = TIPO_GAS_O3;
var ultimoDiaSeleccionado = 0;

function openNav() {
    document.getElementById("sidebar-wrapper").style.width = "15rem";
}

function closeNav() {
    document.getElementById("sidebar-wrapper").style.width = "0";
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Funcion que obtiene los valores interpolados realizados por
// el servidor, para despues mostrarlos
// ----------------------------------------------------------------------------------------------------------
// getMedidasInterpoladas()
// ----------------------------------------------------------------------------------------------------------
function getMedidasInterpoladas(){
   var recurso = servidorAPI+"datosInterpolados"+"?tipo="+ultimoTipoSeleccionado+"&fecha="+ultimoDiaSeleccionado+""; //Recurso para acceder a todas las medidas
   fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        respuesta.text().then((value)=>{
            respuesta = value.substring(value.indexOf('{'));
            var datos = JSON.parse(respuesta);
            //console.log(datos["datos"]);
            getEstacionesOficiales(ultimoTipoSeleccionado, datos["datos"], datos["recursoExtra"]);
        });
    });
} 

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Funcion que obtiene los valores de la estacion oficial de Gandia de 
// un servidor, para despues obtener la ubicacion de dicha estacion
// ----------------------------------------------------------------------------------------------------------
// tipoGas: string, datosInterpolados: [{"latitud":R,"longitud":R,"valor":R}] --> getEstacionesOficiales()
// ----------------------------------------------------------------------------------------------------------
function getEstacionesOficiales(tipoGas, datosMapa, imagenValoresMapa){
   var recurso = servidorAPI+"estacionesMedidas/"; //Recurso para acceder a todas las medidas
    fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){
        inicializarMapaReal(ultimoTipoSeleccionado,datosMapa,datos["datos"], imagenValoresMapa);
    });
    
} 

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Seteamos el tipo de gas seleccionado y cambiamos el texto del
// elemento seleccionado en el dropdown.
// Después mira si está el modo tester activado:
//      1. Tester desactivado --> se muestra mapa normal
//      2. Tester activado --> se mira el tipo de mapa de testeo a mostrar
// ----------------------------------------------------------------------------------------------------------
// tipoSeleccionado: Txt --> tipoGasSeleccionado()
// ----------------------------------------------------------------------------------------------------------
function tipoGasSeleccionado(tipoSeleccionado){
    switch(tipoSeleccionado){
        case TIPO_GAS_O3:
            document.getElementById("dropdownMenuGas").textContent = "O3 - Ozono";
            ultimoTipoSeleccionado = TIPO_GAS_O3;
            break;
        case TIPO_GAS_CO:
            document.getElementById("dropdownMenuGas").textContent = "CO - Dióxido de azufre";
            ultimoTipoSeleccionado = TIPO_GAS_CO;
            break;
        case TIPO_GAS_NO2:
            document.getElementById("dropdownMenuGas").textContent = "NO2 - Dióxido de nitrógeno";
            ultimoTipoSeleccionado = TIPO_GAS_NO2;
            break;
        case TIPO_GAS_SO2:
            document.getElementById("dropdownMenuGas").textContent = "SO2 - Monóxido de carbono";
            ultimoTipoSeleccionado = TIPO_GAS_SO2;
            break;
    }

    var modoTester = document.getElementById("iconoTester").textContent;
    if(modoTester != "T"){
        switch(modoTester){
            case "R":
                inicializarMapaFake(ultimoTipoSeleccionado, TIPO_MAPA_TEST_ALEATORIO);
                break;
            case "D":
                inicializarMapaFake(ultimoTipoSeleccionado, TIPO_MAPA_TEST_DEGRADADO);
                break;
            case "B":
                inicializarMapaFake(ultimoTipoSeleccionado, TIPO_MAPA_TEST_VALORES_BAJOS);
                break;
            case "M":
                inicializarMapaFake(ultimoTipoSeleccionado, TIPO_MAPA_TEST_VALORES_MEDIOS);
                break;
            case "A":
                inicializarMapaFake(ultimoTipoSeleccionado, TIPO_MAPA_TEST_VALORES_ALTOS);
                break;
    
        }
    }else{
        getMedidasInterpoladas();
    }
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Cambiamos el tipo icono del floattingMenu
// ----------------------------------------------------------------------------------------------------------
// cambiarIconoFloattingMenu()
// ----------------------------------------------------------------------------------------------------------
function cambiarIconoFloattingMenu(){
    var clase = document.getElementById("menuFloating").className;
    if(clase == "fas fa-plus"){
        document.getElementById("menuFloating").className = "fas fa-times";
    }else{
        document.getElementById("menuFloating").className = "fas fa-plus"
    }
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Al hacer click en el botón del modo tester:
//      1. Si esta desactivado --> lo activa y muestra el mapa con el último gas seleccionado
//      2. Si esta activado --> cambia el tipo de mapa a mostrar y setea el icono y texto del botón
//      2.2. Si esta activado en el último modo --> lo desactiva
// ----------------------------------------------------------------------------------------------------------
// modoTesterClick()
// ----------------------------------------------------------------------------------------------------------
function modoTesterClick(){
    switch(document.getElementById("iconoTester").textContent){
        case "T":
            document.getElementById("modoTester").className = "fab-icon-holder";
            document.getElementById("tituloTester").textContent = "Modo Testeo Aleatorio";
            inicializarMapaFake(ultimoTipoSeleccionado, TIPO_MAPA_TEST_ALEATORIO);
            document.getElementById("iconoTester").textContent = "R";
            break;
        case "R":
            inicializarMapaFake(ultimoTipoSeleccionado, TIPO_MAPA_TEST_DEGRADADO);
            document.getElementById("tituloTester").textContent = "Modo Testeo Degradado";
            document.getElementById("iconoTester").textContent = "D";
            break;
        case "D":
            inicializarMapaFake(ultimoTipoSeleccionado, TIPO_MAPA_TEST_VALORES_BAJOS);
            document.getElementById("tituloTester").textContent = "Modo Testeo Valores Bajos";
            document.getElementById("iconoTester").textContent = "B";
            break;
        case "B":
            inicializarMapaFake(ultimoTipoSeleccionado, TIPO_MAPA_TEST_VALORES_MEDIOS);
            document.getElementById("tituloTester").textContent = "Modo Testeo Valores Medios";
            document.getElementById("iconoTester").textContent = "M";
            break;
        case "M":
            inicializarMapaFake(ultimoTipoSeleccionado, TIPO_MAPA_TEST_VALORES_ALTOS);
            document.getElementById("tituloTester").textContent = "Modo Testeo Valores Altos";
            document.getElementById("iconoTester").textContent = "A";
            break;
        case "A":
            document.getElementById("modoTester").className = "fab-icon-holder desactivado";
            getMedidasInterpoladas();
            document.getElementById("iconoTester").textContent = "T";
            document.getElementById("tituloTester").textContent = "Modo Testeo Desactivado";
            break;
    }
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Funcion carga de mapa de Google
// ----------------------------------------------------------------------------------------------------------
// inicializarMapa()
// ----------------------------------------------------------------------------------------------------------
function inicializarMapa() {
    inicializarMapaSinDatos();
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Se crea un mapa por defecto de Google
// ----------------------------------------------------------------------------------------------------------
// inicializarMapaSinDatos()
// ----------------------------------------------------------------------------------------------------------
function inicializarMapaSinDatos(){
  var map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -34.397, lng: 150.644 },
    zoom: 8,
  });
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Se crea un mapa con los valores de la interpolacion de Matlab
// ----------------------------------------------------------------------------------------------------------
// tipoGas: string, datosInterpolados: [{"latitud":R,"longitud":R,"valor":R}], 
//                                                                              --> inicializarMapaReal()
// datosEstaciones: [{"lat":R,"lng":R,"nombre":String}], imagenValoresMapa: Txt
// ----------------------------------------------------------------------------------------------------------
function inicializarMapaReal(tipoGas, datosInterpolados, datosEstaciones, imagenValoresMapa){
    
    document.getElementById("map").innerHTML = "";
    
    var medio = obtenerPuntoMedio(datosInterpolados);
    
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 16,
        center: { lat: medio.lat(), lng:  medio.lng() },
        mapTypeId: "satellite",
  });
    
  var limitesImagen = obtenerLimites(datosInterpolados);
    
 const bounds = limitesImagen;
    
  // The photograph is courtesy of the U.S. Geological Survey.
  let image = cambiarImagenURL(imagenValoresMapa);
    console.log(image);

  /**
   * The custom USGSOverlay object contains the USGS image,
   * the bounds of the image, and a reference to the map.
   */
  class USGSOverlay extends google.maps.OverlayView {
    constructor(bounds, image) {
      super();
      this.bounds = bounds;
      this.image = image;
    }
    /**
     * onAdd is called when the map's panes are ready and the overlay has been
     * added to the map.
     */
    onAdd() {
      this.div = document.createElement("div");
      this.div.style.borderStyle = "none";
      this.div.style.borderWidth = "0px";
      this.div.style.position = "absolute";
      // Create the img element and attach it to the div.
      const img = document.createElement("img");
      img.src = this.image;
      img.style.width = "100%";
      img.style.height = "100%";
      img.style.position = "absolute";
        img.style.opacity = "0.5";
      this.div.appendChild(img);
      // Add the element to the "overlayLayer" pane.
      const panes = this.getPanes();
      panes.overlayLayer.appendChild(this.div);
    }
    draw() {
      // We use the south-west and north-east
      // coordinates of the overlay to peg it to the correct position and size.
      // To do this, we need to retrieve the projection from the overlay.
      const overlayProjection = this.getProjection();
      // Retrieve the south-west and north-east coordinates of this overlay
      // in LatLngs and convert them to pixel coordinates.
      // We'll use these coordinates to resize the div.
      const sw = overlayProjection.fromLatLngToDivPixel(
        this.bounds.getSouthWest()
      );
      const ne = overlayProjection.fromLatLngToDivPixel(
        this.bounds.getNorthEast()
      );

      // Resize the image's div to fit the indicated dimensions.
      if (this.div) {
        this.div.style.left = sw.x + "px";
        this.div.style.top = ne.y + "px";
        this.div.style.width = ne.x - sw.x + "px";
        this.div.style.height = sw.y - ne.y + "px";
      }
    }
    /**
     * The onRemove() method will be called automatically from the API if
     * we ever set the overlay's map property to 'null'.
     */
    onRemove() {
      if (this.div) {
        this.div.parentNode.removeChild(this.div);
        delete this.div;
      }
    }
    /**
     *  Set the visibility to 'hidden' or 'visible'.
     */
    hide() {
      if (this.div) {
        this.div.style.visibility = "hidden";
      }
    }
    show() {
      if (this.div) {
        this.div.style.visibility = "visible";
      }
    }
    toggle() {
      if (this.div) {
        if (this.div.style.visibility === "hidden") {
          this.show();
        } else {
          this.hide();
        }
      }
    }
    toggleDOM(map) {
      if (this.getMap()) {
        this.setMap(null);
      } else {
        this.setMap(map);
      }
    }
  }
  const overlay = new USGSOverlay(bounds, image);
  overlay.setMap(map);
  const toggleButton = document.createElement("button");
  toggleButton.textContent = "Toggle";
  toggleButton.classList.add("custom-map-control-button");
  const toggleDOMButton = document.createElement("button");
  toggleDOMButton.textContent = "Toggle DOM Attachment";
  toggleDOMButton.classList.add("custom-map-control-button");
  toggleButton.addEventListener("click", () => {
    overlay.toggle();
  });
  toggleDOMButton.addEventListener("click", () => {
    overlay.toggleDOM(map);
  });
  map.controls[google.maps.ControlPosition.TOP_RIGHT].push(toggleDOMButton);
  map.controls[google.maps.ControlPosition.TOP_RIGHT].push(toggleButton);
    
 markersEstacionesMedidas(datosEstaciones, map);
    
}

// -----------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------
// Se buscan los limites de los puntos obtenidos para poner la imagen de la interpolación
// -----------------------------------------------------------------------------------------------------------------
// datos: [{"latitud":R,"longitud":R,"valor":R}] --> obtenerLimites() --> limites: LatLngBounds{[LatLng]}
// -----------------------------------------------------------------------------------------------------------------
function obtenerLimites(datos){
    
    var latMax = datos[0]["latitud"];
    var lonMax = datos[0]["longitud"];
    var latMin = datos[0]["latitud"];
    var lonMin = datos[0]["longitud"];
    
    for(i=1;i<datos.length;i++){
        
        var lat = datos[i]["latitud"];
        var lng = datos[i]["longitud"];
        
        if(datos[i]["valor"] != 0){
            if(lat > latMax){
                latMax = lat;
            } else if (lat < latMin){
                latMin = lat;
            }
        
            if(lng > lonMax){
                lonMax = lng;
            } else if (lng < lonMin){
                lonMin = lng;
            }
        }
        
    }
    
    const limites = new google.maps.LatLngBounds(
        new google.maps.LatLng(latMin, lonMax),
        new google.maps.LatLng(latMax, lonMin));
    
    return limites;
    
}

// -----------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------
// Funcion recurso para buscar la imagen del servidor
// -----------------------------------------------------------------------------------------------------------------
// path: Txt --> cambiarImagenURL() --> finalRecurso: Txt
// -----------------------------------------------------------------------------------------------------------------
function cambiarImagenURL(path){
    var recurso = path.substring(path.indexOf("s") + 1); //Recurso para acceder a todas las medidas
    var finalRecurso = servidor+recurso;
    return finalRecurso;
}

// -----------------------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------
// SSe transforman los datos de la peticion REST en Medidas para ser representadas en el mapa
// -----------------------------------------------------------------------------------------------------------------
// datosRest: [{"latitud":R,"longitud":R,"valor":R}] --> convertirDatosObtenidosRestAMedidas() --> datos: [Medidas]
// -----------------------------------------------------------------------------------------------------------------
function convertirDatosObtenidosRestAMedidas(datosRest){
    var pesoMin;
    var pesoMax;
    switch (ultimoTipoSeleccionado){
        case TIPO_GAS_CO: 
            pesoMax = VALOR_MAXIMO_CO;
            pesoMin = VALOR_MINIMO_CO;
            break;
        case TIPO_GAS_NO2: 
            pesoMax = VALOR_MAXIMO_NO2;
            pesoMin = VALOR_MINIMO_NO2;
            break;
        case TIPO_GAS_O3: 
            pesoMax = VALOR_MAXIMO_O3;
            pesoMin = VALOR_MINIMO_O3;
            break;
        case TIPO_GAS_SO2: 
            pesoMax = VALOR_MAXIMO_SO2;
            pesoMin = VALOR_MINIMO_SO2;
            break;
    }
    
    var datos = [];
    for(i=0;i<datosRest.length;i++){
        var lat = datosRest[i]["latitud"];
        var lon = datosRest[i]["longitud"];
        var peso = datosRest[i]["valor"];
        if(peso != "NaN"){
            var pesoNormalizado = (((peso - pesoMin) * (1 - 0.01)) / (pesoMax - pesoMin)) + 0.01;
            datos.push(new Medidas(lat, lon, parseFloat(pesoNormalizado).toFixed(6)));
        }
    }
    
    return datos;
    
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Funcion que genera un mapa fake con la finalidad de mostrar los
// datos para saber si va bien.
// ----------------------------------------------------------------------------------------------------------
// tipoGas:Txt, tipoDegradado: Txt --> inicializarMapaFake()
// ----------------------------------------------------------------------------------------------------------
function inicializarMapaFake(tipoGas, tipoDegradado){
    
    document.getElementById("map").innerHTML = "";
    
    var datos = generarMapaDatosFake(tipoGas,tipoDegradado,38.94460007805937,39.052614890343506,-0.226978537452108,-0.14469997740041482,1000);
    
    var medio = obtenerPuntoMedioMedidas(datos);
    
    var datosHeatMap = convertirMedidasAHeatMapData(datos);
    
    var mapa = new google.maps.Map(document.getElementById("map"), {
        center: { lat: medio.lat(), lng:  medio.lng() },
        zoom: 12.8
    });
    
    var heatmap = new google.maps.visualization.HeatmapLayer({
        data: datosHeatMap,
        radius: 30,
        maxIntensity: 1,
        opacity: 0.3
    });
    
    heatmap.setMap(mapa);
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Funcion que dado una lista de objetos Medidas, lo transforma
// al formato necesario para el heatmap
// ----------------------------------------------------------------------------------------------------------
// datos: [Medidas] --> convertirMedidasAHeatMapData() --> listaConvertida: [{LatLng,weight}]
// ----------------------------------------------------------------------------------------------------------
function convertirMedidasAHeatMapData(datos){
    var listaConvertida = [];
    
    for(i=0;i<datos.length;i++){
        listaConvertida.push({location: new google.maps.LatLng(datos[i]["lat"],datos[i]["lng"]),weight:datos[i]["medida"]});
    }
    
    return listaConvertida;
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Llamada a la generación de datos fake a partir del tipo de gas
// ----------------------------------------------------------------------------------------------------------
// tipoGas:Txt, tipoDegradado:Txt, latMin:R, latMax:R, lonMin:R, lonMax:R, numPuntos: N 
//       --> generarMapaDatosFake() --> datos: [Medidas]
// ----------------------------------------------------------------------------------------------------------
function generarMapaDatosFake(tipoGas, tipoDegradado, latMin, latMax, lonMin, lonMax, numPuntos){
    
    var datos = [];
    
    switch (tipoGas){
        case TIPO_GAS_CO: 
            datos = generarPuntosRandom(latMin,latMax,lonMin,lonMax,VALOR_MINIMO_CO,VALOR_MAXIMO_CO, VALOR_MEDIO_CO,numPuntos,tipoDegradado); 
            break;
        case TIPO_GAS_NO2: 
            datos = generarPuntosRandom(latMin,latMax,lonMin,lonMax,VALOR_MINIMO_NO2,VALOR_MAXIMO_NO2, VALOR_MEDIO_NO2,numPuntos,tipoDegradado);
            break;
        case TIPO_GAS_O3: 
            datos = generarPuntosRandom(latMin,latMax,lonMin,lonMax,VALOR_MINIMO_O3,VALOR_MAXIMO_O3, VALOR_MEDIO_O3,numPuntos,tipoDegradado);
            break;
        case TIPO_GAS_SO2: 
            datos = generarPuntosRandom(latMin,latMax,lonMin,lonMax,VALOR_MINIMO_SO2,VALOR_MAXIMO_SO2, VALOR_MEDIO_SO2,numPuntos,tipoDegradado);
            break;
    }
    
    return datos;

}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Dadas todas los puntos como JSON de la lista obtiene le punto medio para
// poder centrar el mapa
// ----------------------------------------------------------------------------------------------------------
// datos: [Medidas] --> obtenerPuntoMedio() --> puntoMedio: LatLng
// ----------------------------------------------------------------------------------------------------------
function obtenerPuntoMedio(datos){
    
    var latMedia = 0;
    var lonMedia = 0;
    var puntoMedio = 0;
    
    for(i=0;i<datos.length;i++){
        lonMedia += parseFloat(datos[i]["longitud"]);
        latMedia += parseFloat(datos[i]["latitud"]);
    }
    
    lonMedia = lonMedia / datos.length;
    latMedia = latMedia / datos.length;
    
    puntoMedio = new google.maps.LatLng(latMedia, lonMedia);
    
    return puntoMedio;

}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Dadas todas los puntos como Medidas de la lista obtiene le punto medio para
// poder centrar el mapa
// ----------------------------------------------------------------------------------------------------------
// datos: [Medidas] --> obtenerPuntoMedio() --> puntoMedio: LatLng
// ----------------------------------------------------------------------------------------------------------
function obtenerPuntoMedioMedidas(datos){
    
    var latMedia = 0;
    var lonMedia = 0;
    var puntoMedio = 0;
    
    for(i=0;i<datos.length;i++){
        lonMedia += parseFloat(datos[i].lng);
        latMedia += parseFloat(datos[i].lat);
    }
    
    lonMedia = lonMedia / datos.length;
    latMedia = latMedia / datos.length;
    
    puntoMedio = new google.maps.LatLng(latMedia, lonMedia);
    
    console.log(puntoMedio);
    
    return puntoMedio;
    
}

// ------------------------------------------------------------------------------------------------------------------
// ------------------------------------------------------------------------------------------------------------------
// Generación de x número de datos fake para el mapa dadas las coordenadas máximas y mínimas así como el efecto que 
// queremos mostrar
// ------------------------------------------------------------------------------------------------------------------
// tipoDegradado:Txt, latMin:R, latMax:R, lonMin:R, lonMax:R, pesoMin:R, pesoMax:R, pesoMedio:R, numPuntos: N  
//       --> generarPuntosRandom() --> listaMedidasRandom: [Medidas]
// ------------------------------------------------------------------------------------------------------------------
function generarPuntosRandom(latMin, latMax, lonMin, lonMax, pesoMin, pesoMax, pesoMedio, numPuntos, tipoDegradado){
    var listaMedidasRandom = [];
    for(i=0; i<numPuntos; i++){
        var lat = (Math.random() * (latMax - latMin)) + latMin;
        var longi = (Math.random() * (lonMax - lonMin)) + lonMin;
        var peso = 0;
        var pesoNormalizado = 0;
        switch(tipoDegradado){
            case "aleatorio":
                peso = (Math.random() * (pesoMax - pesoMin)) + pesoMin;
                pesoNormalizado = (((peso - pesoMin) * (1 - 0.01)) / (pesoMax - pesoMin)) + 0.01;
                break;
            case "degradado":
                peso = 0;
                break;
            case "valoresBajos":
                pesoNormalizado = (((pesoMin - pesoMin) * (1 - 0.1)) / (pesoMax - pesoMin)) + 0.1;
                break;
            case "valoresMedios":
                pesoNormalizado = (((pesoMedio - pesoMin) * (1 - 0.01)) / (pesoMax - pesoMin)) + 0.01;
                break;
            case "valoresAltos":
                pesoNormalizado = (((pesoMax - pesoMin) * (1 - 0.1)) / (pesoMax - pesoMin)) + 0.1;
                break;
        }
        listaMedidasRandom.push(new Medidas(lat, longi, parseFloat(pesoNormalizado).toFixed(6)));
    }
    
    if(tipoDegradado == "degradado"){
        listaMedidasRandom = ordenarPuntosMayorAMenor(numPuntos, pesoMax, pesoMin, listaMedidasRandom);
    }
    
    return listaMedidasRandom;
    
}

// -------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------
// Ordenar los puntos de mayor a menor para por ejemplo poder mostrar un gradiente
// -------------------------------------------------------------------------------------------------------------
// tipoDegradado:Txt, latMin:R, latMax:R, lonMin:R, lonMax:R, pesoMin:R, pesoMax:R, pesoMedio:R, numPuntos: N  
//       --> generarPuntosRandom() --> listaMedidasRandom: [Medidas]
// -------------------------------------------------------------------------------------------------------------
function ordenarPuntosMayorAMenor(numPuntos, pesoFin, pesoIn, listaMedidasRandom){
    var puntosX = [];
    var listaCopia = [];
    listaCopia = listaMedidasRandom;
    
    for(i=0; i<listaCopia.length;i++){
        puntosX.push(listaCopia[i]["lng"]);
    }
    
    puntosX.sort(function(a, b){return a - b});
    
    listaMedidasRandom = [];
    
    for(i=0; i<puntosX.length; i++){
        for(n=0; n<listaCopia.length; n++){
            if(listaCopia[n]["lng"] == puntosX[i]){
                listaMedidasRandom.push(listaCopia[n]);
                var peso = (pesoFin / numPuntos) * i;
                var pesoNormalizado = (((peso - pesoIn) * (1 - 0.1)) / (pesoFin - pesoIn)) + 0.1;
                if(pesoNormalizado < 0){
                    pesoNormalizado = 0;
                }
                listaMedidasRandom[i] = new Medidas(listaCopia[n]["lat"],listaCopia[n]["lng"], parseFloat(pesoNormalizado).toFixed(6));
            }
        }
    }
    
    return listaMedidasRandom;
    
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Dadas una lista, obtiene la máxima medida
// ----------------------------------------------------------------------------------------------------------
// datos: [Medidas] --> obtenerPuntoMedio() --> R
// ----------------------------------------------------------------------------------------------------------
function obtenerMaximo (datos){
    var max = datos[0]["medida"];
    for(i=1;i<datos.length;i++){
        var val = datos[i]["medida"];
        if (max < val){
            max = val;
        }
    }
    return max;
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Dados una lista de markers y un mapa, posiciona dichos markers en el mapa
// ----------------------------------------------------------------------------------------------------------
// datos: [{"lat":R,"lng":R,"nombre":String}], mapa: google.Maps.map --> markersEstacionesMedidas()
// ----------------------------------------------------------------------------------------------------------
function markersEstacionesMedidas(datos, mapa){
    
    var listaMarkers = [];
    
    var marker = {};
    
    for(i=0;i<datos.length;i++){
        var latitud = datos[i]["lat"];
        var longitud = datos[i]["lng"];
        var nombre = datos[i]["nombre"];
        var punto = new google.maps.LatLng(latitud, longitud);
        marker["nombreMarker"] = "Estación de medición de "+nombre;
        marker["puntoMarker"] = punto;
        listaMarkers.push(marker);
    } 
    
    const image = {
        url: "http://localhost/proyecto3A/app/css/imagenes/marker.png",
        scaledSize : new google.maps.Size(32, 32)
    }
    
    for(i=0;i<listaMarkers.length;i++){
        var marker = new google.maps.Marker({
            position: listaMarkers[i]["puntoMarker"],
            title: listaMarkers[i]["nombreMarker"],
            icon: image,
            map: mapa
        });
        
    }

   
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Funcion que cambia el mapa cuando se selecciona una nueva fecha en el picker
// ----------------------------------------------------------------------------------------------------------
// e: Event [Evento que salta al seleccionar una nueva fecha en el caldenario] --> fechaCambiada()
// ----------------------------------------------------------------------------------------------------------
function fechaCambiada(e){
    var fecha = String(e.target.value);
    var fechaDividida = fecha.split("-");
    var fechaFormato = fechaDividida[0] + '_' + fechaDividida[1] + '_' + fechaDividida[2];
    ultimoDiaSeleccionado = fechaFormato;
    getMedidasInterpoladas();
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Funcion que limita el datePicker a los dias establecidos
// ----------------------------------------------------------------------------------------------------------
// limitarCalendario()
// ----------------------------------------------------------------------------------------------------------
function limitarCalendario(){
    var calendario = document.getElementById("picker");
    var hoy = new Date();
    var dd = String(hoy.getDate()).padStart(2, '0');
    var mm = String(hoy.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = hoy.getFullYear();
    var hoyDia = yyyy + '-' + mm + '-' + dd;
    calendario.setAttribute("value",hoyDia);
    if(dd == 1){
        calendario.setAttribute("min",hoyDia);
        calendario.setAttribute("max",hoyDia);
    } else {
        var primerDiaMes = yyyy + '-' + mm + '-' + "01";
        calendario.setAttribute("min",primerDiaMes);
        calendario.setAttribute("max",hoyDia);
    }
    
    var fechaDividida = hoyDia.split("-");
    var fechaFormato = fechaDividida[0] + '_' + fechaDividida[1] + '_' + fechaDividida[2];
    ultimoDiaSeleccionado = fechaFormato;
}

// ----------------------------------------------------------------------------------------------------------
// ----------------------------------------------------------------------------------------------------------
// Funcion que lanza las peticiones a la API y realiza los procesos
// necesarios al acceder y cargar la web
// ----------------------------------------------------------------------------------------------------------
// iniciarPeticionesAlServidor()
// ----------------------------------------------------------------------------------------------------------
function iniciarPeticionesAlServidor(){
    window.onload = function(){ // cuando el navegador ha cargado, inicio las peticiones
        getMedidasInterpoladas();
    }
}
