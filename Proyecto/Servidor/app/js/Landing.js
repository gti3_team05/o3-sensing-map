//funciones para preguntar los cookies

function opcionesInicio(){
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());

	gtag('config', 'UA-132388353-1');
}

function iniciarInfoCookies(){
	window.addEventListener("load", function() {
		window.cookieconsent.initialise({
			"palette": {
				"popup": {
					"background": "#eaf7f7",
					"text": "#5c7291"
				},
				"button": {
					"background": "#56cbdb",
					"text": "#ffffff"
				}
			},
			"content": {
				"message": "Este sitio web utiliza cookies para garantizar que obtenga la mejor experiencia en nuestro sitio web",
				"dismiss": "Aceptar",
				"link": "Más información"
			}
		})
	});
}

//Funciones para alertas

function existenCookies(){
	console.log("dentro de existenCookies");
	if(getCookie("exitoContacto")==true){
		//mostramos el mensaje
		document.getElementById("todoBien").style.display="block"
		//borramos las cookies
		deleteCookie("exitoContacto")
	}
	
	if(getCookie("errorEnvio")==true){
		//mostramos el mensaje
		document.getElementById("errorEnvio").style.display="block"
		//borramos las cookies
		deleteCookie("errorEnvio")
	}
	
}

//funciones para ir a otras paginas

function irMapa(){
    window.location.href="Mapa.html";
}

function irLanding(){
    window.location.href="index.html";
}
