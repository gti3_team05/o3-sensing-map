// funciones para abrir el menu

function openNav() {
    document.getElementById("sidebar-wrapper").style.width = "250px";
}

function closeNav() {
    document.getElementById("sidebar-wrapper").style.width = "0";
}

// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que obtiene todas las medidas realizadas
// solicitandolas a la API
// --------------------------------------------------------------
// getNodosActividad()
// --------------------------------------------------------------
function getNodosActividad() {
    var recurso = servidorAPI + "estadoNodosInactivos/"; 
    
    fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){        
        // Se obtienen los datos retornados y se pasan a la funcion de procesado
        if(datos["datos"].length>0){
            procesarDatosActividadNodos(datos["datos"]);
        } else {console.log("No hay datos de medidas de ningun usuario!");}
    });
}

// funcion para procesar la info de nodos
// nodos -> procesarDatosActividadNodos()

function procesarDatosActividadNodos(usuarios){
    
    // Contenedor html en el que mostrar los datos
    var contenedor = document.getElementById("contenedor_nodosInactivos");
    
    // Cabecera de la tabla
    var visualizacion  = `<table class="table table-hover">`;
    visualizacion += `<thead class="table-primary">`;
        visualizacion += `<th class="px-lg-5">Usuario</th>`;
        visualizacion += `<th class="px-lg-5">Nombre</th>`;
        visualizacion += `<th class="px-lg-5">Nodo asociado</th>`; //TODO: en la columna se puede mostrar un QR a traves de la api de google del nodo, en vez de la ID sin mas
        visualizacion += `<th class="px-lg-5">Ultima actividad</th>`;
        visualizacion += `<th class="px-lg-5">Tiempo que lleva inactivo</th>`;
        visualizacion += `<th class="px-lg-5">Alerta</th>`;
    visualizacion += `</thead>`;

    
    // se crea una fila por cada medida
    for (const usuario of usuarios) {
        //console.log(medida);
        visualizacion += `<tr>`;
            visualizacion += `<td class="px-lg-5">${usuario["correo"]}</td>`;
            visualizacion += `<td class="px-lg-5">${usuario["nombre"]}</td>`;
            visualizacion += `<td class="px-lg-5">${usuario["idNodo"]}</td>`;
            visualizacion += `<td class="px-lg-5">${usuario["ultimaMedida"]}</td>`;
            visualizacion += `<td class="px-lg-5">${usuario["tiempoInactividad"]}</td>`;
        if(usuario["alerta"] = "roja"){
            visualizacion += `<td class="px-lg-5 bg-danger text-white">Sensor inactivo</td>`;
        } else if(usuario["alerta"] = "verde"){
            visualizacion += `<td class="px-lg-5 bg-success text-white">Sensor activo</td>`;
        }

        visualizacion += `</tr>`;
    }

    visualizacion += `</table>`;

    // Se muestra en pantalla
    contenedor.innerHTML = visualizacion;    


}

// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que lanza las peticiones a la API y realiza los procesos
// necesarios al acceder y cargar la web
// --------------------------------------------------------------
// iniciarEstadoNodos()
// --------------------------------------------------------------
function iniciarEstadoNodos(){
    window.onload = function(){ // cuando el navegador ha cargado, inicio las peticiones
        getNodosActividad();
    }
}
