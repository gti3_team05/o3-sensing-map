
// Direccion del servidor a la API del servidor
//const servidorAPI = "http://localhost/o3-sensing-map/Proyecto/Servidor/api/v1.0/";





// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que obtiene todas las medidas realizadas
// solicitandolas a la API
// --------------------------------------------------------------
// getMedidas()
// --------------------------------------------------------------
function getMedidas(){

   var recurso = servidorAPI+"medidas/"; //Recurso para acceder a todas las medidas
    fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){
        //console.log(datos["datos"]); 
        
        // Se obtienen los datos retornados y se pasan a la funcion de procesado
        procesarMedidas(datos["datos"]);
        console.log(datos["datos"]);
    });
} 


// --------------------------------------------------------------
// --------------------------------------------------------------
// Recibe un listado de medidas (en formato JSON)
// se disponen en una tabla y se muestran en la pagina 
// --------------------------------------------------------------
// [Medidas] --> procesarMedidas()
// --------------------------------------------------------------
function procesarMedidas(medidas){

    //console.log(typeof(medidas));
    //console.log(medidas);

    // Contenedor html en el que mostrar los datos
    var contenedor = document.getElementById("contenedor_datos_medidas");
    
    // Cabecera de la tabla
    var visualizacion  = `<table class="table table-responsive table-hover scrollbar-personalizada">`;
    visualizacion += `<thead class="table-primary">`;
        visualizacion += `<th class="px-lg-5">Fecha</th>`;
        visualizacion += `<th class="px-lg-5">Tipo de gas</th>`;
        visualizacion += `<th class="px-lg-5">Valor</th>`;
        visualizacion += `<th class="px-lg-5">Latitud</th>`;
        visualizacion += `<th class="px-lg-5">Longitud</th>`;
    visualizacion += `</thead>`;

    
    // se crea una fila por cada medida
    for (const medida of medidas) {
        //console.log(medida);
        visualizacion += `<tr>`;
            visualizacion += `<td class="px-lg-5">${medida["momento"]}</td>`;
            visualizacion += `<td class="px-lg-5">${medida["tipoMedida"]}</td>`;
            visualizacion += `<td class="px-lg-5">${medida["valor"]} ug/m<sup>3</sup></td>`;
            visualizacion += `<td class="px-lg-5">${medida["latitud"]}</td>`;
            visualizacion += `<td class="px-lg-5">${medida["longitud"]}</td>`;

        visualizacion += `</tr>`;
    }

    visualizacion += `</table>`;

    // Se muestra en pantalla
    contenedor.innerHTML = visualizacion;


}




// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que obtiene todas las medidas realizadas
// solicitandolas a la API
// --------------------------------------------------------------
// getIniciarSesion()
// --------------------------------------------------------------

function getInicioSesion(correo, clave){
  
    var parametros = "?correo="+correo+"&clave="+clave;
    
    var recurso = servidorAPI+"inicioSesionWeb/"+parametros; //Recurso para acceder a todas los usuarios
    
    fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){        
        // Se obtienen los datos retornados y se pasan a la funcion de procesado
        iniciarSesion(datos["datos"]);
    });

}






function getSesionIniciada(){
    var recurso = servidorAPI+"sesionIniciada/" 
    
    fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){        
        // Se obtienen los datos retornados y se pasan a la funcion de procesado
       iniciarSesion(datos["datos"]);
    });

}


function iniciarSesion(datos){
    
    if(datos.length>0){
        var usuario = datos[0];
        console.log(usuario);
        var esAdmin = usuario["esAdministrador"];
        if((esAdmin)==1){
            console.log("inicio sesion!");
           setCookie("proyecto3A_usuario",
                usuario["correo"],2);
            
            setCookie("proyecto3A_clave",
                usuario["clave"],2);
            
            // si el dialogo es visible, se oculta al haber iniciado sesion
           
                //document.getElementById("btnInicioSesion").click(); // Cierra el dialogo
            

            //mostrarControlesAdmin();
            irPanelAdmin();

        } else{
            console.log("No es admin el usuario");
   
        }
    } else {
        console.log("No existe el usuario/clave incorrecta");
    }
}







function mostrarControlesAdmin(){

    // boton acceso panel de control
    var controles_sesion = document.getElementById("controles_sesion");
    controles_sesion.innerHTML = "<button id='panel2' onclick='peticionCerrarSesion()'>Cerrar sesion</button>"

}



function irPanelAdmin(){
    window.location.href="panelAdmin.html";
}



// ----------------------------------------------------




// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que muestra el dialogo de inicio de sesion en la pagina
// web
// --------------------------------------------------------------
// mostrarDialogoInicioSesion()
// --------------------------------------------------------------
function peticion_inicioSesion(){

    // Obtengo los datos de los campos del formulario de inicio de sesion
    var correo = document.getElementById("inicioSesion_correo").value;
    var clave = document.getElementById("inicioSesion_clave").value;

    if(correo.length>0 && clave.length>0){
        getInicioSesion(correo, clave);

    } else {
        console.log("Campos vacios")
    }


}





// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que lanza las peticiones a la API y realiza los procesos
// necesarios al acceder y cargar la web
// --------------------------------------------------------------
// iniciarPeticionesAlServidor()
// --------------------------------------------------------------
function iniciarPeticionesAlServidor(){
    window.onload = function(){ // cuando el navegador ha cargado, inicio las peticiones
        getSesionIniciada(); 
        getMedidas();
    }
}


