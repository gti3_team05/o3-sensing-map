
//funciones para abrir el menu
function openNav() {
    document.getElementById("sidebar-wrapper").style.width = "250px";
}

function closeNav() {
    document.getElementById("sidebar-wrapper").style.width = "0";
}

// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que obtiene todas las medidas realizadas
// solicitandolas a la API
// --------------------------------------------------------------
// getTipo()
// --------------------------------------------------------------
function getTipo(){
    
    var recurso = servidorAPI + "tipos/"; //Recurso para acceder a todas las medidas
    fetch(recurso, {
        method: 'GET'
    }).then(function (respuesta) {
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function (datos) {
        //console.log(datos["datos"]);
        tablatipo(datos["datos"]);

        // Se obtienen los datos retornados y se pasan a la funcion de procesado

    });
}

// funcion para procesar la info de tipo
// tipos -> tablatipo()
function tablatipo(tipos){
    // Contenedor html en el que mostrar los datos
    var contenedor = document.getElementById("tipo");
    
    
    // Cabecera de la tabla
    var visualizacion  = `<table class="table table-hover">`;
    visualizacion += `<thead class="table-primary">`;
        visualizacion += `<th class="px-lg-5">Nombre Tipo</th>`;
        visualizacion += `<th class="px-lg-5">Unidades</th>`;
        visualizacion += `<th class="px-lg-5">Valor Optimo</th>`;
        visualizacion += `<th class="px-lg-5">Valor Alerta</th>`;
        visualizacion += `<th class="px-lg-5">Valor Peligro</th>`;
        visualizacion += `<th class="px-lg-1">Opciones</th>`;

    visualizacion += `</thead>`;
    
    // se crea una fila por cada medida
    for (const tipo of tipos) {
        
        visualizacion += `<tr>`;
           
            visualizacion += `<td class="px-lg-5">${tipo["nombreTipo"]}</td>`;
            visualizacion += `<td class="px-lg-5">${tipo["unidades"]}</td>`; 
        visualizacion += `<td class="px-lg-5">${tipo["valorOptimo"]}</td>`; 
        visualizacion += `<td class="px-lg-5">${tipo["valorAlerta"]}</td>`; visualizacion += `<td class="px-lg-5">${tipo["valorPeligro"]}</td>`;
        visualizacion += `<td class="px-lg-1"><div class="row"><div id="editar" data-toggle="modal" data-target="#exampleModalCentered">
<button class="correo-todo" onclick="pasareditar('${tipo["nombreTipo"]}','${tipo["unidades"]}','${tipo["valorOptimo"]}','${tipo["valorAlerta"]}','${tipo["valorPeligro"]}')">
                    <i class="fas fa-edit"></i></button>
                </div>&nbsp<div><button type="button" class="correo-todo" data-toggle="modal" data-target="#exampleModalCentered3" onclick="darbaja('${tipo["nombreTipo"]}')">
  <i class="fas fa-trash-alt"></i>
</button></div></div></td>`;
        
        visualizacion += `</tr>`;
        
    }

    visualizacion += `</table>`;

    // Se muestra en pantalla
    contenedor.innerHTML = visualizacion;
    
    
    
}


//Funcion para pasar los parametros para editar el tipo
async function pasareditar(nombre, unidad, op, ale, peli) {
    var contenedor = document.getElementById("nombreTipo");
    var contenedor1 = document.getElementById("unidad");
    var contenedor2 = document.getElementById("optimo");
    var contenedor3 = document.getElementById("alerta");
    var contenedor4 = document.getElementById("peligro");

    contenedor.value = nombre;
    contenedor1.value = unidad;
    contenedor2.value = op;
    contenedor3.value = ale;
    contenedor4.value = peli;
   

}


//funcion para pasar el nombre y eliminar el tipo
function darbaja(nombre) {
    var contenedor2 = document.getElementById("eliminar");
    var contenedor = document.getElementById("tipoeliminar");
    contenedor.value = nombre;
    var boton = `<div>`;
    boton += `  <button type="button" class="boton cancelar" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="boton">Eliminar</button>`;
    boton += `</div>`;
    contenedor2.innerHTML = boton;
}


// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que obtiene todas las medidas realizadas
// solicitandolas a la API
// --------------------------------------------------------------
// getMedidas()
// --------------------------------------------------------------
function getMedidas(){

   var recurso = servidorAPI+"medidas/"; //Recurso para acceder a todas las medidas
    fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){
        //console.log(datos["datos"]); 
        
        // Se obtienen los datos retornados y se pasan a la funcion de procesado
        procesarMedidas(datos["datos"]);
        //console.log(datos["datos"]);
    });
} 
var dato = [];

var nombre = [];

//procesar losdatos de medidas y tipo, para obtener la grafica
function procesarMedidas(medidas){
     var recurso = servidorAPI + "tipos/"; //Recurso para acceder a todas las medidas
    fetch(recurso, {
        method: 'GET'
    }).then(function (respuesta) {
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function (datos) {
        console.log(datos["datos"][0].nombreTipo);
        console.log(medidas);
        var a = 0;
           
            for(var i=0;i<datos["datos"].length;i++){
                dato[i] = 0;
                for (const medida of medidas) {
         
                if(medida["tipoMedida"]==datos["datos"][i].nombreTipo){
                    dato[i] = dato[i]+1; // añadir la cantidad de veces
                    
                }
                
               
            }
            
            
           nombre.push(datos["datos"][i].nombreTipo); // añadir el nombre de los tipos
                 
        }
        var popCanvas = document.getElementById("popChart");

var barChart = new Chart(popCanvas, {
  type: 'bar',
  data: {
    labels: nombre,
    datasets: [{
      labelString: 'Veces de medidas',
      data: dato,
      backgroundColor: [
        'rgba(153, 102, 255, 0.6)',
        'rgba(153, 102, 255, 0.6)',
        'rgba(153, 102, 255, 0.6)',
        'rgba(153, 102, 255, 0.6)',
        'rgba(153, 102, 255, 0.6)',
        'rgba(153, 102, 255, 0.6)',
        'rgba(153, 102, 255, 0.6)',
        'rgba(153, 102, 255, 0.6)',
        'rgba(153, 102, 255, 0.6)',
        'rgba(153, 102, 255, 0.6)'
      ]
    }]
  },
    options: {
    legend: {
      display: false // Ocultar legendas
    }
}
});
        
        
    });

}
