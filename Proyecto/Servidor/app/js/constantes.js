
// ------------------------------------------------
// CONSTANTES
// ------------------------------------------------

// Direccion del servidor a la API del servidor
const servidorAPI = "http://localhost/proyecto3A/api/v1.0/";
//const servidorAPI = "http://localhost/api/v1.0/";
//const servidorAPI = "http://adgaran1.upv.edu.es/api/v1.0/";
//const servidorAPI = "http://localhost/o3-sensing-map/Proyecto/Servidor/api/v1.0/";
// Constantes de valores del mapa para mostrar el heatmap
const VALOR_MINIMO_O3 = 0;
const VALOR_MEDIO_O3 = 180;
const VALOR_MAXIMO_O3 = 240;
const VALOR_MINIMO_NO2 = 200;
const VALOR_MEDIO_NO2 = 500;
const VALOR_MAXIMO_NO2 = 1080;
const VALOR_MINIMO_CO = 10;
const VALOR_MEDIO_CO = 34;
const VALOR_MAXIMO_CO = 115;
const VALOR_MINIMO_SO2 = 20;
const VALOR_MEDIO_SO2 = 125;
const VALOR_MAXIMO_SO2 = 350;

// Constantes de diferentes tipos de gases (selector de gases)
const TIPO_GAS_O3 = "O3";
const TIPO_GAS_SO2 = "SO2";
const TIPO_GAS_NO2 = "NO2";
const TIPO_GAS_CO = "CO";

// Tipos de mapas de testeo
const TIPO_MAPA_TEST_ALEATORIO="aleatorio";
const TIPO_MAPA_TEST_DEGRADADO="degradado";
const TIPO_MAPA_TEST_VALORES_BAJOS="valoresBajos";
const TIPO_MAPA_TEST_VALORES_MEDIOS="valoresMedios";
const TIPO_MAPA_TEST_VALORES_ALTOS="valoresAltos";

// ------------------------------------------------
// Funciones generales (pa otros scripts)
// ------------------------------------------------

function getCierroSesion(){

  var recurso = servidorAPI+"cierroSesion/" //Recurso para acceder al cicerre de sesion
  
  fetch(recurso, {
      method: 'GET'
  }).then(function(respuesta){
      return respuesta.json(); // se retorna la respuesta de la API en formato JSON
  }).then(function(datos){        
      // Se obtienen los datos retornados y se pasan a la funcion de procesado
     cierroSesion(datos["datos"]);
  });

}


function cierroSesion(datos){
  if(datos){
      console.log(" Borrando sesion ");
      deleteCookie("proyecto3A_usuario");
      deleteCookie("proyecto3A_clave");

      // Se ocultan los controles de admin
      /*
      var controlesAdmin = document.getElementsByClassName("control-admin");
      for (const control of controlesAdmin) {
          control.remove();
      }

      // se vuelve a agregar boton de login
      var controles_sesion = document.getElementById("controles_sesion");
      if(controlesAdmin!=null){
        controles_sesion.innerHTML = "button type='button' onclick='peticion_inicioSesion()' class='btn btn-info'>Iniciar</button>"
      }
*/
      //Compruebo si se esta en una zona que requiere autorizacion
      comprobarLogin();

  }
}


function peticionCerrarSesion(){
  getCierroSesion();
}


// -------------------------------------------------
// COOKIES


function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }
  
  function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  function deleteCookie(name) {
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }