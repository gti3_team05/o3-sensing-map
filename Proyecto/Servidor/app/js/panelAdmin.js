// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que obtiene todas las medidas realizadas
// solicitandolas a la API
// --------------------------------------------------------------
// getUsuario()
// --------------------------------------------------------------
function getUsuario() {

    var recurso = servidorAPI + "usuarios/"; //Recurso para acceder a todas las medidas
    fetch(recurso, {
        method: 'GET'
    }).then(function (respuesta) {
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function (datos) {
        //console.log(datos["datos"]);

        // Se obtienen los datos retornados y se pasan a la funcion de procesado

    });
}

// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que obtiene todas las medidas realizadas
// solicitandolas a la API
// --------------------------------------------------------------
// getUsuarioActividad()
// --------------------------------------------------------------
function getUsuariosActividad() {
    var recurso = servidorAPI + "usuariosActividad/"

    fetch(recurso, {
        method: 'GET'
    }).then(function (respuesta) {
        return respuesta.json(); 
        //console.log(respuesta);// se retorna la respuesta de la API en formato JSON
    }).then(function (datos) {
        // Se obtienen los datos retornados y se pasan a la funcion de procesado
        console.log(datos);
        if (datos["datos"].length > 0) {
            procesarUsuariosActividad(datos["datos"]);
            inactivos(datos["datos"]);
            totalusuario(datos["datos"]);

        } else {
            console.log("No hay datos de medidas de ningun usuario!");
        }
    });
}



// funcion para procesar la info de usuarios
// usuarios -> procesarUsuariosActividad()
async function procesarUsuariosActividad(usuarios) {


    // Contenedor html en el que mostrar los datos
    var contenedor = document.getElementById("contenedor_datos_estadisticaActividad");


    // Cabecera de la tabla
    var visualizacion = `<table class="table table-hover order-table">`;
    visualizacion += `<thead class="table-primary">`;
    visualizacion += `<th class="px-lg-1">Usuario</th>`;
    visualizacion += `<th class="px-lg-1">Nombre</th>`;
    visualizacion += `<th class="px-lg-1">Apellidos</th>`;
    visualizacion += `<th class="px-lg-1">Telefono</th>`;
    visualizacion += `<th class="px-lg-1">Nodo asociado</th>`; //TODO: en la columna se puede mostrar un QR a traves de la api de google del nodo, en vez de la ID sin mas
    visualizacion += `<th class="px-lg-1">Ultima actividad</th>`;
    visualizacion += `<th class="px-lg-1">Distancia recorrida ultimo dia (Km)</th>`;
    visualizacion += `<th class="px-lg-1">Tiempo activo</th>`;
    visualizacion += `<th class="px-lg-1">Opciones</th>`;

    visualizacion += `</thead>`;


    // se crea una fila por cada medida
    for (const usuario of usuarios) {


        //console.log(medida);
        visualizacion += `<tr>`;
        visualizacion += `<td class="px-lg-1">${usuario["correo"]}</td>`;
        visualizacion += `<td class="px-lg-1">${usuario["nombre"]}</td>`;
        visualizacion += `<td class="px-lg-1">${usuario["apellidos"]}</td>`;
        visualizacion += `<td class="px-lg-1">${usuario["telefono"]}</td>`;
        visualizacion += `<td class="px-lg-1">${usuario["idNodo"]}</td>`;
        visualizacion += `<td class="px-lg-1">${usuario["ultimaActividad"]}</td>`;

        distanciaKM = usuario["distancia"];
        distanciaKM = parseFloat(distanciaKM);
        if (distanciaKM < 1) {
            distanciaKM = distanciaKM * 1000;
            distanciaKM = distanciaKM + " (metros)"
        } else {
            distanciaKM = parseFloat(distanciaKM).toFixed(2);

        }

        visualizacion += `<td class="px-lg-1 text-center">${distanciaKM}</td>`;

        visualizacion += `<td class="px-lg-1">${usuario["actividadMinutos"]}</td>`;
        visualizacion += `<td class="px-lg-1"><div class="row"><button class="correo-todo" data-toggle="modal" data-target="#exampleModalCentered2" onclick="pasarPara('${usuario["correo"]}')">
                   <i class="fas fa-envelope"></i>
                </button>&nbsp<div id="editar" data-toggle="modal" data-target="#exampleModalCentered">
<button class="correo-todo" onclick="pasareditar('${usuario["correo"]}','${usuario["clave"]}','${usuario["nombre"]}','${usuario["apellidos"]}','${usuario["telefono"]}','${usuario["idNodo"]}')">
                    <i class="fas fa-edit"></i></button>
                </div>&nbsp<div><button type="button" class="correo-todo" data-toggle="modal" data-target="#exampleModalCentered3" onclick="darbaja('${usuario["correo"]}')">
  <i class="fas fa-trash-alt"></i>
</button></div></div></td>`;


        visualizacion += `</tr>`;



    }

    visualizacion += `</table>`;


    // Se muestra en pantallas
    contenedor.innerHTML = visualizacion;

/*&nbsp<div><button type="button" class="correo-todo" data-toggle="modal" data-target="#exampleModalCentered3" onclick="darbaja('${usuario["correo"]}')">
  <i class="fas fa-trash-alt"></i>
</button></div>*/

}

// funcion para mostrar el correo en el formulario
function pasarPara(correo) {
    console.log(correo);
    var contenedor = document.getElementById("correoenvio");
    contenedor.value = correo;
    console.log(contenedor);
}

//Funcion para pasar los parametros para editar el usuario
async function pasareditar(correo, clave, nombre, apellidos, telefono, idNodo) {
    var contenedor = document.getElementById("correouser");
    var contenedor1 = document.getElementById("correouser2");
    var contenedor2 = document.getElementById("contasenauser");
    var contenedor3 = document.getElementById("name");
    var contenedor4 = document.getElementById("apellidos");
    var contenedor5 = document.getElementById("phone");
    var contenedor6 = document.getElementById("nodos");

    contenedor.value = correo;
    contenedor1.value = correo;
    contenedor2.value = clave;
    contenedor3.value = nombre;
    contenedor4.value = apellidos;
    contenedor5.value = telefono;
    contenedor6.value = idNodo;


}


//funcion para pasar el correo y eliminar el usuario
function darbaja(correo) {
    var contenedor2 = document.getElementById("eliminar");
    var contenedor = document.getElementById("correoeliminar");
    contenedor.value = correo;
    var boton = `<div>`;
    boton += `  <button type="button" class="boton cancelar" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="boton">Eliminar</button>`;
    boton += `</div>`;
    contenedor2.innerHTML = boton;
}


// funciones que calcula los dias de ausencia, si son mas de tres aparecera en la tabla
function calcularDiasAusencia(fechaIni, fechaFin)
{

var diasDif = fechaFin.getTime() - fechaIni.getTime();
var dias = Math.round(diasDif/(1000 * 60 * 60 * 24));

return dias;
    
}

function inactivos(usuarios) {
    // Contenedor html en el que mostrar los datos

    var contenedor = document.getElementById("usuarios-inactivos");

    // Cabecera de la tabla
    var visualizacion = `<table class="table table-hover" id="tab-ina">`;
    visualizacion += `<thead class="table-primary">`;
    visualizacion += `<th class="px-lg-5">Usuario</th>`;
    visualizacion += `<th class="px-lg-5">Nodo asociado</th>`; //TODO: en la columna se puede mostrar un QR a traves de la api de google del nodo, en vez de la ID sin mas
    visualizacion += `<th class="px-lg-5">Ultima actividad</th>`;
    visualizacion += `<th class="px-lg-5">Opciones</th>`;

    visualizacion += `</thead>`;




    // se crea una fila por cada medida
    for (const usuario of usuarios) {
        var f1 = new Date(usuario["ultimaActividad"]);
        var f2 = new Date();
        var dia = calcularDiasAusencia(f1, f2);
    
        
        //console.log(f1.getDate());
        //console.log(f2.getDate());
        if (dia>=3) {
            //console.log(medida);
            visualizacion += `<tr>`;
            visualizacion += `<td class="px-lg-5">${usuario["correo"]}</td>`;
            visualizacion += `<td class="px-lg-5">${usuario["idNodo"]}</td>`;
            visualizacion += `<td class="px-lg-5">${usuario["ultimaActividad"]}</td>`;
            visualizacion += `<td class="px-lg-5"><div id="correo-inactivos" data-toggle="modal" data-target="#exampleModalCentered2">
            <button class="correo-todo" onclick="pasarPara('${usuario["correo"]}')">
                   <i class="fas fa-envelope"></i>
                </button>
                  
                </div></td>`;
            visualizacion += `</tr>`;
        
    }
    }


    visualizacion += `</table>`;

    // Se muestra en pantalla
    contenedor.innerHTML = visualizacion;

}

//funcion que calcula el total de usuarios
function totalusuario(usuario) {

    //console.log(usuario);
    // Contenedor html en el que mostrar los datos
    var contenedor = document.getElementById("total");
    var ver = `<div class="card-body">`

    ver += `<h5 class="card-title">
                                    Total de Usuarios registrados:
                                </h5>`;
    ver += `  <h6  class="card-text">
                          ${usuario.length}&nbsp<i class="fas fa-users"></i>
                                </h6>`;
    ver += `</div>`

    // Se muestra en pantalla
    contenedor.innerHTML = ver;
    //console.log(contenedor);
}

//funciones para abrir el menu
function openNav() {
    document.getElementById("sidebar-wrapper").style.width = "15rem";
}

function closeNav() {
    document.getElementById("sidebar-wrapper").style.width = "0";
}

//funcin para el buscador
(function (document) {
    'use strict';

    var LightTableFilter = (function (Arr) {

        var _input;

        function _onInputEvent(e) {
            _input = e.target;
            var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
            Arr.forEach.call(tables, function (table) {
                Arr.forEach.call(table.tBodies, function (tbody) {
                    Arr.forEach.call(tbody.rows, _filter);
                });
            });
        }

        function _filter(row) {
            var text = row.textContent.toLowerCase(),
                val = _input.value.toLowerCase();
            row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
        }

        return {
            init: function () {
                var inputs = document.getElementsByClassName('light-table-filter');
                Arr.forEach.call(inputs, function (input) {
                    input.oninput = _onInputEvent;
                });
            }
        };
    })(Array.prototype);

    document.addEventListener('readystatechange', function () {
        if (document.readyState === 'complete') {
            LightTableFilter.init();
        }
    });

})(document);


// funcion para cookies y alertas
function existenCookies() {
    if (getCookie("exitoContacto") == true) {
        //mostramos el mensaje
        document.getElementById("todoBien").style.display = "block"
        //borramos las cookies
        deleteCookie("exitoContacto")
    }

    if (getCookie("errorEnvio") == true) {
        //mostramos el mensaje
        document.getElementById("errorEnvio").style.display = "block"
        //borramos las cookies
        deleteCookie("errorEnvio")
    }
    if (getCookie("exitoEditar") == true) {
        //mostramos el mensaje
        document.getElementById("editarbien").style.display = "block"
        //borramos las cookies
        deleteCookie("exitoEditar")
    }

    if (getCookie("errorEditar") == true) {
        //mostramos el mensaje
        document.getElementById("editarmal").style.display = "block"
        //borramos las cookies
        deleteCookie("errorEnvio")
    }

}


// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que lanza las peticiones a la API y realiza los procesos
// necesarios al acceder y cargar la web
// --------------------------------------------------------------
// iniciarEstadisticas()
// --------------------------------------------------------------
function iniciarEstadisticas() {
    window.onload = function () { // cuando el navegador ha cargado, inicio las peticiones
       
        getUsuariosActividad();


    }
}
