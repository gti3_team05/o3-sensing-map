const servidorAPI = "http://localhost/Servidor/api/v1.0/";
function getNodosActividad() {
    var recurso = servidorAPI + "estadoNodosInactivos/"; 
    
    fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){        
        // Se obtienen los datos retornados y se pasan a la funcion de procesado
        if(datos["datos"].length>0){
            procesarDatosActividadNodos(datos["datos"]);
        } else {console.log("No hay datos de medidas de ningun usuario!");}
    });
}

function procesarDatosActividadNodos(usuarios){
    
    // Contenedor html en el que mostrar los datos
    var contenedor = document.getElementById("contenedor_nodosInactivos");
    
    // Cabecera de la tabla
    var visualizacion  = `<table class="table table-responsive table-hover">`;
    visualizacion += `<thead class="thead-light">`;
        visualizacion += `<th class="px-lg-5">Usuario</th>`;
        visualizacion += `<th class="px-lg-5">Nombre</th>`;
        visualizacion += `<th class="px-lg-5">Nodo asociado</th>`; //TODO: en la columna se puede mostrar un QR a traves de la api de google del nodo, en vez de la ID sin mas
  
        visualizacion += `<th class="px-lg-5">Tiempo que lleva inactivo</th>`;
        visualizacion += `<th class="px-lg-5">Alerta</th>`;
    visualizacion += `</thead>`;

    
    // se crea una fila por cada medida
    for (const usuario of usuarios) {
        //console.log(medida);
        visualizacion += `<tr>`;
            visualizacion += `<td class="px-lg-5">${usuario["correo"]}</td>`;
            visualizacion += `<td class="px-lg-5">${usuario["nombre"]}</td>`;
            visualizacion += `<td class="px-lg-5">${usuario["idNodo"]}</td>`;
           
            visualizacion += `<td class="px-lg-5">${usuario["tiempoInactividad"]}</td>`;
                    visualizacion +=
                        `<td class="px-lg-"><div class="row" style="margin:auto: center"><div id="correo-todo" data-toggle="modal" data-target="#exampleModalCentered4">
                   <i class="fa fa-qrcode"></i>
                </div>&nbsp<div id="editar" data-toggle="modal" data-target="#exampleModalCentered">
                    <i class="fas fa-edit"></i>
                </div>&nbsp<div id="editar" data-toggle="modal" data-target="#exampleModalCentered3"><i class="fas fa-trash-alt" onclick=""></i></div></div></td>`;
                        
                        
                        
                        
                        /*
                            `<td>
                                <a href="#" class="view" title="View" data-toggle="tooltip"><i class="material-icons">&#xE417;</i></a>
                                <a href="#" class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                                <a href="#" class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                            </td>`; 
                            */
        
        
        //if(usuario["alerta"] = "roja"){
        //    visualizacion += `<td class="px-lg-5 bg-danger text-white">Sensor inactivo</td>`;
        //} else if(usuario["alerta"] = "verde"){
        //    visualizacion += `<td class="px-lg-5 bg-success text-white">Sensor activo</td>`;
        //}

        visualizacion += `</tr>`;
    }

    visualizacion += `</table>`;

    // Se muestra en pantalla
    contenedor.innerHTML = visualizacion; 
    
    
// --------------------------------------------------------------
// --------------------------------------------------------------
// tabla 2
// --------------------------------------------------------------
// --------------------------------------------------------------
    
        // Contenedor html en el que mostrar los datos
    var contenedor2 = document.getElementById("contenedor_nodosInactivos2");
    
    // Cabecera de la tabla
    var visualizacion2  = `<table class="table table-responsive table-hover">`;
    visualizacion2 += `<thead class="thead-light">`;
        visualizacion2 += `<th class="px-lg-5">Nodo asociado</th>`; 
        visualizacion2 += `<th class="px-lg-5">Usuario</th>`;
//TODO: en la columna se puede mostrar un QR a traves de la api de google del nodo, en vez de la ID sin mas
        visualizacion2 += `<th class="px-lg-5">Ultima actividad</th>`;
        visualizacion2 += `<th class="px-lg-5">opciones</th>`;
    visualizacion2 += `</thead>`;

    
    // se crea una fila por cada medida
    for (const usuario of usuarios) {
        //console.log(medida);
        visualizacion2 += `<tr>`;
            visualizacion2 += `<td class="px-lg-5">${usuario["idNodo"]}</td>`;
            visualizacion2 += `<td class="px-lg-5">${usuario["correo"]}</td>`;
            visualizacion2 += `<td class="px-lg-5">${usuario["ultimaMedida"]}</td>`;
            //visualizacion2 += `<td class="px-lg-5">${usuario["tiempoInactividad"]}</td>`;

     
        
        visualizacion2 +=
                            `<td class="px-lg-5"><div><div id="correo-todo" data-toggle="modal" data-target="#exampleModalCentered2">
                   <i class="fas fa-envelope"></i>
                </div></div></td>`;

        
        visualizacion2 += `</tr>`;
    }

    visualizacion2 += `</table>`;

    // Se muestra en pantalla
    contenedor2.innerHTML = visualizacion2;    
    
// --------------------------------------------------------------
// --------------------------------------------------------------
// tabla 3
// --------------------------------------------------------------
// --------------------------------------------------------------

    // Contenedor html en el que mostrar los datos
var contenedor3 = document.getElementById("contenedor_nodosInactivos3");

// Cabecera de la tabla
var visualizacion3  = `<table class="table table-responsive table-hover">`;
visualizacion3 += `<thead class="thead-light">`;
    visualizacion3 += `<th class="px-lg-5">Nodo asociado</th>`; 
    visualizacion3 += `<th class="px-lg-5">Usuario</th>`;
//TODO: en la columna se puede mostrar un QR a traves de la api de google del nodo, en vez de la ID sin mas
    visualizacion3 += `<th class="px-lg-5">Ultima actividad</th>`;
    visualizacion3 += `<th class="px-lg-5">Tiempo inactivo</th>`;
    visualizacion3 += `<th class="px-lg-5">Alerta</th>`;
visualizacion3 += `</thead>`;


// se crea una fila por cada medida
for (const usuario of usuarios) {
    //console.log(medida);
    visualizacion3 += `<tr>`;
        visualizacion3 += `<td class="px-lg-5">${usuario["idNodo"]}</td>`;
        visualizacion3 += `<td class="px-lg-5">${usuario["correo"]}</td>`;
        visualizacion3 += `<td class="px-lg-5">${usuario["ultimaMedida"]}</td>`;
        visualizacion3 += `<td class="px-lg-5">${usuario["tiempoInactividad"]}</td>`;
    if(usuario["alerta"] = "roja"){
        visualizacion3 += `<td class="px-lg-5 bg-danger text-white">Sensor inactivo</td>`;
    } else if(usuario["alerta"] = "verde"){
        visualizacion3 += `<td class="px-lg-5 bg-success text-white">Sensor activo</td>`;
    }

    visualizacion3 += `</tr>`;
}

visualizacion3 += `</table>`;

// Se muestra en pantalla
contenedor3.innerHTML = visualizacion3; 

}
function openNav() {
    document.getElementById("sidebar-wrapper").style.width = "250px";
}

function closeNav() {
    document.getElementById("sidebar-wrapper").style.width = "0";
}



// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que lanza las peticiones a la API y realiza los procesos
// necesarios al acceder y cargar la web
// --------------------------------------------------------------
// iniciarEstadoNodos()
// --------------------------------------------------------------
function iniciarEstadoNodos(){
    window.onload = function(){ // cuando el navegador ha cargado, inicio las peticiones
        getNodosActividad();
    }
}
