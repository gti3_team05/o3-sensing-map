// --------------------------------------------------------------
// --------------------------------------------------------------
// Pedimos a la base de datos una lista con las incidencias
// del tipo requerido
// --------------------------------------------------------------
// [tipoIncidencia] --> getIncidencias()
// --------------------------------------------------------------


function getIncidencia(tipoIncidencia){
  
    var parametros = "?tipoIncidencia="+tipoIncidencia;
    
    var recurso = servidorAPI+"incidencias/"+parametros;
    
    fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){        
        // Se obtienen los datos retornados y se pasan a la funcion de procesado
        console.log( datos["datos"]);
        mostrarIncidencias(datos["datos"]);
        porcentaje(datos["datos"]);
    });

}

// --------------------------------------------------------------
// --------------------------------------------------------------
// Pedimos a la base de datos las fechas de una incidencia en concreto
// --------------------------------------------------------------
// [tipoIncidencia, usuario] --> getFechasIncidencia()
// --------------------------------------------------------------

function getFechasIncidencia(tipoIncidencia, usuario){
  
    var parametros = "?tipoIncidencia="+tipoIncidencia+"&usuario="+usuario;
    
    var recurso = servidorAPI+"fechasIncidenciasAcumuladas/"+parametros;
    
    fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){        
        // Se obtienen los datos retornados y se pasan a la funcion de procesado
        mostrarFechasIncidencia(datos["datos"]);
    });

}


// --------------------------------------------------------------
// --------------------------------------------------------------
// Recibe un listado de incidencias (en formato JSON)
// se disponen en una tabla y se muestran en la pagina 
// --------------------------------------------------------------
// [incidencias] --> mostrarIncidencias()
// --------------------------------------------------------------
function mostrarIncidencias(incidencias){

    // Contenedor html en el que mostrar los datos
    var contenedor = document.getElementById("contenedor_incidencias");
    
    // Cabecera de la tabla
    var visualizacion  = `<table class="table table-hover">`;
    visualizacion += `<thead class="table-primary">`;
        visualizacion += `<th class="px-lg-5">Usuario</th>`;
        visualizacion += `<th class="px-lg-5">Id Nodo</th>`;
        visualizacion += `<th class="px-lg-5">Ultima fecha en la que se produjo</th>`;
        visualizacion += `<th class="px-lg-5">Tipo de incidencia</th>`;
        visualizacion += `<th class="px-lg-5">Estado</th>`;
        visualizacion += `<th class="px-lg-5">Comentarios</th>`;
        visualizacion += `<th class="px-lg-5">Número de veces que se ha producido</th>`;
    visualizacion += `</thead>`;

    
    // se crea una fila por cada medida
    for (const incidencia of incidencias) {
        //console.log(medida);
        visualizacion += `<tr>`;
            visualizacion += `<td class="px-lg-5">${incidencia["usuario"]}</td>`;
            visualizacion += `<td class="px-lg-5">${incidencia["idNodo"]}</td>`;
            visualizacion += `<td class="px-lg-5">${incidencia["ultimaIncidencia"]}</td>`;
            visualizacion += `<td class="px-lg-5">${incidencia["tipoIncidencia"]} </td>`;
            visualizacion += `<td class="px-lg-5">${incidencia["estado"]} </td>`;
            visualizacion += `<td class="px-lg-5">${incidencia["comentarios"]} </td>`;
            visualizacion += `<td class="px-lg-5">${incidencia["contadorIncidencias"]}</td>`;

        visualizacion += `</tr>`;
    }

    visualizacion += `</table>`;

    // Se muestra en pantalla
    contenedor.innerHTML = visualizacion;


}

// funcion para sabe rel porcentaje de error de medidas

function porcentaje(incidencias){
       var recurso = servidorAPI+"medidas/"; //Recurso para acceder a todas las medidas
    fetch(recurso, {
        method: 'GET'
    }).then(function(respuesta){
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function(datos){
         var contenedor = document.getElementById("fallos");
         var a = parseInt(incidencias[0]["contadorIncidencias"])/datos["datos"].length*100;
        var ver = `<div class="card-body">`

    ver += `<h5 class="card-title">
                                   Porcentaje de fallos totales:
                                </h5>`;
    ver += `  <h6  class="card-text">
                         El porcentaje de fallos que hay en las veces de total de las medidas es de ${a.toFixed(2)} %
                                </h6>`;
    ver += `</div>`

    // Se muestra en pantalla
    contenedor.innerHTML = ver;
    //console.log(contenedor);
    });
}

// --------------------------------------------------------------
// --------------------------------------------------------------
// Funcion que obtiene todas las medidas realizadas
// solicitandolas a la API
// --------------------------------------------------------------
// getNodos()
// --------------------------------------------------------------
function getNodos(){
    
    var recurso = servidorAPI + "nodos/"; //Recurso para acceder a todas las medidas
    fetch(recurso, {
        method: 'GET'
    }).then(function (respuesta) {
        return respuesta.json(); // se retorna la respuesta de la API en formato JSON
    }).then(function (datos) {
        //console.log(datos["datos"]);
        tablanodo(datos["datos"]);
        nuevonodo(datos["datos"]);

        // Se obtienen los datos retornados y se pasan a la funcion de procesado

    });
}

//funcion que procesa los datos de nodos
//nodos -> tablanodos()
function tablanodo(nodos){
    // Contenedor html en el que mostrar los datos
    var contenedor = document.getElementById("Nodos");
    
    
    // Cabecera de la tabla
    var visualizacion  = `<table class="table table-hover">`;
    visualizacion += `<thead class="table-primary">`;
        visualizacion += `<th class="px-lg-5">Id Nodo</th>`;
        visualizacion += `<th class="px-lg-5">Enlace</th>`;
        visualizacion += `<th class="px-lg-5">Opcion</th>`;
    visualizacion += `</thead>`;
    
    // se crea una fila por cada medida
    for (const nodo of nodos) {
        
        visualizacion += `<tr>`;
           
            visualizacion += `<td class="px-lg-5">${nodo["idNodo"]}</td>`;
            visualizacion += `<td class="px-lg-5">${nodo["enlace"]}</td>`;
             visualizacion += `<td class="px-lg-1"><div class="row"><button class="correo-todo" data-toggle="modal" data-target="#exampleModal" onclick="pasarPara('${nodo["enlace"]}')">
                    <i class="fas fa-qrcode"></i></button>
                &nbsp<div><button type="button" class="correo-todo" data-toggle="modal" data-target="#exampleModalCentered3" onclick="darbaja('${nodo["idNodo"]}')">
  <i class="fas fa-trash-alt"></i>
</button></div></div></div></td>`;
        visualizacion += `</tr>`;
        
    }

    visualizacion += `</table>`;

    // Se muestra en pantalla
    contenedor.innerHTML = visualizacion;
    
    
    
}

// funcion par mostrar el codigo QR
function pasarPara(nodo){
    var contendor2 = document.getElementById("frame");
    
    var a = `<img  src="${nodo}" style="width:100%">
          </img>`;
    
    console.log(a);
    contendor2.innerHTML = a;
    console.log(contendor2);
    
}

//funcion para añadir un nuevo nodo
function nuevonodo(nodos){
     var contenedor = document.getElementById("nodos");
    
     var contenedor2 = document.getElementById("enlace");
    var a = nodos.length+1;
    contenedor.value = nodos.length+1;
  
    contenedor2.value = "https://chart.googleapis.com/chart?chs=320x320&cht=qr&chl=" + a;
}

// funcion que pasa el idNodo para eliminar el nodo
function darbaja(nodo) {
    var contenedor2 = document.getElementById("eliminar");
    var contenedor = document.getElementById("nodoeliminar");
    contenedor.value = nodo;
    var boton = `<div>`;
    boton += `  <button type="button" class="boton cancelar" data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="boton">Eliminar</button>`;
    boton += `</div>`;
    contenedor2.innerHTML = boton;
}
