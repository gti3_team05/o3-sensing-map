<?php
// primero hay que incluir la clase phpmailer para poder instanciar
//un objeto de la misma
require "../api/v1.0/includes/class.phpmailer.php";
//incluimos la recpachtcha
require_once "../api/v1.0/includes/recaptchalib.php";

//instanciamos un objeto de la clase phpmailer al que llamamos
//por ejemplo mail
$mail = new phpmailer();

//variables
$correo=$_POST['correoenvio'];
$mensaje=$_POST['mensajecorreo'];
$titulo=$_POST['titulocorreo'];

//Definimos las propiedades y llamamos a los métodos
//correspondientes del objeto mail

//Con PluginDir le indicamos a la clase phpmailer donde se
//encuentra la clase smtp que como he comentado al principio de
//este ejemplo va a estar en el subdirectorio includes
$mail->PluginDir = "../api/v1.0/includes/";

//Con la propiedad Mailer le indicamos que vamos a usar un
//servidor smtp
$mail->Mailer = "smtp";

//Asignamos a Host el nombre de nuestro servidor smtp
$mail->Host = "smtp.gmail.com";
//Le indicamos que el servidor smtp requiere autenticación
$mail->SMTPAuth = true;
$mail->SMTPDebug = 1;
$mail->SMTPSecure = 'ssl';
$mail ->Port = 465;
//Le decimos cual es nuestro nombre de usuario y password  

$mail->Username = "atmozonito3@gmail.com";

$mail->Password = "Grupo5AtmO3";

//Indicamos cual es nuestra dirección de correo y el nombre que

//queremos que vea el usuario que lee nuestro correo

$mail->From = "atmozonito3@gmail.com"; //correo@minijuegos.cc

$mail->FromName = "Atm03";

//el valor por defecto 10 de Timeout es un poco escaso dado que

//voy a usar una cuenta gratuita, por tanto lo pongo a 30
$mail->Timeout=30;

//Indicamos cual es la dirección de destino del correo

$mail->AddAddress($correo);

//Asignamos asunto y cuerpo del mensaje

//El cuerpo del mensaje lo ponemos en formato html, haciendo

//que se vea en negrita

$mail->Subject = $titulo;

//$mail->Body = $mensaje;
$mail->Body = "Buenos días: "; $mail->Body .= $mensaje;

//Definimos AltBody por si el destinatario del correo
//no admite email con formato html
/*$mail->AltBody = "De: "; $mail->Body .= $nombre;
$mail->AltBody .= "\n Correo electrónico: "; $mail->Body .= $correo;
$mail->AltBody .= "\n Teléfono: "; $mail->Body .= $telefono;
$mail->AltBody .= "\n Código postal: "; $mail->Body .= $codigoPostal;
$mail->AltBody .= "\n Mensaje: "; $mail->Body .= $mensaje;*/

$mail -> AddAttachment ($tmp_name, $name);

//se envia el mensaje, si no ha habido problemas

//la variable $exito tendra el valor true

$exito = $mail->Send();
//Si el mensaje no ha podido ser enviado se realizaran 4 intentos mas

//como mucho para intentar enviar el mensaje, cada intento se hará 5

//segundos despues del anterior, para ello se usa la funcion sleep

$intentos=1;

while ((!$exito) && ($intentos < 5)) { 	sleep(5);

//echo $mail->ErrorInfo;

$exito = $mail->Send();

$intentos=$intentos+1;

}

if(!$exito) {
	//si no se ha podido enviar
	setcookie("errorEnvio","primeraCarga");
	header("Location: /app/panelAdmin.html");
}

else {
	//si todo va bien
	setcookie("exitoContacto","primeraCarga");
	header("Location: /app/panelAdmin.html");  }
?>