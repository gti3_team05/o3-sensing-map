function interpolarGas(tipo, limiteMaximo)
    %Interpolacion de valores en unas latitudes y longitudes concretas de
    %medidas tomadas por una red de sensores móvil

    % Variables generales
    servidorPath = "C:\xampp\htdocs\proyecto3A\app\servidor\recursos\";
    formatoFecha = 'yyyy_mm_dd';
    fechaActual = datestr(now,formatoFecha);

    %fechaActual = "2020_12_17"; %% DEBUG


    %% LECTURA DE DATOS

    % Nombre en funcion del tipo indicado 
    switch tipo
        case 1
            tipoNombre = 'CO';
        case 2
            tipoNombre = 'NO2';
        case 3
            tipoNombre = 'O3';
        case 4
            tipoNombre = 'SO2';
        otherwise
            tipoNombre = 'O3';
    end

    % fechaActual = "2020_12_18"; % Fecha Fake 

    fileID_m = fopen(servidorPath+'medidas_'+fechaActual+'_'+tipoNombre+'.txt','r');
    fileID_lat = fopen(servidorPath+'latitudes_'+fechaActual+'_'+tipoNombre+'.txt','r');
    fileID_lon = fopen(servidorPath+'longitudes_'+fechaActual+'_'+tipoNombre+'.txt','r');

    valores = fscanf(fileID_m,"%f");
    latitud = fscanf(fileID_lat,"%f");
    longitud = fscanf(fileID_lon,"%f");
    fclose('all');

    % Trasposición de los vectores para poder operar con ellos
    valores = valores.';
    latitud = latitud.';
    longitud = longitud.';


    %% PROCESADO DE DATOS
    % formato de los datos long
    format long

    % Representación de los datos en caso de necesitarla (se comprueba que los
    % datos no son correlativos

    %plot(latitud, longitud,'*')

    resolucion = 100;
    decimales_precision = 10;


    latInt = linspace(min(latitud),max(latitud), resolucion);
    lonInt = linspace(min(longitud),max(longitud), resolucion);

    % Malla de valores
    [LATITUD,LONGITUD] = meshgrid(latInt, lonInt);

    % Interpolación sobre la nueva malla
    valoresInt = griddata(latitud,longitud,valores, LATITUD, LONGITUD, 'natural');


    %% ALMACENAMIENTO DE DATOS

    % Ruta para almacenar los datos
    ruta = servidorPath+'interpolacion_'+fechaActual+'_'+tipoNombre;


    % Se guarda una iamgen para representar sobre el mapa
    s = pcolor(LATITUD, LONGITUD, valoresInt);  % mapa de colores

    colormap jet % Mapa de colores de frio a caliente
    shading interp; % interpolación de colores

    axis off; caxis([0,limiteMaximo]); % Limites por gas
    set(gca, 'Color', 'none'); % Elimina los ejes
    f=gcf; % obtiene la figura actual

    % Se ocultan las barras y menús antes de generar la imágen parea evitar problemas
    set(f, 'MenuBar', 'none');
    set(f, 'ToolBar', 'none');

    % Exporta una imagen con los datos
    cd C:\xampp\htdocs\proyecto3A\app\servidor\funciones\matlab\export_fig
    export_fig(ruta+'.png' , '-transparent')




    datos = ""; % Almacena los datos númericos

    % Vectores de forma unidimensional (lineales para el grabado secuencial
    p_lat = LATITUD(:);
    p_lon = LONGITUD(:);
    p_val = valoresInt(:);


    for (i = 1:1:size(p_val));
        datos = datos + string(num2str(p_lat(i),10));
        datos = datos + ",";
        datos = datos + string(num2str(p_lon(i),10));
        datos = datos + ",";
        if(isnan(p_val(i))==1)
            datos = datos + "0";
            datos = datos + ";"; 
        else
            datos = datos + string(num2str(p_val(i),10));
            datos = datos + ";";
    
        end
    end



    % Apertura del archivo  
    fileID = fopen(ruta+'.txt','w');
    fprintf(fileID, '%s', datos);  %Escritura del vector de valores
    fclose('all'); % Cierra todos los archivos

end