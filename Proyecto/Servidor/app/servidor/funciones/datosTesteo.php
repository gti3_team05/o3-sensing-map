<?php
/**
 * Funciones para la gestión de datos de testeo sobre el servidor
 * 
 * @author Adrián García Andreu
 * @version 1.0
 */


 /**
  * Crea una instancia de la BBDD
  */
  function instanciarBBDD(){
     
    //Nombre del servidor, bbdd, usuario y clave para realizar la conexion a la bbd
    $bbddServer = "localhost";

    // Servidor local
    $bbddName = "proyecto3a";
    $bbddUser = "root"; $bbddPass = ""; 

    // Servidor publico
    //$bbddName = "adgaran1_proyecto3a";
    //$bbddUser = "adgaran1_root"; $bbddPass = "Y5t22k?s"; 

    //Se realiza la conexion y se establece el charset de los datos en utf-8
    $conexion = mysqli_connect($bbddServer,$bbddUser,$bbddPass,$bbddName);
    mysqli_query($conexion, "SET NAMES utf8");

    return $conexion;

}

/**
* Cierra una instancia con la BBDD
*/
function cerrarBBDD($conexion){
 mysqli_close($conexion);
}


 /**
  * genera un número de medidas falsas para testear 
  */
 function generarMedidasFake(){
    
    // Cantidad de medidas a generar
    $cantidad =100;
    
    // Constantes, parte invariable de la latitud y longitud para la zona de estudio (Gandia)
    $latitudK = "39."; // 38.90 a 39.02
    $longitudK = "-0.1"; // 0.084 a 0.254

    // Vectores para almacenar los datos
    $latitudesFake = array(); $latitudesTemp = array();
    $longitudesFake = array(); $longitudesTemp = array();
    $medidasFake = array();

    // Genera los datos aleatorios y los almacena
    for($i =1; $i<$cantidad; $i++){
        $_medida = camapanaAleatoria(10,300,20,1);
         array_push($medidasFake, $_medida); // medidas


         // Obtiene la latitud ponderando de forma diferente los primeros y los ultimos numeros
         $_latitudA = camapanaAleatoria(38.80,39.10,0.08,0.01);
         $_latitudB = camapanaAleatoria(0,9999,100,1);

    
         // Si el valor mas significativo obtenido no tiene ningun decimal
         if(strpos(strval($_latitudA),".")<1){
            $_latitud = sprintf($_latitudA).".00".sprintf('%04d',$_latitudB);
         }else { // Si este tiene solo 1 decimal
             if(numeroDeDecimales($_latitudA)<2){ // Se asegura de que la 1a parte de la latitud sean 4 cifras (2 decimales)
                 $_latitud = sprintf($_latitudA)."0".sprintf('%04d',$_latitudB);
                } // Si tiene 2 decimales
             else{$_latitud = sprintf($_latitudA).sprintf('%04d',$_latitudB);} // En caso de tener 4 cifras ya, se concatena la 2a parte del valor
            
         }
         array_push($latitudesTemp, $_latitud); // latitud
         


         $_longitudA = camapanaAleatoria(84,255,30,5);
         $_longitudB = camapanaAleatoria(0,999,100,1);

         // Si son menos de 3 cifras...
         if($_longitudA<100){ 
            $_longitud = "0".sprintf($_longitudA).sprintf($_longitudB);
         } else {$_longitud = sprintf($_longitudA).sprintf($_longitudB);}
        
        $_longitud = "-0.".$_longitud;
         array_push($longitudesTemp, $_longitud); // latitud

    }


    // Se crean los datos de latitud con la parte fija
    foreach($latitudesTemp as $lat){
        //$lat = sprintf('%07d',$lat); // se asegura de tener al menos 3 cifras para mantener la precisión
        $valor = $lat;
        //echo "latitud: ".$valor." <br>";

        array_push($latitudesFake, (double)$valor);
    }

    // Se crean los datos de longitudes con la parte fija
    foreach($longitudesTemp as $lon){
        //$lon = sprintf('%03d',$lon); // se asegura de tener al menos 3 cifras para mantener la precisión
        $valor = $lon;
    
        array_push($longitudesFake, (double)$valor);
    }

    $respuesta = array();
    $contador = 0;
    foreach($medidasFake as $medida){
        $m["latitud"] = $latitudesFake[$contador];
        $m["longitud"] = $longitudesFake[$contador];
        $m["valor"] = $medidasFake[$contador];

        array_push($respuesta, $m);
        $contador++;
    }

    return $respuesta;
 }


 /**
  * Inserta un array de medidas fake en la BBDD
  */
function insertarListadoDeMedidas($medidas, $fecha, $tipo){
    
    $usuarioFake = "admin@gmail.com";

    $conexion = instanciarBBDD();

    $partes = explode('_', $fecha);
    $anno = $partes[0];
    $mes = $partes[1];
    $dia = $partes[2];

    $fecha = $anno."-".$mes."-".$dia." 00:00:00";

    // Borra los datos de test del día de hoy 1º
    //$anno = date("Y"); $mes = date("m"); $dia = date("d") ;
    
    $sql = "DELETE FROM medidas WHERE YEAR(momento)=".$anno." AND MONTH(momento)=".$mes." ";
    $sql .= "AND DAY(momento)=".$dia." AND datosDeTest=true AND tipoMedida LIKE '".$tipo."' ";

    //echo $sql."<br>";

    $resultado = mysqli_query($conexion, $sql);
    // Cuando se eliminan las previas, se insertan las nuevas

    foreach($medidas as $medida){
        $sql = "INSERT INTO medidas(valor, latitud, longitud, momento, tipoMedida, correo, datosDeTest, zona) ";
        $sql .= "VALUES(".$medida["valor"].", ".$medida["latitud"].", ".$medida["longitud"].", '".$fecha."', ";
        $sql .= " '".$tipo."', '".$usuarioFake."', true, 'Gandia');";

        $resultado = mysqli_query($conexion, $sql);
        // Cuando se eliminan las previas, se insertan las nuevas

        usleep(3000); // espera entre insercciones
        if(!$resultado){
            // medidas no insertadas
        };

    }

    cerrarBBDD($conexion);

}





 /**
  * Genera una distribucion de valores aleatorios correlacionados
  */
 function camapanaAleatoria($min,$max,$std_deviation,$step=1) {
    $rand1 = (float)mt_rand()/(float)mt_getrandmax();
    $rand2 = (float)mt_rand()/(float)mt_getrandmax();
    $gaussian_number = sqrt(-2 * log($rand1)) * cos(2 * M_PI * $rand2);
    $mean = ($max + $min) / 2;
    $random_number = ($gaussian_number * $std_deviation) + $mean;
    $random_number = round($random_number / $step) * $step;
    if($random_number < $min || $random_number > $max) {
      $random_number = camapanaAleatoria($min, $max,$std_deviation);
    }
    return $random_number;
  }



  /**
   * Devuelve el numero de decimales de un valor
   */
  function numeroDeDecimales($value)
  {
      if ((int)$value == $value)
      {
          return 0;
      }
      else if (! is_numeric($value))
      {
          // throw new Exception('numberOfDecimals: ' . $value . ' is not a number!');
          return false;
      }
  
      return strlen($value) - strrpos($value, '.') - 1;
  }
  



  // Genera los datos
$medidas = generarMedidasFake();



// Valores por defecto
$tipo = "O3";
$fecha = date("Y_m_d"); // Fecha actual

if(isset($_GET["tipo"])){
    $tipo = $_GET["tipo"];
}

if(isset($_GET["fecha"])){
    $fecha = $_GET["fecha"];
}

insertarListadoDeMedidas($medidas,$fecha, $tipo);
  //echo json_encode($medidas);

 ?>