<?php
/**
 * Funciones para manejar la interpolación, obtención de medidas reales,
 * escritura de datos interpolados, etc
 * 
 * @author Adrián García
 * @version 1.0
 */


 require_once("constantes.php");

 // Variables publicas
$medidasFichero = $rutaRecursos."/medidas";
$latitudesFichero = $rutaRecursos."/latitudes";
$longitudesFichero = $rutaRecursos."/longitudes";




 /**
  * Crea una instancia de la BBDD
  */
 function instanciarBBDD(){
     
        //Nombre del servidor, bbdd, usuario y clave para realizar la conexion a la bbd
        $bbddServer = "localhost";

        // Servidor local
        $bbddName = "proyecto3a";
        $bbddUser = "root"; $bbddPass = ""; 

        // Servidor publico
        //$bbddName = "adgaran1_proyecto3a";
        //$bbddUser = "adgaran1_root"; $bbddPass = "Y5t22k?s"; 

        //Se realiza la conexion y se establece el charset de los datos en utf-8
        $conexion = mysqli_connect($bbddServer,$bbddUser,$bbddPass,$bbddName);
        mysqli_query($conexion, "SET NAMES utf8");

        return $conexion;

 }

 /**
  * Cierra una instancia con la BBDD
  */
 function cerrarBBDD($conexion){
     mysqli_close($conexion);
 }



/**
 * Obtiene todos los tipos de los que hay medidas en la BBDD 
 */
function obtenerTiposMedidas(){

    $conexion = instanciarBBDD();
    $sql = "SELECT * FROM tiposmedida ORDER BY nombreTipo";
    
    $resultado = mysqli_query($conexion, $sql);
    
    $respuesta = array();
    while ($fila = mysqli_fetch_assoc($resultado)) {
        $tipo= $fila;
        array_push($respuesta, $tipo);
    };
    
    cerrarBBDD($conexion);

    return $respuesta;
}



 /**
  * Obtiene las medidas reales de la BBDD
  * Fecha con formato YYYY-MM-DD
  * Texto:momento, Texto:tipo --> f --> [Medidas]
  */
 function obtenerMedidasReales($tipo, $fecha, $zona){

    $conexion = instanciarBBDD();
    
    $sql = "SELECT * FROM medidas WHERE tipoMedida LIKE \"".$tipo."\" AND (DATE(momento) = '".$fecha."') ";
    $sql .= " AND zona LIKE '".$zona."' GROUP BY latitud, longitud ORDER BY Momento DESC";
    
    $resultado = mysqli_query($conexion, $sql);
    

    // Almacena la respuesta en un array asociativo
    $respuesta = array();
    while ($fila = mysqli_fetch_assoc($resultado)) {
        array_push($respuesta, $fila);
    };
    
    cerrarBBDD($conexion);

    return $respuesta;

 }



 /**
  * Lee la última interpolación diaria
  */
 function leerInterpolacion($tipo, $fecha){

    $rutaRecursos = "C:/xampp/htdocs/proyecto3a/app/servidor/recursos";
    $interpolacionDiariaFichero = $rutaRecursos."/interpolacion_".$fecha."_".$tipo.".txt";

    //echo $interpolacionDiariaFichero;


    // Vectores con los valores resultado de la operacion
    $respuesta = array();
    
    $datosArchivo = file_get_contents($interpolacionDiariaFichero);
    //echo $datosArchivo;


    // Obtengo cada registro en función de los delimitadores
    $puntos = explode(";", $datosArchivo, -1);
    foreach($puntos as $punto){ // Por cada punto, obtengo la latitud y la longitud

        $lat = explode(",", $punto)[0]; // Latitud
        $lon = explode(",", $punto)[1]; // Longitud
        $val = explode(",", $punto)[2]; // Longitud


        if($val != NULL){
            $medida["latitud"] = $lat;
            $medida["longitud"] = $lon;
            $medida["valor"] = $val;
            array_push($respuesta, $medida);
        }

    }


    return $respuesta;

 }




?>