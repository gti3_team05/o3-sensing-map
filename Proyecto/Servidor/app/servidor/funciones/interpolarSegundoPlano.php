<?php

/**
 * 
 * Llama en 2º plano a una instancia de MatLab para interpolar las medidas del día en curso
 * 
 * @author Adrián García
 * @version 1.0
 * 
 */


require_once("constantes.php");

require_once("manejoDeArchivos.php");
require_once("interpolacion.php");
 

// Se obtienen las medidas de la BBDD
$fecha = date("Y-m-d"); // Fecha actual

// Se obtienen los tipos de medidas de la BBDD
$tipos = obtenerTiposMedidas();

$contador = 1; // Contador para el bucle

foreach($tipos as $tipo){

       $tipoNombre = $tipo["nombreTipo"];
       $tipoLimite = $tipo["valorPeligro"]; // Valores máximos permitidos

       // Se utiliza la fecha del día actual para interpolar
        $fecha = date("Y-m-d"); // Fecha actual
       //$fecha = "2020-12-17"; // Fecha actual
       
       // Se obtienen las medidas del tipo actual en la fecha de hoy de la region de Gandia
       $medidas =  (obtenerMedidasReales($tipoNombre, $fecha, "Gandia"));

       
       if (count($medidas)>1){ // Si hay medidas de dicho tipo hoy...
              
              echo "Interpolando ".$tipoNombre."...\n";

              // Se instancian vectores para almacenar los datos de interés
              $valores = array(); $latitudes = array(); $longitudes = array();
              $valoresTexto=""; $latitudesTexto=""; $longitudesTexto="";

              // Se cargan los vectores con los datos de interés
              foreach($medidas as $medida){
                     array_push($valores, (int)$medida["valor"]);
                     array_push($latitudes, (double)$medida["latitud"]);
                     array_push($longitudes,(double)$medida["longitud"]);

                     $valoresTexto .= $medida["valor"]."\n";
                     $latitudesTexto .= $medida["latitud"]."\n";
                     $longitudesTexto .= $medida["longitud"]."\n";

              }

              $fechaNombre = date("Y_m_d"); // Fecha actual con el formato para el fichero de entrada MatLab
              //$fechaNombre = "2020_12_17";

              escribirEnArcvhio($medidasFichero."_".$fechaNombre."_".$tipoNombre.".txt", $valoresTexto);
              escribirEnArcvhio($latitudesFichero."_".$fechaNombre."_".$tipoNombre.".txt", $latitudesTexto);
              escribirEnArcvhio($longitudesFichero."_".$fechaNombre."_".$tipoNombre.".txt", $longitudesTexto);

              sleep(1); // 1 segundo para procesar la petición

              // Comando de MatLab
              $comando = "start matlab -batch run('".$rutaFunciones."/matlab/interpolarGas(".$contador.",".$tipoLimite.")')";
              sleep(1);
              exec($comando);
              
              echo "Interpolación realizada ".$tipoNombre."\n";

              sleep(1); // 3 segundos entre tipos


       } else {
              echo "No hay suficientes medidas para realizar una interpolación de ".$tipoNombre."\n";
       } // Fin de las medidas de un tipo


       $contador++;

}


?>