// -*- mode: c++ -*-

// --------------------------------------------------------------
// Jordi Bataller i Mascarell
// --------------------------------------------------------------

#ifndef PUBLICADOR_H_INCLUIDO
#define PUBLICADOR_H_INCLUIDO

// --------------------------------------------------------------
// --------------------------------------------------------------
class Publicador {

  // ............................................................
  // ............................................................
private:

// .................................................................
// UUID nombrado como EPSG-PROY-3A-G05
  uint8_t beaconUUID[16] = { 
	'E', 'P', 'S', 'G', '-', 'P', 'R', 'O', 'Y', '-', 
	'3', 'A', '-', 'G', '0', '5',
	};

  // ............................................................
  // ............................................................
  // Nombre de la emisora
public:
  EmisoraBLE laEmisora {
	"GTI-3A-G5", //  nombre emisora
	  0x004c, // fabricanteID (Apple)
	  4 // txPower
	  };
  
  const int RSSI = -53; // por poner algo, de momento no lo uso

  // ............................................................
  // ............................................................
public:

  // ............................................................
  // ............................................................
  // Tipos de medidas 
  enum MedicionesID  {
	O3 = 11,
	TEMPERATURA = 12,
	RUIDO = 13,
  BATERIA_BAJA = 14
  };

  // ............................................................
  // ............................................................
  Publicador( ) {
	// ATENCION: no hacerlo aquí. (*this).laEmisora.encenderEmisora();
	// Pondremos un método para llamarlo desde el setup() más tarde
  } // ()

  // ............................................................
  // ............................................................
  void encenderEmisora() {
	(*this).laEmisora.encenderEmisora();
  } // ()

  // ............................................................
  // ............................................................
  void publicarO3( int16_t valorO3, uint8_t contador,
					long tiempoEspera ) {

          //Serial.print("Valor O3: ");
          //Serial.println(valorO3);

	//
	// 1. empezamos anuncio
	//
	uint16_t major = (MedicionesID::O3 << 8) + contador;
	(*this).laEmisora.emitirAnuncioIBeacon( (*this).beaconUUID, 
											major,
											valorO3, // minor
											(*this).RSSI // rssi
									);

	/*
	Globales::elPuerto.escribir( "   publicarCO2(): valor=" );
	Globales::elPuerto.escribir( valorO3 );
	Globales::elPuerto.escribir( "   contador=" );
	Globales::elPuerto.escribir( contador );
	Globales::elPuerto.escribir( "   todo="  );
	Globales::elPuerto.escribir( major );
	Globales::elPuerto.escribir( "\n" );
	*/

	//
	// 2. esperamos el tiempo que nos digan
	//
	esperar( tiempoEspera );

	//
	// 3. paramos anuncio
	//
	(*this).laEmisora.detenerAnuncio();
  } // ()

  // ............................................................
  // ............................................................
  void publicarTemperatura( int16_t valorTemperatura,
							uint8_t contador, long tiempoEspera ) {

	uint16_t major = (MedicionesID::TEMPERATURA << 8) + contador;
	(*this).laEmisora.emitirAnuncioIBeacon( (*this).beaconUUID, 
											major,
											valorTemperatura, // minor
											(*this).RSSI // rssi
									);
	esperar( tiempoEspera );

	(*this).laEmisora.detenerAnuncio();
  } // ()


  // ............................................................
  // ............................................................
  void publicarBajaBateria( int16_t bajaBateria, uint8_t contador,
          long tiempoEspera ) {
  //
  // 1. empezamos anuncio
  //
  uint16_t major = (MedicionesID::BATERIA_BAJA << 8) + contador;
  (*this).laEmisora.emitirAnuncioIBeacon( (*this).beaconUUID, 
                      major,
                      bajaBateria, // minor
                      (*this).RSSI // rssi
                  );
  //
  // 2. esperamos el tiempo que nos digan
  //
  esperar( tiempoEspera );

  //
  // 3. paramos anuncio
  //
  (*this).laEmisora.detenerAnuncio();
  } // ()

  
}; // class


  
  // ............................................................
  // ............................................................

// --------------------------------------------------------------
// --------------------------------------------------------------
// --------------------------------------------------------------
// --------------------------------------------------------------
#endif
