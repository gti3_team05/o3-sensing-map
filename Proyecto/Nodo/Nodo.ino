// -*-c++-*-

// --------------------------------------------------------------
//
// Jordi Bataller i Mascarell
// 2019-07-07
//
// --------------------------------------------------------------

// https://learn.sparkfun.com/tutorials/nrf52840-development-with-arduino-and-circuitpython

// https://stackoverflow.com/questions/29246805/can-an-ibeacon-have-a-data-payload

// --------------------------------------------------------------
// --------------------------------------------------------------
#include <bluefruit.h>

#undef min // vaya tela, están definidos en bluefruit.h y  !
#undef max // colisionan con los de la biblioteca estándar

// --------------------------------------------------------------
// --------------------------------------------------------------
#include "LED.h"
#include "Pantallita.h"

// --------------------------------------------------------------
// --------------------------------------------------------------
namespace Globales {
  
  LED elLED ( /* NUMERO DEL PIN LED = */ 7 );
  
  LED ledBateriaBaja ( 23 );
  
  Pantallita elPuerto ( /* velocidad = */ 115200 ); // 115200 o 9600 o ...

  // Serial1 en el ejemplo de Curro creo que es la conexión placa-sensor 
};

// --------------------------------------------------------------
// --------------------------------------------------------------
#include "EmisoraBLE.h"
#include "Publicador.h"
#include "Medidor.h"


// --------------------------------------------------------------
// --------------------------------------------------------------
namespace Globales {

  Publicador elPublicador;

  Medidor elMedidor;

}; // namespace

// --------------------------------------------------------------
// --------------------------------------------------------------
void inicializarPlaquita () {

  // de momento nada

} // ()

// --------------------------------------------------------------
// setup()
// --------------------------------------------------------------
void setup() {

  // Hay que deshabilitarlo para que el micro envíe sin necesidad de que el usuario abra la consola del puerto serie
  // ---------------------
  //Globales::elPuerto.esperarDisponible();

  // 
  // 
  // 
  inicializarPlaquita();

  // Suspend Loop() to save power
  // suspendLoop();

  // 
  // 
  // 
  Globales::elPublicador.encenderEmisora();

  // Globales::elPublicador.laEmisora.pruebaEmision();
  
  // 
  // 
  // 
  Globales::elMedidor.iniciarMedidor( /* Indico los baudios del sensor*/ 9600);

  // 
  // 
  // 
  esperar( 1000 );

  Globales::elPuerto.escribir( "---- setup(): fin ---- \n " );

} // setup ()

// --------------------------------------------------------------
// --------------------------------------------------------------
inline void lucecitas() {
  using namespace Globales;

  elLED.brillar( 100 ); // 100 encendido
  esperar ( 400 ); //  100 apagado
  elLED.brillar( 100 ); // 100 encendido
  esperar ( 400 ); //  100 apagado
  Globales::elLED.brillar( 100 ); // 100 encendido
  esperar ( 400 ); //  100 apagado
  Globales::elLED.brillar( 1000 ); // 1000 encendido
  esperar ( 1000 ); //  100 apagado
} // ()


// --------------------------------------------------------------
// loop ()
// --------------------------------------------------------------
namespace Loop {
  uint8_t cont = 0;

  // Pin para obtener los datos del la bateria
  int bateriaPin = 2;
  // Variable para almacenar el valor raw que nos proporciona el pin
  int bateriaValue;
};

// ..............................................................
// ..............................................................
void loop () {

  using namespace Loop;
  using namespace Globales;

  cont++;

  elPuerto.escribir( "\n---- loop(): empieza " ); elPuerto.escribir( cont ); elPuerto.escribir( "\n" );

  //
  // comprobamos la batería del sensor
  //

  bateriaValue = analogRead(bateriaPin);   // realizar la lectura

  // Valor de bateria para testear
  // bateriaValue = 800;
  
  elPuerto.escribir("\n ------- Batería value: "); elPuerto.escribir(bateriaValue);elPuerto.escribir( "\n" );

  // si la batería es baja mandamos a la app un aviso para que mande una notificación
  if(bateriaValue<870)
  {
    ledBateriaBaja.encender();
    elPuerto.escribir("\n ------- Batería baja "); elPuerto.escribir( "\n" );
    elPublicador.publicarBajaBateria( 1,
              cont,
              50000 // intervalo de emisión  establecido en 5 segundos
              );
  }else
  {
    ledBateriaBaja.apagar();
  }
  
  // 
  // mido y publico
  // 
  
  int valorO3 = elMedidor.medirO3();

  if(valorO3 != NULL){
    elPuerto.escribir("\n ------- valorO3 ug = ");   elPuerto.escribir(valorO3);elPuerto.escribir( "\n" );

  
  elPublicador.publicarO3( valorO3,
              cont,
              5000 // intervalo de emisión  establecido en 5 segundos
              );

  } else{

    esperar(10000); // Si el sensor se está estabilizando se espera 10s entre comprobaciones
    //TODO: En adelante indicar al usuario de alguna forma que el sensor se está estabilizando
    
  }


  

  elPuerto.escribir( "---- loop(): acaba **** " ); elPuerto.escribir( cont ); elPuerto.escribir( "\n" );
  

  
  
} // loop ()
