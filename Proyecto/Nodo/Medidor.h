// -*- mode: c++ -*-

#ifndef MEDIDOR_H_INCLUIDO
#define MEDIDOR_H_INCLUIDO


#define RX_PIN 15
#define TX_PIN 17

// ------------------------------------------------------
// ------------------------------------------------------
class Medidor {

  // .....................................................
  // .....................................................
private:
int sensorData[11];




// ............................................................
// ............................................................
// Indice de los valores obtenidos al leer datos del sensor de O3 
// ............................................................
enum DatosIndices {
 SN = 0,
 O3 = 1,
 TEMP = 2,
 HR = 3,
 O3_RAW = 4,
 TEMP_RAW = 5,
 HR_RAW = 6,
 DIAS = 7, HH = 8, MM = 9, SS = 10
};



// .....................................................
// .....................................................
// Solicita una lectura de datos al sensor de gas (via serie) 
// y actualiza los ultimos valores leidos 
// en la variable ´sensorData´
// .....................................................
// leer11Datos()
// .....................................................


void leer11Datos(){
    Serial1.print('\r'); //Pide al sensor una lectura de datos mediante el puerto serie
    //Serial.println("Lectura del sensor iniciada...esperando...");

    while(!Serial1.available()) { delay(10); } //Espera disponibilidad del sensor

    for (int i =0; i<11; i++) { this->sensorData[i] = Serial1.parseInt(); } //Guarda los datos


}

public:

  // .....................................................
  // .....................................................
  // Constructor de la clase
  // .....................................................
  // Medidor()
  // .....................................................
  Medidor() {
  } // ()


  // .....................................................
  // Inicia el medidor abriendo la comunicacion serie con el,
  // indicandole los baudios.
  // .....................................................
  // R --> inciarMedidor ()
  // .....................................................
  void iniciarMedidor(long baudios) {
	// las cosas que no se puedan hacer en el constructor, if any

  Serial1.begin( baudios );
 
  } // ()



  // .....................................................
  // Tras tener estabilizado el sensor, obtiene una medida de O3,
  // Promedidada, calibrada y procesada (en ug/m^3)
  // .....................................................
  // medirO3() -> R
  // .....................................................
  int medirO3() {
	  
	  leer11Datos(); //Se realiza una lectura de datos

      // ...........................................
      // Se comprueba si el sensor se ha estabilizado (1 hora desde su encendido)
     // if(this->sensorData[DatosIndices::HH]<1){
      if(this->sensorData[DatosIndices::HH]<-1){  //SOLO PARA DEBUG
       Serial.println("Menos de 1 hora, no envia");
       delay(1000);
      return NULL;
     } 
     else{ //Al transcurrir una hora se empiezan a considerar validas las medidas
      Serial.println("Mas de 1 hora, envia");
      int medidaResultado = promediarO3(); // Medida promediada, calibrada y procesada.
      return medidaResultado;
     }


    
  } // ()



  // .....................................................
  // .....................................................
  // Promedia medidas del sensor de O3 con una cantidad 
  // de muestras determinadas tomadas cada X segundos. 
  // Se retorna el promedio de estas medidas, calibrado y procesado.
  // ....................................................
  // promediarO3() --> Z
  // ....................................................
  int promediarO3() {

    //   const int bufferLongitud = 30;
     const int bufferLongitud = 5; //SOLO PARA DEBUG
    const int tiempoEsperaEntreMedidas = 1000;
  
    // Para guardar los resultados
    int o3Promedio, temperaturaPromedio; 
        
  
    // ........................................................
    // Se toman las medidas
    //
    int medidasO3Buffer[bufferLongitud]; //Buffers de O3 y temperatura para promediar
    int medidasTemperaturaBuffer[bufferLongitud];
    
    for(int i=0; i<bufferLongitud; i++){
  
     Serial.print("Tomando medida para promediar "); Serial.println(i);
      leer11Datos();
  
      // Se almacenan en los buffers, el O3 y la temperatura
      medidasO3Buffer[i]=this->sensorData[DatosIndices::O3];
      medidasTemperaturaBuffer[i]=this->sensorData[DatosIndices::TEMP];
  
      //Tiempo de espera entre medida y medida
      esperar(tiempoEsperaEntreMedidas); 
      
    } // 
  
    
    // .......................................................
    // Promedio de las medidas tomadas
    //
    int sumaMedidasO3 = 0, sumaMedidasTemperatura = 0;
   
    for(int i=0; i<bufferLongitud; i++){
  
      sumaMedidasO3 += medidasO3Buffer[i];
      sumaMedidasTemperatura += medidasTemperaturaBuffer[i];
      
    }

    //Si la suma es distinto de 0 se promedia entre el numero total de muestras
    if(sumaMedidasO3!=0){o3Promedio = (round(sumaMedidasO3/bufferLongitud));}
    if(sumaMedidasTemperatura!=0){temperaturaPromedio = (round(sumaMedidasTemperatura/bufferLongitud));}
  
  
    //Se calibra el O3 en funcion del promedio
    int medidaCalibrada = calibrarO3(o3Promedio, temperaturaPromedio);
  
    
    return medidaCalibrada;
 
  } // ()




  // .....................................................
  // .....................................................
  // Calibra las medidas tomadas por el sensor en funcion
  // de otras medidas de referencia para corregir la componente 
  // de error sistematico del sensor
  // ....................................................
  // O3:Z, Temperatura:Z --> calibrar() --> Z
  // ....................................................
  int calibrarO3(int o3Promedio, int temperaturaPromedio) {

    // Para guardar los valores calibrados
    int o3Calibrado = o3Promedio;
    int temperaturaCalibrada = temperaturaPromedio;

    //TODO: Calibrado con estación de referencia
    //Calibrar Temperatura y O3

    //Se procesa el O3 en funcion del O3 y la temperatura calibradas
    int medidaProcesada = procesarO3(o3Calibrado, temperaturaCalibrada);
    
    return medidaProcesada;
 
  } // ()



  // .....................................................
  // .....................................................
  // Se procesa el O3 en funcion de la temperatura para obtener
  // un valor de O3 en ug/m^3
  // .....................................................
  //  O3:Z, Temperatura:Z --> procesarO3() --> Z
  // .....................................................

  int procesarO3(int o3Calibrado, int temperaturaCalibrada) {

     // Factor de conversion de ppb a ug/m^3
    float factorTemperaturaConversionO3 = 1; 

    // En funcion de la temperatura se toma un valor para pasar a ug u otro
    if(temperaturaCalibrada>=20){ factorTemperaturaConversionO3 =2.00f; }
    else{factorTemperaturaConversionO3=2.15f;}




    // Valor en ug/m^3 
    //int medidaResultado = (int)(round(o3Calibrado * factorTemperaturaConversionO3)); //Resultado
    int medidaResultado = (int)(round((o3Calibrado * 12.187 * 48) /(temperaturaCalibrada + 273.15))); //Resultado
    //int medidaResultado = (int)(round(o3Calibrado));
  return medidaResultado-228;
 
  } // ()





  // .....................................................
  // .....................................................
  int medirTemperatura() {
	return -12; // qué frío !
  } // ()
	
}; // class

// ------------------------------------------------------
// ------------------------------------------------------
// ------------------------------------------------------
// ------------------------------------------------------
#endif
