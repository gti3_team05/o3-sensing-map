package org.g05.o3receptor.model.repository.remote;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import androidx.fragment.app.Fragment;

import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MedidasTipoUsuarioTiempo_get extends AsyncTask<Void, Void, String> {

    private final int CODIGO_PETICION = Constantes.PETICION_MEDIDAS_TIPO_USUARIO_TIEMPO;
    private final String recursoREST = "medidasTipoUsuarioTiempo/";

    private TareaCallback callback;
    private String correo;
    private String tipoMedida;
    private String fechaPrincipio;
    private String fechaFinal;
    private String parametros;
    private ProgressDialog pd;


    public MedidasTipoUsuarioTiempo_get(String correo, String tipoMedida, String fechaPrincipio, String fechaFinal, TareaCallback callback){
        this.correo = correo;
        this.tipoMedida = tipoMedida;
        this.fechaPrincipio = fechaPrincipio;
        this.fechaFinal = fechaFinal;
        this.callback = callback;
    }

    public MedidasTipoUsuarioTiempo_get(String correo, String tipoMedida, String fechaPrincipio, String fechaFinal, TareaCallback callback, ProgressDialog pd){
        this.correo = correo;
        this.tipoMedida = tipoMedida;
        this.fechaPrincipio = fechaPrincipio;
        this.fechaFinal = fechaFinal;
        this.callback = callback;
        this.pd = pd;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if(pd != null){
            this.pd.show();
        }
    }

    private String obtenerParametros(){


        //Se añaden los parametros del request a la API REST como clave Valor
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("correo", correo);
        parametros.put("tipoMedida", tipoMedida);
        parametros.put("fechaPrincipio", fechaPrincipio);
        parametros.put("fechaFinal", fechaFinal);


        // Se recorre el objeto clave valor y se insertan los valores en un Texto
        Iterator it = parametros.entrySet().iterator();
        boolean primero = true;
        String parametrosString = "";
        while (it.hasNext()){
            if(primero){
                primero = false;
            } else{
                parametrosString +="&"; //Se separan los datos con el caracter '&'
            }

            Map.Entry pair = (Map.Entry)it.next();
            parametrosString +=pair.getKey()+"="+pair.getValue(); //A cada dato se le asigna el valor tras el caracter '='
        }

        //Se devuelven los datos con sus valores como Texto
        return parametrosString;

    }

    @Override
    protected String doInBackground(Void... voids) {

        String retorno = null;
        this.parametros = obtenerParametros();

        URL url;
        HttpURLConnection connection = null;

        try {
            // ---------------------------------------
            // Peticion GET datos en la URL

            url = new URL(Constantes.API_URI + recursoREST + "?" + parametros);

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "*/*");



            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }


            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.d("Resultado medidas", "Response: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }

        return retorno;
    }

    @Override
    protected void onPostExecute(String resultado) {
        super.onPostExecute(resultado);

        if(pd != null){
            this.pd.dismiss();
        }

        boolean todoCorrecto = false;

        JSONObject resultadoJSON = null;
        JSONArray datos = null;

        if(resultado != null){
            try {
                resultadoJSON = new JSONObject(resultado);

                datos = resultadoJSON.getJSONArray("datos");
                if(datos.length()>0){
                    todoCorrecto = true;
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("Resultado medidas",String.valueOf(e));
            }
        }

        // Si el resultado es el esperado...
        if(todoCorrecto){
            callback.terminadoCorrectoConDatos(CODIGO_PETICION, datos, tipoMedida);
        } else{
            callback.terminadoFallo(CODIGO_PETICION);
        }

    }
}
