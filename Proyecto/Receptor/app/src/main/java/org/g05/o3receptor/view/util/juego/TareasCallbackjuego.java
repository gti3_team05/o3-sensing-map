package org.g05.o3receptor.view.util.juego;

import android.graphics.Bitmap;

import org.g05.o3receptor.model.entity.Medida;
import org.json.JSONArray;

import java.util.List;


public interface TareasCallbackjuego {

    void iniciado(int codigoSolicitud); // Cuando una tarea comience
    void terminadoCorrecto(int codigoSolicitud); // Cuando una tarea termine satisfactoriamente
    void terminadoCorrectoConDatos(int codigoSolicitud, JSONArray datos, String tipoDatos); // Cuando una tarea termine satisfactoriamente
    void terminadoCorrectoConDatosDetalle(int codigoSolicitud, JSONArray datos, String tipoDatos); // Cuando una tarea termine satisfactoriamente
    void terminadoFallo(int codigoSolicitud); // Cuando una tarea termine con un fallo

}
