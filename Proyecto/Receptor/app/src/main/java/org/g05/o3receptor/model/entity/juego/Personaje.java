package org.g05.o3receptor.model.entity.juego;

import android.graphics.Bitmap;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Clase para representar a un perosnaje del juego, con
 * todos sus parametros, y funciones básicas
 *
 * @author Adrián García Andreu
 */
public class Personaje {

    // -----------------------------------------
    // -----------------------------------------
    // Variables
    // -----------------------------------------
    public static final int ESTADO_CANSADO = 1, ESTADO_ENFERMO = 2, ESTADO_HAMBRIENTO = 3, ESTADO_NECESIDAD_IR_AL_SERVICIO = 4; // Posibles estados físicos

    public final int ANIMO_FELIZ = 1;
    public final int ANIMO_NORMAL = 2;
    public final int ANIMO_TRISTE = 3; // Posibles estados de ánimo
    private final float pasoDelTiempo = 0.1f; // Tiempo 10 veces más lento de lo normal para el personaje

    // -----------------------------------------
    // Limites para el cambio de estado
    public final int LIMITE_HAMBRE = 75, LIMITE_CANSANCIO = 80, LIMITE_NECESIDAD_SERVICIO = 75;

    public final int LIMITE_OPTIMO_HAMBRE = 50;
    public final int LIMITE_OPTIMO_CANSANCIO = 50;
    public final int LIMITE_OPTIMO_NECESIDAD_SERVICIO = 50;



    public float FACTOR_ENVEJECIMIENTO_ORIGINAL = 1f;
    public float FACTOR_ENVEJECIMIENTO_DORMIDO = 0.01f;

    public  float FACTOR_HAMBRE_ORIGINAL = 2f;
    public  float FACTOR_NECESIDAD_ORIGINAL = 1f;
    public  float FACTOR_CANSANCIO_ORIGINAL = .5f;



    private String nombre;
    private float esperanzaDeVida; // Tiempo de vida por defecto del personaje (valor entre 0 y 100)
    private boolean dormido;

    private float hambre;
    private float cansancio;
    private float necesidadIrServicio; // Parametros de vida básicos

    private List<Integer> estadosFisicos; // Lista de estados del personaje
    private int estadoAnimo; // Estado de ánimo del personaje

    // factores de incremento en función del tiempo
    private float factorHambre;
    private float factorCansancio;
    private float factorNecesidadServicio;
    private float factorEnvejecimiento;


    // Apartado visual
    private Bitmap imagenInicial;

    //private Bitmap imagenFeliz, imagenNormal, imagenTriste;
    //private Bitmap imagenMuerto, imagenDormido;

    private int x = -1000;
    private int y = -1000; // posicion del PJ en pantalla



    private int Accion =0; // Estado de ánimo del personaje
    private int comprobacion =0;
    boolean muerto;

    private ArrayList<Bitmap> ArrayDormido = new ArrayList<Bitmap>();
    private ArrayList<Bitmap> ArrayMuerto = new ArrayList<Bitmap>();
    private ArrayList<Bitmap> ArrayNormal = new ArrayList<Bitmap>();
    private ArrayList<Bitmap> ArrayFeliz = new ArrayList<Bitmap>();
    private ArrayList<Bitmap> ArrayTriste = new ArrayList<Bitmap>();

    private ArrayList<Bitmap> ArrayComer = new ArrayList<Bitmap>();
    private ArrayList<Bitmap> ArrayServicio = new ArrayList<Bitmap>();

    private ArrayList<Bitmap> ArrayMorir = new ArrayList<Bitmap>();
    private ArrayList<Bitmap> ArrayResucitar = new ArrayList<Bitmap>();
    private ArrayList<Bitmap> ArrayAcariciar = new ArrayList<Bitmap>();


    private  ArrayList<Alimento> ArrayAlmacen = new ArrayList<Alimento>();


    // Otros parametros
    private float peso;
    private int estadoDeVida; // Edad del personaje (NIÑO, JOVEN, ADULTO, ANCIANO)


    /**
     * Constructor por defecto del personaje,
     * crea un nuevo personaje por defecto
     *
     */
    public Personaje(String nombrePorDefecto, Bitmap imagenInicial){

        // Valores principales por defecto
        darParametrosVitalesPorDefecto();

        // Se inicializan los factores de incremento de los parametros vitales
        darFactoresDeIncrementoPorDefecto();

        // Valores secundarios por defecto (En caso de ser nulos, coger de constantes)
        this.nombre = nombrePorDefecto;
        this.imagenInicial = imagenInicial;


    }


    /**
     *  Función para dar parametros vitales por defecto
     *  al crear un personaje
     *
     */
    public void darParametrosVitalesPorDefecto(){

        this.esperanzaDeVida = 100;

        this.hambre = 50;
        this.cansancio = 50;
        this.necesidadIrServicio = 50;

        this.estadosFisicos = new ArrayList<>(); // Ningún estado alterado por defecto
        this.estadoAnimo = ANIMO_FELIZ; // Ánimo por defecto, FELIZ

        this.Accion = 0;

        this.dormido = false;

        this.muerto=false;
    }


    /**
     * Función para dar valores por defecto a los incrementos
     * de las funciones vitales
     *
     */
    public void darFactoresDeIncrementoPorDefecto(){

        this.factorHambre = FACTOR_HAMBRE_ORIGINAL;
        this.factorNecesidadServicio = FACTOR_NECESIDAD_ORIGINAL;
        this.factorCansancio = FACTOR_CANSANCIO_ORIGINAL;


        this.factorEnvejecimiento = FACTOR_ENVEJECIMIENTO_ORIGINAL;

    }



    /**

     * Función para dar valores a los incrementos
     * de las funciones vitales cuando el PJ esta
     * dormido
     *
     */
    public void darFactoresDeIncrementoPersonajeDormido(){

        this.factorHambre = .02f;
        this.factorNecesidadServicio = .01f;
        this.factorCansancio = 10f;

        this.factorEnvejecimiento = 0.01f;

    }



    /**
     * Funcion para saber si el personaje sigue vivo
     * @return vivo o no
     */
    public boolean estoyVivo(){
        boolean vivo = true;
            if (this.esperanzaDeVida<1){ vivo = false; }
        return vivo;
    }


    /**
     * Función para saber que estados alterados (físicos)
     * padece el personaje
     *
     * @return listado de estados alterados activos
     */
    public List<Integer> comoMeEncuentro(){
        return getEstadosFisicos();
    }

    /**
     * Funcion para saber como se muestra el personaje en función
     * de su estado de ánimo
     *
     * @return estado de ánimo
     */
    public int comoMeMuestro(){
        return getEstadoAnimo();
    }

    public int queHago(){
        return getAccion();
    }

    // -----------------------------------------
    // Incrementar o Decrementar Parametros
    // -----------------------------------------


    // FUNCIONES DE INCREMENTO

    public void darEsperanzaDeVida(float valor){
        this.esperanzaDeVida = Math.min(100, this.esperanzaDeVida+valor);
    }
    public void darHambre(float valor){
        this.hambre = Math.min(100, this.hambre+valor);
    }
    public void darCansancio(float valor){
        this.cansancio = Math.min(100, this.cansancio+valor);
    }
    public void darNecesidadIrAlServicio(float valor){
        this.necesidadIrServicio = Math.min(100, this.necesidadIrServicio+valor);
    }


    // FUNCIONES DE DECREMENTO

    public void restarEsperanzaDeVida(float valor){
        this.esperanzaDeVida = Math.max(0, this.esperanzaDeVida-valor);
    }
    public void restarHambre(float valor){
        this.hambre = Math.max(0, this.hambre-valor);
    }
    public void restarCansancio(float valor){
        this.cansancio = Math.max(0, this.cansancio-valor);
    }
    public void restarNecesidadIrAlServicio(float valor){
        this.necesidadIrServicio = Math.max(0, this.necesidadIrServicio-valor);
    }





    // -----------------------------------------
    // Getters & Setters
    // -----------------------------------------

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Bitmap getImagenInicial() {
        return imagenInicial;
    }

    public void setImagenInicial(Bitmap imagenInicial) {
        this.imagenInicial = imagenInicial;
    }

    public List<Integer> getEstadosFisicos() {
        return estadosFisicos;
    }

    public void setEstadosFisicos(List<Integer> estadosFisicos) {
        this.estadosFisicos = estadosFisicos;
    }

    public int getEstadoAnimo() {
        return estadoAnimo;
    }

    public void setEstadoAnimo(int estadoAnimo) {
        this.estadoAnimo = estadoAnimo;
    }

    public float getFactorHambre() {
        return factorHambre;
    }

    //experimental
    public int getAccion() {
        return Accion;
    }

    public void setAccion(int accion) {
        this.Accion = accion;
    }


    public void setFactorHambre(float factorHambre) {
        this.factorHambre = factorHambre;
    }

    public float getFactorCansancio() {
        return factorCansancio;
    }

    public void setFactorCansancio(float factorCansancio) {
        this.factorCansancio = factorCansancio;
    }

    public float getFactorNecesidadServicio() {
        return factorNecesidadServicio;
    }

    public void setFactorNecesidadServicio(float factorNecesidadServicio) {
        this.factorNecesidadServicio = factorNecesidadServicio;
    }

    public float getFactorEnvejecimiento() {
        return factorEnvejecimiento;
    }

    public void setFactorEnvejecimiento(float factorEnvejecimiento) {
        this.factorEnvejecimiento = factorEnvejecimiento;
    }


    public float getPasoDelTiempo() {
        return pasoDelTiempo;
    }

    public float getEsperanzaDeVida() {
        return esperanzaDeVida;
    }

    public void setEsperanzaDeVida(float esperanzaDeVida) {
        this.esperanzaDeVida = esperanzaDeVida;
    }

    public float getHambre() {
        return hambre;
    }

    public void setHambre(float hambre) {
        this.hambre = hambre;
    }

    public float getCansancio() {
        return cansancio;
    }

    public void setCansancio(float cansancio) {
        this.cansancio = cansancio;
    }

    public float getNecesidadIrServicio() {
        return necesidadIrServicio;
    }

    public void setNecesidadIrServicio(float necesidadIrServicio) {
        this.necesidadIrServicio = necesidadIrServicio;
    }

    public float getPeso() {
        return peso;
    }

    public int getEstadoDeVida() {
        return estadoDeVida;
    }

    public boolean isDormido() {
        return dormido;
    }

    public void setDormido(boolean dormido) {
        this.dormido = dormido;
    }
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }


    public void setY(int y) {
        this.y = y;
    }


    public void setX(int x) {
        this.x = x;
    }




    public void checklock() {
        if (this.Accion>=1){this.comprobacion = this.comprobacion-1;}
        if (this.comprobacion<=0){this.Accion=0;}
    }
    public void setLock (int e) {
        this.comprobacion = e;
    }



    public  ArrayList<Alimento> getbolsillo() {
        return ArrayAlmacen;
    }
    public void setbolsillo(Alimento alimento) {
        this.ArrayAlmacen.add(alimento);
    }

    public void vaciarbolsillo(int posicion) {
        if (posicion>ArrayAlmacen.size()){
            this.ArrayAlmacen.remove(posicion);
        }

    }



    public  ArrayList<Bitmap> getImagenFeliz() {
        return ArrayFeliz;
    }
    public void setImagenFeliz(Bitmap imagenFeliz) {
        this.ArrayFeliz.add(imagenFeliz);
    }
    public  ArrayList<Bitmap> getImagenNormal() {
        return ArrayNormal;
    }
    public void setImagenNormal(Bitmap imagenNormal) {
        this.ArrayNormal.add(imagenNormal);
    }
    public  ArrayList<Bitmap> getImagenTriste() {
        return ArrayTriste;
    }
    public void setImagenTriste(Bitmap imagenTriste) {
        this.ArrayTriste.add(imagenTriste);
    }
    public  ArrayList<Bitmap> getImagenMuerto() {
        return ArrayMuerto;
    }
    public void setImagenMuerto(Bitmap imagenMuerto) {
        this.ArrayMuerto.add(imagenMuerto);
    }
    public ArrayList<Bitmap> getImagenDormido() {
        return ArrayDormido;
    }
    public void setImagenDormido(Bitmap imagenDormido) {
        this.ArrayDormido.add(imagenDormido);
    }

    public ArrayList<Bitmap> getImagenComer() {
        return ArrayComer;
    }
    public void setImagenComer(Bitmap imagenComer) {
        this.ArrayComer.add(imagenComer);
    }
    public ArrayList<Bitmap> getImagenServicio() {
        return ArrayServicio;
    }
    public void setImagenServicio(Bitmap imagenServicio) {
        this.ArrayServicio.add(imagenServicio);
    }

    public ArrayList<Bitmap> getImagenMorir() {
        return ArrayMorir;
    }
    public void setImagenMorir(Bitmap imagenMorir) {
        this.ArrayMorir.add(imagenMorir);
    }
    public ArrayList<Bitmap> getImagenResucitar() {
        return ArrayResucitar;
    }
    public void setImagenResucitar(Bitmap imagenResucitar) {
        this.ArrayResucitar.add(imagenResucitar);
    }
    public ArrayList<Bitmap> getImagenAcariciar() {
        return ArrayAcariciar;
    }
    public void setImagenAcariciar(Bitmap imagenAcariciar) {
        this.ArrayAcariciar.add(imagenAcariciar);
    }

    public  boolean getmuerto() {
        return muerto;
    }
    public void setmuerto (boolean e ) {
        this.muerto=e;
    }




}
