package org.g05.o3receptor.view.util;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

public class ServicioSegundoPlano extends Service {
    private final String ETIQUETA_LOG = "SERVICIO SEGUNDO PLANO";

    private ServicioBTLE servicioBTLE;

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Cuando hacemos el onStartComand:
     * 1. creamos un intent
     * 2. iniciamos un servicio
     * 3. creamos una referencia binded, para poder acceder a las funciones públicas del servicio
     * --------------------------------------------------------------
     * intent:Intent, flags:N, startId:N --> onStartCommand() -->N
     * --------------------------------------------------------------
     * @param intent
     * @param flags
     * @param startId
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        Log.d(ETIQUETA_LOG,"onStartCommand()");

        // Con un intent iniciamos el servicio y luego lo vinculamos con el bindService, esto es muy importante, ya que sino no podemos acceder a funciones públicas del otro servicio
        Intent intentBuscarDispositivosBTLE = new Intent(getApplicationContext(), ServicioBTLE.class);
        startService(intentBuscarDispositivosBTLE);
        bindService(intentBuscarDispositivosBTLE, conexion, Context.BIND_AUTO_CREATE);

        // El START_STICKY es importante para que cuando se cierre la app, siga ejecutandose el servicio
        return START_STICKY;
    }

    // *************************************************************************************************************************************************************************** //
    // ******************************************************************** CONEXION  ******************************************************************************************** //
    private ServiceConnection conexion = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // Creamos un servicio Local, casteando el IBinder y obteniendo la instancia del servicio. Esto es necesario para poder acceder a sus métodos públicos
            ServicioBTLE.ServicioSegundoPlanoBinder binder = (ServicioBTLE.ServicioSegundoPlanoBinder) service;
            servicioBTLE = binder.getService();
            // Guardamos la referencia para poder acceder desde EnvioMedidas y ProcesadorMediciones a las funciones
            servicioBTLE.guardarServicioSegundoPlano(servicioBTLE);
            // Iniciamos la busqueda de beacons
            servicioBTLE.iniciarEscaneoSegundoPlano();

        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            Log.d(ETIQUETA_LOG, "onServiceDisconnected");
        }
    };


    // *************************************************************************************************************************************************************************** //
    // ******************************************************************* CICLO VIDA ACTIVIDAD ********************************************************************************** //
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(ETIQUETA_LOG, "onBind()");
        return null;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        Log.d(ETIQUETA_LOG, "Servicio inicializado");
    }

    @Override
    public void onDestroy(){
        unbindService(conexion);
        Log.d(ETIQUETA_LOG, "Servicio destruido");
        super.onDestroy();
    }
}
