package org.g05.o3receptor.view.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;

import java.util.Locale;

public class CambiarIdioma {
    private static SharedPreferences languagePreferences;

    public static void putSelectedLanguage(Context context){
        String language = getPreferencesLanguage(context);
        setAppLocale(language, context);
    }

    public static void setAppLocale(String localeCode, Context context){
        Resources res =  context.getResources();
        DisplayMetrics ds = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            conf.setLocale(new Locale(localeCode.toLowerCase()));
        }else{
            conf.locale = new Locale(localeCode.toLowerCase());
        }
        res.updateConfiguration(conf,ds);
    }

    public static String getPreferencesLanguage(Context context){
        String languagePredetermined = Locale.getDefault().getDisplayLanguage();
        languagePreferences = context.getSharedPreferences(Constantes.PREF_IDIOMA_APP, Context.MODE_PRIVATE);
        String language = languagePreferences.getString(Constantes.PREF_IDIOMA, languagePredetermined);
        Log.d("idioma",language);
        return language;
    }
}
