package org.g05.o3receptor.model.entity;


import android.location.Location;

/**
 * POJO que contiene una medida capturada de un beacon BLE, de un tipo determinado
 * en unas coordenadas geograficas
 * @author Adrián García Andreu
 * @version 1.1
 */
public class Medida {


    private String tipo; // tipo de la medida
    private int medida; // valor de la medida del sensor
    private float medidaNormalizada; // valor de la medida del sensor
    private Location ubicacion; // Ubicacion donde se tomo la medida
    private String correo; // Usuario que envia la medida


    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Constructor de clase
     * --------------------------------------------------------------
     * @param tipo Tipo de la medida
     * @param medida Valor de la medida
     * @param ubicacion Ubicacion de la medida
     */
    public Medida(String tipo, int medida, Location ubicacion, String correo){
        this.tipo = tipo;
        this.medida = medida;
        this.ubicacion = ubicacion;
        this.correo = correo;
    } // ()

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Constructor de clase
     * --------------------------------------------------------------
     * @param medidaNormalizada Valor de la medida
     * @param ubicacion Ubicacion de la medida
     */
    public Medida(Location ubicacion, float medidaNormalizada){
        this.medidaNormalizada = medidaNormalizada;
        this.ubicacion = ubicacion;
    } // ()



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * getTipo() --> Texto
     * --------------------------------------------------------------
     * @return Texto con el tipo de la medida
     */
    public String getTipo() {
        return tipo;
    } // ()



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * getMedida() --> Z
     * --------------------------------------------------------------
     * @return Valor de la medida tomada
     */
    public int getMedida() {
        return medida;
    } // ()

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * getMedidaNormalizada() --> Z
     * --------------------------------------------------------------
     * @return Valor de la medida tomada
     */
    public float getMedidaNormalizada() {
        return medidaNormalizada;
    } // ()


    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * getUbicacion() --> Location
     * --------------------------------------------------------------
     * @return Ubicacion en la que se tomo la medida
     */
    public Location getUbicacion() {
        return ubicacion;
    } // ()


    public String getCorreo() {
        return correo;
    }
}
