package org.g05.o3receptor.model.repository.remote;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class NodoAsociado_get extends AsyncTask<Void, Void, String> {

    private final int CODIGO_PETICION = Constantes.PETICION_NODO_ASOCIADO;

    private final String recursoREST = "nodoAsociado/";
    private String parametros;

    private int idNodo;
    private TareaCallback callback;


    /**
     * Constructor de clase
     * @param idNodo nodo a comprobar
     */
    public NodoAsociado_get(int idNodo, TareaCallback callback){
        this.idNodo = idNodo;
        this.callback = callback;
    }




    @Override
    protected String doInBackground(Void... voids) {

        String retorno = null;
        this.parametros = "idNodo="+idNodo; //Solo un parametro

        URL url;
        HttpURLConnection connection = null;

        try {
            // ---------------------------------------
            // Peticion GET datos en la URL

            url = new URL(Constantes.API_URI + recursoREST + "?" + parametros);

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "*/*");


            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }


            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.d("Inicio sesion", "Response: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }


        return retorno;

    }


    @Override
    protected void onPostExecute(String resultado) {
        super.onPostExecute(resultado);

        boolean nodoAsociado = false;
        JSONObject resultadoJSON = null;

        try {
            resultadoJSON = new JSONObject(resultado);

            //Si el usuario se ha encontrado, las credenciales son correctas
            if(resultadoJSON.getInt("datos")!=-1){
                int datos = resultadoJSON.getInt("datos");
                if(datos==1){ nodoAsociado = false;} // nodo ya asociado
                else if(datos==0){nodoAsociado = true;} // nodo sin asociar a nadie
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Si el resultado es el esperado...
        if(nodoAsociado){
            callback.terminadoCorrecto(CODIGO_PETICION);
        } else{
            callback.terminadoFallo(CODIGO_PETICION);
        }


    }





}
