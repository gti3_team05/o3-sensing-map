package org.g05.o3receptor.model.repository.remote;

import android.os.AsyncTask;
import android.util.Log;

import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.ServicioBTLE;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Crea una estacion de medida en el servidor REST
 * @author Carlos Ibanez Lamas
 */
public class EstacionesMedidas_post extends AsyncTask<Void, Void, String> {

    private final int CODIGO_PETICION = Constantes.PETICION_REGISTRO_ESTACION;
    private final String recursoREST = "estacionesMedidas/";
    private String parametros;

    private double latitud;
    private double longitud;
    private String nombre;
    private ServicioBTLE servicio;

    /**
     * Constructor de clase
     * @param latitud   Latitud de la estacion
     * @param longitud  Longitud de la estacion
     * @param nombre    Nombre de la estacion
     */
    public EstacionesMedidas_post (ServicioBTLE servicio, double latitud, double longitud, String nombre ){
        this.servicio = servicio;
        this.latitud = latitud;
        this.longitud = longitud;
        this.nombre = nombre;
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion para obtener de un usuario los parametros necesarios para
     * realizar la peticion REST
     * --------------------------------------------------------------
     *  Usuario --> obtenerParametros() --> String
     * --------------------------------------------------------------
     * @param latitud   Latitud de la estacion
     * @param longitud  Longitud de la estacion
     * @param nombre    Nombre de la estacion
     * @return          Retorna un Texto con los parametros de la peticion formateados
     */
    private String obtenerParametros(double latitud, double longitud, String nombre){

        //Se añaden los parametros del request a la API REST como clave Valor
        HashMap<String, Object> parametros = new HashMap<>();
        // Campos obligatorios
        parametros.put("lat", latitud);
        parametros.put("lng", longitud);
        parametros.put("nombre", nombre);

        // Se recorre el objeto clave valor y se insertan los valores en un Texto
        Iterator it = parametros.entrySet().iterator();
        boolean primero = true;
        String parametrosString = "";
        while (it.hasNext()){
            if(primero){
                primero = false;
            } else{
                parametrosString +="&"; //Se separan los datos con el caracter '&'
            }

            Map.Entry pair = (Map.Entry)it.next();
            parametrosString +=pair.getKey()+"="+pair.getValue(); //A cada dato se le asigna el valor tras el caracter '='
        }

        //Se devuelven los datos con sus valores como Texto
        return parametrosString;

    }

    @Override
    protected String doInBackground(Void... voids) {
        String retorno = null;
        URL url;
        HttpURLConnection connection = null;

        this.parametros = obtenerParametros(latitud, longitud, nombre);

        try {
            url = new URL(Constantes.API_URI + recursoREST );

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Accept", "*/*");

            // Escribe los datos a enviar en la peticion REST de tipo POST
            connection.setDoOutput(true);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
            writer.write(parametros);
            writer.close();

            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }

            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }

            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.d("Posteo estacion", "Response: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la solicitud de subida");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }

        return retorno;

    }

}
