package org.g05.o3receptor.model.repository.remote;

import android.os.AsyncTask;
import android.util.Log;

import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Crea un usuario en el servidor REST
 * @author Adrián García Andreu
 */
public class Usuarios_update extends AsyncTask<Void, Void, String> {

    private final int CODIGO_PETICION = Constantes.PETICION_ACTUALIZAR_PERFIL;
    private final String ETIQUETA_LOG = "Usuarios_update";
    private final String recursoREST = "usuarios/";
    private String parametros;

    private TareaCallback callback;
    private Usuario usuario;


    /**
     * Constructor de clase
     * @param usuario usuario a crear
     * @param callback callback de la tarea
     */
    public Usuarios_update(Usuario usuario, TareaCallback callback ){
        this.usuario = usuario;
        this.callback =  callback;
    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion para obtener de un usuario los parametros necesarios para
     * realizar la peticion REST
     * --------------------------------------------------------------
     *  Usuario --> obtenerParametros() --> String
     * --------------------------------------------------------------
     * @param u Usuario del que obtener los parametros de la peticion
     * @return retorna un Texto con los parametros de la peticion formateados
     */
    private String obtenerParametros(Usuario u){

        //Se añaden los parametros del request a la API REST como clave Valor
        HashMap<String, Object> parametros = new HashMap<>();
        // Campos obligatorios
        parametros.put("nombre", u.getNombre());
        parametros.put("apellidos", u.getApellidos());
        parametros.put("telefono", u.getTelefono());
        parametros.put("correo", u.getCorreo());

        // Se recorre el objeto clave valor y se insertan los valores en un Texto
        Iterator it = parametros.entrySet().iterator();
        boolean primero = true;
        String parametrosString = "";
        while (it.hasNext()){
            if(primero){
                primero = false;
            } else{
                parametrosString +="&"; //Se separan los datos con el caracter '&'
            }

            Map.Entry pair = (Map.Entry)it.next();
            parametrosString +=pair.getKey()+"="+pair.getValue(); //A cada dato se le asigna el valor tras el caracter '='
        }

        //Se devuelven los datos con sus valores como Texto
        return parametrosString;

    }





    @Override
    protected String doInBackground(Void... voids) {

        String retorno = null;
        this.parametros = obtenerParametros(usuario);

        URL url;
        HttpURLConnection connection = null;

        // ---------------------------------------
        // Peticion POST datos en la URL para crear el usuario
        try {
            url = new URL(Constantes.API_URI + recursoREST +"?"+parametros);

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion
            connection.setDoOutput(true);
            connection.setRequestMethod("PUT");
            connection.setRequestProperty("Content-Type", "text/plain");
            connection.setRequestProperty("Accept", "*/*");

            // Escribte los datos a enviar en la peticion REST de tipo PUT
            connection.setDoOutput(true);

            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }


            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.d("Inicio sesion", "Response: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }



        return retorno;
    }



    @Override
    protected void onPostExecute(String resultado) {
        super.onPostExecute(resultado);

        boolean todoCorrecto = false;
        JSONObject resultadoJSON = null;

        try {
            resultadoJSON = new JSONObject(resultado);

            Log.d(ETIQUETA_LOG, resultadoJSON.toString());

            // Si el usuario ha podido actualizarse
            todoCorrecto = resultadoJSON.getBoolean("datos");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Si el resultado es el esperado...
        if(todoCorrecto){
            callback.terminadoCorrecto(CODIGO_PETICION);
        } else{
            callback.terminadoFallo(CODIGO_PETICION);
        }

    }
}
