package org.g05.o3receptor.view.util;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import org.g05.o3receptor.R;

import static android.content.Context.NOTIFICATION_SERVICE;

public class Notificaciones {
    private Context contexto;
    private NotificationManager notificationManager;

    public Notificaciones(Context contexto){
        this.contexto = contexto;
    }

    public void crearNotificacion(String titulo, String descripcion, int imagen) {
        // Notificación necesaria para lanzar un serivio en primer plano
        final String CANAL_ID = "mi_canal";
        final int NOTIFICACION_ID = 1;
        notificationManager = (NotificationManager) contexto.getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CANAL_ID, "Mis Notificaciones", NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("Descripcion del canal");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(R.color.colorPrimary);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificacion = new NotificationCompat.Builder(contexto, CANAL_ID)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(titulo)
                .setContentText(descripcion)
                .setSmallIcon(imagen)
                .setLights(ContextCompat.getColor(contexto, R.color.colorPrimary), 1000, 1000);
        // Esta linea de abajo la dejas si quieres que sea una simple noficicación; si quieres un servicio y no una notificación (la diferencia es que uno se puede quitar y el otro no) dejas lo de startForeground
        notificationManager.notify(NOTIFICACION_ID, notificacion.build());
    }

    public void crearNotificacionCliclable(String titulo, String descripcion, int imagen, Class claseActividad) {
        // Notificación necesaria para lanzar un serivio en primer plano
        final String CANAL_ID = "mi_canal2";
        final int NOTIFICACION_ID = 2;
        notificationManager = (NotificationManager) contexto.getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CANAL_ID, "Mis Notificaciones", NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("Descripcion del canal");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(R.color.colorPrimary);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder notificacion = new NotificationCompat.Builder(contexto, CANAL_ID)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(titulo)
                .setContentText(descripcion)
                .setSmallIcon(imagen)
                .setAutoCancel(true)
                .setLights(ContextCompat.getColor(contexto, R.color.colorPrimary), 1000, 1000);
        PendingIntent intencionPendiente = PendingIntent.getActivity(contexto, 0, new Intent(contexto, claseActividad), 0);
        notificacion.setContentIntent(intencionPendiente);
        // Esta linea de abajo la dejas si quieres que sea una simple noficicación; si quieres un servicio y no una notificación (la diferencia es que uno se puede quitar y el otro no) dejas lo de startForeground
        notificationManager.notify(NOTIFICACION_ID, notificacion.build());
    }

    public void eliminarNotificacion(int notificacionID){
        if ( notificacionID != 0 ){
            notificationManager.cancel(notificacionID) ;
        }
    }
}
