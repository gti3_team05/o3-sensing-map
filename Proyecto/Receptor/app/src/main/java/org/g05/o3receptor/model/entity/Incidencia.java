package org.g05.o3receptor.model.entity;

/**
 * POJO que contiene una incidencia en la app o sensor
 * @author Laura Pons Castelló
 * @version 1.1
 */
public class  Incidencia {

    private String correo; // Usuario que tiene la incidencia
    private String tipoIncidencia; // Tipo de incidencia que se ha producido
    private String estado; // Estado de la incidencia ( al ser creada este es pendiente)



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Constructor de clase
     * --------------------------------------------------------------
     * @param correo Usuario que sufre la incidencia
     * @param tipoIncidencia Tipo de incidencia
     */
    public Incidencia(String correo, String tipoIncidencia){
        this.correo = correo;
        this.tipoIncidencia = tipoIncidencia;
        this.estado = "pendiente";
    } // ()



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * getTipoIncidencia() --> Texto
     * --------------------------------------------------------------
     * @return Texto con el tipo de medida
     */
    public String getTipoIncidencia() {
        return tipoIncidencia;
    } // ()


    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * getEstadoIncidencia() --> Texto
     * --------------------------------------------------------------
     * @return Texto con el estado de la incidencia
     */
    public String getEstadoIncidencia() {
        return estado;
    } // ()


    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * getCorreoIncidencia() --> Texto
     * --------------------------------------------------------------
     * @return Correo de la persona que tiene la incidencia
     */
    public String getCorreoIncidencia() {
        return correo;
    }

}
