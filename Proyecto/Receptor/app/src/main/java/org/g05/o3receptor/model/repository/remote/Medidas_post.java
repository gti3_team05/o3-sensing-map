package org.g05.o3receptor.model.repository.remote;


import android.os.AsyncTask;
import android.util.Log;

import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.ServicioBTLE;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Clase asincrona para enviar una medida a un servidor mediante una API de tipo REST
 * @author Adrián García Andreu
 * @version 1.1
 */
public class Medidas_post extends AsyncTask<String, Void, String> {
    private String ETIQUETA_LOG = "ENVIO MEDIDAS";

    private ServicioBTLE servicioBTLE;
    private final String recursoREST = "medidas/";

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Constructor de la clase
     * --------------------------------------------------------------
     * EnvioMedidas()
     * --------------------------------------------------------------
     */
    public Medidas_post(ServicioBTLE servicioBTLE){
        this.servicioBTLE = servicioBTLE;
    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Muestra un dialogo en pantalla para impedir la interaccion del usuario
     * mientras se están realizando peticiones al servidor
     * --------------------------------------------------------------
     * onPreExecute()
     * --------------------------------------------------------------
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Log.d("Peticion REST", "Enviando beacon al servidor REST");

    }


    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * datos:Texto --> doInBackground()
     * --------------------------------------------------------------
     * @param datos contiene en la posicion 0 la URL del servidor REST
     *              y en la posicion 1 los parametros y valores
     *              a procesar por la peticion
     */
    @Override
    protected String doInBackground(String... datos) {

        String retorno = null;
        Log.d(ETIQUETA_LOG,"RESP INICIO");


        URL url;
        String parametros;
        HttpURLConnection connection = null;

        try {
            parametros = datos[1];
            url = new URL(Constantes.API_URI + recursoREST );

            Log.e("ENVIO", url.toString());
            Log.e("ENVIO", parametros);

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("Accept", "*/*");

            // Escribte los datos a enviar en la peticion REST de tipo POST
            connection.setDoOutput(true);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
            writer.write(parametros);
            writer.close();


            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }


            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.d(ETIQUETA_LOG, "Response: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }


    return retorno;
    }



    /** --------------------------------------------------------------
     * --------------------------------------------------------------
     * Cierra el dialogo que se muestra en pantalla
     * para devolver la visibilidad y el control de
     * interaccion al usuario
     * --------------------------------------------------------------
     * onPostExecute()
     * --------------------------------------------------------------
     */
    @Override
    protected void onPostExecute(String resultado) {

        super.onPostExecute(resultado);

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        // Se procesa el resultado

        boolean todoCorrecto = false;

        // Si el servidor devuelve un verdadero, se entiende que las operaciones han ido como se esperaba
        try {
            JSONObject resultadoJSON = new JSONObject(resultado);
            todoCorrecto = resultadoJSON.getBoolean("datos");

           // Log.d("INFO API", "JSON: "+resultadoJSON.toString());
           // Log.d("INFO API", "JSON: "+todoCorrecto);
            if(todoCorrecto){
                Log.d(ETIQUETA_LOG, "Beacon enviado al servidor");
            }else{
                Log.d(ETIQUETA_LOG, "Beacon no enviado, fallo en el servidor!");
            }

        } catch (JSONException ex){
            Log.e(ETIQUETA_LOG, "Error Api --> No se ha podido procesar el resultado como JSON --> resultado: "+resultado);
        } // Fin del procesado de la respuesta del servidor

        this.servicioBTLE.iniciarEscaneoSegundoPlano();
    }

}


