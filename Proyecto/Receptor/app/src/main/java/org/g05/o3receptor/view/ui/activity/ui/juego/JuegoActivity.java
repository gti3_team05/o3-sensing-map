package org.g05.o3receptor.view.ui.activity.ui.juego;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.g05.o3receptor.R;
import org.g05.o3receptor.model.entity.juego.Alimento;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.juego.JuegoView;
import org.g05.o3receptor.model.entity.juego.Personaje;

import java.util.ArrayList;

import java.util.List;

/**
 * Actividad que lanza el modo juego
 *
 * @author Adrián García
 */
public class JuegoActivity extends AppCompatActivity implements View.OnTouchListener {

    private JuegoView juegoView; // Vista de jeugo
    private View juegoIndicadores; // View con los indicadores de juego
    private FrameLayout framePrincipal; // Layout principal que contiene la vista de juego, y el resto de componentes

    private Handler  manejadorEstadoDeJuego;
    private Runnable hiloEstadoDeJuego;

    // Vistas con controles de juego
    private View vistaConBotonesDeJuego;
    private View vistaConBotonesTrasMorir;
    private View vistaConBotonesDormido;
    private View vistaComida;

    //especificos comida
    ArrayList<ComidaItem> listaComida;
    RecyclerView mRecyclerView;
    ComidaAdapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    // Controles y vistas para dar de comer a la mascota
    private ViewGroup marco;
    private ImageView imagen;
    private int xDelta;
    private int yDelta;
    private int ultimoAlimentoVisualizado = 0;
    private List<Alimento> listaAlimentos;
    private boolean dentroSeccionComida = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        inicializarVistaJuego();
        mostrarControlesPrincipales(); // Se incorporan por defecto los controles principales

        // Hilo para comprobar el estado del juego desde la actividad y poder modificar
        // elementos del sistema (controles o flujo de la aplicación completa)
        this.manejadorEstadoDeJuego = new Handler();
        inicializarHiloEstadoDeJuego();
    }
    /**
     * Carga la vista de juego por defecto
     */
    private void inicializarVistaJuego(){

        //Maximizar pantalla
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Point punto = new Point();
        getWindowManager().getDefaultDisplay().getSize(punto);

        // Layout base, actuara como holder de la vista de juego
        this.framePrincipal = new FrameLayout(this);

        this.juegoView = new JuegoView(this, punto.x, punto.y); // Creacion de la vista de juegp
        this.framePrincipal.addView(this.juegoView); //Se añade al layout primario, la vista de juego

        // Indicadores de juego
        LayoutInflater li = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.juegoIndicadores = li.inflate(R.layout.juego_indicadores, null);
        this.framePrincipal.addView(this.juegoIndicadores);
        mostrarIndicadoresDeJuego();

        setContentView(this.framePrincipal); // Se establece como layout principal de la actividad el frame principal

    }

    /**
     * Actualiza los indicadores del personaje
     */
    private void actualizarIndicadoresEnPantalla(){

        long tiempoActual = System.currentTimeMillis();


        Personaje personaje = juegoView.getJuegoMotor().getPersonaje();
        TextView nombre = juegoIndicadores.findViewById(R.id.juego_tv_nombre);
        ProgressBar progressBarHambre= (ProgressBar)juegoIndicadores.findViewById(R.id.pb_hambre);
        ProgressBar progressBarCansancio= (ProgressBar)juegoIndicadores.findViewById(R.id.pb_cansancio);
        ProgressBar progressBarNecesidad= (ProgressBar)juegoIndicadores.findViewById(R.id.pb_necesidad);
        //indicadorNecesidad
        progressBarHambre.setProgress(Math.round(personaje.getHambre()));
        progressBarCansancio.setProgress(Math.round(personaje.getCansancio()));
        progressBarNecesidad.setProgress(Math.round(personaje.getNecesidadIrServicio()));

        //nombre mascota

        nombre.setText("" + personaje.getNombre());

        // Actualizar cantidad minerales
        SharedPreferences preferencias = this.getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO,MODE_PRIVATE);
        int minerales = preferencias.getInt(Constantes.PREF_MINERALES, 30);

        //Log.d("PJ", "Minerales: "+minerales);
        this.juegoView.getJuegoMotor().setCantidadMinerales(minerales);

        //mostrar minerales
        TextView contadorMinerales = juegoIndicadores.findViewById(R.id.juego_tv_minerales);
        contadorMinerales.setText(" "+minerales);

        //conveertir el nombre en un modificable
        nombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialog();
            }
        });
    }

    //funcion para cambiar el nombre a la mascota
    //una vez realizado, llama a la BD
    private void showAlertDialog() {
        final EditText nombreEditText = new EditText(this);
        new AlertDialog.Builder(this)
                .setTitle("Elige un nuevo nombre")
                .setView(nombreEditText)
                .setPositiveButton("Cambiar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO,MODE_PRIVATE);
                        juegoView.getJuegoMotor().cambiarnombre(nombreEditText.getText().toString());
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }



    /**
     * Comprueba si el personaje esta vivo, en caso de no estarlo,
     * oculta los controles basicos
     */
    private void cuandoMuereElPersonaje(){
        // Si el personaje esta muerto, reemplaza la comandera de botones
        Personaje pj = juegoView.getJuegoMotor().getPersonaje();
        if(!pj.estoyVivo()){

            quitarControlesDeJuegoPrincipales(); // retira los botones
            quitarControlesDePersonajeDormido();
            quitarControlesComida();
            ocultarIndicadoresDeJuego(); // retira los indicadores

            mostrarControlesDePersonajeMuerto(); // establece la vista de botones al haber muerto
        }
        // Si estta vivo, no se hace nada

    }

    /**
     * Funcion que crea un hilo para la comprobación del estado de juego
     * desde la Actividad, para poder controlar a este desde la app
     *
     */
    private void inicializarHiloEstadoDeJuego(){

        //Funcion del hilo
        this.hiloEstadoDeJuego = new Runnable() {
            @Override
            public void run() {

                // Comprueba el estado del personaje
                cuandoMuereElPersonaje();

                // Gestiona los indicadores
                actualizarIndicadoresEnPantalla();

                manejadorEstadoDeJuego.postDelayed(hiloEstadoDeJuego, 1000); // Cada segundo
            }
        };

        this.manejadorEstadoDeJuego.post(this.hiloEstadoDeJuego); // Lanza el hilo de forma inmediata

    }//()

    // ----------------------------------
    // Control del layout de controles

    /**
     * Funcion para agregar los controles principales
     * de juego en pantalla (cuando el pj esta vivo)
     */



    private void mostrarControlesPrincipales(){

        quitarControlesDePersonajeDormido();
        quitarControlesDePersonajeMuerto();

        // Se infla como vista el layout que coniene los elementos de interacción del juego
        LayoutInflater li = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.vistaConBotonesDeJuego = li.inflate(R.layout.juego_botones, null); // Obtiene la vista del layout inflado
        this.framePrincipal.addView(vistaConBotonesDeJuego); // Se añade la botonera de juego


        // En caso de que este el modo testeo, se activan botones para hacer tests
        SharedPreferences preferencias = this.getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        boolean testeo = preferencias.getBoolean(Constantes.PREF_MODO_TESTER, false);

        if(testeo){
            vistaConBotonesDeJuego.findViewById(R.id.juego_layout_testeo).setVisibility(View.VISIBLE);
        }

        // Instancia Listeners
        instanciarListenersControlesPrincipales();
    }





    /**
     * Funcion para agregar los controles cuando el personaje
     * ha muerto
     */
    private void mostrarControlesDePersonajeMuerto(){
        Personaje pj = juegoView.getJuegoMotor().getPersonaje();
        LayoutInflater li = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(this.vistaConBotonesTrasMorir == null){
            this.vistaConBotonesTrasMorir = li.inflate(R.layout.juego_botones_muerte, null); // Obtiene la vista del layout inflado
            this.framePrincipal.addView(vistaConBotonesTrasMorir); // Se añade la botonera de juego
            // Instancia Listeners
            instanciarListenersControlesMuerte();
            if(pj.getmuerto()== false){
                pj.setLock(pj.getImagenMorir().size());
                pj.setAccion(5);
                pj.setmuerto(true);
            }
        }
    }


    /**
     * Funcion para agregar los controles cuando el personaje
     * se ha dormido
     */
    private void mostrarControlesDePersonajeDormido(){

        LayoutInflater li = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.vistaConBotonesDormido = li.inflate(R.layout.juego_botones_dormido, null); // Obtiene la vista del layout inflado
        this.framePrincipal.addView(vistaConBotonesDormido); // Se añade la botonera de juego

        // Instancia Listeners
        instanciarListenersControlesDormido();


    }

    /**
     * Funcion para elegir la comida
     */
    private void mostrarControlesComida(){
        //ocultarIndicadoresDeJuego();
        LayoutInflater li = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.vistaComida = li.inflate(R.layout.juego_comida_activity, null); // Obtiene la vista del layout inflado
        this.framePrincipal.addView(vistaComida); // Se añade la botonera de juego
        crearListaComida();
        buildRecyclerView();
        quitarControlesDeJuegoPrincipales();

        //Intent myIntent = new Intent(JuegoActivity.this, NextActivity.class);
        //myIntent.putExtra("key", value); //Optional parameters
        //JuegoActivity.this.startActivity(myIntent);

        // Instancia Listeners
        //instanciarListenersComida();
        //setContentView(R.layout.bienvenida_slide1);
        dentroSeccionComida = true;

    }




    private void mostrarIndicadoresDeJuego(){

        actualizarIndicadoresEnPantalla();

        /*juegoIndicadores.findViewById(R.id.juego_tv_hambre).setVisibility(View.VISIBLE);
        juegoIndicadores.findViewById(R.id.juego_tv_necesidad).setVisibility(View.VISIBLE);
        juegoIndicadores.findViewById(R.id.juego_tv_cansancio).setVisibility(View.VISIBLE);*/
        juegoIndicadores.findViewById(R.id.pb_necesidad).setVisibility(View.VISIBLE);
        juegoIndicadores.findViewById(R.id.pb_cansancio).setVisibility(View.VISIBLE);
        juegoIndicadores.findViewById(R.id.pb_hambre).setVisibility(View.VISIBLE);
        juegoIndicadores.findViewById(R.id.imViewCom).setVisibility(View.VISIBLE);
        juegoIndicadores.findViewById(R.id.imViewSuenyo).setVisibility(View.VISIBLE);
        juegoIndicadores.findViewById(R.id.imViewBanyo).setVisibility(View.VISIBLE);



    }

    private void ocultarIndicadoresDeJuego(){

        //this.framePrincipal.removeView(this.juegoIndicadores);

        /*juegoIndicadores.findViewById(R.id.juego_tv_hambre).setVisibility(View.INVISIBLE);
        juegoIndicadores.findViewById(R.id.juego_tv_necesidad).setVisibility(View.INVISIBLE);
        juegoIndicadores.findViewById(R.id.juego_tv_cansancio).setVisibility(View.INVISIBLE);*/
        juegoIndicadores.findViewById(R.id.pb_necesidad).setVisibility(View.INVISIBLE);
        juegoIndicadores.findViewById(R.id.pb_cansancio).setVisibility(View.INVISIBLE);
        juegoIndicadores.findViewById(R.id.pb_hambre).setVisibility(View.INVISIBLE);
        juegoIndicadores.findViewById(R.id.imViewCom).setVisibility(View.INVISIBLE);
        juegoIndicadores.findViewById(R.id.imViewSuenyo).setVisibility(View.INVISIBLE);
        juegoIndicadores.findViewById(R.id.imViewBanyo).setVisibility(View.INVISIBLE);


    }


    /**
     * Funcion que elimina los controles de juego principales
     */
    private void quitarControlesDeJuegoPrincipales(){

        this.framePrincipal.removeView(this.vistaConBotonesDeJuego);

    }


    /**
     * Funcion que elimina los controles de un personaje
     * muerto
     */
    private void quitarControlesDePersonajeMuerto(){

        this.framePrincipal.removeView(this.vistaConBotonesTrasMorir);
        this.vistaConBotonesTrasMorir = null;

    }




    /**
     * Funcion que elimina los controles de un personaje
     * dormido
     */
    private void quitarControlesDePersonajeDormido(){

        this.framePrincipal.removeView(this.vistaConBotonesDormido);
    }

    /**
     * quitar opciones comida
     */
    private void quitarControlesComida(){

        this.framePrincipal.removeView(this.vistaComida);
    }


    // ----------------------------------
    // Listeners

    /**
     * Eventos de los controles cuando el personaje
     * a muerto
     */
    private void instanciarListenersControlesMuerte(){
        //Personaje pj = juegoView.getJuegoMotor().getPersonaje();
        //pj.setLock(pj.getImagenMorir().size());
        //pj.setAccion(5);
        // BOTON DE RENACER
        ImageButton btnRevivir = this.vistaConBotonesTrasMorir.findViewById(R.id.juego_btn_revivir);
        btnRevivir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // cambia el layout de controles a personaje vivo
               quitarControlesDePersonajeMuerto();
                //quitarControlesDePersonajeDormido();
                mostrarControlesPrincipales();



                // crea un nuevo personaje por defecto
                juegoView.getJuegoMotor().crearNuevoPersonaje(Constantes.NOMBRE_PORDEFECTO);
                juegoView.getJuegoGraficos().instanciarPersonaje();


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Personaje pj = juegoView.getJuegoMotor().getPersonaje();
                        pj.setLock(pj.getImagenResucitar().size());
                        pj.setAccion(3);
                    }
                }, 100);


                mostrarIndicadoresDeJuego();
                //framePrincipal.findViewById(R.id.juego_btn_despertar).setVisibility(View.INVISIBLE);
                //quitarControlesDePersonajeMuerto();

            }
        });


    }

    @Override
    public void onResume(){
        super.onResume();
        juegoView.getJuegoMotor().actualizarValoresTrasTiempoEnReposo();
        // put your code here...
        Log.d("lavida", "ayudame");

    }


    private void visual(){
        Personaje pj = juegoView.getJuegoMotor().getPersonaje();
        pj.setLock(pj.getImagenServicio().size());
        pj.setAccion(2);
    }
    private void instanciarListenersControlesPrincipales(){
        Personaje pj = juegoView.getJuegoMotor().getPersonaje();
        // BOTON DE ALIMENTAR
        // TODO: OBTENER LA LISTA DE ALIMENTOS
        listaAlimentos = new ArrayList<>();
        listaAlimentos = pj.getbolsillo();
        //Personaje pj = juegoView.getJuegoMotor().getPersonaje();

        /*
        listaAlimentos.add(new Alimento(true, "Bebida", BitmapFactory.decodeResource(getResources(), R.drawable.bebida), 10));
        listaAlimentos.add(new Alimento(true, "Chocolate", BitmapFactory.decodeResource(getResources(), R.drawable.choc), 3));

*/



        // Seteamos la imagen sobre el layout
        marco = (ViewGroup)findViewById(R.id.marco);
        imagen = new ImageView(this);
        if (listaAlimentos.size()>=1){
            imagen.setImageBitmap(listaAlimentos.get(ultimoAlimentoVisualizado).getIcono());
            List<Integer> parametros = obtenerParametrosPantalla();
            imagen.setX(140);
            imagen.setY(parametros.get(0)-280);

            // Añadimos el Listener de la clase
            imagen.setOnTouchListener(this);

            // Añadimos la imagen al marco
            marco.addView(imagen);
        }


        Button btnAlimentar = this.vistaConBotonesDeJuego.findViewById(R.id.juego_btn_alimentar);
        btnAlimentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Actualizar cantidad minerales
                //SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO,MODE_PRIVATE);
                //int minerales = preferencias.getInt(Constantes.PREF_MINERALES, 30);
                //mostrarControlesComida();
                cambiarAlimento();
            }
        });

        ImageButton tienda = this.vistaConBotonesDeJuego.findViewById(R.id.juego_btn_tienda);
        tienda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Actualizar cantidad minerales
                //SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO,MODE_PRIVATE);
                //int minerales = preferencias.getInt(Constantes.PREF_MINERALES, 30);
                mostrarControlesComida();
                //cambiarAlimento();
            }
        });


        // BOTON DE DORMIR
        ImageButton btnDormir = this.vistaConBotonesDeJuego.findViewById(R.id.juego_btn_dormir);
        btnDormir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                quitarControlesDeJuegoPrincipales();
                juegoView.getJuegoMotor().descansarPersonaje();

                mostrarControlesDePersonajeDormido();

            }
        });

        // BOTON DE IR AL SERVICIO
        ImageButton btnServicio = this.vistaConBotonesDeJuego.findViewById(R.id.juego_btn_irServicio);
        btnServicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                juegoView.getJuegoMotor().atenderNecesidadesPersonaje();
                visual();

            }
        });

        // Accion acariciar
        Button Acariciar = this.vistaConBotonesDeJuego.findViewById(R.id.acariciar);
        Acariciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //juegoView.getJuegoMotor().atenderNecesidadesPersonaje();
                //visual();

                Personaje pj = juegoView.getJuegoMotor().getPersonaje();


                if (pj.getAccion() == 0){
                    pj.setLock(pj.getImagenAcariciar().size());
                    pj.setAccion(4);
                }
            }
        });



        // Botones de testeo

        // En caso de que este el modo testeo, se activan botones para hacer tests
        SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        boolean testeo = preferencias.getBoolean(Constantes.PREF_MODO_TESTER, false);

        if(testeo){

            Button darMinerales = vistaConBotonesDeJuego.findViewById(R.id.juego_btn_dar_minerales);
            Button estadoOptimo = vistaConBotonesDeJuego.findViewById(R.id.juego_btn_estado_optimo);
            Button acelerar = vistaConBotonesDeJuego.findViewById(R.id.juego_btn_acelerar);
            Button decelerar = vistaConBotonesDeJuego.findViewById(R.id.juego_btn_decelerar);


            darMinerales.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Se añaden 100 minerales

                    SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO, MODE_PRIVATE);
                    int contador = preferencias.getInt(Constantes.PREF_MINERALES, 30);
                    SharedPreferences.Editor miEditor = preferencias.edit();
                    miEditor.putInt(Constantes.PREF_MINERALES, contador+100);

                    miEditor.commit();
                }
            });


            estadoOptimo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // Pone todos los valores negativos a 0
                    juegoView.getJuegoMotor().getPersonaje().restarCansancio(100);
                    juegoView.getJuegoMotor().getPersonaje().restarHambre(100);
                    juegoView.getJuegoMotor().getPersonaje().restarNecesidadIrAlServicio(100);
                }
            });


            acelerar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    // Se alteran los factores originales de incrmeneto

                    juegoView.getJuegoMotor().getPersonaje().FACTOR_ENVEJECIMIENTO_ORIGINAL = 10;

                    juegoView.getJuegoMotor().getPersonaje().FACTOR_HAMBRE_ORIGINAL = 20;
                    juegoView.getJuegoMotor().getPersonaje().FACTOR_CANSANCIO_ORIGINAL = 15;
                    juegoView.getJuegoMotor().getPersonaje().FACTOR_NECESIDAD_ORIGINAL = 15;


                }
            });

            decelerar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    juegoView.getJuegoMotor().getPersonaje().FACTOR_ENVEJECIMIENTO_ORIGINAL = 1;

                    juegoView.getJuegoMotor().getPersonaje().FACTOR_HAMBRE_ORIGINAL = 2;
                    juegoView.getJuegoMotor().getPersonaje().FACTOR_CANSANCIO_ORIGINAL = .5f;
                    juegoView.getJuegoMotor().getPersonaje().FACTOR_NECESIDAD_ORIGINAL = 1;                }
            });

        } // fin listeners modo testeo
    }

    /**
     * ----------------------------------------------------------------------
     * Se cambia de alimento en la lista
     * ----------------------------------------------------------------------
     * cambiarAlimento()
     * ----------------------------------------------------------------------
     */
    private void cambiarAlimento(){
        // Incrementamos la posición de la lista para que cambie de alimento
        ultimoAlimentoVisualizado++;

        if(ultimoAlimentoVisualizado >= listaAlimentos.size()){
            Log.d("COMIDA", "dentro de ultimoElementoVisualizado");
            ultimoAlimentoVisualizado = 0;
        }
        if(listaAlimentos.size() == 0){
            Log.d("COMIDA", "dentro de no hay más comida");
            imagen.setImageBitmap(null);
        }else{
            Log.d("COMIDA", "dentro de cambiando comida");
            imagen.setImageBitmap(listaAlimentos.get(ultimoAlimentoVisualizado).getIcono());
        }
    }

    /**
     * ----------------------------------------------------------------------
     * Se llama a la función que alimenta a la mascota teniendo en cuenta
     * el tipo de alimento que es
     * ----------------------------------------------------------------------
     * alimento:Alimento --> alimentarMascota()
     * ----------------------------------------------------------------------
     * @param alimento
     */
    private void alimentarMascota(Alimento alimento){
        // Actualizar cantidad minerales
        juegoView.getJuegoMotor().alimentarPersonaje(alimento);
    }

    /**
     * ----------------------------------------------------------------------
     * Se obtienen los pixeles de la pantalla para por ejemplo poder centrar
     * cosas o para poder ponerlas en posiciones concretas
     * ----------------------------------------------------------------------
     * obtenerParametrosPantalla() --> List<Integer> [height, widht]
     * ----------------------------------------------------------------------
     * @return List<Integer>, siendo la lista de 2 valores, altura (height) y ancho (widht)
     */
    private List<Integer> obtenerParametrosPantalla(){
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        List<Integer> parametros = new ArrayList<>();
        parametros.add(height); parametros.add(width);
        return parametros;
    }


    /**
     * Eventos de los controles cuando el personaje esta dormido
     */
    private void instanciarListenersControlesDormido(){

        // BOTON DE DESPERTAR
        ImageButton btnDespertar = this.vistaConBotonesDormido.findViewById(R.id.juego_btn_despertar);
        btnDespertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                quitarControlesDePersonajeDormido();
                juegoView.getJuegoMotor().despertarPersonaje();
                mostrarControlesPrincipales();

            }
        });

    }

    /**
     * Resta minerales
     */
    private void restarMinerales(int valor){

        SharedPreferences preferencias = this.getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO, MODE_PRIVATE);
        int contador = preferencias.getInt(Constantes.PREF_MINERALES, 30);
        SharedPreferences.Editor miEditor = preferencias.edit();
        miEditor.putInt(Constantes.PREF_MINERALES, contador-valor);
        miEditor.commit();


    }


    public void comprarcomida(int position) {
        Personaje pj = juegoView.getJuegoMotor().getPersonaje();
        //listaComida.get(position).changeText1(text);
        mAdapter.notifyItemChanged(position);
        int img = listaComida.get(position).getImageResource();
        String nombre =  listaComida.get(position).getText1();
        int llena =  Integer.parseInt(listaComida.get(position).getText2());
        //String cuesta =  listaComida.get(position).getText3();
        int cuesta =  Integer.parseInt(listaComida.get(position).getText3());


        //Toast.makeText(getBaseContext(), " primero " + nombre + " segundo " + llena + " tercero " + cuesta, Toast.LENGTH_LONG).show();

        SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO,MODE_PRIVATE);
        int minerales = preferencias.getInt(Constantes.PREF_MINERALES, 30);
        //Alimento alimentoBasico = new Alimento(true, "BioZeta Basica");
        Alimento alimentoBasico = new Alimento(true, nombre,llena, cuesta);
        Toast.makeText(getBaseContext(), getString(R.string.titulo_objeto_adquirido), Toast.LENGTH_LONG).show();
        restarMinerales(alimentoBasico.getCoste());
        pj.setbolsillo(new Alimento(true, nombre, BitmapFactory.decodeResource(getResources(), img), llena, cuesta));
    }

    private void crearListaComida() {
        listaComida = new ArrayList<>();
        listaComida.add(new ComidaItem(R.drawable.sandw, "Sandwich", "11","200"));
        listaComida.add(new ComidaItem(R.drawable.sushi, "Sushi", "22","50"));
        listaComida.add(new ComidaItem(R.drawable.choc, "Chocolate", "33","3"));
        listaComida.add(new ComidaItem(R.drawable.helado, "Helado", "44","4"));
        listaComida.add(new ComidaItem(R.drawable.hotdog, "Hot Dog", "55","5"));
        listaComida.add(new ComidaItem(R.drawable.manzana, "Manzana", "66","6"));
    }
    private void buildRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new ComidaAdapter(listaComida, JuegoActivity.this);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    public void actualizarRecyclerView(){
        mAdapter = new ComidaAdapter(listaComida, JuegoActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // almacenamos los valores de la mascota en el shared preferences
        this.juegoView.getJuegoMotor().guardarParametrosPersonaje();
        this.manejadorEstadoDeJuego.removeCallbacks(this.hiloEstadoDeJuego);
        this.juegoView.pausar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.juegoView.iniciar();
        inicializarHiloEstadoDeJuego();
    }

    @Override
    public void onBackPressed() {
        if(dentroSeccionComida){
            dentroSeccionComida = false;
            quitarControlesComida();
            quitarControlesDePersonajeDormido();
            mostrarControlesPrincipales();
        }else{
            super.onBackPressed();
        }
    }

    /**
     * ----------------------------------------------------------------------
     * Controlador de movimiento y tacto sobre un objeto determinado, en este
     * caso, los alimentos
     * ----------------------------------------------------------------------
     * v:View, event:MotionEvent --> onTouch() --> VoF
     * ----------------------------------------------------------------------
     * @param v
     * @param event
     * @return
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Personaje pj = juegoView.getJuegoMotor().getPersonaje();
        //Recogemos las coordenadas del dedo
        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                //Al tocar la pantalla recogemos los parametros de la imagen que hemo tocado
                RelativeLayout.LayoutParams Params = (RelativeLayout.LayoutParams) v.getLayoutParams();
                xDelta = X - Params.leftMargin;
                yDelta = Y - Params.topMargin;
                break;
            case MotionEvent.ACTION_UP:
                //Al levantar el si el alimento está donde está la mascota, hacemos que coma, sino, lo devolvemos al sitio original
                if(juegoView.elAlimentoEstaSobreLaMascota(imagen.getX(), imagen.getY())){
                    // Alimentamos a la mascota
                    alimentarMascota(listaAlimentos.get(ultimoAlimentoVisualizado));
                    //listaAlimentos.add(listaAlimentos.get(ultimoAlimentoVisualizado));
                    // Borramos ese objeto de la lista (porque ya se ha consumido)
                    listaAlimentos.remove(ultimoAlimentoVisualizado);
                    pj.vaciarbolsillo(ultimoAlimentoVisualizado);
                    // Cambiamos de tipo de alimento
                    cambiarAlimento();
                    pj.setAccion(1);
                    pj.setLock(pj.getImagenComer().size());
                    //listaAlimentos.add(listaAlimentos.get(ultimoAlimentoVisualizado));
                }
                // Seteamos los siguientes valores para que se actualice la posición del objeto cuando se haga cambios de tasa de frames
                List<Integer> parametros = obtenerParametrosPantalla();
                juegoView.imagenComida = imagen;
                juegoView.posX = 140;
                juegoView.posY = parametros.get(0)-280;
                juegoView.seHaSoltadoElObjeto = true;
                break;
            case MotionEvent.ACTION_MOVE:
                //Al mover el dedo vamos actualizando los margenes de la imagen para crear efecto de arrastrado
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) v.getLayoutParams();
                layoutParams.leftMargin = X - xDelta;
                layoutParams.topMargin = Y - yDelta;
                //Quitamos un poco de margen para que la imagen no se deforme al llegar al final de la pantalla y pueda ir más allá probar también el codigo omitiendo estas dos líneas
                layoutParams.rightMargin = -50;
                layoutParams.bottomMargin = -50;
                //Le añadimos los nuevos parametros para mover la imagen
                v.setLayoutParams(layoutParams);
                break;
        }
        // Dibujamos la posicion de la imagen dentro del marco
        marco.invalidate();
        return true;
    }




}
