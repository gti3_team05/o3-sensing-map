package org.g05.o3receptor.view.util;

import android.Manifest;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.maps.model.LatLng;

import org.g05.o3receptor.R;
import org.g05.o3receptor.model.entity.Incidencia;
import org.g05.o3receptor.model.entity.Medida;
import org.g05.o3receptor.model.entity.TramaIBeacon;
import org.g05.o3receptor.model.repository.remote.EstacionesMedidas_post;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class ServicioBTLE extends Service {
    private static String ETIQUETA_LOG = "SERVICIO BTLE";

    private int i = 0;
    public static LocationManager locationManagerUser;
    private int contadorMedidasErroneasSeguidas = 0;

    private BluetoothAdapter.LeScanCallback  callbackLeScan = null;
    private final String DISPOSITIVO_UUID = "EPSG-PROY-3A-G05"; //UUID A BUSCAR PARA LOS BEACONS
    private final int TIEMPO_MAX_ENTRE_BEACONS = 7000; // TIEMPO MAXIMO SIN LEER BEACONS, EN SEGUNDOS

    private long momentoUltimoBeacon; // Momento en el que se detecto el ultimo beacon
    private ProcesadorMediciones pm;

    // Manejador y Runnable para ejecutar en segundo plano la comprobacion de tiempo desde el ultimo beacon recibido
    private final Handler manejadorBLEBeaconsTiempo = new Handler();
    private Runnable ejecutableBLEBeaconsTiempo;

    private ServicioBTLE servicioBTLE;

    private Notificaciones notificaciones;

    @Override
    public void onCreate(){
        super.onCreate();
        notificaciones = new Notificaciones(this);
        Log.d(ETIQUETA_LOG, "Servicio inicializado");
    }

    // *************************************************************************************************************************************************************************** //
    // ******************************************************************** PERMISOS  ******************************************************************************************** //

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Miramos si en ese momento está encendido el bluetooth
     * --------------------------------------------------------------
     * estaEncendidoBluetooth()
     * --------------------------------------------------------------
     * @return
     */
    private boolean estaEncendidoBluetooth(){
        BluetoothAdapter bla = BluetoothAdapter.getDefaultAdapter();

        if(bla!=null && bla.isEnabled()) {
            // Significa que el dispositivo tiene bluetooth y está activado para usarse
            return true;
        }
        return false;
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Miramos si en ese momento está encendido el gps o la conexión a internet para poder
     * obtener la ubicación
     * --------------------------------------------------------------
     * estaEncendidoGPS()
     * --------------------------------------------------------------
     * @return
     */
    private boolean estaEncendidoGPS(){
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            guardarEstadoGPS(false);
            return false;
        }
        guardarEstadoGPS(true);
        return true;
    }

    private void guardarEstadoGPS(boolean encendido){
        SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putBoolean(Constantes.PREF_GPS_ENCENDIDO, encendido);
        editor.commit();
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Miramos si está el gps y bluetooth:
     * 1. Si lo están buscamos beacons y procesamos las mediciones antes de enviarlas
     * 2. Si no lo están, esperamos a que el usuario los encienda
     *  2.1 Enviamos notificaciones para que el usuario los encienda
     * --------------------------------------------------------------
     * iniciarEscaneoSegundoPlano()
     * --------------------------------------------------------------
     */
    public void iniciarEscaneoSegundoPlano(){
        boolean estaEncendidoBluetooth = estaEncendidoBluetooth();
        boolean estaEncendidoGPS = estaEncendidoGPS();
        if(estaEncendidoBluetooth && estaEncendidoGPS){
            this.getLocation();
            Log.d(ETIQUETA_LOG, "Bluethooth y GPS activados");
            pm = new ProcesadorMediciones(servicioBTLE, obtenerUltimoContadorVisto());
            buscarEsteDispositivoBTLE( Utilidades.stringToUUID( DISPOSITIVO_UUID ) );
        }else if(!estaEncendidoBluetooth && !estaEncendidoGPS){
            // Avisamos al usuario para que los active
            notificaciones.crearNotificacion(getString(R.string.bluetooth_gps_off),getString(R.string.bluetooth_gps_off_text), R.drawable.ic_ozono);
            comprobarBluetoothGps();
        }else if(!estaEncendidoBluetooth){
            // Avisamos al usuario para que los active
            notificaciones.crearNotificacion(getString(R.string.bluetooth_off),getString(R.string.bluetooth_off_text),R.drawable.ic_ozono);
            comprobarBluetoothGps();
        }else if(!estaEncendidoGPS){
            // Avisamos al usuario para que los active
            notificaciones.crearNotificacion(getString(R.string.gps_off),getString(R.string.gps_off_text),R.drawable.ic_ozono);
            comprobarBluetoothGps();
        }
    }

    private void comprobarBluetoothGps(){
        new Thread(new Runnable() {
            public void run() {
                if(estaEncendidoGPS() && estaEncendidoBluetooth()){
                    notificaciones.eliminarNotificacion(1);
                    //Se instancia el procesador de mediciones
                    pm = new ProcesadorMediciones(servicioBTLE, obtenerUltimoContadorVisto());
                    iniciarEscaneo();
                }else{
                    comprobarBluetoothGps();
                }
            }
        }).start();
    }

    // *************************************************************************************************************************************************************************** //
    // ***************************************************** GUARDAR VALORES SERVICIO SEGUNDO PLANO ****************************************************************************** //

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Permite almacenar la referencia bind para poder llamar a las funciones del servicio desde otras clases
     * --------------------------------------------------------------
     * ServicioSegundoPlano --> guardarServicioSegundoPlano()
     * --------------------------------------------------------------
     * @param servicioBTLE
     */
    public void guardarServicioSegundoPlano(ServicioBTLE servicioBTLE){
        this.servicioBTLE = servicioBTLE;
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Almacenamos en las sharedPreferences el valor del ultimo contador, ya que puede dar problemas cuando:
     * 1. Cerramos la app y la volvemos a abrir
     * 2. Se desactiva el bluetooth o gps y por tanto el contador se reiniciaria a 0
     * --------------------------------------------------------------
     * ultimoValor: N --> guardarUltimoContadorVisto()
     * --------------------------------------------------------------
     * @param ultimoValor
     */
    public void guardarUltimoContadorVisto(int ultimoValor){
        SharedPreferences preferencias = getSharedPreferences("preferenciasBTLE",MODE_PRIVATE);
        SharedPreferences.Editor miEditor = preferencias.edit();
        miEditor.clear();
        miEditor.putInt("ultimoContador", ultimoValor);
        miEditor.commit();
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Obtenemos el ultimo contador almacenado en las preferencias de usuario
     * --------------------------------------------------------------
     * guardarUltimoContadorVisto() --> ultimoValor: N
     * --------------------------------------------------------------
     * @return
     */
    public int obtenerUltimoContadorVisto(){
        // Obtenemos el ultimo contador, para que si por ejemplo cerramos la app, no se pierdan los datos
        SharedPreferences preferencias = getSharedPreferences("preferenciasBTLE",MODE_PRIVATE);
        int ultimoContadorVisto = preferencias.getInt("ultimoContador",0);

        return ultimoContadorVisto;
    }

    // *************************************************************************************************************************************************************************** //
    // **************************************************************** OBTENER UBICACION **************************************************************************************** //

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * 1. Obtiene a traves de una peticion gps el valor de la ubicación del dispositivo movil.
     * 2. Envia con un intent la información a la función (asincronia)
     * --------------------------------------------------------------
     * getLocation()
     * --------------------------------------------------------------
     */
    public void getLocation() {
        String location_context = Context.LOCATION_SERVICE;
        locationManagerUser = (LocationManager) this.getSystemService(location_context);
        List<String> providers = locationManagerUser.getProviders(true);
        for (String provider : providers) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManagerUser.requestLocationUpdates(provider, 0, 100, new LocationListener() {
                public void onLocationChanged(Location location) {
                    if (i == 0) {
                        i++;
                    } else {
                        pm.ultimaUbicacionVista = location; //Actualizo la ubicacion
                        pm.ubicacionActualizada = true; // Marco la ubicacion como actualizada (se usa como un flag)
                        Log.d(ETIQUETA_LOG, "Localizacion --> "+location.getLatitude()+location.getLongitude());
                        almacenarUbicacionUsuarioSharedPreferences(location.getLatitude(),location.getLongitude());
                    }
                }

                public void onProviderDisabled(String provider) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }
            });
        }
    }

    private void almacenarUbicacionUsuarioSharedPreferences(double latitude, double longitude){
        SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        SharedPreferences.Editor miEditor = preferencias.edit();
        miEditor.putLong(Constantes.PREF_LATITUD_USUARIO, Double.doubleToRawLongBits(latitude));
        miEditor.putLong(Constantes.PREF_LONGITUD_USUARIO, Double.doubleToRawLongBits(longitude));
        miEditor.commit();
    }

    // *************************************************************************************************************************************************************************** //
    // ****************************************************************** BUSCAR DISPOSITIVOS BTLE ******************************************************************************* //
    public void buscarTodosLosDispositivosBTLE() {
        this.callbackLeScan = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] bytes) {

                //
                //  se ha encontrado un dispositivo
                //
                mostrarInformacionDispositivoBTLE( bluetoothDevice, rssi, bytes );

            } // onLeScan()
        }; // new LeScanCallback

        boolean resultado = BluetoothAdapter.getDefaultAdapter().startLeScan( this.callbackLeScan );

        // Log.d(ETIQUETA_LOG, " buscarTodosLosDispositivosBTL(): startLeScan(), resultado= " + resultado );
    } // ()

    private void mostrarInformacionDispositivoBTLE( BluetoothDevice bluetoothDevice, int rssi, byte[] bytes) {

        Log.d(ETIQUETA_LOG, " ****************************************************");
        Log.d(ETIQUETA_LOG, " ****** DISPOSITIVO DETECTADO BTLE ****************** ");
        Log.d(ETIQUETA_LOG, " ****************************************************");
        Log.d(ETIQUETA_LOG, " nombre = " + bluetoothDevice.getName());
        Log.d(ETIQUETA_LOG, " dirección = " + bluetoothDevice.getAddress());
        Log.d(ETIQUETA_LOG, " rssi = " + rssi );

        Log.d(ETIQUETA_LOG, " bytes = " + new String(bytes));
        Log.d(ETIQUETA_LOG, " bytes (" + bytes.length + ") = " + Utilidades.bytesToHexString(bytes));

        TramaIBeacon tib = new TramaIBeacon(bytes);

        Log.d(ETIQUETA_LOG, " ----------------------------------------------------");
        Log.d(ETIQUETA_LOG, " prefijo  = " + Utilidades.bytesToHexString(tib.getPrefijo()));
        Log.d(ETIQUETA_LOG, " advFlags = " + Utilidades.bytesToHexString(tib.getAdvFlags()));
        Log.d(ETIQUETA_LOG, " advHeader = " + Utilidades.bytesToHexString(tib.getAdvHeader()));
        Log.d(ETIQUETA_LOG, " companyID = " + Utilidades.bytesToHexString(tib.getCompanyID()));
        Log.d(ETIQUETA_LOG, " iBeacon type = " + Integer.toHexString(tib.getiBeaconType()));
        Log.d(ETIQUETA_LOG, " iBeacon length 0x = " + Integer.toHexString(tib.getiBeaconLength()) + " ( "
                + tib.getiBeaconLength() + " ) ");
        Log.d(ETIQUETA_LOG, " uuid  = " + Utilidades.bytesToHexString(tib.getUUID()));
        Log.d(ETIQUETA_LOG, " uuid  = " + Utilidades.bytesToString(tib.getUUID()));
        Log.d(ETIQUETA_LOG, " major  = " + Utilidades.bytesToHexString(tib.getMajor()) + "( "
                + Utilidades.bytesToInt(tib.getMajor()) + " ) ");
        Log.d(ETIQUETA_LOG, " minor  = " + Utilidades.bytesToHexString(tib.getMinor()) + "( "
                + Utilidades.bytesToInt(tib.getMinor()) + " ) ");
        Log.d(ETIQUETA_LOG, " txPower  = " + Integer.toHexString(tib.getTxPower()) + " ( " + tib.getTxPower() + " )");
        Log.d(ETIQUETA_LOG, " ****************************************************");

    } // ()

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    private void buscarEsteDispositivoBTLE(final UUID dispositivoBuscado ) {

        this.callbackLeScan = new BluetoothAdapter.LeScanCallback() {
            @Override
            public void onLeScan(BluetoothDevice bluetoothDevice, int rssi, byte[] bytes) {
                // Dispositivo encontrado
                TramaIBeacon tib = new TramaIBeacon( bytes );
                String uuidString =  Utilidades.bytesToString( tib.getUUID() );

                if ( uuidString.compareTo( Utilidades.uuidToString( dispositivoBuscado ) ) == 0 )  {

                    mostrarInformacionDispositivoBTLE( bluetoothDevice, rssi, bytes );

                    // -------------------------------------------------
                    // Al detectar un beacon de nuestro sensor...
                    momentoUltimoBeacon = System.currentTimeMillis(); // Actualizo el momento
                    detenerBusquedaDispositivosBTLE(); // Detengo la busqueda

                    haLlegadoUnBeacon(tib, rssi);

                } else {
                    Log.d( ETIQUETA_LOG, " * UUID buscado >" +
                            Utilidades.uuidToString( dispositivoBuscado ) + "< no concuerda con este uuid = >" + uuidString + "<");

                    centinelaBLE();
                }

            } // onLeScan()


        }; // new LeScanCallback

        BluetoothAdapter.getDefaultAdapter().startLeScan( this.callbackLeScan );

        Log.d(ETIQUETA_LOG, "Buscando beacon");

    } // ()

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    public void detenerBusquedaDispositivosBTLE() {
        if ( this.callbackLeScan == null ) {
            return;
        }

        BluetoothAdapter.getDefaultAdapter().stopLeScan(this.callbackLeScan);
        this.callbackLeScan = null;

        // Detiene el segundo hilo
        manejadorBLEBeaconsTiempo.removeCallbacks(this.ejecutableBLEBeaconsTiempo);

        Log.d(ETIQUETA_LOG, "En espera");

    } // ()

    // *************************************************************************************************************************************************************************** //
    // ****************************************************************** LECTURA Y TRATAMIENTO BEACON *************************************************************************** //

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion que recibe un beacon, extrae sus valores y si estos son
     * validos se obtiene la ubicacion, pero antes de enviarlas al servidor
     * las compara con las de una API de detección de medidas
     * --------------------------------------------------------------
     * TramaIBeacon, RSSI:N  --> haLlegadoUnBeacon()
     * --------------------------------------------------------------
     * @param beacon  Beacon recibido por el dispositivo
     * @param rssi    Valor de la fuerza de la señal recibida, usado
     *                para saber la distancia entre sensor y receptor
     */
    private void haLlegadoUnBeacon(TramaIBeacon beacon, int rssi) {
        Log.d(ETIQUETA_LOG, "Beacon detectado");

        int major = Utilidades.bytesToInt(beacon.getMajor());
        int minor = Utilidades.bytesToInt(beacon.getMinor());

        // Se extrae el valor de la medida del sensor correspondiente del major y del minor
        final Integer medicion = this.pm.extraerMedicion(major, minor);

        switch (this.pm.ultimoTipoVisto){
            case Constantes.ID_TIPO_GAS_O3:

                // Si hay una medida buena, se envia
                if (medicion!=null){
                    Log.d(ETIQUETA_LOG, "Medición != null");
                    // Se actualiza la ultima posicion GPS del usuario
                    getLocation();

                    // Se comprueba la distancia al usuario mediante el RSSI y el txPower
                    estaLejosElBeacon(rssi, Utilidades.byteToInt(beacon.getTxPower()));

                    // Sumamos minerales al modo juego
                    sumarMinerales(Constantes.VALOR_MINERAL_PORDEFECTO);

                    // Sumamos 1 a la lista de beacons para las tareas de las recompensas
                    if(estaActivadaLaMacota()){
                        sumarBeaconAlListado();
                    }

                    // Se llama a la funcion que consultara la API de la RVVCCA
                    consultaApiMediciones(medicion, 5);

                } else { // Si la medida no es valida

                    // Se vuelve a la busqueda de beacons
                    iniciarEscaneoSegundoPlano();

                } //
                break;
            case Constantes.ID_TIPO_BATERIA_BAJA:
                // Notificamos de que la bateria es baja
                notificaciones.crearNotificacion(getString(R.string.bateria_baja), getString(R.string.bateria_baja_text),R.drawable.ic_ozono);
                // Volvemos a iniciar el escaneo de beacons
                iniciarEscaneoSegundoPlano();
                break;
        }


    } // ()

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Miramos mediante el sharedPreferences si el usuario quiere tener
     * mascota o no
     * --------------------------------------------------------------
     * estaActivadaLaMacota()
     * --------------------------------------------------------------
     * @return boolean
     */
    private boolean estaActivadaLaMacota(){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        boolean mascota  = sharedPref.getBoolean(Constantes.PREF_MASCOTA, true); // Indica que está logeado
        if(mascota){
            return true;
        }else{
            return false;
        }
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion que recibe la medicion del sensor, y un contador y que
     * hace una consulta a la API en función del modo del movil y que
     * se realiza un n numero de veces en funcion del valor del
     * contador
     * --------------------------------------------------------------
     * medicionApi:N , medicion:N --> compararMedicionesOzono()
     * --------------------------------------------------------------
     * @param contador     Contador con el numero de ejecuciones de
     *                     la llamada a la API
     * @param medicion     Medición obtenida por el sensor
     *
     */
    public void consultaApiMediciones(int medicion, int contador){
        Log.d("Realizando peticion API","Inicializando...");
        if(contador > 0){
            if(!estaModoTester()){
                // Peticion de medida al servidor
                APIServidor peticion = new APIServidor(servicioBTLE, medicion, contador);
                Log.d("Realizando peticion API","Ejecutando...");
                peticion.execute();
            } else {
                Log.d("Realizando post sin API","Ejecutando...");
                subirMedidaBDTester(medicion);
            }
        }

        if (contador == 0){
            Log.d("Contador a 0","Ejecutando...");
            subirMedidaBD(5, medicion);
        }

    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion que recibe la medicion del sensor, y un json de la primera
     * consulta al servidor de medidas, que vuelve a hacer una peticion
     * para conseguir los valores de la estacion y ubica la estacion si
     * la consulta no es nula
     * --------------------------------------------------------------
     * json: JSON(peticion API) , medicion:N --> obtenerMedidasApi()
     * --------------------------------------------------------------
     * @param json         Objeto JSON de la primera consulta a la API
     * @param medicion     Medición obtenida por el sensor
     *
     */
    public void obtenerMedidasApi (JSONObject json, int medicion){
        Log.d("Obtencion medidas API", "Segundo paso");
        if(json != null){
            ObtenerValorPeticion peticion = new ObtenerValorPeticion(servicioBTLE, json, medicion);
            peticion.execute();
        } else {
            subirMedidaBDTester(medicion);
        }
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion que recibe un json de la primera consulta al servidor
     * de medidas y envia una consulta para subir dicha estacion a la
     * base de datos
     * --------------------------------------------------------------
     * json: JSON(peticion API) --> obtenerEstacionMedida()
     * --------------------------------------------------------------
     * @param json      Objeto JSON de la primera consulta a la API
     */
    public void obtenerEstacionMedida(JSONObject json) throws JSONException {
        Log.d("Funcion post estacion", "Ejecutandose...");
        double longitud = json.getDouble("longitudGoogleMaps");
        double latitud = json.getDouble("latitudGoogleMaps");
        String nombre = json.getString("descMunicipio");
        EstacionesMedidas_post peticion = new EstacionesMedidas_post(servicioBTLE, latitud,longitud,nombre);
        peticion.execute();
    }

    private boolean estaModoTester(){
        SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        boolean modoTester = preferencias.getBoolean(Constantes.PREF_MODO_TESTER,false);
        return modoTester;
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion que recibe un beacon, extrae sus valores y si estos son
     * validos se obtiene la ubicacion y se envian al servidor REST
     * la medida y la posicion en la que ha sido tomada
     * --------------------------------------------------------------
     * medicionApi:N , medicion:N --> compararMedicionesOzono()
     * --------------------------------------------------------------
     * @param medicionApi  Medición obtenida por la API
     * @param medicion     Medición obtenida por el sensor
     *
     */
    private boolean compararMedicionesOzono(int medicionApi, Integer medicion){

        double rangoApi=(medicionApi*20.0)/100;

        if(((rangoApi+medicion)>medicion) || ((rangoApi+medicion)<medicion)){
            return false;
        }else{
            return true;
        }

    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion que despues de comparar las mediciones de la API con la
     * del beacon, procede a enviar la medida a la base de datos
     * --------------------------------------------------------------
     * medicionApi:N , medicion:N --> subirMedidaBD()
     * --------------------------------------------------------------
     * @param medicionApi  Medición obtenida por la API
     * @param medicion     Medición obtenida por el sensor
     *
     */
    public void subirMedidaBD(int medicionApi, final Integer medicion){

        Log.d(ETIQUETA_LOG,"MedicionApi --> "+medicionApi+" Medición --> "+medicion);

        if(!compararMedicionesOzono(medicionApi, medicion)){

            Log.d(ETIQUETA_LOG,"Medición comparada --> res=correcto");

            //Se notifica al usuario si la medicion es peligrosa
            informarSiMedicionEsAlta(medicion);

            // --------------------------------------------------------
            // Tras 3 segundos, se realizan 5 intentos de obtener la ubicacion (1 por segundo),
            // si esta se obtiene, se envia la medida, si no, se descarta

            Handler postProcesador = new Handler();
            postProcesador.postDelayed(new Runnable() {
                @Override
                public void run() {
                    int intentos = 5; // Nuemero de intentos
                    boolean ubicacionObtenida = false;
                    while(intentos>0){
                        if(pm.isUbicacionActualizada()){ //Si la ubicacion se ha obtenido, se envia la medida
                            ubicacionObtenida = true;
                            Log.d("ENVIO","Se envia");

                            //Se crea la medida
                            Medida m = new Medida(pm.getTiposMedida().get(pm.getUltimoTipoVisto()),
                                    medicion, pm.getUltimaUbicacionVista(), obtenerMailUsuario());

                            //Se envia la medida
                            pm.enviarMedida(m);

                            Log.d(ETIQUETA_LOG, "Valor "+String.valueOf(m.getMedida())+" ug/m^3");
                            Log.d(ETIQUETA_LOG, "Tipo "+m.getTipo());
                            Log.d(ETIQUETA_LOG, "Latitud \n"+String.valueOf(m.getUbicacion().getLatitude()));
                            Log.d(ETIQUETA_LOG, "Longitud \n"+String.valueOf(m.getUbicacion().getLongitude()));
                            Log.d(ETIQUETA_LOG, "Beacon enviado");

                            String fecha = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                            String tiempo = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

                            Log.d(ETIQUETA_LOG, "Momento de deteccion\n "+fecha+" "+tiempo);

                            /////////////////////////////////////////////
                            /////////////////////////////////////////////

                            break; // Si la medida se ha enviado, se sale del bucle

                        } else { //Si no se ha obtenido la ubicacion se cancela el envio

                            esperar(1000);
                            intentos--; // un intento menos

                        } // end

                    } // end while()

                    if(!ubicacionObtenida) { //Si la ubicacion no se obtiene, se vuelve a la busqueda
                        buscarEsteDispositivoBTLE( Utilidades.stringToUUID( DISPOSITIVO_UUID ) );
                    }

                } // end run()
            }, 3000); // Se espera 3 segundos por defecto para la obtencion de la localizacion.
        }else{
            Log.d(ETIQUETA_LOG,"Medición comparada --> res=erroneo");
            contadorMedidasErroneasSeguidas++;
            if(contadorMedidasErroneasSeguidas > Constantes.MAXIMO_CONTADOR_MEDIDAS_ERRONEAS){
                
                if(estanActivadasLasNotificacionesDeValores()){
                    notificaciones.crearNotificacion(getString(R.string.mediciones_erroneas), getString(R.string.mediciones_erroneas_text),R.drawable.ic_ozono);
                }

                // Subimos a la base de datos que ha habido un error
                //Se crea la incidencia
                Incidencia i = new Incidencia(obtenerMailUsuario(),"lecturasErroneas");
                //Se envia la medida
                pm.enviarIncidencia(i);
                
                contadorMedidasErroneasSeguidas = 0;
            }
            servicioBTLE.iniciarEscaneoSegundoPlano();
        }

    }

    private boolean estanActivadasLasNotificacionesDeValores(){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        boolean notificaciones  = sharedPref.getBoolean(Constantes.PREF_NOTIFICACIONES_MEDIDAS, true); // Indica que está logeado
        if(notificaciones){
            return true;
        }else{
            return false;
        }
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion que despues de comparar las mediciones de la API con la
     * del beacon, procede a enviar la medida a la base de datos
     * --------------------------------------------------------------
     * medicion:N --> subirMedidaBD()
     * --------------------------------------------------------------
     * @param medicion     Medición obtenida por el sensor
     *
     */
    public void subirMedidaBDTester(final Integer medicion){
        //Se notifica al usuario si la medicion es peligrosa
        informarSiMedicionEsAlta(medicion);

        // --------------------------------------------------------
        // Tras 3 segundos, se realizan 5 intentos de obtener la ubicacion (1 por segundo),
        // si esta se obtiene, se envia la medida, si no, se descarta

        Handler postProcesador = new Handler();
        postProcesador.postDelayed(new Runnable() {
            @Override
            public void run() {
                int intentos = 5; // Nuemero de intentos
                boolean ubicacionObtenida = false;
                while(intentos>0){
                    if(pm.isUbicacionActualizada()){ //Si la ubicacion se ha obtenido, se envia la medida
                        ubicacionObtenida = true;
                        Log.d("ENVIO","Se envia");

                        //Se crea la medida
                        Medida m = new Medida(pm.getTiposMedida().get(pm.getUltimoTipoVisto()),
                                medicion, pm.getUltimaUbicacionVista(), obtenerMailUsuario());

                        //Se envia la medida
                        pm.enviarMedida(m);

                        Log.d(ETIQUETA_LOG, "Valor "+String.valueOf(m.getMedida())+" ug/m^3");
                        Log.d(ETIQUETA_LOG, "Tipo "+m.getTipo());
                        Log.d(ETIQUETA_LOG, "Latitud \n"+String.valueOf(m.getUbicacion().getLatitude()));
                        Log.d(ETIQUETA_LOG, "Longitud \n"+String.valueOf(m.getUbicacion().getLongitude()));
                        Log.d(ETIQUETA_LOG, "Beacon enviado");

                        String fecha = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
                        String tiempo = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

                        Log.d(ETIQUETA_LOG, "Momento de deteccion\n "+fecha+" "+tiempo);

                        /////////////////////////////////////////////
                        /////////////////////////////////////////////

                        break; // Si la medida se ha enviado, se sale del bucle

                    } else { //Si no se ha obtenido la ubicacion se cancela el envio

                        esperar(1000);
                        intentos--; // un intento menos

                    } // end

                } // end while()

                if(!ubicacionObtenida) { //Si la ubicacion no se obtiene, se vuelve a la busqueda
                    buscarEsteDispositivoBTLE( Utilidades.stringToUUID( DISPOSITIVO_UUID ) );
                }

            } // end run()
        }, 3000); // Se espera 3 segundos por defecto para la obtencion de la localizacion.
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Se comprueba el RSSI (valor fuerza señal) de la trama Beacon
     * y si supera un valor determinado, enviará una notificación
     * al usuario para informarle de que está lejos del sensor
     * --------------------------------------------------------------
     * RSSI:N , txPower:N --> estaLejosElBeacon()
     * --------------------------------------------------------------
     *  @param rssi    Valor de la fuerza de la señal recibida
     *  @param txPower Valor de la fuerza de la señal a 1 m de distancia
     */
    private void estaLejosElBeacon(int rssi, int txPower) {

        // Formula que obtiene la distancia del tx y del rssi
        // IMPORTANTE: El 6 de (10 * 6) es una constante que está referida al espacio donde esta el usuario.
        //             Con este valor la distancia tiene un error de centimetros en interior, pero deberia
        //             cambiar en el momento en el que el usuario sale a tomar medidas (comentar al grupo)

        Log.d(ETIQUETA_LOG,"estaLejosEElBeacon");
        
        double valorDistancia = Math.pow(10, ((double) txPower - rssi) / (10 * 6));

        Log.d("Test distancia", String.valueOf(valorDistancia));

        // Constante de la distancia a considerar informar al usuario de que tiene su sensor lejos
        double valorLargaDistancia = 3;

        if (valorDistancia > valorLargaDistancia){
                notificaciones.crearNotificacion(getString(R.string.beacon_lejano),getString(R.string.beacon_lejano_text), R.drawable.ic_ozono);
        }

    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Se compara el valor recibido del Beacon con la información
     * encontrada sobre los niveles de ozono en el aire establecido
     * por organismos nacionales, si es superior se avisa al usuario
     * --------------------------------------------------------------
     * medicion:N --> informarSiMedicionEsAlta()
     * --------------------------------------------------------------
     *  @param medicion  Valor de la medicion medido en ug/m3
     *
     */
    private void informarSiMedicionEsAlta(int medicion){
        if(estanActivadasLasNotificacionesDeDistancia()){
            if (medicion >= 240){
                //En este caso, cualquier persona esta respirando demasiado ozono, es peligroso
                notificaciones.crearNotificacion(getString(R.string.concentracion_alta),getString(R.string.concentracion_alta_text), R.drawable.ic_ozono);
            } else if (medicion >= 180 && medicion < 240){
                //En este caso, cualquier persona con problemas respiratorios puede tener problemas
                notificaciones.crearNotificacion(getString(R.string.concentracion_peligrosa),getString(R.string.concentracion_peligrosa_text), R.drawable.ic_ozono);
            } else {
                Log.d("Medida no peligrosa", "La medida tomada no resulta nociva");
            }
        }
    }

    private boolean estanActivadasLasNotificacionesDeDistancia(){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        boolean notificaciones  = sharedPref.getBoolean(Constantes.PREF_NOTIFICACIONES_DISTANCIA, true); // Indica que está logeado
        if(notificaciones){
            return true;
        }else{
            return false;
        }
    }

    // --------------------------------------------------------------
    // --------------------------------------------------------------
    public String obtenerMailUsuario() {
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);

        String correo  = sharedPref.getString(Constantes.PREF_CORREO, null); // Indica que está logeado

        if(correo != null){
            return correo;
        }
        return null;
    } // ()

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Espera una cantidad de tiempo en milisegundos
     * --------------------------------------------------------------
     * N --> esperar()
     * --------------------------------------------------------------
     * @param tiempo  Tiempo a esperar en ms
     */
    private void esperar(long tiempo){
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() { }
        }, tiempo); //()
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Inicia el escaneo de beacons BLE en la aplicacion
     * --------------------------------------------------------------
     * iniciarEscaneo()
     * --------------------------------------------------------------
     */
    private void iniciarEscaneo(){

        // Se actualiza el tiempo en el que se empieza a escanear beacons
        this.momentoUltimoBeacon = System.currentTimeMillis();

        // Se ejectua un 2o hilo, para contabilizar que el tiempo entre beacons
        ejecutableBLEBeaconsTiempo = new Runnable() {
            @Override
            public void run() {

                // Comprueba cuando fue el ultimo beacon valido en ser detectado
                centinelaBLE();
                manejadorBLEBeaconsTiempo.postDelayed(ejecutableBLEBeaconsTiempo, 1000); //Ejecuta cada segundo en bucle
            }
        };
        manejadorBLEBeaconsTiempo.post(ejecutableBLEBeaconsTiempo); // Inicia el 2o hilo


        //Busca beacons
        this.buscarEsteDispositivoBTLE( Utilidades.stringToUUID( this.DISPOSITIVO_UUID ) );

    } // ()

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     *  Comprueba en el instante actual cuanto tiempo hace que no se recibe
     *  un beacon valido, en caso de no recibir un beacon valido en dicho tiempo,
     *  detiene el buscador de beacons y lo vuelve a inciar, minimizando
     *  la perdida de beacons por casuistica de mala sincronizacion entre
     *  la busqueda y la emision de beacons
     * --------------------------------------------------------------
     * centinelaBLE()
     */
    private void centinelaBLE(){

        long actual = System.currentTimeMillis(); // Se actualiza el tiempo
        //comprobarBlueetooth(); // Se comprueba si el bluetooth sigue encendido

        if((actual -momentoUltimoBeacon )> TIEMPO_MAX_ENTRE_BEACONS){
            Log.d("CENTINLEA", "REINICIO");
            this.momentoUltimoBeacon = System.currentTimeMillis();
            this.detenerBusquedaDispositivosBTLE();
            // Reiniciar
            this.iniciarEscaneoSegundoPlano();
        }

    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Se suma un valor a el contador de minerales del usuario
     * --------------------------------------------------------------
     * valor:N --> sumarMinerales()
     * --------------------------------------------------------------
     *  @param valor  Valor de aumento de cristales
     *
     */
    private void sumarMinerales(int valor) {

        Log.d("Enviando minerales", String.valueOf(valor));

        SharedPreferences preferencias = servicioBTLE.getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO, MODE_PRIVATE);
        int contador = preferencias.getInt(Constantes.PREF_MINERALES, 30);
        SharedPreferences.Editor miEditor = preferencias.edit();
        miEditor.putInt(Constantes.PREF_MINERALES, contador+valor);

        Log.d("Contador minerales", String.valueOf(contador+valor));

        miEditor.commit();

    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Editamos los parámetros del sharedPreferences para cada una
     * de las recompensas que requieran saber cuantos ha obtenido
     * el usuario
     * --------------------------------------------------------------
     * sumarBeaconAlListado()
     * --------------------------------------------------------------
     */
    private void sumarBeaconAlListado(){
        SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE_RECOMPENSAS,MODE_PRIVATE);
        SharedPreferences.Editor miEditor = preferencias.edit();
        int tam = preferencias.getInt(Constantes.PREF_RECOMPENSAS_NUMERO, 0);

        for(int i = 0; i<tam; i++){
            if(preferencias.getString(Constantes.PREF_RECOMPENSA_TIPO+i, "").equals(Constantes.TIPO_RECOMPENSA_RECOGER_MEDIDAS)){
                int cantidadConseguida = preferencias.getInt(Constantes.PREF_RECOMPENSA_CANTIDAD_CONSEGUIDA+i, 0);
                int cantidadAConseguir = preferencias.getInt(Constantes.PREF_RECOMPENSA_CANTIDAD_A_CONSEGUIR+i, 0);
                if(cantidadAConseguir > cantidadConseguida){
                    cantidadConseguida++;
                    Log.d("beacon","cantidadConseguida "+cantidadConseguida);
                    miEditor.putInt(Constantes.PREF_RECOMPENSA_CANTIDAD_CONSEGUIDA+i,cantidadConseguida);
                    if(cantidadAConseguir == cantidadConseguida){
                        miEditor.putBoolean(Constantes.PREF_RECOMPENSA_SE_PUEDE_COBRAR+i, true);
                    }
                }
            }
        }

        miEditor.commit();
    }

    // *************************************************************************************************************************************************************************** //
    // ******************************************************************* CICLO VIDA ACTIVIDAD ********************************************************************************** //
    public int onStartCommand(Intent intent, int flags, int startID){
        Log.d(ETIQUETA_LOG,"onStartCommand()");
        super.onStartCommand(intent, flags, startID);
        return Service.START_STICKY;
    }

    public void onStop() {
        Log.i(ETIQUETA_LOG, "onStop()");
    }

    public void onPause() {
        Log.i(ETIQUETA_LOG, "onPause()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(ETIQUETA_LOG, "Servicio destruido"); }

    @Override
    public void onLowMemory() {
        Log.i(ETIQUETA_LOG, "onLowMemory()");
    }

    // *************************************************************************************************************************************************************************** //
    // ********************************************* CLASE BINDER PARA PODER ACCEDER A METODOS PÚBLICOS ************************************************************************** //

    // Binder given to clients
    private final IBinder binder = new ServicioSegundoPlanoBinder();

    /**
     * Clase usada por el cliente Binder. Muy importante para poder acceder a métodos públicos
     */
    public class ServicioSegundoPlanoBinder extends Binder {

        public ServicioBTLE getService() {
            // Return this instance of LocalService so clients can call public methods
            return ServicioBTLE.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(ETIQUETA_LOG, "Servicio onBind");
        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(ETIQUETA_LOG, "Servicio onUnBind");
        detenerBusquedaDispositivosBTLE();
        stopSelf();
        return false;
    }
}
