package org.g05.o3receptor.view.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.g05.o3receptor.R;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static android.view.View.TEXT_ALIGNMENT_CENTER;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerHolder> {
    private List<ItemList> items;
    private Context contexto;

    public RecyclerAdapter(List<ItemList> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public RecyclerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_view, parent, false);
        contexto = parent.getContext();
        return new RecyclerHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerHolder holder, int position) {
        ItemList item = items.get(position);
        holder.imgItem.setImageResource(item.getImgResource());
        holder.tvTitulo.setText(item.getTitulo());
        holder.tvDescripcion.setText(item.getDescripcion());
        holder.tvOtros.setText(item.getOtros());

        if(item.isRecompensaConseguida()){
            // Deshabilitamos el boton para que el usuario no pueda obtenerlo
            holder.botonCobrar.setText(contexto.getText(R.string.titulo_ya_cobrada));
            setearEstiloBoton(R.drawable.button_background_grey, false, holder);

        }else{
            holder.botonCobrar.setText(contexto.getText(R.string.titulo_cobrar)+" "+item.getRecompensa()+"");
            mostrarImagenMinerales(holder);

            // Miramos si puede obtener la recompensa o no
            if(item.isSePuedeCobrarLaRecompensa()){
                setearEstiloBoton(R.drawable.button_background_blue, true, holder);
                holder.botonCobrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.botonCobrar.setText(contexto.getText(R.string.titulo_ya_cobrada));
                        setearEstiloBoton(R.drawable.button_background_grey, false, holder);
                        anyadirMineralesMascota(item.getRecompensa());
                        modificarEstadoRecompensa(true, position);
                    }
                });
            }else{
                setearEstiloBoton(R.drawable.button_background_grey, false, holder);
            }
        }
    }

    // *************************************************************************************************************************************************************************** //
    // ************************************************************ MINERALES Y ESTADO RECOMPENSA ******************************************************************************** //

    /**
     * --------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------
     * Sumamos los minerales correspondientes a la tarea conseguida a la mascota
     * --------------------------------------------------------------------------------
     * recompensa: N --> anyadirMineralesMascota()
     * --------------------------------------------------------------------------------
     * @param recompensa
     */
    private void anyadirMineralesMascota(int recompensa){
        SharedPreferences preferencias = contexto.getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO, MODE_PRIVATE);
        int contador = preferencias.getInt(Constantes.PREF_MINERALES, 30);
        SharedPreferences.Editor miEditor = preferencias.edit();
        miEditor.putInt(Constantes.PREF_MINERALES, contador+recompensa);

        miEditor.commit();
    }

    /**
     * --------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------
     * Modificamos el estado de las recompensas en el sharedPreferences para que queden
     * como ya obtenidas. Esto hará que cando volvamos a entrar en la sección de
     * recompensas, no permita al usuario volverlas a recibir
     * --------------------------------------------------------------------------------
     * estado: VoF, pos: N --> modificarEstadoRecompensa()
     * --------------------------------------------------------------------------------
     * @param estado
     * @param pos
     */
    private void modificarEstadoRecompensa(boolean estado, int pos){
        SharedPreferences preferencias = contexto.getSharedPreferences(Constantes.PREF_NOMBRE_RECOMPENSAS, MODE_PRIVATE);
        SharedPreferences.Editor miEditor = preferencias.edit();
        miEditor.putBoolean(Constantes.PREF_RECOMPENSA_ESTA_CUMPLIDA+pos, estado);

        miEditor.commit();
    }

    // *************************************************************************************************************************************************************************** //
    // ******************************************************************** BOTÓN GANAR  ***************************************************************************************** //

    /**
     * --------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------
     * Seteamos el estilo para el botón de obtener
     * --------------------------------------------------------------------------------
     * tipoBonos: N, estaActivado: VoF, holder: RecyclerHolder --> setearEstiloBoton()
     * --------------------------------------------------------------------------------
     * @param tipoBoton
     * @param estaActivado
     * @param holder
     */
    private void setearEstiloBoton(int tipoBoton, boolean estaActivado, RecyclerHolder holder){
        holder.botonCobrar.setBackground(contexto.getResources().getDrawable(tipoBoton));
        holder.botonCobrar.setEnabled(estaActivado);
        holder.botonCobrar.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        holder.botonCobrar.setPadding(0,0,0,0);
    }

    /**
     * --------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------
     * Mostramos una pequeña inmagen de los minerales del juego en el boton para dar a
     * entender cual es la recompensa
     * --------------------------------------------------------------------------------
     * holder: RecyclerHolder --> mostrarImagenMinerales()
     * --------------------------------------------------------------------------------
     * @param holder
     */
    private void mostrarImagenMinerales(RecyclerHolder holder){
        Drawable img = contexto.getResources().getDrawable(R.drawable.mineral);
        img.setBounds(0, 0, 80, 80);
        holder.botonCobrar.setCompoundDrawables(null, null, img, null);
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    // *************************************************************************************************************************************************************************** //
    // ***************************************************************** CLASE RECYCLERHOLDER  *********************************************************************************** //

    public static class RecyclerHolder extends RecyclerView.ViewHolder{
        private ImageView imgItem;
        private TextView tvTitulo;
        private TextView tvDescripcion;
        private TextView tvOtros;
        private Button botonCobrar;

        public RecyclerHolder(@NonNull View itemView){
            super(itemView);
            imgItem = itemView.findViewById(R.id.imagenComida);
            tvTitulo = itemView.findViewById(R.id.comidanombre);
            tvDescripcion = itemView.findViewById(R.id.comidallenatxt);
            tvOtros = itemView.findViewById(R.id.comidallena);
            botonCobrar = itemView.findViewById(R.id.comidacoste);
        }
    }
}
