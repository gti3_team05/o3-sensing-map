package org.g05.o3receptor.model.repository.remote;


import android.os.AsyncTask;
import android.util.Log;

import org.g05.o3receptor.model.entity.Incidencia;
import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.ServicioBTLE;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Clase asincrona para enviar una incidencia a un servidor mediante una API de tipo REST
 * @author Laura Pons Castelló
 * @version 1.1
 */
public class Incidencias_post extends AsyncTask<String, Void, String> {
    private Incidencia i;

    private String ETIQUETA_LOG = "ENVIO INCIDENCIA";

    private String parametros;

    private final String recursoREST = "incidencias/";

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Constructor de la clase
     * --------------------------------------------------------------
     * EnvioIncidencias()
     * --------------------------------------------------------------
     */
    public Incidencias_post(Incidencia i){
        this.i=i;
    }


    @Override
    protected String doInBackground(String... strings) {
        String retorno = null;
        this.parametros = obtenerParametros(i);

        URL url;
        HttpURLConnection connection = null;

        // ---------------------------------------
        // Peticion POST datos en la URL para crear el usuario
        try {
            url = new URL(Constantes.API_URI + recursoREST + "?" + parametros);

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();


            // Se establece el tipo de la conexion
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Accept", "*/*");

            // Escribte los datos a enviar en la peticion REST de tipo POST
            connection.setDoOutput(true);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
            writer.write(parametros);
            writer.close();


            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }


            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.d("Inicio sesion", "Response: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }



        return retorno;
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Muestra un dialogo en pantalla para impedir la interaccion del usuario
     * mientras se están realizando peticiones al servidor
     * --------------------------------------------------------------
     * onPreExecute()
     * --------------------------------------------------------------
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Log.d("Peticion REST", "Enviando incidencia al servidor REST");

    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion para obtener de un usuario los parametros necesarios para
     * realizar la peticion REST
     * --------------------------------------------------------------
     *  Usuario --> obtenerParametros() --> String
     * --------------------------------------------------------------
     * @param i Usuario del que obtener los parametros de la peticion
     * @return retorna un Texto con los parametros de la peticion formateados
     */
    private String obtenerParametros(Incidencia i){

        //Se añaden los parametros del request a la API REST como clave Valor
        HashMap<String, Object> parametros = new HashMap<>();

        // Campos obligatorios
        parametros.put("correo", i.getCorreoIncidencia());
        parametros.put("estado", i.getEstadoIncidencia());
        parametros.put("tipoIncidencia", i.getTipoIncidencia());

        // Se recorre el objeto clave valor y se insertan los valores en un Texto
        Iterator it = parametros.entrySet().iterator();
        boolean primero = true;
        String parametrosString = "";
        while (it.hasNext()){
            if(primero){
                primero = false;
            } else{
                parametrosString +="&"; //Se separan los datos con el caracter '&'
            }

            Map.Entry pair = (Map.Entry)it.next();
            parametrosString +=pair.getKey()+"="+pair.getValue(); //A cada dato se le asigna el valor tras el caracter '='
        }

        //Se devuelven los datos con sus valores como Texto
        return parametrosString;

    }


    /** --------------------------------------------------------------
     * --------------------------------------------------------------
     * Cierra el dialogo que se muestra en pantalla
     * para devolver la visibilidad y el control de
     * interaccion al usuario
     * --------------------------------------------------------------
     * onPostExecute()
     * --------------------------------------------------------------
     */
    @Override
    protected void onPostExecute(String resultado) {

        super.onPostExecute(resultado);

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        // Se procesa el resultado

        boolean todoCorrecto = false;

        // Si el servidor devuelve un verdadero, se entiende que las operaciones han ido como se esperaba
        try {
            JSONObject resultadoJSON = new JSONObject(resultado);
            todoCorrecto = resultadoJSON.getBoolean("datos");

            // Log.d("INFO API", "JSON: "+resultadoJSON.toString());
            // Log.d("INFO API", "JSON: "+todoCorrecto);
            if(todoCorrecto){
                Log.d(ETIQUETA_LOG, "Incidencia enviada al servidor");
            }else{
                Log.d(ETIQUETA_LOG, "Incidencia no enviada, fallo en el servidor!");
            }

        } catch (JSONException ex){
            Log.e(ETIQUETA_LOG, "Error Api --> No se ha podido procesar el resultado como JSON --> resultado: "+resultado);
        } // Fin del procesado de la respuesta del servidor
    }

}