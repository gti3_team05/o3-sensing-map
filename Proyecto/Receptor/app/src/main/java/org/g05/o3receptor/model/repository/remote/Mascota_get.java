package org.g05.o3receptor.model.repository.remote;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.g05.o3receptor.view.util.juego.TareasCallbackjuego;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Mascota_get extends AsyncTask<Void, Void, String> {

    private final int CODIGO_PETICION = Constantes.PETICION_MASCOTA;

    private final String recursoREST = "mascota/";
    private String parametros;

    private String usuario;
    private TareasCallbackjuego callback;


    /**
     * Constructor de clase
     * mascota a solicitar
     */
    public Mascota_get(String usuario, TareasCallbackjuego callback){
        this.usuario = usuario;
        this.callback = callback;
    }


    @Override
    protected String doInBackground(Void... voids) {

        //se filtra por el mail
        String retorno = null;
        this.parametros = "usuario="+usuario;
        URL url;
        HttpURLConnection connection = null;

        try {
            // ---------------------------------------
            // Peticion GET datos en la URL

            url = new URL(Constantes.API_URI + recursoREST + "?" + parametros);
            //Log.d("test_mascotageturl", String.valueOf(url));
            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();
            // Se establece el tipo de la conexion
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "*/*");
            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }
            // Establece la conexion
            connection.connect();
            //Muestra por consola la respuesta
            Log.d("Inicio sesion", "Response: " + connection.getResponseMessage() + "");
        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }
        return retorno;
    }

    @Override
    protected void onPostExecute(String resultado) {
        super.onPostExecute(resultado);
        boolean todoCorrecto = false;

        JSONObject resultadoJSON = null;
        JSONArray datos = null;

        if(resultado!=null){
            try {
                resultadoJSON = new JSONObject(resultado);

                datos = resultadoJSON.getJSONArray("datos");
                if(datos.length()>0){
                    todoCorrecto = true;
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Log.d("estadoMascota",String.valueOf(e));
            }
            Log.d("test_getmascota",String.valueOf(datos));
        }else{
            todoCorrecto = false;
        }

        // Si el resultado es el esperado...
        if(todoCorrecto){
            callback.terminadoCorrectoConDatos(CODIGO_PETICION, datos,"0");
        } else{
            callback.terminadoFallo(CODIGO_PETICION);
        }

    }
}
