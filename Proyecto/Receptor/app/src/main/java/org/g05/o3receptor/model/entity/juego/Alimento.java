package org.g05.o3receptor.model.entity.juego;

import android.graphics.Bitmap;

public class Alimento extends JuegoItem {

    private int valorNutricional = 10;
    private int coste = 5;
    private Bitmap icono;
    private String nombre;

    public Alimento(boolean consumible, String nombre) {
        super(consumible, nombre);
    }

    public Alimento(boolean consumible, String nombre, int valorNutricional) {
        super(consumible, nombre);
        this.valorNutricional = valorNutricional;
    }


    public Alimento(boolean consumible, String nombre, Bitmap icono) {
        super(consumible, nombre, icono);
    }

    public Alimento(boolean consumible, String nombre, Bitmap icono, int valorNutricional, int coste) {
        super(consumible, nombre, icono);
        this.icono = icono;
        this.coste = coste;
        this.valorNutricional = valorNutricional;
        this.nombre = nombre;
    }

    public Alimento(boolean consumible, String nombre, int valorNutricional, int coste) {
        super(consumible, nombre);
        this.valorNutricional = valorNutricional;
        this.coste = coste;
        this.nombre = nombre;

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getValorNutricional() {
        return valorNutricional;
    }

    public Bitmap getIcono() { return icono; }

    public void setValorNutricional(int valorNutricional) {
        this.valorNutricional = valorNutricional;
    }


    public int getCoste() {
        return coste;
    }




}
