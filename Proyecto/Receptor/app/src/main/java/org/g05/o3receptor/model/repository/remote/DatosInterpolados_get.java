package org.g05.o3receptor.model.repository.remote;

import android.os.AsyncTask;
import android.os.Debug;
import android.util.Log;

import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.Console;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DatosInterpolados_get extends AsyncTask<Void, Void, String> {

    private final String recursoREST = "datosInterpolados/";
    private final int CODIGO_PETICION = Constantes.PETICION_INTERPOLACION;
    private TareaCallback callback;
    private String parametros;

    private String tipo ="";
    private String fecha ="";

    public DatosInterpolados_get(TareaCallback callback, String tipo, String fecha){
        this.callback = callback;
        this.tipo = tipo;
        this.fecha = fecha;
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion para obtener de un tipo de gas los parametros necesarios para
     * realizar la peticion REST
     * --------------------------------------------------------------
     * @return retorna un Texto con los parametros de la peticion formateados
     */
    private String obtenerParametros(){

        //Se añaden los parametros del request a la API REST como clave Valor
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("tipo", tipo);
        parametros.put("fecha", fecha);


        // Se recorre el objeto clave valor y se insertan los valores en un Texto
        Iterator it = parametros.entrySet().iterator();
        boolean primero = true;
        String parametrosString = "";
        while (it.hasNext()){
            if(primero){
                primero = false;
            } else{
                parametrosString +="&"; //Se separan los datos con el caracter '&'
            }

            Map.Entry pair = (Map.Entry)it.next();
            parametrosString +=pair.getKey()+"="+pair.getValue(); //A cada dato se le asigna el valor tras el caracter '='
        }

        //Se devuelven los datos con sus valores como Texto
        return parametrosString;

    }

    @Override
    protected String doInBackground(Void... voids) {
        Log.d("Datos Interpolados","dentro doInBackground");
        String retorno = null;
        this.parametros = obtenerParametros();

        URL url;
        HttpURLConnection connection = null;

        try {
            // ---------------------------------------
            // Peticion GET datos en la URL

            url = new URL(Constantes.API_URI + recursoREST + "?" + parametros);

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "*/*");


            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }

            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.d("Peticion Interpolacion", "Response: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.d("Peticion Interpolacion","Response "+e);
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }

        return retorno;
    }


    @Override
    protected void onPostExecute(String resultado) {

        super.onPostExecute(resultado);

        boolean todoCorrecto = false;

        String resultadoRecortado = null;
        try{
            resultadoRecortado = resultado.substring(resultado.indexOf('{'));
        }catch (Exception e){ }

        JSONArray datos = null;
        String imagenDatos = null;
        if(resultadoRecortado!=null){
            Log.d("Datos interpolados", resultadoRecortado);
            JSONObject resultadoJSON = null;

            try {
                resultadoJSON = new JSONObject(resultadoRecortado);
                Log.d("Datos interpolados", resultadoJSON.toString());

                //Si el usuario se ha encontrado, las credenciales son correctas
                datos = resultadoJSON.getJSONArray("datos");
                imagenDatos = resultadoJSON.getString("recursoExtra");
                if(datos.length()>0){ todoCorrecto = true;}
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } // end if

        // Si el resultado es el esperado...
        if(todoCorrecto){
            // Cambiamos la dirección de la imagen para poder obtenerla
            String direccion=imagenDatos.substring(imagenDatos.indexOf("app/") + 4);
            String direccionFinal = Constantes.APP_URI+direccion;
            // Ejecutamos otra petición asincrona de tipo get pasandole el callback, la dirección de la imagen y los datos
            DescargarImagen_get peticionImagen = new DescargarImagen_get(direccionFinal, datos, callback);
            peticionImagen.execute();
        } else{
            callback.terminadoFallo(CODIGO_PETICION);
        }


    } // ()



}
