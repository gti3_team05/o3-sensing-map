package org.g05.o3receptor.model.repository.remote;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;

import java.io.InputStream;

public class DescargarImagen_get extends AsyncTask<Void, Void, Bitmap> {
    private String url;
    private JSONArray datos;
    private TareaCallback callback;

    public DescargarImagen_get(String url, JSONArray datos, TareaCallback callback) {
        this.url = url;
        this.datos = datos;
        this.callback = callback;
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(url).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
            Log.e("DESCARGAR IMAGEN", "imagen obtenida con éxito");
        } catch (Exception e) {
            Log.e("DESCARGAR IMAGEN", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        // Retornamos el bitmap resultante así como la lista de datos que posteriormente usaremos para obtener el punto medio
        callback.terminadoCorrectoMapa(Constantes.PETICION_IMAGENES, datos, result,"mapa");
    }

}
