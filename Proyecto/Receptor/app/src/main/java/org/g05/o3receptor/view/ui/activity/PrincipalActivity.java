package org.g05.o3receptor.view.ui.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import org.g05.o3receptor.R;
import org.g05.o3receptor.view.ui.activity.ui.juego.JuegoActivity;
import org.g05.o3receptor.view.util.CambiarIdioma;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.ServicioSegundoPlano;

import java.util.Random;

public class PrincipalActivity extends AppCompatActivity {
    private static String ETIQUETA_LOG = "ACTIVIDAD PRINCIPAL";

    private final int SOLICITUD_PERMISO_LOCALIZACION = 99;
    private final int SOLICITUD_ENCENDIDO_BL = 98;

    private AppBarConfiguration mAppBarConfiguration;
    private NavController navController;

    private NavigationView navigationView;

    private View iconoMascota;
    private MenuItem itemAbrirJuego;
    private MenuItem itemAbrirRecompensas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CambiarIdioma.putSelectedLanguage(PrincipalActivity.this);
        setContentView(R.layout.activity_principal);

        iconoMascota = findViewById(R.id.perfil_mascota);

        if(estaElUsuarioLogueado()){
            iniciarToolbarLogueado();
        }else{
            iniciarToolbarSinLoguear();
        }
    }

    // *************************************************************************************************************************************************************************** //
    // ******************************************************************** BARRAS SUPERIORES  *********************************************************************************** //
    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Escondemos la toolbar que usamos para cuando no esté loguead
     * --------------------------------------------------------------
     * iniciarToolbarLogueado()
     * --------------------------------------------------------------
     * @return
     */
    private void iniciarToolbarLogueado(){
        findViewById(R.id.barraLogueado).setVisibility(View.VISIBLE);
        findViewById(R.id.barraSinLoguear).setVisibility(View.GONE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        cargarMenuNavegacion(toolbar);

        if(estaActivadaLaMacota()){
            iconoMascota.setVisibility(View.VISIBLE);
        }else{
            iconoMascota.setVisibility(View.GONE);
        }
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Escondemos la toolbar que usamos para cuando esté logueado
     * --------------------------------------------------------------
     * iniciarToolbarSinLoguear()
     * --------------------------------------------------------------
     * @return
     */
    private void iniciarToolbarSinLoguear(){
        findViewById(R.id.barraLogueado).setVisibility(View.GONE);
        findViewById(R.id.barraSinLoguear).setVisibility(View.VISIBLE);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        
        // Muy importante, esconder menu lateral
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Cargamos el menu de navegación, y añadimos un listener especial
     * para el boton de cerrar sesión ya que es el único que no redirige a otro fragment
     * simplemente cierra sesión
     * --------------------------------------------------------------
     * toolbar:Toolbar --> cargarMenuNavegacion()
     * --------------------------------------------------------------
     * @param toolbar
     * @return
     */
    private void cargarMenuNavegacion(Toolbar toolbar){
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.menu_avegacion);

        // Si está logueado, buscamos su usuario y mail para mostrarlos en el perfil
        rellenarNombreEmail(navigationView);

        // Ponemos un escuchador para el botón de cerrar sesión concreto
        navigationView.getMenu().findItem(R.id.nav_cerrarSesion).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                item.setCheckable(false);
                cerrarSesion();
                return true;
            }
        });

        // Incializamos la actividad de la mascota
        itemAbrirJuego = navigationView.getMenu().findItem(R.id.nav_mascota);
        itemAbrirRecompensas = navigationView.getMenu().findItem(R.id.nav_recompensas);

        if(estaActivadaLaMacota()){
            // Lo hacemos no checkable para que al volver de la actividad, no esté marcado el item como seleccionable mientras que estamos viendo otro fragment
            itemAbrirJuego.setCheckable(false);
            itemAbrirJuego.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    abrirSeccionJuego();
                    return true;
                }
            });
        }else{
            itemAbrirJuego.setVisible(false);
            itemAbrirRecompensas.setVisible(false);
        }


        MenuItem itemAjustes = navigationView.getMenu().findItem(R.id.nav_opciones);
        itemAjustes.setCheckable(false);
        itemAjustes.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                abrirAjustes();
                return true;
            }
        });

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(true);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_mapa)
                .setOpenableLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Rellenamos los textView del perfil del usuario en caso de que
     * esté registrado
     * --------------------------------------------------------------
     * rellenarNombreEmail()
     * --------------------------------------------------------------
     * @return
     */
    private void rellenarNombreEmail(NavigationView navigationView){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        String correo  = sharedPref.getString(Constantes.PREF_CORREO, null); // Indica que está logeado
        String nombre  = sharedPref.getString(Constantes.PREF_USUARIO, "usuario"); // Indica que está logeado

        View headerView = navigationView.getHeaderView(0);

        TextView nombreUsuario = (TextView) headerView.findViewById(R.id.tv_menu_nombre);
        TextView correoUsuario = (TextView) headerView.findViewById(R.id.tv_menu_mail);

        if(nombre == "usuario"){
            Random r = new Random();
            int i1 = r.nextInt(150 - 2) + 2;
            nombre = nombre+i1;
            editor.putString(Constantes.PREF_USUARIO, nombre);
            editor.commit();
        }
        if(nombreUsuario!=null && correoUsuario!=null){
            nombreUsuario.setText(nombre);
            correoUsuario.setText(correo);
        }
    }

    // *************************************************************************************************************************************************************************** //
    // ***************************************************************** ABRIR Y CERRAR ACTIVIDADES  ***************************************************************************** //

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Intent para abrir la sección de juego
     * --------------------------------------------------------------
     * v:View --> abrirSeccionJuego()
     * --------------------------------------------------------------
     * @param v
     * @return
     */
    public void abrirSeccionJuego(View v){
        // Se crea el intent para el cambio de actividad
        Intent i = new Intent(this, JuegoActivity.class);
        startActivity(i); // Se inicia la actividad
    }

    public void abrirSeccionJuego(){
        // Se crea el intent para el cambio de actividad
        Intent i = new Intent(this, JuegoActivity.class);
        startActivity(i); // Se inicia la actividad
    }

    public void abrirAjustes(){
        // Se crea el intent para el cambio de actividad
        Intent i = new Intent(this, AjustesActivity.class);
        startActivity(i); // Se inicia la actividad
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Intent para abrir el inicio de sesión
     * --------------------------------------------------------------
     * v:View --> abrirInicioSesion()
     * --------------------------------------------------------------
     * @param v
     * @return
     */
    public void abrirInicioSesion(View v){
        // Se crea el intent para el cambio de actividad
        Intent i = new Intent(this, InicioSesionActivity.class);
        startActivity(i); // Se inicia la actividad
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * 1. Borramos los valores del usuario guardades en las shared
     * preferences
     * 2. Detemos las ejecuciones en 2º plano
     * 3. Recargamos la actividad
     * --------------------------------------------------------------
     * cerrarSesion()
     * --------------------------------------------------------------
     * @return
     */
    public void cerrarSesion(){
        // Se borran los datos del shared preferences
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.remove(Constantes.PREF_CORREO);
        editor.remove(Constantes.PREF_CLAVE);
        editor.remove(Constantes.PREF_USUARIO);
        editor.remove(Constantes.PREF_APELLIDOS);
        editor.remove(Constantes.PREF_TELEFONO);
        editor.remove(Constantes.PREF_MODO_TESTER);

        editor.commit();

        // Se borran los datos del shared preferences de la mascota
        SharedPreferences sharedPrefMascota = getApplicationContext().getSharedPreferences(Constantes.PREF_MASCOTA, MODE_PRIVATE);
        SharedPreferences.Editor editorMascota = sharedPrefMascota.edit();

        editorMascota.clear();
        editorMascota.commit();

        // Detenemos los 2 servicios
        cerrarServicio(ServicioSegundoPlano.class);

        Intent i = new Intent(this, PrincipalActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK); // No permite volver a dicha actviad al usuario
        startActivity(i); // Se inicia la actividad

        finish();
    }

    // *************************************************************************************************************************************************************************** //
    // **************************************************************************** SERVICIOS  *********************************************************************************** //

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Dado un servicio, comprueba si está en ejecución y si lo
     * está, lo cierra
     * --------------------------------------------------------------
     * serviceClass: Class --> cerrarServicio()
     * --------------------------------------------------------------
     * @param serviceClass
     * @return
     */
    private void cerrarServicio(Class<?> serviceClass){
        if(estaSiendoEjecutadoElServicio(serviceClass)){
            Intent intentBuscarDispositivosBTLE = new Intent(getApplicationContext(), serviceClass);
            stopService(intentBuscarDispositivosBTLE);
        }
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Comprobamos si está siendo ejecutado un servicio determinado,
     * si lo está devuelve true, sino false
     * --------------------------------------------------------------
     * serviceClass:Class --> estaSiendoEjecutadoElServicio()
     * --------------------------------------------------------------
     * @param serviceClass
     * @return bool
     */
    private boolean estaSiendoEjecutadoElServicio(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Mirando en las sharedPreferences, podemos saber si el usuario está
     * logueado o no
     * --------------------------------------------------------------
     * estaElUsuarioLogueado()
     * --------------------------------------------------------------
     * @return boolean
     */
    private boolean estaElUsuarioLogueado(){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        String correo  = sharedPref.getString(Constantes.PREF_CORREO, null); // Indica que está logeado
        String clave  = sharedPref.getString(Constantes.PREF_CLAVE, null); // Indica que está logeado

        if(correo == null && clave == null){ // Está logeado
            return false;
        }else{
            return true;
        }
    }

    // *************************************************************************************************************************************************************************** //
    // ******************************************************************************** OTROS  *********************************************************************************** //

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Miramos mediante el sharedPreferences si el usuario quiere tener
     * mascota o no
     * --------------------------------------------------------------
     * estaActivadaLaMacota()
     * --------------------------------------------------------------
     * @return boolean
     */
    private boolean estaActivadaLaMacota(){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        boolean mascota  = sharedPref.getBoolean(Constantes.PREF_MASCOTA, true); // Indica que está logeado
        if(mascota){
            return true;
        }else{
            return false;
        }
    }

    private void actualizarMascota(){
        if(estaActivadaLaMacota()){
            itemAbrirJuego.setVisible(true);
            // Lo hacemos no checkable para que al volver de la actividad, no esté marcado el item como seleccionable mientras que estamos viendo otro fragment
            itemAbrirJuego.setCheckable(false);
            itemAbrirJuego.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    abrirSeccionJuego();
                    return true;
                }
            });

            itemAbrirRecompensas.setVisible(true);

            iconoMascota.setVisibility(View.VISIBLE);
        }else{
            itemAbrirJuego.setVisible(false);
            itemAbrirRecompensas.setVisible(false);
            iconoMascota.setVisibility(View.GONE);
        }
    }

    private boolean estaActivadoElSegundoPlano(){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        boolean segundoPlano  = sharedPref.getBoolean(Constantes.PREF_SEGUNDO_PLANO, true); // Indica que está logeado
        if(segundoPlano){
            return true;
        }else{
            return false;
        }
    }

    // *************************************************************************************************************************************************************************** //
    // ************************************************************************ CICLO DE VIDA  *********************************************************************************** //

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(!estaActivadoElSegundoPlano()){
            // Detenemos los 2 servicios
            cerrarServicio(ServicioSegundoPlano.class);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(estaElUsuarioLogueado()){
            rellenarNombreEmail(navigationView);
            actualizarMascota();
        }
    }
}