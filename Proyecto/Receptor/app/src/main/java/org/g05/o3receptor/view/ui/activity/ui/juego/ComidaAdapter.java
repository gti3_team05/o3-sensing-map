package org.g05.o3receptor.view.ui.activity.ui.juego;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import org.g05.o3receptor.R;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.RecyclerAdapter;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;
import static android.view.View.TEXT_ALIGNMENT_CENTER;

public class ComidaAdapter extends RecyclerView.Adapter<ComidaAdapter.comidaViewHolder> {
    private ArrayList<ComidaItem> mcomidaList;
    private Context contexto;
    private JuegoActivity activity;

    public static class comidaViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mTextView1;
        public TextView mTextView2;
        public Button botonComprar;

        public comidaViewHolder(View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.imagenComida);
            mTextView1 = itemView.findViewById(R.id.comidanombre);
            mTextView2 = itemView.findViewById(R.id.comidallena);
            botonComprar = itemView.findViewById(R.id.comidacoste);
        }
    }
    public ComidaAdapter(ArrayList<ComidaItem> comidaList, JuegoActivity activity) {
        mcomidaList = comidaList;
        this.activity = activity;
    }
    @Override
    public comidaViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.juego_comida_item, parent, false);
        comidaViewHolder evh = new comidaViewHolder(v);
        contexto = parent.getContext();
        return evh;
    }
    @Override
    public void onBindViewHolder(comidaViewHolder holder, int position) {
        ComidaItem currentItem = mcomidaList.get(position);
        holder.mImageView.setImageResource(currentItem.getImageResource());
        holder.mTextView1.setText(currentItem.getText1());
        holder.mTextView2.setText(currentItem.getText2());
        holder.botonComprar.setText(currentItem.getText3());

        if(sePuedeComprar(Integer.valueOf(currentItem.getText3()))){
            setearEstiloBoton(R.drawable.button_background_blue, true, holder.botonComprar);
            holder.botonComprar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activity.comprarcomida(position);
                    activity.actualizarRecyclerView();
                }
            });
        }else{
            setearEstiloBoton(R.drawable.button_background_grey, true, holder.botonComprar);
        }
        mostrarImagenMinerales(holder.botonComprar);
    }

    private void mostrarImagenMinerales(Button boton){
        Drawable img = contexto.getResources().getDrawable(R.drawable.mineral);
        img.setBounds(0, 0, 80, 80);
        boton.setCompoundDrawables(null, null, img, null);
    }

    private boolean sePuedeComprar(int coste){
        SharedPreferences preferencias = contexto.getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO,MODE_PRIVATE);
        int cantidadMinerales = preferencias.getInt(Constantes.PREF_MINERALES,30);
        if(coste <= cantidadMinerales){
            return true;
        }else{
            return false;
        }
    }

    private void setearEstiloBoton(int tipoBoton, boolean estaActivado, Button botonComprar){
        botonComprar.setBackground(contexto.getResources().getDrawable(tipoBoton));
        botonComprar.setEnabled(estaActivado);
        botonComprar.setTextAlignment(TEXT_ALIGNMENT_CENTER);
        botonComprar.setPadding(0,0,0,0);
    }

    @Override
    public int getItemCount() {
        return mcomidaList.size();
    }
}