package org.g05.o3receptor.model.entity.juego;

import android.graphics.Bitmap;

public class JuegoItem {

    private boolean consumible;
    private String nombre;
    private Bitmap icono;


    public JuegoItem(boolean consumible, String nombre) {
        this.consumible = consumible;
        this.nombre = nombre;
    }

    public JuegoItem(boolean consumible, String nombre, Bitmap icono) {
        this.consumible = consumible;
        this.nombre = nombre;
        this.icono = icono;
    }
}
