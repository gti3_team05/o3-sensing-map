package org.g05.o3receptor.model.repository.remote;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MedidasAlrededorUsuario_get extends AsyncTask<Void, Void, String> {

    private final int CODIGO_PETICION = Constantes.PETICION_DATOS_ALREDEDOR_USUARIO;
    private final String recursoREST = "medidasAlrededorUsuario/";
    private String parametros;

    private TareaCallback callback;
    private LatLng puntoUsuario;
    private ProgressDialog pd;

    /**
     * Constructor de clase por defecto
     *
     * @param puntoUsuario longitud y latitud del usuario
     * @param callback callback para ejectuar funciones en la actividad
     *                 que ejecuta la tarea asincrona
     */
    public MedidasAlrededorUsuario_get(LatLng puntoUsuario, TareaCallback callback){
        this.puntoUsuario = puntoUsuario;
        this.callback =  callback;
    }

    /**
     * Constructor de clase por defecto
     *
     * @param puntoUsuario
     * @param callback
     * @param pd
     */
    public MedidasAlrededorUsuario_get(LatLng puntoUsuario, TareaCallback callback, ProgressDialog pd){
        this.puntoUsuario = puntoUsuario;
        this.callback =  callback;
        this.pd = pd;
    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion para obtener de un usuario los parametros necesarios para
     * realizar la peticion REST
     * --------------------------------------------------------------
     *  Latlng --> obtenerParametros() --> String
     * --------------------------------------------------------------
     * @return retorna un Texto con los parametros de la peticion formateados
     */
    private String obtenerParametros(LatLng p){

        //Se añaden los parametros del request a la API REST como clave Valor
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("latitud", p.latitude);
        parametros.put("longitud", p.longitude);


        // Se recorre el objeto clave valor y se insertan los valores en un Texto
        Iterator it = parametros.entrySet().iterator();
        boolean primero = true;
        String parametrosString = "";
        while (it.hasNext()){
            if(primero){
                primero = false;
            } else{
                parametrosString +="&"; //Se separan los datos con el caracter '&'
            }

            Map.Entry pair = (Map.Entry)it.next();
            parametrosString +=pair.getKey()+"="+pair.getValue(); //A cada dato se le asigna el valor tras el caracter '='
        }

        //Se devuelven los datos con sus valores como Texto
        return parametrosString;

    }




    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if(pd != null){
            this.pd.show();
        }
    }


    @Override
    protected String doInBackground(Void... voids) {

        String retorno = null;
        this.parametros = obtenerParametros(puntoUsuario);

        URL url;
        HttpURLConnection connection = null;

        try {
            // ---------------------------------------
            // Peticion GET datos en la URL

            url = new URL(Constantes.API_URI + recursoREST + "?" + parametros);

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "*/*");



            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }


            // Establece la conexion
            connection.connect();

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }





        return retorno;
    }


    @Override
    protected void onPostExecute(String resultado) {
        super.onPostExecute(resultado);

        if(pd != null){
            this.pd.dismiss();
        }

        boolean todoCorrecto = false;

        JSONArray datos = null;
        if(resultado!=null){
            JSONObject resultadoJSON = null;

            try {
                resultadoJSON = new JSONObject(resultado);

                // Hay que tener en cuenta que datos puede estar vacio porque no haya ninguna medida tomada alrededor del usuario, por tanto,
                // los puntos que le aparecerán seran aleatorios 100%
                datos = resultadoJSON.getJSONArray("datos");
                todoCorrecto = true;
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } // end if

            // Si el resultado es el esperado...
            if(todoCorrecto){
                callback.terminadoCorrectoConDatos(CODIGO_PETICION, datos, "medidasAlrededorUsuario");
            } else{
                callback.terminadoFallo(CODIGO_PETICION);
            }


    } // ()



}
