package org.g05.o3receptor.view.ui.activity;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import org.g05.o3receptor.R;
import org.g05.o3receptor.view.util.Constantes;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        int splashTimeOut=5000;
        ImageView logo=(ImageView)findViewById(R.id.logo);
        TextView nombreApp = (TextView)findViewById(R.id.app_nombre);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = null;
                if(esLaPrimeraVezEnLaApp()){
                    i = new Intent(SplashActivity.this,BienvenidaActivity.class);
                }else{
                    i = new Intent(SplashActivity.this,PrincipalActivity.class);
                }
                startActivity(i);
                finish();
            }
        },splashTimeOut);

        Animation animacion = AnimationUtils.loadAnimation(this,R.anim.animacion_splash_screen);
        logo.startAnimation(animacion);
        nombreApp.startAnimation(animacion);

    }

    private boolean esLaPrimeraVezEnLaApp(){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);

        boolean res = sharedPref.getBoolean(Constantes.PREF_PRIMERA_VEZ_APP,true);
        return res;
    }
}