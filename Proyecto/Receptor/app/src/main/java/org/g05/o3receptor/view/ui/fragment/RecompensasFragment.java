package org.g05.o3receptor.view.ui.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.android.gms.maps.model.LatLng;

import org.g05.o3receptor.R;
import org.g05.o3receptor.model.entity.Medida;
import org.g05.o3receptor.model.entity.juego.Recompensa;
import org.g05.o3receptor.model.repository.remote.InicioSesion_get;
import org.g05.o3receptor.model.repository.remote.MedidasAlrededorUsuario_get;
import org.g05.o3receptor.model.repository.remote.MedidasTipoUsuarioTiempo_get;
import org.g05.o3receptor.view.ui.activity.InicioSesionActivity;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.ItemList;
import org.g05.o3receptor.view.util.RecyclerAdapter;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

import static android.content.Context.MODE_PRIVATE;

public class RecompensasFragment extends Fragment implements TareaCallback{
    private String ETIQUETA_LOG = "RECOMPENSAS FRAGMENT";

    private Activity actividadQueEjecuta;
    private View vistaRaiz;
    private Context contextoQueEjecuta;

    private RecyclerView rvLista;
    private RecyclerAdapter adapter;
    private List<ItemList> items;

    private List<Recompensa> recompensas;
    private LatLng puntoUsuario;
    private int cantidadRecompensasZonas;

    /**
     * Inicio de comprobaciones al crear la actividad y estar
     * infladas las vistas
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        inicializarBarra();

        generarRecomensasDiarias();
    }

    // *************************************************************************************************************************************************************************** //
    // ********************************************************* BARRA MENÚ ****************************************************************************************************** //

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Inicializamos la barra superior con la finalidad de que no se vea la flechita para volver
     * atrás, sino el botón del menú
     * --------------------------------------------------------------------------------------------
     * inicializarBarra()
     * --------------------------------------------------------------------------------------------
     */
    private void inicializarBarra(){
        AppCompatActivity appCompatActivity = (AppCompatActivity)getActivity();
        appCompatActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    // *************************************************************************************************************************************************************************** //
    // ********************************************************* INICIALIZACIÓN DEL RECYCLER VIEW  ******************************************************************************* //

    /**
     * ------------------------------------------------------
     * ------------------------------------------------------
     * Método para setear el recycler, es el encargado de:
     *  1. inicializar las vistas
     *  2. inicializar los valores
     * ------------------------------------------------------
     * setearRecycler()
     * ------------------------------------------------------
     */
    private void setearRecycler(){
        inicializarVistas();
        inicializarValores();
    }

    /**
     * ------------------------------------------------------
     * ------------------------------------------------------
     * Incializa la vista
     * ------------------------------------------------------
     * setearRecycler()
     * ------------------------------------------------------
     */
    private void inicializarVistas(){
        rvLista = vistaRaiz.findViewById(R.id.rvLista);
    }

    /**
     * -----------------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------------
     * Inicializza los valores de la lista cogiendo las recompensas posibles y convirtiendolas
     * a un objeto de tipo ItemList
     * -----------------------------------------------------------------------------------------
     * inicializarValores()
     * -----------------------------------------------------------------------------------------
     */
    private void inicializarValores(){
        LinearLayoutManager manager = new LinearLayoutManager(contextoQueEjecuta);
        rvLista.setLayoutManager(manager);

        items = getItems();
        adapter = new RecyclerAdapter(items);
        rvLista.setAdapter(adapter);
    }

    // *************************************************************************************************************************************************************************** //
    // ********************************************************* GENERAR RECOMPENSAS  ******************************************************************************************** //

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Generamos una lista de recompensas diarias, si:
     *  1. Ya se han generado recompensas hoy --> leemos los archivos que las contienen y
     *     actualizamos los parámetros de la calidad del aire para ver si se ha cumplido la
     *     recompensa
     *  2. Si no se han generado recompensas hoy --> en función de la ubicación del jugador
     *     generamos puntos en zonas con menor cantidad de forma aleatoria. Despúes generamos otros
     *     tipos de recompensa como la de la calidad de aire o la de la toma de medidas
     * --------------------------------------------------------------------------------------------
     * generarRecomensasDiarias()
     * --------------------------------------------------------------------------------------------
     */
    private void generarRecomensasDiarias(){
        // Miramos si hoy ya se han generado recompensas diarias
        if(yaSeHanGeneradoLasRecompensasDiarias()){
            recompensas = obtenerListaRecompensasGuardada();
            // Mirar la calidad de aire
            comprobarCalidadAire();
            //recompensas = generarListaRecompensasAleatoria();
        }else{
            // Generamos una lista aleatoria de recompensas
            recompensas = generarListaRecompensasAleatoria();
        }
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Generamos una lista de recompensas aleatorias obteniendo:
     *  1. Una cantidad de recompensas aleatorias para cada zona
     *  2. Recompensas de un tipo de gas determinado (no se puede repetir el gas) para la calidad
     *     del aire
     *  3. Recompensas del tipo de cantidad de medidas tomadas (con sus valores de recompensa
     *     en función de la distancia)
     * --------------------------------------------------------------------------------------------
     * generarListaRecompensasAleatoria() --> List<Recompensa>
     * --------------------------------------------------------------------------------------------
     * @return
     */
    private List<Recompensa> generarListaRecompensasAleatoria(){
        // Numero de cada tipo de recompensa
        cantidadRecompensasZonas = obtenerValorRandom(1,5);
        int cantidadRecompensasCalidadAire = obtenerValorRandom(1,3);
        int cantidadRecompensasCantidadMedidas = obtenerValorRandom(1,5);

        recompensas = new ArrayList<>();

        // Generamos la cantidad de recompensas en función de la calidad del aire
        for(int i=0; i<cantidadRecompensasCalidadAire; i++){
            // Generamos una recompensa aleatoria
            int valorRecompensa =  obtenerValorRandom(Constantes.valorMinimo,Constantes.valorMaximoCalidadAire);

            int valoresEncontrados = recompensas.size();
            String tipoGas = "";
            int valorMedioGas = 0;

            // NOTA: realizamos esto para evitar que genere retos del mismo tipo de gas
            if(valoresEncontrados != 0){
                while (valoresEncontrados != 0){
                    // Obtenemos el tipo de gas y su valor medio (es decir, si queremos una buena calidad de aire, el valor máximo que puede alcanzar)
                    tipoGas = obtenerTipoGasRandom(1,4);
                    valorMedioGas = obtenerValorMedioTipoGas(tipoGas);
                    for(int n=0; n<recompensas.size(); n++){
                        if(recompensas.get(n).getCantidadAConseguir() != valorMedioGas){
                            valoresEncontrados--;
                        }
                    }
                }
            }else{
                tipoGas = obtenerTipoGasRandom(1,4);
                valorMedioGas = obtenerValorMedioTipoGas(tipoGas);
            }

            Recompensa recompensa = new Recompensa(R.drawable.ic_recompensa_gauge, getResources().getString(R.string.titulo_calidad), getResources().getString(R.string.descripcion_calidad_p1)+" "+tipoGas+getResources().getString(R.string.descripcion_calidad_p2),Constantes.TIPO_RECOMPENSA_CALIDAD_AIRE, false, false, valorRecompensa, 0,valorMedioGas, null);
            recompensas.add(recompensa);
        }

        // Generamos la cantidad de recompensas en función del número de medidas
        for(int i=0; i<cantidadRecompensasCantidadMedidas; i++){
            int valorAleatorio =  obtenerValorRandom(Constantes.valorMinimo,Constantes.valorMinimoCantidadMedidas);
            int valorRecompensa =  realizarReglaDeTres(Constantes.valorMinimoCantidadMedidas, Constantes.valorMaximoMedidas, valorAleatorio);

            Recompensa recompensa = new Recompensa(R.drawable.ic_nodo_azul, getResources().getString(R.string.titulo_medidas), getResources().getString(R.string.descripcion_medidas), Constantes.TIPO_RECOMPENSA_RECOGER_MEDIDAS, false, false, valorRecompensa, 0,valorAleatorio, null);
            recompensas.add(recompensa);
        }

        // Obtenemos una sere de recompensas en función de la zona
        obtenerListaPuntosZonas();

        return recompensas;

    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Dada nuestra lista de recompensas, lo transforma a una lista de tipo ItemList para que pueda
     * ser usada posteriormente para el recyclerview
     * --------------------------------------------------------------------------------------------
     * getItems() --> List<ItemList>
     * --------------------------------------------------------------------------------------------
     * @return
     */
    private List<ItemList> getItems(){
        List<ItemList> itemLists = new ArrayList<>();

        for(int i=0; i<recompensas.size(); i++){
            Recompensa rec = recompensas.get(i);
            // En función del tipo de recompensa creamos un ItemList de un tipo u otro
            switch (rec.getTipo()){
                case Constantes.TIPO_RECOMPENSA_ZONA:
                    itemLists.add(new ItemList(rec.getTitulo(),rec.getDescripcion(), "", rec.getImagen(), rec.isEstaCumplida(), rec.isSePuedeCobrarLaRecompensa(), rec.getValorRecompensa()));
                    break;
                case Constantes.TIPO_RECOMPENSA_CALIDAD_AIRE:
                    itemLists.add(new ItemList(rec.getTitulo(),rec.getDescripcion(), "", rec.getImagen(), rec.isEstaCumplida(), rec.isSePuedeCobrarLaRecompensa(), rec.getValorRecompensa()));
                    break;
                case Constantes.TIPO_RECOMPENSA_RECOGER_MEDIDAS:
                    itemLists.add(new ItemList(rec.getTitulo(),rec.getDescripcion(), getString(R.string.titulo_conseguidos)+" "+rec.getCantidadObtenida()+" / "+rec.getCantidadAConseguir(), rec.getImagen(), rec.isEstaCumplida(), rec.isSePuedeCobrarLaRecompensa(), rec.getValorRecompensa()));
                    break;
            }
        }

        return itemLists;
    }

    // *************************************************************************************************************************************************************************** //
    // ******************************************************* ACTUALIZAR CALIDAD DEL AIRE  ************************************************************************************** //

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Una vez creada la lista de recompensas miramos la calidad de aire que está respirando el
     * usuario para ver si ha cumplido con el objetivo o no
     * --------------------------------------------------------------------------------------------
     * comprobarCalidadAire()
     * --------------------------------------------------------------------------------------------
     */
    private void comprobarCalidadAire(){
        // Miramos cuantas recompensas hay en total
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE_RECOMPENSAS,MODE_PRIVATE);
        int tam = preferencias.getInt(Constantes.PREF_RECOMPENSAS_NUMERO, 0);

        for(int i = 0; i<tam; i++){
            // Miramos si cada recompensa de la lista es del tipo Calidad de Aire
            if(preferencias.getString(Constantes.PREF_RECOMPENSA_TIPO+i, "").equals(Constantes.TIPO_RECOMPENSA_CALIDAD_AIRE)){
                // Obtenemos el tipo de gas
                String frase = preferencias.getString(Constantes.PREF_RECOMPENSA_DESCRIPCION+i, "");
                String tipoGas = "";
                if(frase.contains(Constantes.TIPO_GAS_O3)){
                    tipoGas = Constantes.TIPO_GAS_O3;
                }else if(frase.contains(Constantes.TIPO_GAS_CO)){
                    tipoGas = Constantes.TIPO_GAS_CO;
                }else if(frase.contains(Constantes.TIPO_GAS_NO2)){
                    tipoGas = Constantes.TIPO_GAS_NO2;
                }else if(frase.contains(Constantes.TIPO_GAS_SO2)){
                    tipoGas = Constantes.TIPO_GAS_SO2;
                }

                // Si aún no se ha conseguido la recompensa, solicitamos las medidas que ha tomado el usuario para ver si ha respirado buena o mala calidad de aire
                if(!preferencias.getBoolean(Constantes.PREF_RECOMPENSA_SE_PUEDE_COBRAR+i, false)){
                    solicitarMedidasSensorPorUsuarioYTiempo(tipoGas);
                }
            }
        }
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Hacemos una petición a la base de datos de las medidas recogidas HOY por el usuario
     * --------------------------------------------------------------------------------------------
     * tipoMedida: Texto --> solicitarMedidasSensorPorUsuarioYTiempo()
     * --------------------------------------------------------------------------------------------
     * @param tipoMedida
     */
    private void solicitarMedidasSensorPorUsuarioYTiempo(String tipoMedida){
        // Obtenemos los datos para solicitar las medidas
        String correoUsuario = obtenerCorreoUsuario();
        // Obtenemos la fecha de hoy
        Date date = new Date();
        String fecha = new SimpleDateFormat("yyyy-MM-dd").format(date);

        MedidasTipoUsuarioTiempo_get peticion = new MedidasTipoUsuarioTiempo_get(correoUsuario,tipoMedida,fecha +" 00:00:00", fecha +" 23:59:59",(TareaCallback)this, generarDialogoEspera(R.string.titulo_actualizando_recompensas, R.string.descripcion_actualizando_recompensas));
        peticion.execute();
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Obtenemos la dirección de correo electrónco del usuario registrado en la app
     * --------------------------------------------------------------------------------------------
     * obtenerCorreoUsuario()
     * --------------------------------------------------------------------------------------------
     * @return String --> correo electrónico del usuario registrado
     */
    private String obtenerCorreoUsuario(){
        // Obtenemos el ultimo contador, para que si por ejemplo cerramos la app, no se pierdan los datos
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        String correo = preferencias.getString(Constantes.PREF_CORREO,"null");

        return correo;
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Almacenamos en el sharedPreferences los nuevos valores de las cantidades conseguidas al hacer
     * la petición a la bd
     * --------------------------------------------------------------------------------------------
     * tipoMedida: Texto, valor: Z --> almacenarValorCalidadAire()
     * --------------------------------------------------------------------------------------------
     * @param tipoMedida
     * @param valor
     */
    private void almacenarValorCalidadAire(String tipoMedida, long valor){
        // Miramos cuantas recompensas hay en total
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE_RECOMPENSAS,MODE_PRIVATE);
        SharedPreferences.Editor miEditor = preferencias.edit();
        int tam = preferencias.getInt(Constantes.PREF_RECOMPENSAS_NUMERO, 0);

        // Recorremos la lista de recompensas
        for(int i = 0; i<tam; i++){
            // Si la recompensa es de tipo calidad de aire
            if(preferencias.getString(Constantes.PREF_RECOMPENSA_TIPO+i, "").equals(Constantes.TIPO_RECOMPENSA_CALIDAD_AIRE)){
                String frase = preferencias.getString(Constantes.PREF_RECOMPENSA_DESCRIPCION+i, "");
                // Miramos de que tipo de gas es (para no confundirlas)
                if(frase.contains(tipoMedida)){
                    // Si el valor obtenido es BAJO (menor que el valor medio) se da la recompensa como conseguida
                    if(valor > 0 && valor < preferencias.getInt(Constantes.PREF_RECOMPENSA_CANTIDAD_A_CONSEGUIR+i,0)){
                        miEditor.putBoolean(Constantes.PREF_RECOMPENSA_SE_PUEDE_COBRAR+i, true);
                    }
                    // Almacenamos el último valor visto
                    miEditor.putInt(Constantes.PREF_RECOMPENSA_CANTIDAD_CONSEGUIDA+i, (int) valor);
                }
            }
        }

        miEditor.commit();
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Promediamos todas las medidas obtenidas por un usuario del día concreto
     * --------------------------------------------------------------------------------------------
     * datos: JSONArray --> promediarMedidasDiarias()
     * --------------------------------------------------------------------------------------------
     * @param datos
     * @return
     */
    private long promediarMedidasDiarias(JSONArray datos){
        double medida = 0;
        int tam = datos.length();

        for(int i=0; i<tam; i++){
            try {
                medida += datos.getJSONObject(i).getInt("valor");
            }
            catch (JSONException e) {
                Log.d(ETIQUETA_LOG,String.valueOf(e));
            }
        }

        return Math.round(medida/tam);
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Dado un tipo de gas, nos dice cual es su valor medio
     * --------------------------------------------------------------------------------------------
     * tipo: Texto --> obtenerValorMedioTipoGas() --> N
     * --------------------------------------------------------------------------------------------
     * @param tipo
     * @return
     */
    private int obtenerValorMedioTipoGas(String tipo){
        switch (tipo){
            case Constantes.TIPO_GAS_O3:
                return Constantes.VALOR_MEDIO_O3;
            case Constantes.TIPO_GAS_SO2:
                return Constantes.VALOR_MEDIO_SO2;
            case Constantes.TIPO_GAS_NO2:
                return Constantes.VALOR_MEDIO_NO2;
            case Constantes.TIPO_GAS_CO:
                return Constantes.VALOR_MEDIO_CO;
        }
        return 0;
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Dados todos los posibles tipos de gas, devuelve uno de forma aleatoria
     * --------------------------------------------------------------------------------------------
     * minimo:N, maximo:N --> obtenerValorMedioTipoGas() --> Texto
     * --------------------------------------------------------------------------------------------
     * @param minimo
     * @param maximo
     * @return
     */
    private String obtenerTipoGasRandom(int minimo, int maximo){
        int numeroGas =  obtenerValorRandom(minimo, maximo);
        switch (numeroGas){
            case 1:
                return Constantes.TIPO_GAS_O3;
            case 2:
                return Constantes.TIPO_GAS_SO2;
            case 3:
                return Constantes.TIPO_GAS_NO2;
            case 4:
                return Constantes.TIPO_GAS_CO;
        }
        return "";
    }

    // *************************************************************************************************************************************************************************** //
    // ************************************************************** OBTENER ZONAS  ********************************************************************************************* //

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Obtenemos la última posición vista del usuario y en función de eso hacemos una petición
     * a la bd para obtener la lista de medidas que se corresponden a esa zona
     * --------------------------------------------------------------------------------------------
     * obtenerListaPuntosZonas()
     * --------------------------------------------------------------------------------------------
     */
    private void obtenerListaPuntosZonas(){
        puntoUsuario = obtenerPosicionUsuario();

        MedidasAlrededorUsuario_get peticion = new MedidasAlrededorUsuario_get(puntoUsuario, (TareaCallback)this, generarDialogoEspera(R.string.titulo_generando_recompensas, R.string.descripcion_generando_recompensas));
        peticion.execute();
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Generamos un punto (latitud y longitud) aleatorio teniendo en cuenta el radio del usuario a
     * partir del cual se buscan (2km)
     * --------------------------------------------------------------------------------------------
     * obtenerPuntoRandom() --> LatLng
     * --------------------------------------------------------------------------------------------
     */
    private LatLng obtenerPuntoRandom(){
        double latRandom = obtenerValorRandom(puntoUsuario.latitude-0.02, puntoUsuario.latitude+0.02);
        double lonRandom = obtenerValorRandom(puntoUsuario.longitude-0.008, puntoUsuario.longitude+0.008);

        return new LatLng(latRandom, lonRandom);
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Obtenemos la última posición vista del usuario
     * --------------------------------------------------------------------------------------------
     * obtenerPosicionUsuario() --> LatLng
     * --------------------------------------------------------------------------------------------
     * @return
     */
    private LatLng obtenerPosicionUsuario(){
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        double lat = Double.longBitsToDouble(preferencias.getLong(Constantes.PREF_LATITUD_USUARIO, Double.doubleToLongBits(38.97462)));
        double lon = Double.longBitsToDouble(preferencias.getLong(Constantes.PREF_LONGITUD_USUARIO, Double.doubleToLongBits(-0.17990)));
        LatLng posicion = new LatLng(lat, lon);

        return posicion;
    }

    // *************************************************************************************************************************************************************************** //
    // ********************************************** MÉTODOS ASÍNCRONOS - CALIDAD AIRE Y ZONAS  ********************************************************************************* //

    @Override
    public void terminadoCorrectoConDatos(int codigoSolicitud, JSONArray datos, String tipoDatos) {
        if(tipoDatos == "medidasAlrededorUsuario" && codigoSolicitud == Constantes.PETICION_DATOS_ALREDEDOR_USUARIO){
            List<LatLng> puntosRecompensas = new ArrayList<>();
            while (puntosRecompensas.size() != cantidadRecompensasZonas){
                int cantidadCoincidencias = datos.length();

                LatLng puntoRandom = null;
                // Miramos si la lista es distinta de 0 porque si es 0 significa que no se ha encontrado ninguna medición en esa zona y por tanto
                // no se ha de mirar si se cumple la distancia entre puntos
                if(cantidadCoincidencias == 0){
                    puntoRandom = obtenerPuntoRandom();
                }else{
                    while (cantidadCoincidencias != 0){
                        // Creamos una medida random
                        puntoRandom = obtenerPuntoRandom();
                        for(int i=0; i<datos.length(); i++){
                            // Comparamos las latitudes y longitudes para que se lleve al usuario a puntos con menor cantidad de medidas
                            try{
                                double lat = datos.getJSONObject(i).getDouble("latitud");
                                double lon = datos.getJSONObject(i).getDouble("longitud");
                                double latMin = lat-0.00005; double latMax = lat+0.00005;
                                double lonMin = lon-0.00013; double lonMax = lon+0.00013;
                                if((puntoRandom.latitude > latMax || puntoRandom.latitude < latMin) && (puntoRandom.longitude > lonMax || puntoRandom.longitude < lonMin)){
                                    cantidadCoincidencias--;
                                }
                            }catch (Exception e){
                                Log.d(ETIQUETA_LOG,"Error");
                            }
                        }
                    }
                }
                puntosRecompensas.add(puntoRandom);
            }

            // Generamos la cantidad de recompensas en función de la zona
            for(int i=0; i<cantidadRecompensasZonas; i++){
                int valorRecompensa =  realizarReglaDeTres(puntoUsuario.latitude+0.02, Constantes.valorMaximoZona, puntosRecompensas.get(i).latitude);
                Recompensa recompensa = new Recompensa(R.drawable.ic_recompensa_zona, getResources().getString(R.string.titulo_zona), getResources().getString(R.string.descripcion_zona), Constantes.TIPO_RECOMPENSA_ZONA, false, false, valorRecompensa, 0,1, puntosRecompensas.get(i));
                recompensas.add(recompensa);
            }

            // Guardamos en el shared preferences la lista para poder obtenerla después
            guardarListaRecompensas(recompensas);

        }else if(codigoSolicitud == Constantes.PETICION_MEDIDAS_TIPO_USUARIO_TIEMPO){
            Log.d(ETIQUETA_LOG,"Terminado correcto --> "+datos);
            // Obtenemos el tipo de medida
            String tipoMedida = "";
            try {
                tipoMedida = datos.getJSONObject(0).getString("tipoMedida");
            }
            catch (JSONException e) {
                Log.d(ETIQUETA_LOG,String.valueOf(e));
            }

            // Obtenemos el promediado de la calidad de aire diária
            long diaPromediado = promediarMedidasDiarias(datos);
            // Almacenamos el valor de la calidad de aire
            almacenarValorCalidadAire(tipoMedida, diaPromediado);
        }
        // Actualizamos el recyclerView
        setearRecycler();
    }

    @Override
    public void terminadoFallo(int codigoSolicitud) {
        if(codigoSolicitud == Constantes.PETICION_DATOS_ALREDEDOR_USUARIO){
            setearRecycler();
        }else if(codigoSolicitud == Constantes.PETICION_MEDIDAS_TIPO_USUARIO_TIEMPO){
            setearRecycler();
        }
    }

    // *************************************************************************************************************************************************************************** //
    // ********************************************************* MÉTODOS GENERALES  ********************************************************************************************** //

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Realizamos una regla de 3 para obener los valores de la recompensa
     * --------------------------------------------------------------------------------------------
     * valorMax:N, equivalenciaValorMax:N, valorActual:N --> realizarReglaDeTres() --> N
     * --------------------------------------------------------------------------------------------
     * @param valorMax
     * @param equivalenciaValorMax
     * @param valorActual
     * @return
     */
    private int realizarReglaDeTres(int valorMax, int equivalenciaValorMax, int valorActual){
        int resultado = Math.round((valorActual*equivalenciaValorMax)/valorMax);
        return resultado;
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Realizamos una regla de 3 para obener los valores de la recompensa
     * --------------------------------------------------------------------------------------------
     * valorMax:R, equivalenciaValorMax:N, valorActual:R --> realizarReglaDeTres() --> N
     * --------------------------------------------------------------------------------------------
     * @param valorMax
     * @param equivalenciaValorMax
     * @param valorActual
     * @return
     */
    private int realizarReglaDeTres(double valorMax, int equivalenciaValorMax, double valorActual){
        int resultado = (int) Math.round((valorActual*equivalenciaValorMax)/valorMax);
        return resultado;
    }


    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Dado un valor máximo y uno mínimo devuelve un valor aleatorio
     * --------------------------------------------------------------------------------------------
     * minimo:N, maximo:N --> obtenerValorMedioTipoGas() --> N
     * --------------------------------------------------------------------------------------------
     * @param minimo
     * @param maximo
     * @return
     */
    private int obtenerValorRandom(int minimo, int maximo){
        Random r = new Random();
        int valor = r.nextInt(maximo-minimo) + minimo;
        return valor;
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Dado un valor máximo y uno mínimo devuelve un valor aleatorio
     * --------------------------------------------------------------------------------------------
     * minimo:Z, maximo:Z --> obtenerValorMedioTipoGas() --> Z
     * --------------------------------------------------------------------------------------------
     * @param minimo
     * @param maximo
     * @return
     */
    private double obtenerValorRandom(double minimo, double maximo){
        double valor = minimo + (Math.random() * (maximo - minimo));
        return valor;
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Miramos si ya se han generado las recompensas de hoy
     * --------------------------------------------------------------------------------------------
     * obtenerValorMedioTipoGas() --> Z
     * --------------------------------------------------------------------------------------------
     * @return
     */
    private boolean yaSeHanGeneradoLasRecompensasDiarias(){
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE_RECOMPENSAS,MODE_PRIVATE);

        // Miramos si ya se ha cambiado de día para renovar las recompensas
        int diaActual = getDiaActual();
        int diaAlmacenado = preferencias.getInt(Constantes.PREF_RECOMPENSA_DIARIA, 0);
        if(diaActual != diaAlmacenado){
            return false;
        }else{
            return true;
        }
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Obtenemos el día actual
     * --------------------------------------------------------------------------------------------
     * getDiaActual() --> N
     * --------------------------------------------------------------------------------------------
     * @return
     */
    private int getDiaActual(){
        Calendar fecha = new GregorianCalendar();
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        return dia;
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Función que genera un dialogo que se muestra en pantalla para dar información al usuario
     * mientras se realizan tareas en 2º plano
     * --------------------------------------------------------------------------------------------
     * generarDialogoEspera() --> ProgressDialog
     * --------------------------------------------------------------------------------------------
     * @return
     */
    private ProgressDialog generarDialogoEspera(int titutlo, int mensaje){
        ProgressDialog pd = new ProgressDialog(contextoQueEjecuta);
        pd.setTitle(titutlo);
        pd.setMessage(getString(mensaje));
        pd.setCancelable(false);

        return pd;
    } // ()

    // *************************************************************************************************************************************************************************** //
    // ********************************************************* GUARDAR Y LEER LISTAS RECOMPENSAS ******************************************************************************* //

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Guardamos en el sharedPreferences todos los parámetros de las recompensas
     * --------------------------------------------------------------------------------------------
     * listaRecom:Lista<Recompensa> --> guardarListaRecompensas()
     * --------------------------------------------------------------------------------------------
     * @param listaRecom
     */
    private void guardarListaRecompensas(List<Recompensa> listaRecom){
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE_RECOMPENSAS,MODE_PRIVATE);
        SharedPreferences.Editor miEditor = preferencias.edit();
        miEditor.clear();

        int diaActual = getDiaActual();
        miEditor.putInt(Constantes.PREF_RECOMPENSA_DIARIA, diaActual);
        miEditor.putInt(Constantes.PREF_RECOMPENSAS_NUMERO, listaRecom.size());

        for(int i=0; i<listaRecom.size(); i++){
            Recompensa recom = listaRecom.get(i);
            miEditor.putInt(Constantes.PREF_RECOMPENSA_IMAGEN+i,recom.getImagen());
            miEditor.putString(Constantes.PREF_RECOMPENSA_TITULO+i, recom.getTitulo());
            miEditor.putString(Constantes.PREF_RECOMPENSA_DESCRIPCION+i, recom.getDescripcion());
            miEditor.putString(Constantes.PREF_RECOMPENSA_TIPO+i, recom.getTipo());
            miEditor.putBoolean(Constantes.PREF_RECOMPENSA_ESTA_CUMPLIDA+i, recom.isEstaCumplida());
            miEditor.putBoolean(Constantes.PREF_RECOMPENSA_SE_PUEDE_COBRAR+i, recom.isSePuedeCobrarLaRecompensa());
            miEditor.putInt(Constantes.PREF_RECOMPENSA_VALOR+i,recom.getValorRecompensa());
            miEditor.putInt(Constantes.PREF_RECOMPENSA_CANTIDAD_CONSEGUIDA+i,recom.getCantidadObtenida());
            miEditor.putInt(Constantes.PREF_RECOMPENSA_CANTIDAD_A_CONSEGUIR+i,recom.getCantidadAConseguir());
            if(recom.getLocalizacionPunto() != null){
                miEditor.putLong(Constantes.PREF_RECOMPENSA_LATITUD+i, Double.doubleToRawLongBits(recom.getLocalizacionPunto().latitude));
                miEditor.putLong(Constantes.PREF_RECOMPENSA_LONGITUD+i, Double.doubleToRawLongBits(recom.getLocalizacionPunto().longitude));
            }
        }

        miEditor.commit();
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Leemos en el sharedPreferences todos los parámetros de las recompensas
     * --------------------------------------------------------------------------------------------
     * obtenerListaRecompensasGuardada() --> Lista<Recompensa>
     * --------------------------------------------------------------------------------------------
     * @return
     */
    private List<Recompensa> obtenerListaRecompensasGuardada(){
        List<Recompensa> recom = new ArrayList<>();

        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE_RECOMPENSAS,MODE_PRIVATE);
        int tamanyoLista = preferencias.getInt(Constantes.PREF_RECOMPENSAS_NUMERO, 0);

        for(int i=0; i<tamanyoLista; i++){
            int imagen = preferencias.getInt(Constantes.PREF_RECOMPENSA_IMAGEN+i,0);
            String titulo = preferencias.getString(Constantes.PREF_RECOMPENSA_TITULO+i, null);
            String descripcion = preferencias.getString(Constantes.PREF_RECOMPENSA_DESCRIPCION+i, null);
            String tipo = preferencias.getString(Constantes.PREF_RECOMPENSA_TIPO+i, null);
            boolean estaCumplida = preferencias.getBoolean(Constantes.PREF_RECOMPENSA_ESTA_CUMPLIDA+i, false);
            boolean sePuedeCobrar = preferencias.getBoolean(Constantes.PREF_RECOMPENSA_SE_PUEDE_COBRAR+i, false);
            int valor = preferencias.getInt(Constantes.PREF_RECOMPENSA_VALOR+i,0);
            int cantidadConseguida = preferencias.getInt(Constantes.PREF_RECOMPENSA_CANTIDAD_CONSEGUIDA+i,0);
            int cantidadAConseguir = preferencias.getInt(Constantes.PREF_RECOMPENSA_CANTIDAD_A_CONSEGUIR+i,0);
            double latitud = Double.longBitsToDouble(preferencias.getLong(Constantes.PREF_RECOMPENSA_LATITUD+i, 0));
            double longitud = Double.longBitsToDouble(preferencias.getLong(Constantes.PREF_RECOMPENSA_LONGITUD+i, 0));

            recom.add(new Recompensa(imagen, titulo, descripcion, tipo, estaCumplida, sePuedeCobrar, valor, cantidadConseguida, cantidadAConseguir, new LatLng(latitud, longitud)));
        }

        return recom;
    }

    // *************************************************************************************************************************************************************************** //
    // ****************************************************************** CICLO VIDA  ******************************************************************************************** //

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_recompensas, container, false);
        this.vistaRaiz = root;

        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.actividadQueEjecuta = getActivity(); // Obtengo la actividad desde la que se ejecuta el fragment
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.contextoQueEjecuta = context; // Obtengo el contexto desde la que se ejecuta el fragment

    }

    @Override
    public void iniciado(int codigoSolicitud) {

    }

    @Override
    public void terminadoCorrecto(int codigoSolicitud) {

    }


    @Override
    public void terminadoCorrectoMapa(int codigoSolicitud, JSONArray datos, Bitmap imagen, String tipoDatos) {

    }

    @Override
    public void terminadoCorrectoConDatosEstaciones(int codigoSolicitud, JSONArray datos, List<Medida> datosPuntos, String tipoDatos) {

    }
}