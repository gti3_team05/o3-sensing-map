package org.g05.o3receptor.model.repository.remote;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import org.g05.o3receptor.model.entity.Medida;
import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class InicioSesion_get extends AsyncTask<Void, Void, String> {

    private final int CODIGO_PETICION = Constantes.PETICION_INICIO_SESION;
    private final String recursoREST = "inicioSesion/";
    private String parametros;

    private TareaCallback callback;
    private Usuario usuario;
    private ProgressDialog pd;

    /**
     * Constructor de clase por defecto
     *
     * @param usuario usuario que trata de iniciar la sesion
     * @param callback callback para ejectuar funciones en la actividad
     *                 que ejecuta la tarea asincrona
     */
    public InicioSesion_get(Usuario usuario, TareaCallback callback){
        this.usuario = usuario;
        this.callback =  callback;
    }

    /**
     * Constructor de clase con dialogo para el usuario
     * @param usuario usuario que trata de iniciar sesion
     * @param callback callback para ejectuar funciones en la actividad
     *                 que ejecuta la tarea asincrona
     * @param pd dialogo de progresion del usuairo
     */
    public InicioSesion_get(Usuario usuario, TareaCallback callback, ProgressDialog pd){
        this.usuario = usuario;
        this.pd = pd;
        this.callback =  callback;
    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion para obtener de un usuario los parametros necesarios para
     * realizar la peticion REST
     * --------------------------------------------------------------
     *  Usuario --> obtenerParametros() --> String
     * --------------------------------------------------------------
     * @param u Usuario del que obtener los parametros de la peticion
     * @return retorna un Texto con los parametros de la peticion formateados
     */
    private String obtenerParametros(Usuario u){

        //Se añaden los parametros del request a la API REST como clave Valor
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("correo", u.getCorreo());
        parametros.put("clave", u.getClave());


        // Se recorre el objeto clave valor y se insertan los valores en un Texto
        Iterator it = parametros.entrySet().iterator();
        boolean primero = true;
        String parametrosString = "";
        while (it.hasNext()){
            if(primero){
                primero = false;
            } else{
                parametrosString +="&"; //Se separan los datos con el caracter '&'
            }

            Map.Entry pair = (Map.Entry)it.next();
            parametrosString +=pair.getKey()+"="+pair.getValue(); //A cada dato se le asigna el valor tras el caracter '='
        }

        //Se devuelven los datos con sus valores como Texto
        return parametrosString;

    }




    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        this.pd.show();
    }


    @Override
    protected String doInBackground(Void... voids) {

        String retorno = null;
        this.parametros = obtenerParametros(usuario);

        URL url;
        HttpURLConnection connection = null;

        try {
            // ---------------------------------------
            // Peticion GET datos en la URL

            url = new URL(Constantes.API_URI + recursoREST + "?" + parametros);

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "*/*");



            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }


            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.d("Inicio sesion", "Response: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }





        return retorno;
    }


    @Override
    protected void onPostExecute(String resultado) {
        super.onPostExecute(resultado);

        this.pd.dismiss();

        boolean todoCorrecto = false;

        JSONArray datos = null;
        if(resultado!=null){
            JSONObject resultadoJSON = null;

            try {
                resultadoJSON = new JSONObject(resultado);

                //Si el usuario se ha encontrado, las credenciales son correctas
                datos = resultadoJSON.getJSONArray("datos");
                if(datos.length()>0){ todoCorrecto = true;}
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } // end if

            // Si el resultado es el esperado...
            if(todoCorrecto){
                callback.terminadoCorrectoConDatos(CODIGO_PETICION, datos, "usuario");
            } else{
                callback.terminadoFallo(CODIGO_PETICION);
            }


    } // ()



}
