package org.g05.o3receptor.model.repository.remote;


import android.os.AsyncTask;
import android.util.Log;

import org.g05.o3receptor.model.entity.Incidencia;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Clase asincrona para crear un token de recuperacion mediante una API de tipo REST
 * @author Adrián García ANdreu
 * @version 1.1
 */
public class Recuperaciones_put extends AsyncTask<String, Void, String> {
    private String correo;
    private TareaCallback tareaCallback;

    private String ETIQUETA_LOG = "RECUPERACION ";
    private String parametros;

    private final String recursoREST = "recuperaciones/";

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Constructor de la clase
     * --------------------------------------------------------------
     * EnvioIncidencias()
     * --------------------------------------------------------------
     */
    public Recuperaciones_put(TareaCallback tareaCallback, String correo){
        this.correo=correo;
        this.tareaCallback = tareaCallback;
    }


    @Override
    protected String doInBackground(String... strings) {
        String retorno = null;
        this.parametros = obtenerParametros();

        URL url;
        HttpURLConnection connection = null;

        // ---------------------------------------
        // Peticion POST datos en la URL para crear el usuario
        try {
            url = new URL(Constantes.API_URI + recursoREST+"?"+parametros);

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();


            // Se establece el tipo de la conexion
            connection.setDoOutput(true);
            connection.setRequestMethod("PUT");
            connection.setRequestProperty("Content-Type", "text/plain");
            connection.setRequestProperty("Accept", "*/*");


            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }


            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.d("Inicio sesion", "Response: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }



        return retorno;
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Muestra un dialogo en pantalla para impedir la interaccion del usuario
     * mientras se están realizando peticiones al servidor
     * --------------------------------------------------------------
     * onPreExecute()
     * --------------------------------------------------------------
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        Log.d("Peticion REST", "Creacion TOKEN REST");

    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion para obtener de un usuario los parametros necesarios para
     * realizar la peticion REST
     * --------------------------------------------------------------
     *  Usuario --> obtenerParametros() --> String
     * --------------------------------------------------------------
     * @return retorna un Texto con los parametros de la peticion formateados
     */
    private String obtenerParametros(){

        //Se añaden los parametros del request a la API REST como clave Valor
        HashMap<String, Object> parametros = new HashMap<>();

        // Campos obligatorios
        parametros.put("correo", this.correo);

        // Se recorre el objeto clave valor y se insertan los valores en un Texto
        Iterator it = parametros.entrySet().iterator();
        boolean primero = true;
        String parametrosString = "";
        while (it.hasNext()){
            if(primero){
                primero = false;
            } else{
                parametrosString +="&"; //Se separan los datos con el caracter '&'
            }

            Map.Entry pair = (Map.Entry)it.next();
            parametrosString +=pair.getKey()+"="+pair.getValue(); //A cada dato se le asigna el valor tras el caracter '='
        }

        //Se devuelven los datos con sus valores como Texto
        return parametrosString;

    }


    /** --------------------------------------------------------------
     * --------------------------------------------------------------
     * Cierra el dialogo que se muestra en pantalla
     * para devolver la visibilidad y el control de
     * interaccion al usuario
     * --------------------------------------------------------------
     * onPostExecute()
     * --------------------------------------------------------------
     */
    @Override
    protected void onPostExecute(String resultado) {

        super.onPostExecute(resultado);

        //---------------------------------------------------------------
        //---------------------------------------------------------------
        // Se procesa el resultado

        boolean todoCorrecto = false;

        // Si el servidor devuelve un verdadero, se entiende que las operaciones han ido como se esperaba
        try {
            JSONObject resultadoJSON = new JSONObject(resultado);
            todoCorrecto = resultadoJSON.getBoolean("datos");

            // Log.d("INFO API", "JSON: "+resultadoJSON.toString());
            // Log.d("INFO API", "JSON: "+todoCorrecto);
            if(todoCorrecto){
                Log.d(ETIQUETA_LOG, "TOKEN creado");
                //TODO: Enviar correo para acceder a web de recuperacion
                tareaCallback.terminadoCorrecto(0);
            }else{
                Log.d(ETIQUETA_LOG, "TOKEN NO CREADO!");
                tareaCallback.terminadoFallo(1);
            }

        } catch (JSONException ex){
            Log.e(ETIQUETA_LOG, "FALLO AL CREAR TOKEN--> resultado: "+resultado);
            tareaCallback.terminadoFallo(2);
        } // Fin del procesado de la respuesta del servidor
    }

}