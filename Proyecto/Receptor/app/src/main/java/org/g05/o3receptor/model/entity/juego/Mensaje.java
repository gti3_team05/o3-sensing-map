package org.g05.o3receptor.model.entity.juego;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.g05.o3receptor.R;

/**
 *
 * Representa un mensaje del personaje hacia el jugador
 * para indicar cuando tiene alguna necesidad
 *
 * @author Adrian Garcia Andreu
 */
public class Mensaje {

    private int x,y;
    private final long tiempoMaximoEnPantalla = 1000;
    private long tiempoEnPantalla;

    private Bitmap imagen;

    /**
     * Constructor de clase
     */
    public Mensaje(int x, int y, Bitmap imagen){
        this.imagen = imagen;

        this.x = x; this.y = y;
    }


    public long getTiempoEnPantalla() {
        return tiempoEnPantalla;
    }


    /**
     * Actualiza el tiempo en pantalla del mensaje
     * al momento actual
     */
    public void actualizarTiempoEnPantalla(){
        this.tiempoEnPantalla = System.currentTimeMillis();
    }


    /**
     * Obtiene el tiempo maximo que puede estar el mensaje en pantalla
     * @return
     */
    public long getTiempoMaximoEnPantalla() {
        return tiempoMaximoEnPantalla;
    }



    /**
     * Establece un tiempo maximo para que el mensaje aparezca en pantalla
     * @param tiempoEnPantalla
     */
    public void setTiempoEnPantalla(long tiempoEnPantalla) {
        this.tiempoEnPantalla = tiempoEnPantalla;
    }


    public Bitmap getImagen() {
        return imagen;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
