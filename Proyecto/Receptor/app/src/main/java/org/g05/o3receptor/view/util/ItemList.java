package org.g05.o3receptor.view.util;

public class ItemList {
    private String titulo;
    private String descripcion;
    private String otros;
    private int imgResource;
    private boolean recompensaConseguida;
    private boolean sePuedeCobrarLaRecompensa;
    private int recompensa;

    public ItemList(String titulo, String descripcion, String otros, int imgResource, boolean recompensaConseguida, boolean sePuedeCobrarLaRecompensa, int recompensa){
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.otros = otros;
        this.imgResource = imgResource;
        this.recompensaConseguida = recompensaConseguida;
        this.sePuedeCobrarLaRecompensa = sePuedeCobrarLaRecompensa;
        this.recompensa = recompensa;
    }

    public boolean isSePuedeCobrarLaRecompensa() {
        return sePuedeCobrarLaRecompensa;
    }

    public void setSePuedeCobrarLaRecompensa(boolean sePuedeCobrarLaRecompensa) {
        this.sePuedeCobrarLaRecompensa = sePuedeCobrarLaRecompensa;
    }

    public int getRecompensa() {
        return recompensa;
    }

    public void setRecompensa(int recompensa) {
        this.recompensa = recompensa;
    }

    public boolean isRecompensaConseguida() {
        return recompensaConseguida;
    }

    public void setRecompensaConseguida(boolean recompensaConseguida) {
        this.recompensaConseguida = recompensaConseguida;
    }

    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getImgResource() {
        return imgResource;
    }

    public void setImgResource(int imgResource) {
        this.imgResource = imgResource;
    }
}
