package org.g05.o3receptor.view.ui.activity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.g05.o3receptor.R;
import org.g05.o3receptor.model.entity.Medida;
import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.model.repository.remote.NodoAsociado_get;
import org.g05.o3receptor.model.repository.remote.Usuarios_post;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;

import java.util.List;

public class RegistroActivity extends AppCompatActivity implements TareaCallback {

    // Codigos permisos
    private final int TELEFONO_SOLICITUD_CODIGO = 101;
    private final int CAMARA_SOLICITUD_CODIGO = 102;

    // Codigos actividad escaneo
    private final int INTENT_ESCANER = 10;
    private final String NODO_CLAVE = "idNodo";


    private int idNodoEscaneado = 0; // Id del nodo asociado al usuario, por defecto ninguno
    private ProgressDialog dialogoEspera;

    private Usuario u;


    /**
     * Función para leer los datos recibidos de la actividad desde la que se
     * ha llamado a la actual
     */
    private void procesarDatosActividadAnterior() {

        Intent i = getIntent(); // Inteción de la actividad con los datos de la anterior

        // Pone el correo de la actividad de login en la actividad de creación de usuario
        EditText et = findViewById(R.id.et_registro_correo);
        et.setText(i.getStringExtra("correo"));
    }


    /**
     * Función que comprueba los p
     */
    public void  prepararRegistrarUsuario() {


        if (comprobarCamposRegistro()) {

            // Creo el dialogo de espera
            this.dialogoEspera =  generarDialogoEspera();
            this.dialogoEspera.show();

            // Se comprueba si el nodo escaneado esta previamente registrado por otro usuario
            comprobarNodoAsociado();

        } else {
            Toast.makeText(this, R.string.campos_obligatorios_incorrectos, Toast.LENGTH_LONG).show();
        } // Fin comprobación de valores correctos para el registro

    } // ()


    /**
     * Función para registrar a un usuario contra el servidor REST
     *
     */
    private void registrarUsuario(){
        // -----------
        // Se obtienen los valores de los campos
        EditText correo, clave;
        String correoTexto, claveTexto;


        correo = findViewById(R.id.et_registro_correo);
        clave = findViewById(R.id.et_registro_clave);

        correoTexto = correo.getText().toString();
        claveTexto = clave.getText().toString();



        // ---------

        u = new Usuario(correoTexto, claveTexto); // Se crea un usuario con los datos introducidos por el usuario
        u.setNodo(this.idNodoEscaneado); //se añade el nodo

        // Se envia el usuario al servidor REST para tratar de crearlo....
        Usuarios_post peticion = new Usuarios_post(u, (TareaCallback)this);
        peticion.execute();

    } // ()




    /**
     * Comprueba que los campos requeridos para el registro cumplen las condiciones
     * para registrar al usuario
     *
     * @return verdadero si los campos están de forma correcta, falso si hay algún problema
     */
    private boolean comprobarCamposRegistro(){

        boolean resultado = true;

        // -----------
        // Se obtienen los valores de los campos
        EditText correo, clave, confirmarClave;
        String correoTexto, claveTexto, confirmarClaveTexto;

        correo = findViewById(R.id.et_registro_correo);
        clave = findViewById(R.id.et_registro_clave);
        confirmarClave = findViewById(R.id.et_registro_claveConfirmar);

        correoTexto = correo.getText().toString();
        claveTexto = clave.getText().toString();
        confirmarClaveTexto = confirmarClave.getText().toString();
        // ---------

        // Se comprueban los requesitos
        if(correoTexto.length()<1){ resultado = false;} // Correo vacío
        if(claveTexto.length()<1){  resultado = false;} // Clave vacía
        if(confirmarClaveTexto.length()<1){  resultado = false;} // Confirmación de clave vacía
        if(claveTexto.compareTo(confirmarClaveTexto)!=0){ resultado = false;} // Clave y confirmación distintas
        if(this.idNodoEscaneado<=0){ resultado = false;} // Nodo no asociado

        return resultado;

    }


    /**
     * Comprueba con una petición REST, si el nodo escaneado por el usuario
     * esta libre, o asociado a otro usuario
     */
    private void comprobarNodoAsociado(){

        // Se crea la petición REST
        NodoAsociado_get peticion = new NodoAsociado_get(this.idNodoEscaneado, (TareaCallback)this);
        peticion.execute();

    }




    /**
     * Función para pedir al usuario acceso a un permiso concreto
     *
     * @param permiso
     * @param codigoSolicitud
     */
    public void comprobarPermiso(String permiso, int codigoSolicitud)
    {
        if (ContextCompat.checkSelfPermission(RegistroActivity.this, permiso)
                == PackageManager.PERMISSION_DENIED) {

            // Requesting the permission
            ActivityCompat.requestPermissions(RegistroActivity.this,
                    new String[] { permiso },
                    codigoSolicitud);
        }
        else {
            Toast.makeText(RegistroActivity.this,
                    R.string.mensaje_permiso_ya_otorgado,
                    Toast.LENGTH_SHORT)
                    .show();
        }
    }






    /**
     * Función que inicia una actividad para escanear el codigo de un nodo/sensor
     * codigos QR, y asociar dicho nodo al usuario que se registra
     *
     * @param v Vista desde la que se lanza el evento del cambio de actividad
     */
    public void registrarNodo(View v){

        // Comprobar permisos camara
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ) {
                comprobarPermiso(Manifest.permission.CAMERA, CAMARA_SOLICITUD_CODIGO);
            return;
        }


        Intent i = new Intent(this, EscanerActivity.class);
        startActivityForResult(i, INTENT_ESCANER);

    } // ()






    /**
     * Función para procesar el resultado obtenido al escanear datos con la camara,
     * si es una ID de algún nodo se da por valida, si no se informa al usuario
     *
     * @param resultado
     */
    private Integer procesarResultadosEscaneo(String resultado){

            Integer idNodo = Integer.parseInt(resultado); //Se obtiene la id del nodo del Texto

            // Cambio el label que indica que el nodo ha sido asociado correctamente
            Log.d("NODO","nodo asociado con éxito. nodo: "+idNodo);

            return idNodo; // Devuelve el id del nodo, este puede ser nulo, en caso un escaneo no bueno
    }



    /**
     * Limpia el nodo registrado en el formulario de registro
     */
    private void limpiarNodoRegistrado(){
        this.idNodoEscaneado = -1;
        Button btn_nodo = findViewById(R.id.btn_registro_registrarNodo);
        btn_nodo.setVisibility(View.VISIBLE);

    }



    /**
     * Función que genera un dialogo que se muestra en pantalla
     * para dar información al usuario mientras se realizan tareas
     * en 2º plano
     *
     *
     * @return
     */
    private ProgressDialog generarDialogoEspera(){
        ProgressDialog pd = new ProgressDialog(RegistroActivity.this);
        pd.setTitle(R.string.title_info_registro);
        pd.setMessage(getString(R.string.mensaje_info_registro));
        pd.setCancelable(false);

        return pd;
    } // ()






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        procesarDatosActividadAnterior();

    } // ()



    /**
     * Función heredada, al obtener el resultado de alguna actividad nacidad en base a esta
     * Usado para capturar la información escaneada con la camara.
     *
     * @param requestCode codigo de la actividad lanzada
     * @param resultCode codigo de resultado de la actividad terminada
     * @param data datos recibidos de la acticidad terminada
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == INTENT_ESCANER){
            if(resultCode == RESULT_OK){
                String idNodo = data.getStringExtra(NODO_CLAVE);
                this.idNodoEscaneado =  procesarResultadosEscaneo(idNodo); // Guardo la id del nodo extraida de la informacion escaneada
                prepararRegistrarUsuario();
            } else{
                Toast.makeText(this, R.string.escaneo_fallido, Toast.LENGTH_LONG).show();
            }

        }
    } // ()


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CAMARA_SOLICITUD_CODIGO){
            View v = findViewById(R.id.btn_registro_registrarNodo);
            registrarNodo(v);
        }

    }




    // --------------------------------------
    // ------- Callbacks de tareas asincronas
    @Override
    public void iniciado(int codigoSolicitud) {

    }

    @Override
    public void terminadoCorrecto(int codigoSolicitud) {
        switch (codigoSolicitud){
            case Constantes.PETICION_NODO_ASOCIADO:

                //Log.d("PETICION CORRECTA", "Nodo sin asociar...");
                registrarUsuario();

                break;
            case Constantes.PETICION_REGISTRO_USUARIO:


                // Se cierra el dialogo de espera, devolviendo control al usuario
                if (this.dialogoEspera.isShowing()){  this.dialogoEspera.dismiss(); }
                Toast.makeText(this, R.string.exito_registro, Toast.LENGTH_LONG).show();

                volverAlLogin();

                break;
        }
    }

    @Override
    public void terminadoCorrectoConDatos(int codigoSolicitud, JSONArray datos, String tipoDatos) {

    }

    @Override
    public void terminadoCorrectoMapa(int codigoSolicitud, JSONArray datos, Bitmap imagen, String tipoDatos) {

    }

    @Override
    public void terminadoCorrectoConDatosEstaciones(int codigoSolicitud, JSONArray datos, List<Medida> datosPuntos, String tipoDatos) {

    }

    @Override
    public void terminadoFallo(int codigoSolicitud) {

        switch (codigoSolicitud){
            case Constantes.PETICION_NODO_ASOCIADO:

                // Se vuelve a permitir asociar un nodo
                Toast.makeText(this, R.string.nodo_ya_asociado, Toast.LENGTH_LONG).show();
                Log.e("PETICION FALLIDA", "NODO YA ASOCIADO");
                limpiarNodoRegistrado();

                break;
            case Constantes.PETICION_REGISTRO_USUARIO:
                Toast.makeText(this, R.string.usuario_ya_registrado, Toast.LENGTH_LONG).show();
                break;
        }

        // Si es fallo el dialogo siempre se cierra
        if (this.dialogoEspera.isShowing()){
            this.dialogoEspera.dismiss();
        }

    }

    private void volverAlLogin(){
        // Almaceno el usuario como valido en las preferencias compartidas de la app
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        if(this.u != null) {
            editor.putString(Constantes.PREF_CORREO, this.u.getCorreo());
            editor.putString(Constantes.PREF_CLAVE, this.u.getClave());

            editor.commit(); //Almacena en las preferencias, de forma inmediata
        }
            // Se accede a la actividad principal
            accederAplicacion();
    }

    public void volverAlLoginBtn(View view){
        Intent i = new Intent(RegistroActivity.this, InicioSesionActivity.class);
        startActivity(i); // Se inicia la actividad
    }


    /**
     * Función para acceder a la aplicacion cuando el usuario
     * ha iniciado sesión con un usuario valido.
     *
     */
    public void accederAplicacion(){
        // Se crea el intent para el cambio de actividad
        Intent i = new Intent(RegistroActivity.this, PrincipalActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK); // No permite volver a dicha actviad al usuario
        startActivity(i); // Se inicia la actividad

    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }



} // Fin de clase