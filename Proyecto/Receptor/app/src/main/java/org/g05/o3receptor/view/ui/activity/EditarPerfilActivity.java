package org.g05.o3receptor.view.ui.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.g05.o3receptor.R;
import org.g05.o3receptor.model.entity.Medida;
import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.model.repository.remote.Usuarios_post;
import org.g05.o3receptor.model.repository.remote.Usuarios_update;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;

import java.util.List;
import java.util.Random;

public class EditarPerfilActivity extends AppCompatActivity implements TareaCallback {
    private String nombre;
    private String apellidos;
    private String contrasenya;
    private String correo;
    private String telefono;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil);

        mostrarDatosUsuario();
    }

    // *************************************************************************************************************************************************************************** //
    // ******************************************************************** DATOS USUARIO  *************************************************************************************** //

    private void mostrarDatosUsuario(){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        String correoPref  = sharedPref.getString(Constantes.PREF_CORREO, null); // Indica que está logeado
        String nombrePref  = sharedPref.getString(Constantes.PREF_USUARIO, "usuario"); // Indica que está logeado
        String clavePref  = sharedPref.getString(Constantes.PREF_CLAVE, null); // Indica que está logeado
        String apellidosPref  = sharedPref.getString(Constantes.PREF_APELLIDOS, "apellidos"); // Indica que está logeado
        String telefonoPref  = sharedPref.getString(Constantes.PREF_TELEFONO, "telefono"); // Indica que está logeado

        TextView nombreUsuario = (TextView) findViewById(R.id.et_edPer_nombre);
        TextView correoUsuario = (TextView) findViewById(R.id.tv_edPer_mail);

        if(nombrePref == "usuario"){
            Random r = new Random();
            int i1 = r.nextInt(150 - 2) + 2;
            nombrePref = nombrePref+i1;
            editor.putString(Constantes.PREF_USUARIO, nombrePref);
            editor.commit();
        }
        if(nombreUsuario!=null && correoUsuario!=null){
            nombreUsuario.setText(nombrePref);
            correoUsuario.setText(correoPref);
        }

        if (!apellidosPref.equals("apellidos")) {
            TextView apellidosUsuario = (TextView) findViewById(R.id.et_edPer_apellidos);
            apellidosUsuario.setText(apellidosPref);
        }

        if (!telefonoPref.equals("telefono")) {
            TextView telefonoUsuario = (TextView) findViewById(R.id.et_edPer_telefono);
            telefonoUsuario.setText(telefonoPref);
        }

        // Guardamos los valores actuales de mail y clave
        correo = correoPref;
        contrasenya = clavePref;
    }

    // *************************************************************************************************************************************************************************** //
    // ***************************************************************** GUARDAR CAMBIOS ***************************************************************************************** //

    public void guardarDatosBD(View v){
        TextView nombreUsuario = (TextView) findViewById(R.id.et_edPer_nombre);
        TextView apellidosUsuario = (TextView) findViewById(R.id.et_edPer_apellidos);
        TextView telefonoUsuario = (TextView) findViewById(R.id.et_edPer_telefono);

        nombre = nombreUsuario.getText().toString();
        apellidos = apellidosUsuario.getText().toString();
        telefono = telefonoUsuario.getText().toString();

        Usuario usuario = new Usuario(correo, contrasenya);
        usuario.setApellidos(apellidos); usuario.setNombre(nombre); usuario.setTelefono(telefono);

        Usuarios_update peticion = new Usuarios_update(usuario,this);
        peticion.execute();
    }

    @Override
    public void terminadoCorrecto(int codigoSolicitud) {
        if(codigoSolicitud==Constantes.PETICION_ACTUALIZAR_PERFIL){
            // Guardamos los valores en shared preferences y volvemos a la anterior ventana
            guardarDatosSharedPreferences();
            onBackPressed();
        }
    }

    @Override
    public void terminadoFallo(int codigoSolicitud) {
        if(codigoSolicitud==Constantes.PETICION_ACTUALIZAR_PERFIL){
            Toast.makeText(this, getResources().getString(R.string.titulo_error_actualizar), Toast.LENGTH_LONG).show();
        }
    }

    private void guardarDatosSharedPreferences(){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString(Constantes.PREF_USUARIO, nombre);
        editor.putString(Constantes.PREF_APELLIDOS, apellidos);
        editor.putString(Constantes.PREF_TELEFONO, telefono);
        editor.commit();
    }

    // *************************************************************************************************************************************************************************** //
    // ******************************************************************** VOLVER ATRAS ***************************************************************************************** //

    public void cancelar(View view){
        // Mensaje para confirmar que el usuario quiere no guardar los cambios realizados
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        //Volvemos atras
                        onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No hacemos nada
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.titulo_estas_seguro).setPositiveButton(R.string.si, dialogClickListener)
                .setNegativeButton(R.string.no, dialogClickListener).show();
    }

    // *************************************************************************************************************************************************************************** //
    // *************************************************************************** OTROS ***************************************************************************************** //

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    @Override
    public void iniciado(int codigoSolicitud) {

    }

    @Override
    public void terminadoCorrectoConDatos(int codigoSolicitud, JSONArray datos, String tipoDatos) {

    }

    @Override
    public void terminadoCorrectoMapa(int codigoSolicitud, JSONArray datos, Bitmap imagen, String tipoDatos) {

    }

    @Override
    public void terminadoCorrectoConDatosEstaciones(int codigoSolicitud, JSONArray datos, List<Medida> datosPuntos, String tipoDatos) {

    }


}