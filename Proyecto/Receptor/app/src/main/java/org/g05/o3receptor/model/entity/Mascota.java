package org.g05.o3receptor.model.entity;

import android.location.Location;

/**
 * POJO para representar una entidad Mascota
 * @author Pablo Miret
 * @version 1.1
 */
public class Mascota {

    private String usuario; // correo asignado
    /**
     * --------------------------------------------------------------
     * getUsuario() --> Texto
     * --------------------------------------------------------------
     */
    public String getUsuario() {
        return usuario;
    }
    /**
     * --------------------------------------------------------------
     * Texto--> setUsuario()
     * --------------------------------------------------------------
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    /**
     * --------------------------------------------------------------
     * getMinerales() --> int
     * --------------------------------------------------------------
     */
    public int getMinerales() {
        return minerales;
    }
    /**
     * --------------------------------------------------------------
     * int--> setMinerales()
     * --------------------------------------------------------------
     */
    public void setMinerales(int minerales) {
        this.minerales = minerales;
    }
    /**
     * --------------------------------------------------------------
     * getComida() --> float
     * --------------------------------------------------------------
     */
    public float getComida() {
        return comida;
    }
    /**
     * --------------------------------------------------------------
     * float--> setComida()
     * --------------------------------------------------------------
     */
    public void setComida(float comida) {
        this.comida = comida;
    }
    /**
     * --------------------------------------------------------------
     * getServicio() --> float
     * --------------------------------------------------------------
     */
    public float getServicio() {
        return servicio;
    }
    /**
     * --------------------------------------------------------------
     * float--> setServicio()
     * --------------------------------------------------------------
     */
    public void setServicio(float servicio) {
        this.servicio = servicio;
    }
    /**
     * --------------------------------------------------------------
     * getSueno() --> float
     * --------------------------------------------------------------
     */
    public float getSueno() {
        return sueno;
    }
    /**
     * --------------------------------------------------------------
     * float--> setSueno()
     * --------------------------------------------------------------
     */
    public void setSueno(float sueno) {
        this.sueno = sueno;
    }
    /**
     * --------------------------------------------------------------
     * getNombre() --> Texto
     * --------------------------------------------------------------
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * --------------------------------------------------------------
     * Texto--> setNombre()
     * --------------------------------------------------------------
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    private int minerales; // valor de la medida del sensor
    private float comida; // valor de la medida del sensor
    private float servicio; // valor de la medida del sensor
    private float sueno; // valor de la medida del sensor
    private float esperanza; // valor de la medida del sensor

    public float getEsperanza() {
        return esperanza;
    }

    public void setEsperanza(float esperanza) {
        this.esperanza = esperanza;
    }

    private String nombre; // tipo de la medida


    /**
     * Constructor de clase
     */

    public Mascota(String usuario, int minerales, float comida, float servicio, float sueno, float esperanza, String nombre) {
        this.usuario = usuario;
        this.minerales = minerales;
        this.comida = comida;
        this.servicio = servicio;
        this.sueno = sueno;
        this.esperanza = esperanza;
        this.nombre = nombre;
    }






}
