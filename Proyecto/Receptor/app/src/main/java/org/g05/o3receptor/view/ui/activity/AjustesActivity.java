package org.g05.o3receptor.view.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import org.g05.o3receptor.R;
import org.g05.o3receptor.view.util.Constantes;

import java.util.Random;

public class AjustesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajustes);

        mostrarDatosUsuario();
        activarSwitchNotificaciones();
        activarSwitchNotificacionesDistancia();
        activarSwitchMascota();
        activarSwitchSubidaDatosSegundoPlano();
        activarSwitchModoTester();
        activarSelectorIdioma();
    }

    // *************************************************************************************************************************************************************************** //
    // ************************************************************************** IDIOMAS  *************************************************************************************** //
    private void activarSelectorIdioma(){
        Button tvSettingsLanguage = findViewById(R.id.btn_ajustes_usuario_selector_idioma);
        tvSettingsLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(AjustesActivity.this);
                builder.setTitle(R.string.titulo_selector_idioma);
                builder.setItems(R.array.seleccionar_idioma, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int position) {
                        switch (position) {
                            case 0:
                                guardarPreferenciasIdioma("es");
                                cambiarIdioma();
                                break;
                            case 1:
                                guardarPreferenciasIdioma("en");
                                cambiarIdioma();
                                break;
                            case 2:
                                guardarPreferenciasIdioma("zh");
                                cambiarIdioma();
                                break;
                            case 3:
                                guardarPreferenciasIdioma("ca");
                                cambiarIdioma();
                                break;
                        }
                    }
                });
                builder.show();
            }
        });
    }

    /**
     * Genera las preferencias del lenguaje
     */
    private void guardarPreferenciasIdioma(String language){
        SharedPreferences languagePreferences = getSharedPreferences(Constantes.PREF_IDIOMA_APP, Context.MODE_PRIVATE);
        SharedPreferences.Editor myEditorLanguage = languagePreferences.edit();
        myEditorLanguage.clear();
        myEditorLanguage.putString(Constantes.PREF_IDIOMA, language);
        myEditorLanguage.apply();
    }

    /**
     * Genera las preferencias del lenguaje
     */
    private void cambiarIdioma(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.pregunta_reiniciar_app);
        builder.setPositiveButton(this.getResources().getString(R.string.si), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
                Intent intent = new Intent(AjustesActivity.this, PrincipalActivity.class);
                startActivity(intent);

            }
        });
        builder.setNegativeButton(this.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    // *************************************************************************************************************************************************************************** //
    // ************************************************************************** SWITCHS  *************************************************************************************** //
    private void activarSwitchNotificaciones(){
        SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        boolean modoNotificacionesAlerta = preferencias.getBoolean(Constantes.PREF_NOTIFICACIONES_MEDIDAS,true);

        Switch switchNotificacionesAlerta = (Switch) findViewById(R.id.switch_alerta_valores_anormales);
        switchNotificacionesAlerta.setChecked(modoNotificacionesAlerta);

        switchNotificacionesAlerta.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
                SharedPreferences.Editor miEditor = preferencias.edit();
                if(isChecked){
                    miEditor.putBoolean(Constantes.PREF_NOTIFICACIONES_MEDIDAS, true);
                }else{
                    miEditor.putBoolean(Constantes.PREF_NOTIFICACIONES_MEDIDAS, false);
                }
                miEditor.commit();
            }
        });
    }

    private void activarSwitchNotificacionesDistancia(){
        SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        boolean modoNotificacionesDistancia = preferencias.getBoolean(Constantes.PREF_NOTIFICACIONES_DISTANCIA,true);

        Switch switchNotificacionesDistancia = (Switch) findViewById(R.id.switch_alerta_distancia_nodo);
        switchNotificacionesDistancia.setChecked(modoNotificacionesDistancia);

        switchNotificacionesDistancia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
                SharedPreferences.Editor miEditor = preferencias.edit();
                if(isChecked){
                    miEditor.putBoolean(Constantes.PREF_NOTIFICACIONES_DISTANCIA, true);
                }else{
                    miEditor.putBoolean(Constantes.PREF_NOTIFICACIONES_DISTANCIA, false);
                }
                miEditor.commit();
            }
        });
    }

    private void activarSwitchMascota(){
        SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        boolean modoMascota = preferencias.getBoolean(Constantes.PREF_MASCOTA,true);

        Switch switchMascota = (Switch) findViewById(R.id.switch_mostrar_mascota_virtual);
        switchMascota.setChecked(modoMascota);

        switchMascota.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
                SharedPreferences.Editor miEditor = preferencias.edit();
                if(isChecked){
                    miEditor.putBoolean(Constantes.PREF_MASCOTA, true);
                }else{
                    miEditor.putBoolean(Constantes.PREF_MASCOTA, false);
                }
                miEditor.commit();
            }
        });
    }

    private void activarSwitchSubidaDatosSegundoPlano(){
        SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        boolean modoMascota = preferencias.getBoolean(Constantes.PREF_SEGUNDO_PLANO,true);

        Switch switchMascota = (Switch) findViewById(R.id.switch_subida_datos_2do_plano);
        switchMascota.setChecked(modoMascota);

        switchMascota.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
                SharedPreferences.Editor miEditor = preferencias.edit();
                if(isChecked){
                    miEditor.putBoolean(Constantes.PREF_SEGUNDO_PLANO, true);
                }else{
                    miEditor.putBoolean(Constantes.PREF_SEGUNDO_PLANO, false);
                }
                miEditor.commit();
            }
        });
    }

    private void activarSwitchModoTester(){
        SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        boolean modoTester = preferencias.getBoolean(Constantes.PREF_MODO_TESTER,false);

        Switch switchTester = (Switch) findViewById(R.id.switch_modo_tester);
        switchTester.setChecked(modoTester);

        switchTester.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences preferencias = getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
                SharedPreferences.Editor miEditor = preferencias.edit();
                if(isChecked){
                    miEditor.putBoolean(Constantes.PREF_MODO_TESTER, true);
                }else{
                    miEditor.putBoolean(Constantes.PREF_MODO_TESTER, false);
                }
                miEditor.commit();
            }
        });
    }

    // *************************************************************************************************************************************************************************** //
    // ******************************************************************** DATOS USUARIO  *************************************************************************************** //

    private void mostrarDatosUsuario(){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        String correo  = sharedPref.getString(Constantes.PREF_CORREO, null); // Indica que está logeado
        String nombre  = sharedPref.getString(Constantes.PREF_USUARIO, "usuario"); // Indica que está logeado

        TextView nombreUsuario = (TextView) findViewById(R.id.tv_nombre_usuario);
        TextView correoUsuario = (TextView) findViewById(R.id.tv_correo_usuario);

        if(nombre == "usuario"){
            Random r = new Random();
            int i1 = r.nextInt(150 - 2) + 2;
            nombre = nombre+i1;
            editor.putString(Constantes.PREF_USUARIO, nombre);
            editor.commit();
        }
        if(nombreUsuario!=null && correoUsuario!=null){
            nombreUsuario.setText(nombre);
            correoUsuario.setText(correo);
        }
    }

    public void iniciarEditarPerfil(View v){
        // Se crea el intent para el cambio de actividad
        Intent i = new Intent(this, EditarPerfilActivity.class);
        startActivity(i); // Se inicia la actividad
    }

    // *************************************************************************************************************************************************************************** //
    // ******************************************************************** CICLO DE VIDA  *************************************************************************************** //
    @Override
    protected void onResume() {
        super.onResume();
        mostrarDatosUsuario();
    }
}