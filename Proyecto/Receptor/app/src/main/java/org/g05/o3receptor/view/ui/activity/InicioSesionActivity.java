package org.g05.o3receptor.view.ui.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.google.android.material.snackbar.Snackbar;

import org.g05.o3receptor.R;
import org.g05.o3receptor.model.entity.Medida;
import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.model.repository.remote.InicioSesion_get;
import org.g05.o3receptor.view.util.CambiarIdioma;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class InicioSesionActivity extends AppCompatActivity implements TareaCallback {


    private Usuario usuario; // Usuario que inicia sesión


    /**
     * Función que genera un dialogo que se muestra en pantalla
     * para dar información al usuario mientras se realizan tareas
     * en 2º plano
     *
     *
     * @return
     */
    private ProgressDialog generarDialogoEspera(){
        ProgressDialog pd = new ProgressDialog(InicioSesionActivity.this);
        pd.setTitle(R.string.title_iniciar_sesion);
        pd.setMessage(getString(R.string.inicio_sesion_cargando));
        pd.setCancelable(false);

        return pd;
    } // ()



    /**
     *  Función para iniciar sesion del usuario
     *
     */
    public void inicioSesion(View v){

        // Oculto el teclado
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

        // Obtengo los campos de inicio de sesión
        EditText correo = findViewById(R.id.et_inicioSesion_correo);
        EditText clave = findViewById(R.id.et_inicioSesion_clave);
        String correoTexto = correo.getText().toString();
        String claveTexto = clave.getText().toString();

        // ---------------------------------
        // Si los campos no están vacíos
        if(correo.length()>0 && clave.length()>0){
            this.usuario = new Usuario(correoTexto, claveTexto);
            //Se envía el usuario (con un dialogo de espera
            InicioSesion_get peticion = new InicioSesion_get(usuario,
                    (TareaCallback)this, generarDialogoEspera());

            peticion.execute();
        }  else {

            Snackbar.make(findViewById(R.id.activity_inicioSesion),
                    R.string.campos_obligatorios, Snackbar.LENGTH_LONG).show();



        }


    } // ()



    /**
     * Salta a la actividad de registro de usuarios,
     * para crear uno nuevo contra el servidor
     *
     *
     */
    public void lanzarRegistroUsuarios(View v){
        EditText correo = findViewById(R.id.et_inicioSesion_correo);
        String correoTexto = correo.getText().toString();

        // Se crea el intent para el cambio de actividad
        Intent i = new Intent(this, RegistroActivity.class);

        // Se añade el valor del campo del correo por comoidad en caso de usar ese correo para registrarse
        i.putExtra("correo", correoTexto);
        startActivity(i); // Se inicia la actividad


    }




    /**
     * Función para acceder a la aplicacion cuando el usuario
     * ha iniciado sesión con un usuario valido.
     *
     */
    public void accederAplicacion(){
        // Se crea el intent para el cambio de actividad
        Intent i = new Intent(this, PrincipalActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK); // No permite volver a dicha actviad al usuario
        startActivity(i); // Se inicia la actividad

    }

    public void lanzarCambioClave(View v){
        Intent i = new Intent(this, CambiarClaveActivity.class);
        startActivity(i); // Se inicia la actividad
    }







    // --------------------------------
    // Funciones de la Activity
    // --------------------------------


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CambiarIdioma.putSelectedLanguage(InicioSesionActivity.this);
        setContentView(R.layout.activity_inicio_sesion);
    }




    // --------------------------------
    // Callbacks de tareas
    // --------------------------------

    @Override
    public void iniciado(int codigoSolicitud) { } // No necesario en esta actividad

    /**
     * Se lanza la actividad principal
     */
    @Override
    public void terminadoCorrecto(int codigoSolicitud) {
    }

    @Override
    public void terminadoCorrectoConDatos(int codigoSolicitud, JSONArray resulado, String tipoDatos) {

        // Almaceno el usuario como valido en las preferencias compartidas de la app
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putString(Constantes.PREF_CORREO, this.usuario.getCorreo());
        editor.putString(Constantes.PREF_CLAVE, this.usuario.getClave());

        if(tipoDatos == "usuario"){
            try{
                JSONObject usu = resulado.getJSONObject(0);
                this.usuario.setNombre(String.valueOf(usu.get("nombre")));
                this.usuario.setApellidos(String.valueOf(usu.get("apellidos")));
                this.usuario.setTelefono(String.valueOf(usu.get("telefono")));
            }catch (Exception e){ }
        }
        editor.putString(Constantes.PREF_USUARIO, this.usuario.getNombre());
        editor.putString(Constantes.PREF_APELLIDOS, this.usuario.getApellidos());
        editor.putString(Constantes.PREF_TELEFONO, this.usuario.getTelefono());

        editor.commit(); //Almacena en las preferencias, de forma inmediata

        // Se accede a la actividad principal
        accederAplicacion();
    }

    @Override
    public void terminadoCorrectoMapa(int codigoSolicitud, JSONArray datos, Bitmap imagen, String tipoDatos) {

    }

    @Override
    public void terminadoCorrectoConDatosEstaciones(int codigoSolicitud, JSONArray datos, List<Medida> datosPuntos, String tipoDatos) {

    }

    /**
     * Se muestra un mensaje de error en el inicio de sesión
     */
    @Override
    public void terminadoFallo(int codigoSolicitud) {

        final Snackbar sb = Snackbar.make(findViewById(R.id.activity_inicioSesion),
                R.string.inicio_fallido, Snackbar.LENGTH_INDEFINITE);

        sb.setAction(R.string.cerrar, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sb.dismiss();
            }
        });

        sb.show();

    }

    public void volverAlMapa(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }
}