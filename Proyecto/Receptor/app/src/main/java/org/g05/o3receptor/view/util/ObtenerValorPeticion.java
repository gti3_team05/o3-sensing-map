package org.g05.o3receptor.view.util;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class ObtenerValorPeticion extends AsyncTask<Void, Void, String> {
    //Variables de la posicion del usuario
    private String parametros;
    private int medidaServidor;
    private int medidaSensor;
    private int contador;

    private ServicioBTLE servicioBTLE;
    private JSONObject consulta;

    public ObtenerValorPeticion(ServicioBTLE servicio, JSONObject result, int medicion){
        this.servicioBTLE = servicio;
        this.consulta = result;
        this.medidaSensor = medicion;
    }

    @Override
    protected String doInBackground(Void... voids) {

        String retorno = null;
        URL url;
        HttpURLConnection connection = null;

        // ---------------------------------------------------------------------------
        // Peticion POST datos en la URL para actualizar y recibir un JSON de la web
        try {
            url = new URL (Constantes.API_EXTRAER_OZONO);

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion y parámetros de la consulta (se saben gracias a POSTMAN)
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept", "*/*");

            // Escribte los datos a enviar en la peticion REST de tipo POST (id de la estacion de Gandia)

            String consultaAPI = String.valueOf(consulta);

            connection.setDoOutput(true);
            try(OutputStream os = connection.getOutputStream()) {
                byte[] input = consultaAPI.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            // Captura la respuesta recibida
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }

            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.d("Consulta API 2", "Response: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la consulta a la API 2");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }

        return retorno;
    }

    @Override
    protected void onPostExecute(String resultado) {
        //Despues de la consulta a la API se recibe un JSON con las mediciones de la estacion, por lo que extraemos la medición de o3
        super.onPostExecute(resultado);

        JSONObject resultadoJSON;

        //Se intenta leer el JSON, si no se devuelve la excepción
        try {

            resultadoJSON = new JSONObject(resultado);

            Log.d("API JSON 2", ">>"+resultadoJSON.toString()+"<<");

            medidaServidor = resultadoJSON.getInt("valor");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Si el resultado es el esperado, se envia a la BD, si no se vuelve a escanear
        if(medidaServidor >= 1){
            try {
                servicioBTLE.obtenerEstacionMedida(consulta);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            servicioBTLE.subirMedidaBD(medidaServidor, medidaSensor);
        } else {
            servicioBTLE.iniciarEscaneoSegundoPlano();
        }

    }


}
