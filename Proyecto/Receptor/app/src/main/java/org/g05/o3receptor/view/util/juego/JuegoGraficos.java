package org.g05.o3receptor.view.util.juego;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.g05.o3receptor.R;
import org.g05.o3receptor.model.entity.juego.Fondo;
import org.g05.o3receptor.model.entity.juego.Mensaje;
import org.g05.o3receptor.model.entity.juego.Personaje;
import org.g05.o3receptor.view.ui.activity.ui.juego.JuegoActivity;
import org.g05.o3receptor.view.util.Constantes;

import java.util.ArrayList;
import java.util.List;

/**
 * Representa los graficos en la vista de juego
 *
 * @author Adrian Garcia Andreu
 */
public class JuegoGraficos {

    private Context contexto;
    private JuegoView vistaDeJuego;

    private Fondo fondoPrincipal;
    private Fondo fondoContaminado;

    private int pantallaX, pantallaY; // tamaño de la pantalla
    private float pantallaRatioX, pantallaRatioY;
    private Paint renderizador; //

    //Animacion
    private String Memoria = "";
    private int last_frame = 0;
    private Personaje personaje;


    // Mensajes para que el personaje muestre por pantalla al jugador
    private List<Mensaje> mensajesAMostrar;
    private int mensajesIndiceActual = 0;


    private long tiempoMaximoEnPantalla = 1000; // Tiempo en pantalla por defecto de un mensaje (por defecto 1 segundo)
    private long cuandoSeMostroUltimoMensaje = System.currentTimeMillis(); // tiempo en pantalla actual
    private Mensaje mensajeActual;

    /**
     * Constructor de clase
     */
    public JuegoGraficos(JuegoView vista, int pantallaX, int pantallaY){

        //if (last_frame <3)
        //this.contexto = contexto;
        this.vistaDeJuego = vista;

        this.pantallaX = pantallaX; this.pantallaY = pantallaY;
        // En base a la resolucion para la que se desarrolla se obtiene el ratio de la pantalla
        this.pantallaRatioX = (1080f/pantallaX);
        this.pantallaRatioY = (2400f/pantallaY);


        this.renderizador = new Paint();

        // Se instancia el fondo principal (ESTATICO)
        instancairFondo();
        instanciarPersonaje();

    }


    private void instancairFondo(){

        // Instancio los fondos
        this.fondoPrincipal = new Fondo(pantallaX, pantallaY,
                vistaDeJuego.getResources(),
                R.drawable.fondo);

        this.fondoContaminado = new Fondo(pantallaX, pantallaY,
                vistaDeJuego.getResources(),
                R.drawable.fondo_contaminado);

    }

    public void instanciarPersonaje(){
        //pantallacarga();
        personaje = vistaDeJuego.getJuegoMotor().getPersonaje();

        Bitmap imagen;String Name;
        String b;int drawableResourceId;
        int h,w;h = 800;w = 800;




        Name = "idlecontento";
        for(int l=0; l<=20; l++){
            b= Name + String.valueOf(l);

            drawableResourceId = vistaDeJuego.getResources().getIdentifier(b, "drawable", "org.jordi.btlealumnos");
            imagen = BitmapFactory.decodeResource(vistaDeJuego.getResources(),  drawableResourceId);
            imagen = Bitmap.createScaledBitmap(imagen, w,h, false);
            personaje.setImagenFeliz(imagen);

        }
        Name = "idlenormal";
        for(int l=0; l<=20; l++){
            b= Name + String.valueOf(l);
            drawableResourceId = vistaDeJuego.getResources().getIdentifier(b, "drawable", "org.jordi.btlealumnos");
            imagen = BitmapFactory.decodeResource(vistaDeJuego.getResources(),  drawableResourceId);
            imagen = Bitmap.createScaledBitmap(imagen, w,h, false);
            personaje.setImagenNormal(imagen);

        }
        Name = "idletriste";
        for(int l=0; l<=20; l++){
            b= Name + String.valueOf(l);
            drawableResourceId = vistaDeJuego.getResources().getIdentifier(b, "drawable", "org.jordi.btlealumnos");
            imagen = BitmapFactory.decodeResource(vistaDeJuego.getResources(),  drawableResourceId);
            imagen = Bitmap.createScaledBitmap(imagen, w,h, false);
            personaje.setImagenTriste(imagen);
        }
        Name = "morir";
        for(int l=0; l<=33; l++){
            b= Name + String.valueOf(l);
            drawableResourceId = vistaDeJuego.getResources().getIdentifier(b, "drawable", "org.jordi.btlealumnos");
            imagen = BitmapFactory.decodeResource(vistaDeJuego.getResources(),  drawableResourceId);
            imagen = Bitmap.createScaledBitmap(imagen, w,h, false);
            personaje.setImagenMorir(imagen);

        }
        Name = "dormir";
        for(int l=0; l<=30; l++){
            b= Name + String.valueOf(l);
            drawableResourceId = vistaDeJuego.getResources().getIdentifier(b, "drawable", "org.jordi.btlealumnos");
            imagen = BitmapFactory.decodeResource(vistaDeJuego.getResources(),  drawableResourceId);
            imagen = Bitmap.createScaledBitmap(imagen, w,h, false);
            personaje.setImagenDormido(imagen);
        }
        Name = "comer";
        for(int l=0; l<=18; l++){
            b= Name + String.valueOf(l);
            drawableResourceId = vistaDeJuego.getResources().getIdentifier(b, "drawable", "org.jordi.btlealumnos");
            imagen = BitmapFactory.decodeResource(vistaDeJuego.getResources(),  drawableResourceId);
            imagen = Bitmap.createScaledBitmap(imagen, w,h, false);
            personaje.setImagenComer(imagen);

        }
        Name = "bano";
        for(int l=0; l<=20; l++){
            b= Name + String.valueOf(l);
            drawableResourceId = vistaDeJuego.getResources().getIdentifier(b, "drawable", "org.jordi.btlealumnos");
            imagen = BitmapFactory.decodeResource(vistaDeJuego.getResources(),  drawableResourceId);
            imagen = Bitmap.createScaledBitmap(imagen, w,h, false);
            personaje.setImagenServicio(imagen);
        }
        Name = "acariciar";
        for(int l=0; l<=19; l++){
            b= Name + String.valueOf(l);
            drawableResourceId = vistaDeJuego.getResources().getIdentifier(b, "drawable", "org.jordi.btlealumnos");
            imagen = BitmapFactory.decodeResource(vistaDeJuego.getResources(),  drawableResourceId);
            imagen = Bitmap.createScaledBitmap(imagen, w,h, false);
            personaje.setImagenAcariciar(imagen);
        }
        Name = "revivir";
        for(int l=0; l<=25; l++){
            b= Name + String.valueOf(l);
            drawableResourceId = vistaDeJuego.getResources().getIdentifier(b, "drawable", "org.jordi.btlealumnos");
            imagen = BitmapFactory.decodeResource(vistaDeJuego.getResources(),  drawableResourceId);
            imagen = Bitmap.createScaledBitmap(imagen, w,h, false);
            personaje.setImagenResucitar(imagen);
            if (l==0){personaje.setImagenMuerto(imagen);}
        }

        // Posiciona la imagen del personaje
        int y = (pantallaY/2);
        int x = ((pantallaX/2)-(int)(w/2));

        personaje.setY(y);
        personaje.setX(x);

    }

    /**
     * Aplica los graficos sobre una vista con superficie
     */

    public void aplicarGraficosPrincipales(){

        Personaje personaje = vistaDeJuego.getJuegoMotor().getPersonaje();

        // Obtiene la superficie de la vista cuando está es valida
        if(vistaDeJuego.getHolder().getSurface().isValid()){

            ArrayList<Bitmap> imagenARenderizar = new ArrayList<Bitmap>();
            imagenARenderizar = renderizarPersonaje();

            Bitmap imagenPersonaje;
            if (renderizarPersonaje().size() <=0) {
                imagenPersonaje = BitmapFactory.decodeResource(vistaDeJuego.getResources(), R.drawable.revivir0);
                imagenPersonaje = Bitmap.createScaledBitmap(imagenPersonaje, 512,512, false);
            }else{
                if (last_frame >= renderizarPersonaje().size()) {last_frame = 0;}
                if (!Memoria.equals(imagenARenderizar.get(0).toString())) {last_frame = 0;}
                imagenPersonaje = imagenARenderizar.get(last_frame);
            }

            //Log.d("DRAWING", "Dibujando fondo...");
            Canvas canvas = vistaDeJuego.getHolder().lockCanvas(); // Se obtiene una "imagen" de lo que hay en pantalla
            if(personaje.getEstadoAnimo() == 3 || personaje.getmuerto()){
                // Dibujado del fondo
                canvas.drawBitmap(this.fondoContaminado.getFondo(),
                        this.fondoContaminado.getX(), this.fondoContaminado.getY(), renderizador); // Se dibuja el fondo sobre el canvas
            }else{
                // Dibujado del fondo
                canvas.drawBitmap(this.fondoPrincipal.getFondo(),
                        this.fondoPrincipal.getX(), this.fondoPrincipal.getY(), renderizador); // Se dibuja el fondo sobre el canvas
            }
            // Dibujado del personaje
            if(imagenPersonaje!=null){
            canvas.drawBitmap(imagenPersonaje,
                    personaje.getX(), personaje.getY(), renderizador); // Se dibuja el fondo sobre el canvas
            // Dibujado del mensaje de personajes
               // renderizarMensajes(canvas);
            }
            personaje.checklock();

            vistaDeJuego.getHolder().unlockCanvasAndPost(canvas); // Actualiza la vista con el canvas actualizado
            last_frame= last_frame+1;
            if(imagenARenderizar.size()>0){
                Memoria= imagenARenderizar.get(0).toString();
            }




        }
        esperar();
    }
    private void esperar(){

        // Espera 17 milisegundos  (la inversa de 0.017 segundos, son 60, que es el nº de imagenes por segundo
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Bitmap> renderizarPersonaje(){


        ArrayList<Bitmap> imagenPersonaje = new ArrayList<Bitmap>();
        Personaje personaje = vistaDeJuego.getJuegoMotor().getPersonaje();

        if(personaje!=null){
            // En función de si está vivo o muerto
            if(personaje.estoyVivo()){
                   imagenPersonaje  = getImagenPersonajeVivo();
            } else{
                limpiarMensajes(); // No se muestran mensajes cuando muere el personaje
                imagenPersonaje = getImagenPersonajeMuerto();
            }
         }
            else{
            limpiarMensajes();
            imagenPersonaje = null;
        }


        return imagenPersonaje;

    }



    /**
     * Dibuja al personaje Vivo
     */
    private ArrayList<Bitmap> getImagenPersonajeVivo(){

        ArrayList<Bitmap> imagenARenderizar = new ArrayList<Bitmap>();
        //Bitmap imagenARenderizar;

        Personaje personaje = vistaDeJuego.getJuegoMotor().getPersonaje();
        if(personaje.isDormido()){
            limpiarMensajes(); // No se muestran mensajes
            imagenARenderizar = personaje.getImagenDormido();
        }
        else{
            int estado = personaje.getEstadoAnimo();
            int accion = personaje.getAccion();

            if (personaje.getAccion() == 0){
                // TODO: SETEAMOS EL FONDO
                switch (estado){
                    case 1:
                        imagenARenderizar = personaje.getImagenFeliz();
                        break;
                    case 2:
                        imagenARenderizar = personaje.getImagenNormal();
                        break;
                    case 3:
                        imagenARenderizar = personaje.getImagenTriste();
                        break;
                    default:
                        imagenARenderizar = personaje.getImagenFeliz();
                        break;
                }
            }else {
                Log.d("reality", String.valueOf(accion));
                switch (accion){
                    case 1:
                        imagenARenderizar = personaje.getImagenComer();
                        Log.d("ESTADISTA", "ESTA COMIENDO");

                        break;
                    case 2:
                        imagenARenderizar = personaje.getImagenServicio();
                        Log.d("ESTADISTA", "ESTA SERVICIO");
                        break;
                    case 3:
                        imagenARenderizar = personaje.getImagenResucitar();
                        break;
                    case 4:
                        imagenARenderizar = personaje.getImagenAcariciar();
                        break;
                    case 5:
                        imagenARenderizar = personaje.getImagenMorir();
                        break;
                    default:
                        imagenARenderizar = personaje.getImagenFeliz();
                        break;
                }
            }

            // El personaje solo muestra mensajes cuando este despierto
            //crearMensajesPersonaje();

        } // Si no está dormido


        return imagenARenderizar;

    }

    /**
     * Dibuja al personaje muerto
     */
    private ArrayList<Bitmap> getImagenPersonajeMuerto(){
        ArrayList<Bitmap> imagenARenderizar = new ArrayList<Bitmap>();
        Personaje personaje = vistaDeJuego.getJuegoMotor().getPersonaje();
        int accion = personaje.getAccion();
        Log.d("laaccion", String.valueOf(accion));
        if (accion == 5){
            imagenARenderizar = personaje.getImagenMorir();
            return imagenARenderizar;

        }else {
            imagenARenderizar = personaje.getImagenMuerto();
            return imagenARenderizar;
        }
    }


    /**
     * Crea los mensajes que el personaje
     * envia al jugador
     */
    private void crearMensajesPersonaje(){

        Personaje personaje = vistaDeJuego.getJuegoMotor().getPersonaje();

        this.mensajesAMostrar = new ArrayList<>();

        // Inserta los mensajes a mostrar por el personaje en un listado de mensajes
        for (Integer estado: personaje.getEstadosFisicos()) {
            int x = ((pantallaX/2)-(int)(400*pantallaRatioX));
            int y = ((pantallaY/2)-(int)(512*pantallaRatioY));

            Mensaje m = new Mensaje(x, y, getImagenEstadoFisicoPersonaje(estado));
            this.mensajesAMostrar.add(m);
        }
    }


    private Bitmap getImagenEstadoFisicoPersonaje(int estado){
        Bitmap imagen = null;

        switch (estado){
            case Personaje.ESTADO_CANSADO:
                imagen = BitmapFactory.decodeResource(vistaDeJuego.getResources(), R.drawable.mensaje_cansancio_20_9);
            break;
            case Personaje.ESTADO_ENFERMO:
                // Nulo de momento
            break;
            case Personaje.ESTADO_HAMBRIENTO:
                imagen = BitmapFactory.decodeResource(vistaDeJuego.getResources(), R.drawable.mensaje_hambre_20_9);
            break;
            case Personaje.ESTADO_NECESIDAD_IR_AL_SERVICIO:
                imagen = BitmapFactory.decodeResource(vistaDeJuego.getResources(), R.drawable.mensaje_necesidad_20_9);
            break;
        }


        return imagen;
    }


    /**
     * Se encarga de gestionar el renderizado de los mensajes que muestra
     * el personaje, controlando el tiempo que estan en pantalla y que no se solapen
     * @param canvas
     */
    private void renderizarMensajes(Canvas canvas){

        if(mensajesAMostrar!=null && mensajesAMostrar.size()>0){

            long momento = System.currentTimeMillis();

            if(momento > (cuandoSeMostroUltimoMensaje + tiempoMaximoEnPantalla)){

                // Comprueba el indice del mensaje mostrado
                if(mensajesIndiceActual>=mensajesAMostrar.size()){
                    mensajesIndiceActual --; //(mensajesAMostrar.size()-1);
                }

                mensajeActual = mensajesAMostrar.get(mensajesIndiceActual);

                // Desplaza el indice del mensaje en el que nos encontramos
                mensajesIndiceActual++;
                if(mensajesIndiceActual>=mensajesAMostrar.size()){
                    mensajesIndiceActual=0; //(mensajesAMostrar.size()-1);
                }

                tiempoMaximoEnPantalla = mensajeActual.getTiempoMaximoEnPantalla();
                cuandoSeMostroUltimoMensaje = momento;

            } // Fin control de tiempo entre notificaciones

            if(mensajeActual!=null){
                canvas.drawBitmap(mensajeActual.getImagen(), mensajeActual.getX(), mensajeActual.getY(), renderizador); // Se dibuja el fondo sobre el canvas
            }


        } else {
            this.mensajeActual = null;
        }

    }

    /*
    private class DownloadFilesTask extends AsyncTask<URL, Integer, Long> {
        protected Long doInBackground(URL... urls) {
            int count = urls.length;
            long totalSize = 0;
            for (int i = 0; i < count; i++) {
                totalSize += Downloader.downloadFile(urls[i]);
                publishProgress((int) ((i / (float) count) * 100));
                // Escape early if cancel() is called
                if (isCancelled()) break;
            }
            return totalSize;
        }

        protected void onProgressUpdate(Integer... progress) {
            setProgressPercent(progress[0]);
        }

        protected void onPostExecute(Long result) {
            showDialog("Downloaded " + result + " bytes");
        }
    }


     */



    /**
     * Reinicia los parametros para mostrar
     * mensajes del personaje por pantalla
     */
    private void limpiarMensajes(){

        this.mensajeActual = null;
        this.mensajesAMostrar = null;
        this.mensajesIndiceActual = 0;

    }

    /**
     * --------------------------------------------------------------------------
     * Al sotar el objeto de la comida vuelve a la posicion indicada
     * --------------------------------------------------------------------------
     * @param imagen
     * @param posX
     * @param posY
     */
    public void volverObjetoComdiaPosicionInicial(ImageView imagen, int posX, int posY){
        imagen.setX(posX);
        imagen.setY(posY);
    }

    /**
     * ---------------------------------------------------------------------------------------
     * Obtenemos el personaje
     * ---------------------------------------------------------------------------------------
     * getPersonaje() --> personaje
     * ---------------------------------------------------------------------------------------
     * @return Personaje
     */
    public Personaje getPersonaje(){
        return personaje;
    }






}