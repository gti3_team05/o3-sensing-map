package org.g05.o3receptor.model.entity;

/**
 * POJO para representar una entidad Usuario en la aplicacion
 * @author Adrián García Andreu
 *
 */
public class Usuario {

    private String correo, clave, nombre, apellidos, telefono; //Datos del usuario
    private int nodo; //Nodo del usuario


    /**
     * Constructor de clase por defecto
     */
    public Usuario(){ }



    /**
     * Constructor de clase con los
     * parametros de inicio de sesion
     *
     * @param correo Correo del usuario
     * @param clave Contraseña del usuario
     */
    public Usuario(String correo, String clave){
        this.clave = clave;
        this.correo = correo;
    }




    /////////////////////////////////////////
    /////// GETTERS Y SETTERS ///////////////
    /////////////////////////////////////////

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getNodo() {
        return nodo;
    }

    public void setNodo(int nodo) {
        this.nodo = nodo;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
