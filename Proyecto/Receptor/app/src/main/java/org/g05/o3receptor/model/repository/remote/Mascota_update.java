package org.g05.o3receptor.model.repository.remote;

import android.os.AsyncTask;
import android.util.Log;

import org.g05.o3receptor.model.entity.Mascota;
import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.g05.o3receptor.view.util.juego.TareasCallbackjuego;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Mascota_update extends AsyncTask<Void, Void, String> {

    private final int CODIGO_PETICION = Constantes.PETICION_MASCOTA;
    private final String recursoREST = "mascota/";
    private String parametros;

    private TareasCallbackjuego callback;
    private Mascota mascota;


    /**
     * Constructor de clase
     * Se genera mascota para enviar save state
     */
    public Mascota_update(Mascota mascota, TareasCallbackjuego callback ){
        this.mascota = mascota;
        this.callback =  callback;
    }

    /**
     * --------------------------------------------------------------
     * Se obtienen los parametros para realizar el envio
     * Se devuelve un texto con estro parametros de froma correcta
     */
    private String obtenerParametros(Mascota u){
        //Se añaden los parametros del request a la API REST como clave Valor
        HashMap<String, Object> parametros = new HashMap<>();
        // Campos obligatorios
        parametros.put("correo", u.getUsuario());
        parametros.put("minerales", String.valueOf(u.getMinerales()));
        parametros.put("comida", String.valueOf(u.getComida()));
        parametros.put("sueno", String.valueOf(u.getSueno()));
        parametros.put("servicio", String.valueOf(u.getServicio()));
        parametros.put("esperanza", String.valueOf(u.getEsperanza()));
        parametros.put("nombre", String.valueOf(u.getNombre()));
        // Se recorre el objeto clave valor y se insertan los valores en un Texto
        Iterator it = parametros.entrySet().iterator();
        boolean primero = true;
        String parametrosString = "";
        while (it.hasNext()){
            if(primero){
                primero = false;
            } else{
                parametrosString +="&"; //Se separan los datos con el caracter '&'
            }

            Map.Entry pair = (Map.Entry)it.next();
            parametrosString +=pair.getKey()+"="+pair.getValue(); //A cada dato se le asigna el valor tras el caracter '='
        }
        //Se devuelven los datos con sus valores como Texto
        return parametrosString;
    }


    @Override
    protected String doInBackground(Void... voids) {

        //url peticion mascota
        String retorno = null;
        this.parametros = obtenerParametros(mascota);

        URL url;
        HttpURLConnection connection = null;

        // ---------------------------------------
        // Peticion POST datos en la URL para actualizar la mascota
        try {
            url = new URL(Constantes.API_URI + recursoREST +"?"+parametros);
            Log.d("guardando", url.toString());
            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion
            connection.setDoOutput(true);
            connection.setRequestMethod("PUT");
            connection.setRequestProperty("Content-Type", "text/plain");
            connection.setRequestProperty("Accept", "*/*");

            // Escribte los datos a enviar en la peticion REST de tipo PUT
            connection.setDoOutput(true);
            Log.d("Inicio sesion", connection.getInputStream().toString());
            // Captura la respuesta
            Log.d("resultadolinea", connection.getInputStream().toString());

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";

            while ((linea = reader.readLine()) !=null){

                resultado += linea;

            }

            //Log.d("linea", linea);
            Log.d("resultadofinn", resultado);

            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }
            // Establece la conexion
            connection.connect();
            //Muestra por consola la respuesta
            Log.d("Inicio sesion", "Response: " + connection.getResponseMessage() + "");
        } catch (Exception e) {
            Log.e(e.toString(), " Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }
        return retorno;
    }

    @Override
    protected void onPostExecute(String resultado) {
        super.onPostExecute(resultado);

        boolean todoCorrecto = false;
        JSONObject resultadoJSON = null;

        if(resultado!=null){
            try {
                resultadoJSON = new JSONObject(resultado);

                //Log.d(ETIQUETA_LOG, resultadoJSON.toString());

                // Si el usuario ha podido actualizarse
                todoCorrecto = resultadoJSON.getBoolean("datos");

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        // Si el resultado es el esperado...
        if(todoCorrecto){
            callback.terminadoCorrecto(CODIGO_PETICION);
            Log.d("test_mascotapost", "conexion realizada");
        } else{
            callback.terminadoFallo(CODIGO_PETICION);
        }

    }
}
