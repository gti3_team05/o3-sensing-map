
package org.g05.o3receptor.view.util;

import android.os.AsyncTask;
import android.util.Log;
import android.view.ViewDebug;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/** -----------------------------------------------------------------------------------
 * Se consulta la RVVCCA para encontrar medidas de ozono obtenidas de una red oficial
 * @author Carlos Ibanez Lamas
 * -----------------------------------------------------------------------------------
 */
public class APIServidor extends AsyncTask<String, Void, String> {

    //Variables de la posicion del usuario
    private int contador;
    private ServicioBTLE servicioBTLE;
    private int medidaSensor;

    public APIServidor(ServicioBTLE servicio, int medicionSensor, int contador){
        this.medidaSensor = medicionSensor;
        this.servicioBTLE = servicio;
        this.contador = contador;
    }

    @Override
    protected String doInBackground(String... strings) {

        Log.d("APIServidor", "doInBack");

        String retorno = null;
        URL url;
        HttpURLConnection connection = null;

        // ---------------------------------------------------------------------------
        // Peticion POST datos en la URL para actualizar y recibir un JSON de la web
        try {
            url = new URL (Constantes.API_OZONO);

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion y parámetros de la consulta (se saben gracias a POSTMAN)
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Cookie","jbosscma7101=ffffffffaf17985c45525d5f4f58455e445a4a422d8d");
            connection.setRequestProperty("Accept", "*/*");

            // Escribte los datos a enviar en la peticion REST de tipo POST (id de la estacion de Gandia)
            connection.setDoOutput(true);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
            writer.write("{ \"idEstacion\" : \"5\" }");
            writer.close();


            // Captura la respuesta recibida
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }


            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.d("Consulta API", "Response: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.e(e.toString(), "Fallo en la consulta a la API");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }

        return retorno;
    }

    @Override
    protected void onPostExecute(String resultado) {
        //Despues de la consulta a la API se recibe un JSON con las mediciones de la estacion, por lo que extraemos la medición de o3
        super.onPostExecute(resultado);

        JSONObject resultadoJSON = null;

        //Se intenta leer el JSON, si no se devuelve la excepción
        try {

            resultadoJSON = new JSONObject(resultado);

            Log.d("Resultado API 1", String.valueOf(resultadoJSON));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        servicioBTLE.obtenerMedidasApi(resultadoJSON, medidaSensor);

    }


}


