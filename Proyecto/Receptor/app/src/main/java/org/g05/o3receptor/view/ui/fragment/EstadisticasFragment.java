package org.g05.o3receptor.view.ui.fragment;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.ekn.gruzer.gaugelibrary.HalfGauge;
import com.ekn.gruzer.gaugelibrary.Range;

import org.g05.o3receptor.R;
import org.g05.o3receptor.model.entity.Medida;
import org.g05.o3receptor.model.repository.remote.MedidasTipoUsuarioTiempo_get;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class EstadisticasFragment extends Fragment implements TareaCallback, AdapterView.OnItemSelectedListener{
    private static String ETIQUETA_LOG = "ESTADISTICAS FRAGMENT";

    private Activity actividadQueEjecuta;
    private View vistaRaiz;
    private Context contextoQueEjecuta;

    private String tipoMedidaSeleccionado = Constantes.TIPO_GAS_O3;

    /**
     * Inicio de comprobaciones al crear la actividad y estar
     * infladas las vistas
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        inicializarBarra();
        inicilializarSpinnerTiposMedidas();

    }

    private void inicializarBarra(){
        AppCompatActivity appCompatActivity = (AppCompatActivity)getActivity();
        appCompatActivity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);
    }

    // *************************************************************************************************************************************************************************** //
    // **************************************************************** CALIDAD AIRE ********************************************************************************************* //

    private void solicitarMedidasSensorPorUsuarioYTiempo(String tipoMedida){
        // Obtenemos los datos para solicitar las medidas
        String correoUsuario = obtenerCorreoUsuario();
        // Obtenemos la fecha de hoy
        Date date = new Date();
        String fecha = new SimpleDateFormat("yyyy-MM-dd").format(date);

        MedidasTipoUsuarioTiempo_get peticion = new MedidasTipoUsuarioTiempo_get(correoUsuario,tipoMedida,fecha +" 00:00:00", fecha +" 23:59:59",(TareaCallback)this);
        peticion.execute();
    }

    @Override
    public void iniciado(int codigoSolicitud) { }

    /**
     * Se lanza la actividad principal
     */
    @Override
    public void terminadoCorrecto(int codigoSolicitud) {
    }

    /**
     * Se muestra un mensaje de error en el inicio de sesión
     */
    @Override
    public void terminadoFallo(int codigoSolicitud) {
        Log.d(ETIQUETA_LOG,"Terminado error");

        // Mostramos indicando que hoy no has recogido medidas de ese sensor
        Toast.makeText(contextoQueEjecuta, getResources().getString(R.string.error_calidad_aire) + " " + tipoMedidaSeleccionado, Toast.LENGTH_LONG).show();
        // Ponemos el gauge a 0 ya que no hay valores
        iniciarGaugeACero();

    }

    @Override
    public void terminadoCorrectoConDatos(int codigoSolicitud, JSONArray datos, String tipoDatos) {
        Log.d(ETIQUETA_LOG,"Terminado correcto --> "+datos);

        long diaPromediado = promediarMedidasDiarias(datos);
        Log.d(ETIQUETA_LOG,"resultado "+diaPromediado);
        iniciarGauge(diaPromediado, tipoDatos);
    }

    @Override
    public void terminadoCorrectoMapa(int codigoSolicitud, JSONArray datos, Bitmap imagen, String tipoDatos) {

    }

    @Override
    public void terminadoCorrectoConDatosEstaciones(int codigoSolicitud, JSONArray datos, List<Medida> datosPuntos, String tipoDatos) {

    }

    private void iniciarGauge(long valor, String tipoMedida){
        HalfGauge halfGauge = vistaRaiz.findViewById(R.id.gauge);
        long valor_rango_inicio = 0;
        long valor_rango_peligro = 0;
        long valor_rango_extremo = 0;
        long valor_rango_final = 0;

        switch (tipoMedida){
            case Constantes.TIPO_GAS_O3:
                valor_rango_inicio = Constantes.VALOR_MINIMO_O3;
                valor_rango_peligro = Constantes.VALOR_MEDIO_O3;
                valor_rango_extremo = Constantes.VALOR_MAXIMO_O3;
                break;
            case  Constantes.TIPO_GAS_NO2:
                valor_rango_inicio = Constantes.VALOR_MINIMO_NO2;
                valor_rango_peligro = Constantes.VALOR_MEDIO_NO2;
                valor_rango_extremo = Constantes.VALOR_MAXIMO_NO2;
                break;
            case Constantes.TIPO_GAS_CO:
                valor_rango_inicio = Constantes.VALOR_MINIMO_CO;
                valor_rango_peligro = Constantes.VALOR_MEDIO_CO;
                valor_rango_extremo = Constantes.VALOR_MAXIMO_CO;
                break;
            case  Constantes.TIPO_GAS_SO2:
                valor_rango_inicio = Constantes.VALOR_MINIMO_SO2;
                valor_rango_peligro = Constantes.VALOR_MEDIO_SO2;
                valor_rango_extremo = Constantes.VALOR_MAXIMO_SO2;
                break;
        }

        // Nos aseguramos de que no den valores mayores que el rango extremo
        if(valor > valor_rango_extremo){
            valor_rango_final = valor;
        }else{
            valor_rango_final = valor_rango_extremo + 100;
        }

        // Nos aseguramos de que el valor no sea menor que el minimo
        if(valor < 0){
            valor = 0;
        }

        Range range = new Range();
        range.setColor(getResources().getColor(R.color.colorNormal));
        range.setFrom(valor_rango_inicio);
        range.setTo(valor_rango_peligro);

        Range range2 = new Range();
        range2.setColor(getResources().getColor(R.color.colorPeligro));
        range2.setFrom(valor_rango_peligro);
        range2.setTo(valor_rango_extremo);

        Range range3 = new Range();
        range3.setColor(getResources().getColor(R.color.colorExtremo));
        range3.setFrom(valor_rango_extremo);
        range3.setTo(valor_rango_final);

        halfGauge.addRange(range);
        halfGauge.addRange(range2);
        halfGauge.addRange(range3);

        halfGauge.setMinValue(valor_rango_inicio);
        halfGauge.setMaxValue(valor_rango_final);
        halfGauge.setValue(valor);
    }

    private void iniciarGaugeACero(){
        HalfGauge halfGauge = vistaRaiz.findViewById(R.id.gauge);

        Range range = new Range();
        range.setColor(Color.GRAY);
        range.setFrom(0);
        range.setTo(1);

        halfGauge.addRange(range);

        halfGauge.setMinValue(0);
        halfGauge.setMaxValue(1);
        halfGauge.setValue(0);
    }

    private String obtenerCorreoUsuario(){
        // Obtenemos el ultimo contador, para que si por ejemplo cerramos la app, no se pierdan los datos
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        String correo = preferencias.getString(Constantes.PREF_CORREO,"null");

        return correo;
    }

    private long promediarMedidasDiarias(JSONArray datos){
        double medida = 0;
        int tam = datos.length();

        for(int i=0; i<tam; i++){
            try {
                medida += datos.getJSONObject(i).getInt("valor");
            }
            catch (JSONException e) {
                Log.d(ETIQUETA_LOG,String.valueOf(e));
            }
        }

        return Math.round(medida/tam);
    }

    // *************************************************************************************************************************************************************************** //
    // ********************************************************************** SPINNERS ******************************************************************************************* //

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Inicializamos el spinner del tipo de medidas con las distintas opciones
     * --------------------------------------------------------------
     * inicilializarSpinnerTiposMedidas()
     * --------------------------------------------------------------
     */
    private void inicilializarSpinnerTiposMedidas(){
        Spinner spinner = (Spinner) vistaRaiz.findViewById(R.id.spinner_tipos_gases);
        // Creamos el ArrayAdaptar utilizando el array de strings y el tipo de item del spinner layout que hemos creado
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(contextoQueEjecuta, R.array.tipos_medidas, R.layout.color_spinner_layout);
        // Especificamos el layut sobre el que van a aparecer todas als opciones al desplegarlo
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_layout);
        // Ponemos el escuchador
        spinner.setOnItemSelectedListener(this);
        // Aplicamos el spinner
        spinner.setAdapter(adapter);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        String titulo = parent.getItemAtPosition(pos).toString();
        if(titulo.contains(Constantes.TIPO_GAS_O3)){

            tipoMedidaSeleccionado =  Constantes.TIPO_GAS_O3;

        }else if(titulo.contains(Constantes.TIPO_GAS_CO)){

            tipoMedidaSeleccionado =  Constantes.TIPO_GAS_CO;

        }else if(titulo.contains(Constantes.TIPO_GAS_NO2)){

            tipoMedidaSeleccionado =  Constantes.TIPO_GAS_NO2;

        }else if(titulo.contains(Constantes.TIPO_GAS_SO2)){

            tipoMedidaSeleccionado =  Constantes.TIPO_GAS_SO2;

        }

        solicitarMedidasSensorPorUsuarioYTiempo(tipoMedidaSeleccionado);
    }

    public void onNothingSelected(AdapterView<?> parent) { }

    // *************************************************************************************************************************************************************************** //
    // **************************************************************** CICLO VIDA *********************************************************************************************** //

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_estadisticas, container, false);
        this.vistaRaiz = root;

        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.actividadQueEjecuta = getActivity(); // Obtengo la actividad desde la que se ejecuta el fragment
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.contextoQueEjecuta = context; // Obtengo el contexto desde la que se ejecuta el fragment

    }

}