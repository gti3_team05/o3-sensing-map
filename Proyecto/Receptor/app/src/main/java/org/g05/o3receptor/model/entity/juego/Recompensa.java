package org.g05.o3receptor.model.entity.juego;

import com.google.android.gms.maps.model.LatLng;

public class Recompensa {
    private int imagen;
    private String titulo;
    private String descripcion;
    private String tipo;
    private boolean estaCumplida;
    private boolean sePuedeCobrarLaRecompensa;
    private int valorRecompensa;

    private int cantidadObtenida;
    private int cantidadAConseguir;

    private LatLng localizacionPunto;

    public Recompensa(int imagen, String titulo, String descripcion, String tipo, boolean estaCumplida, boolean sePuedeCobrarLaRecompensa, int valorRecompensa, int cantidadObtenida, int cantidadAConseguir, LatLng localizacionPunto) {
        this.imagen = imagen;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.estaCumplida = estaCumplida;
        this.sePuedeCobrarLaRecompensa = sePuedeCobrarLaRecompensa;
        this.valorRecompensa = valorRecompensa;
        this.cantidadObtenida = cantidadObtenida;
        this.cantidadAConseguir = cantidadAConseguir;
        this.localizacionPunto = localizacionPunto;
    }

    public LatLng getLocalizacionPunto() {
        return localizacionPunto;
    }

    public void setLocalizacionPunto(LatLng localizacionPunto) {
        this.localizacionPunto = localizacionPunto;
    }

    public boolean isSePuedeCobrarLaRecompensa() {
        return sePuedeCobrarLaRecompensa;
    }

    public void setSePuedeCobrarLaRecompensa(boolean sePuedeCobrarLaRecompensa) {
        this.sePuedeCobrarLaRecompensa = sePuedeCobrarLaRecompensa;
    }

    public int getImagen() {
        return imagen;
    }

    public void setImagen(int imagen) {
        this.imagen = imagen;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isEstaCumplida() {
        return estaCumplida;
    }

    public void setEstaCumplida(boolean estaCumplida) {
        this.estaCumplida = estaCumplida;
    }

    public int getValorRecompensa() {
        return valorRecompensa;
    }

    public void setValorRecompensa(int valorRecompensa) {
        this.valorRecompensa = valorRecompensa;
    }

    public int getCantidadObtenida() {
        return cantidadObtenida;
    }

    public void setCantidadObtenida(int cantidadObtenida) {
        this.cantidadObtenida = cantidadObtenida;
    }

    public int getCantidadAConseguir() {
        return cantidadAConseguir;
    }

    public void setCantidadAConseguir(int cantidadAConseguir) {
        this.cantidadAConseguir = cantidadAConseguir;
    }
}
