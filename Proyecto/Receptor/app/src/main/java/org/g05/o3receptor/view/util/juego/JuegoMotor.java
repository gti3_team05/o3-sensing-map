package org.g05.o3receptor.view.util.juego;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import org.g05.o3receptor.R;
import org.g05.o3receptor.model.entity.Mascota;
import org.g05.o3receptor.model.entity.Medida;
import org.g05.o3receptor.model.entity.Usuario;
import org.g05.o3receptor.model.entity.juego.Alimento;
import org.g05.o3receptor.model.entity.juego.Mensaje;
import org.g05.o3receptor.model.entity.juego.Personaje;
import org.g05.o3receptor.model.repository.remote.Mascota_get;
import org.g05.o3receptor.model.repository.remote.Mascota_get_detalles;
import org.g05.o3receptor.model.repository.remote.Mascota_update;
import org.g05.o3receptor.model.repository.remote.MedidasTipoUsuarioTiempo_get;
import org.g05.o3receptor.model.repository.remote.Usuarios_update;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.juego.TareasCallbackjuego;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;







import static android.content.Context.MODE_PRIVATE;

/**
 * Clase que representa el motoro de las mecanicas del modo juego donde se realizan las operaciones
 * para controlar el juego y utilidades del personaje en el juego.
 *
 * @author Adrián García Andreu
 */

public class JuegoMotor  implements TareasCallbackjuego   {

    private Personaje personaje; // Personaje principal del juego
    private int cantidadMinerales; // Moneda de juego

    private long ultimaActualizacion;
    private final long tiempoEntreActualizaciones = 3000; // 3 Segundos,  PARA DEBUG, el tiempo debería de pasar bastante más lento;

    private Context context;


    public JuegoMotor(Context context){

        this.context = context;

        this.cantidadMinerales = 30;

        cargarpartida();
        //guardarDatosBD();
        //setearDatosAlmacenadosAlPersonaje();
        //Context context = getApplicationContext();

        crearNuevoPersonaje(Constantes.NOMBRE_PORDEFECTO);
        // Miramos si es la primera vez que se accede a la mascota o no
        if(!esLaPrimeraVezConLaMascota()){
            // Leemos los valores del shared preferences y creamos una mascota con esas características
            Log.d("buda", "ayudame");
            setearDatosAlmacenadosAlPersonaje();
            actualizarValoresTrasTiempoEnReposo();
        }

        comprobarEstadoPersonaje();

    }

    /**
     * Obtiene si es la primera vez de la mascota en la bd
     * @return VoF --> V si es la primera vez que se accede a esta sección y F si no.
     */
    public boolean esLaPrimeraVezConLaMascota(){
        SharedPreferences preferencias = context.getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO,MODE_PRIVATE);
        if(preferencias.getBoolean(Constantes.PREF_PRIMERA_VEZ_JUEGO, true)){
            SharedPreferences.Editor miEditor = preferencias.edit();
            miEditor.putBoolean(Constantes.PREF_PRIMERA_VEZ_JUEGO, false);
            miEditor.commit();
            return true;
        }else{
            return false;
        }
    }


    /**
     * Crea un nuevo personaje
     * @param pj_nombre nombre del personaje
     */
    public void crearNuevoPersonaje(String pj_nombre){

        this.ultimaActualizacion = System.currentTimeMillis();

        this.personaje = new Personaje(pj_nombre, null); //TODO: Poner imagen
        cargardetalles();
        /**
         * comentado funciona mejor, pero desaparece el nombre al resucitar
         */


        /*
        cargarpartida();
        if(this.personaje.getHambre()>90){
            this.personaje.setHambre(70);
        }
        if(this.personaje.getCansancio()>90){
            this.personaje.setCansancio(70);
        }
        if(this.personaje.getNecesidadIrServicio()>90){
            this.personaje.setNecesidadIrServicio(70);
        }


         */


    }

    /**
     * Miramos los shared preferences para recoger los parámetros almacenados de:
     * esperanza de vida, hambre, cansancio, necesidad de ir al servicio, estado de animo, si está dormido o no y los distintos estados fisicos.
     */
    public void setearDatosAlmacenadosAlPersonaje(){
        SharedPreferences preferencias = context.getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO,MODE_PRIVATE);
        float esperanzaVida = preferencias.getFloat(Constantes.PREF_ESPERANZA_VIDA,100);
        float hambre = preferencias.getFloat(Constantes.PREF_HAMBRE,50);
        float cansancio = preferencias.getFloat(Constantes.PREF_CANSANCIO,50);
        float necesidadIrServicio = preferencias.getFloat(Constantes.PREF_NECESIDAD_IR_SERVICIO,50);
        int estadoAnimo = preferencias.getInt(Constantes.PREF_ESTADO_ANIMO, personaje.ANIMO_FELIZ);
        boolean dormido = preferencias.getBoolean(Constantes.PREF_DORMIDO, false);

        List<Integer> estadosFisicos = new ArrayList<>();
        SharedPreferences preferenciasEstadosFisicos = context.getSharedPreferences(Constantes.PREF_ESTADOS_FISICOS,MODE_PRIVATE);
        for(int i=0; i<preferenciasEstadosFisicos.getInt(Constantes.PREF_ESTADOS_FISICOS_TAM, 0); i++){
            estadosFisicos.add(preferenciasEstadosFisicos.getInt(Constantes.PREF_ESTADOS_FISICOS+i, 0));
        }

        SharedPreferences preferenciasAlimentos = context.getSharedPreferences(Constantes.PREF_ALIMENTOS, MODE_PRIVATE);

        for(int i=0; i<preferenciasAlimentos.getInt(Constantes.PREF_ALIMENTOS_TAM, 0); i++){
            int valor = preferenciasAlimentos.getInt(Constantes.PREF_ALIMENTOS_VALOR+i, 0);
            int coste = preferenciasAlimentos.getInt(Constantes.PREF_ALIMENTOS_COSTE+i, 0);
            String nombre = preferenciasAlimentos.getString(Constantes.PREF_ALIMENTOS_NOMBRE+i, "");

            byte[] imageAsBytes = Base64.decode(preferenciasAlimentos.getString(Constantes.PREF_ALIMENTOS_IMAGEN+i, "").getBytes(), Base64.DEFAULT);
            Bitmap imagen = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);

            Alimento alimento = new Alimento(true, nombre, imagen, valor, coste);
            this.personaje.setbolsillo(alimento);

        }

        this.cantidadMinerales = preferencias.getInt(Constantes.PREF_MINERALES,30);
        this.personaje.setEsperanzaDeVida(esperanzaVida);
        this.personaje.setHambre(hambre);
        this.personaje.setCansancio(cansancio);
        this.personaje.setNecesidadIrServicio(necesidadIrServicio);
        this.personaje.setEstadosFisicos(estadosFisicos);
        this.personaje.setEstadoAnimo(estadoAnimo);
        this.personaje.setDormido(dormido);
    }

    /**
     * Almacenamos los shared preferences para recoger los parámetros almacenados de:
     * esperanza de vida, hambre, cansancio, necesidad de ir al servicio, estado de animo, si está dormido o no y los distintos estados fisicos.
     */
    public void guardarParametrosPersonaje(){
        SharedPreferences preferencias = context.getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO,MODE_PRIVATE);
        SharedPreferences.Editor miEditor = preferencias.edit();
        miEditor.putFloat(Constantes.PREF_ESPERANZA_VIDA,this.personaje.getEsperanzaDeVida());
        miEditor.putFloat(Constantes.PREF_HAMBRE,this.personaje.getHambre());
        miEditor.putFloat(Constantes.PREF_CANSANCIO,this.personaje.getCansancio());
        miEditor.putFloat(Constantes.PREF_NECESIDAD_IR_SERVICIO,this.personaje.getNecesidadIrServicio());
        miEditor.putInt(Constantes.PREF_ESTADO_ANIMO, this.personaje.getEstadoAnimo());
        miEditor.putInt(Constantes.PREF_MINERALES, this.cantidadMinerales);
        miEditor.putBoolean(Constantes.PREF_DORMIDO, this.personaje.isDormido());
        miEditor.putLong(Constantes.PREF_TIEMPO, System.currentTimeMillis());
        miEditor.commit();

        List<Integer> estadosFisicos = this.personaje.getEstadosFisicos();
        SharedPreferences preferenciasEstadosFisicos = context.getSharedPreferences(Constantes.PREF_ESTADOS_FISICOS,MODE_PRIVATE);
        SharedPreferences.Editor miEditorEstados = preferenciasEstadosFisicos.edit();

        miEditorEstados.clear();
        miEditorEstados.putInt(Constantes.PREF_ESTADOS_FISICOS_TAM, estadosFisicos.size());

        for(int i=0; i<estadosFisicos.size(); i++){
            miEditorEstados.putInt(Constantes.PREF_ESTADOS_FISICOS+i, estadosFisicos.get(i));
        }
        miEditorEstados.commit();


        List<Alimento> alimentos = this.personaje.getbolsillo();
        SharedPreferences preferenciasAlimentos = context.getSharedPreferences(Constantes.PREF_ALIMENTOS, MODE_PRIVATE);
        SharedPreferences.Editor miEditorAlimentos = preferenciasAlimentos.edit();

        miEditorAlimentos.clear();
        miEditorAlimentos.putInt(Constantes.PREF_ALIMENTOS_TAM, alimentos.size());

        for(int i=0; i<alimentos.size(); i++){
            miEditorAlimentos.putInt(Constantes.PREF_ALIMENTOS_VALOR+i, alimentos.get(i).getValorNutricional());
            miEditorAlimentos.putInt(Constantes.PREF_ALIMENTOS_COSTE+i, alimentos.get(i).getCoste());

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            alimentos.get(i).getIcono().compress(Bitmap.CompressFormat.PNG, 100, baos);
            byte[] b = baos.toByteArray();
            String encoded = Base64.encodeToString(b, Base64.DEFAULT);

            miEditorAlimentos.putString(Constantes.PREF_ALIMENTOS_IMAGEN+i,encoded);
            miEditorAlimentos.putString(Constantes.PREF_ALIMENTOS_NOMBRE+i,alimentos.get(i).getNombre());
        }

        miEditorAlimentos.commit();
    }

    public void actualizarValoresTrasTiempoEnReposo(){
        SharedPreferences preferencias = context.getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO,MODE_PRIVATE);
        long tiempoViejo = preferencias.getLong(Constantes.PREF_TIEMPO, System.currentTimeMillis());
        long tiempoActual = System.currentTimeMillis();
        long diferenciaTiempoSegundos = tiempoActual - tiempoViejo;
        // TODO: INCREMENTA CADA 10 SEGUNDOS, CAMBIAR INTERVALO PORQUE SINO MUERE EN 1 MIN
        for(int i=0; i<diferenciaTiempoSegundos; i+=tiempoEntreActualizaciones){
            pasarTiempo();
        }
    }



    /**
     * Funcion para modificar los parametros del personaje
     * en funcion del paso del tiempo
     */
    public void pasarTiempo(){

        // Actualizo estados en funcion de los parametros
        this.personaje.darHambre(this.personaje.getFactorHambre());
        this.personaje.darNecesidadIrAlServicio(this.personaje.getFactorNecesidadServicio());
        if(personaje.isDormido()){ // Si esta dormido, se recupera
            this.personaje.restarCansancio(this.personaje.getFactorCansancio());
        } else {
            this.personaje.darCansancio(this.personaje.getFactorCansancio());

        }

        // Esperanza de vida
        // Si el personaje esta en condiciones optimas su esperanza de vida aumenta, haciendo que este no muera con el tiempo
        if( (personaje.getHambre()<50) && (personaje.getNecesidadIrServicio()<50) && (personaje.getCansancio()<50)){
            this.personaje.darEsperanzaDeVida(10); //TODO: Crear un beneficio de esperanza de vida no unitario
        } else {
            this.personaje.restarEsperanzaDeVida(this.personaje.getFactorEnvejecimiento());
        }


    }


    /**
     * Actualiza los estados y el ritmo de envejecimiento
     * de un personaje en funcion de sus parametros actuales
     */
    public void actualizarEstadoPersonaje(){


        // Si es necesario, se actualizan los estados
        List<Integer> estadosFisicosActualizar = new ArrayList<>(); // Listado de estados alterados
        if(this.personaje.getHambre()>this.personaje.LIMITE_HAMBRE){ // Si se supera un limite, el personaje tiene hambre
            estadosFisicosActualizar.add(this.personaje.ESTADO_HAMBRIENTO);
        }
        if(this.personaje.getCansancio()>this.personaje.LIMITE_CANSANCIO){ // Si se supera un limite, el personaje está cansado
            estadosFisicosActualizar.add(this.personaje.ESTADO_CANSADO);
        }
        if(this.personaje.getNecesidadIrServicio()>this.personaje.LIMITE_NECESIDAD_SERVICIO){ // Si se supera un limite, el personaje tiene ganas de ir al baño
            estadosFisicosActualizar.add(this.personaje.ESTADO_NECESIDAD_IR_AL_SERVICIO);
        }

        this.personaje.setEstadosFisicos(estadosFisicosActualizar);

        // -----------------------------------------
        // Fin actualizacion de estados


        // Modificacion del factor de la esperanza de vida en función de los estados alterados
        // Se tienen en cuenta los estados, pero afectan poco en la esperanza de vida
        float nuevoFactorEnvejecimiento;
        if(this.personaje.isDormido()){ // Personaje dormido/inactivo

            this.personaje.darFactoresDeIncrementoPersonajeDormido(); // Reduce el aumento de hambre, cansancio y ganas de ir al servicio
            nuevoFactorEnvejecimiento = this.personaje.FACTOR_ENVEJECIMIENTO_DORMIDO;
            for (Integer estado : estadosFisicosActualizar) {
                if (estado == personaje.ESTADO_HAMBRIENTO) {
                    nuevoFactorEnvejecimiento += .02f;
                }
                if (estado == personaje.ESTADO_CANSADO) {
                    nuevoFactorEnvejecimiento += .02f;
                }
                if (estado == personaje.ESTADO_NECESIDAD_IR_AL_SERVICIO) {
                    nuevoFactorEnvejecimiento += .01f;
                } // Se debería tener en cuenta el factor de enfermedad, al cabo de superar el maximo de ir al baño en X ocasiones
            }



        }else { // Personaje despierto/activo

            this.personaje.darFactoresDeIncrementoPorDefecto();

            nuevoFactorEnvejecimiento = this.personaje.FACTOR_ENVEJECIMIENTO_ORIGINAL;
            for (Integer estado : estadosFisicosActualizar) {
                if (estado == personaje.ESTADO_HAMBRIENTO) {
                    nuevoFactorEnvejecimiento += .2f;
                }
                if (estado == personaje.ESTADO_CANSADO) {
                    nuevoFactorEnvejecimiento += .2f;
                }
                if (estado == personaje.ESTADO_NECESIDAD_IR_AL_SERVICIO) {
                    nuevoFactorEnvejecimiento += .1f;
                } // Se debería tener en cuenta el factor de enfermedad, al cabo de superar el maximo de ir al baño en X ocasiones
            }


            // actualizo el estado de ánimo
            switch(estadosFisicosActualizar.size()){
                case 0:
                    this.personaje.setEstadoAnimo(personaje.ANIMO_FELIZ);
                    break;
                case 1:
                    this.personaje.setEstadoAnimo(personaje.ANIMO_NORMAL);
                    break;
                case 2:
                    this.personaje.setEstadoAnimo(personaje.ANIMO_TRISTE);
                    break;
            }
            // Si al personaje le queda poca vida... se muestra triste
            if(personaje.getEsperanzaDeVida()<15){
                this.personaje.setEstadoAnimo(personaje.ANIMO_TRISTE);
            }


        } // end if
        this.personaje.setFactorEnvejecimiento(nuevoFactorEnvejecimiento); // Se establece el factor de envejecimiento


    } // ()




    /**
     * Comprueba el estado del personaje/Juego, para asegurarse
     * de que el personaje está vivo, y en que estado se encuentra
     */
    public void comprobarEstadoPersonaje(){

        Log.d("PJ", " ");
        // Se comprueban los parametros
        if(!this.personaje.estoyVivo()){
            Log.d("PJ", "Ha muerto!");
            //TODO: Funciones para resucitar
        } else{

            List<Integer> estadosActuales = personaje.comoMeEncuentro();

            // ---------------------------------------
            // --------------- DEBUG
            // ---------------------------------------

            String str_estados = "";
            for (Integer estado: estadosActuales) {
                if(estado == personaje.ESTADO_HAMBRIENTO){ str_estados += "| HAMBRE ";}
                if(estado == personaje.ESTADO_CANSADO){str_estados += "| CANSADO ";}
                if(estado == personaje.ESTADO_NECESIDAD_IR_AL_SERVICIO){str_estados += "| GANAS DE IR AL BAÑO ";}
            }

            Log.d("PJ", "Factor env "+personaje.getFactorEnvejecimiento());
            Log.d("PJ", "Vida restante "+personaje.getEsperanzaDeVida()+" !!");

            Log.d("PJ", "Hambre "+personaje.getHambre());
            Log.d("PJ", "Sueño "+personaje.getCansancio());
            Log.d("PJ", "Necesidad ir al baño "+personaje.getNecesidadIrServicio());


            Log.d("PJ", " Estoy... "+str_estados);


            // ---------------------------------------
            // --------------- FIN DE DEBUG
            // ---------------------------------------

            // Pasa a los graficos el animo del personaje, para saber qcomo se muestra en funcion del animo

            int estadoAnimo = personaje.comoMeMuestro();

            Log.d("PJ", " ME encuentro... "+estadoAnimo);
            Log.d("PJ", " ");

        } // end if


    } // ()

    public Personaje getPersonaje() {
        return personaje;
    }

    public int getCantidadMinerales() {
        return cantidadMinerales;
    }

    public long getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public long getTiempoEntreActualizaciones() {
        return tiempoEntreActualizaciones;
    }


    /**
     * Actualiza el momento en el que se ha realizado la ultima actualizacion de datos
     * del personaje
     * @param ultimaActualizacion
     */
    public void setUltimaActualizacion(long ultimaActualizacion) {
        this.ultimaActualizacion = ultimaActualizacion;
    }


    // ----------------------------------------
    // ------ Utilidades de personaje
    // ----------------------------------------

    /**
     * Alimenta al personaje
     * @param alimento alimento con el que se alimenta al pj
     */
    public void alimentarPersonaje(Alimento alimento){

        // Sacia el hambre del personaje en funcion del alimento
        this.personaje.restarHambre(alimento.getValorNutricional());

        // Para proximos sprints, posibles perjuicios por comer, y aumento de peso en funcion del alimento

        this.actualizarEstadoPersonaje();
        this.comprobarEstadoPersonaje();
        guardarDatosBD();
    }

    /**
     * Alimenta al personaje
     *  cambiar nombre con el que se alimenta al pj
     */
    public void cambiarnombre(String nombre){

        // Sacia el hambre del personaje en funcion del alimento
        this.personaje.setNombre(nombre);

        this.actualizarEstadoPersonaje();
        this.comprobarEstadoPersonaje();
        guardarDatosBD();
    }

    /**
     * Pone al personaje a descansar
     * (Reduce los factores de envejecimiento y reduce el cansancio)
     */
    public void descansarPersonaje(){

        this.personaje.setDormido(true);

        this.actualizarEstadoPersonaje();
        this.comprobarEstadoPersonaje();
        guardarDatosBD();
    }


    /**
     * Pone al personaje activo, haciendo que se despierte
     * (Vuelve a establecer los factores de envejecimiento y otros a sus valores de juego por defecto)
     */
    public void despertarPersonaje(){

        this.personaje.setDormido(false);

        this.actualizarEstadoPersonaje();
        this.comprobarEstadoPersonaje();
        guardarDatosBD();
    }


    /**
     * Hace que el personaje vaya al servicio y alivie
     * su nivel de necesidad de ir a este de nuevo
     */
    public void atenderNecesidadesPersonaje(){
        // Se eliminan todas las ganas de ir al Servicio
        this.personaje.restarNecesidadIrAlServicio(100);

        this.actualizarEstadoPersonaje();
        this.comprobarEstadoPersonaje();
        guardarDatosBD();

        //mostrarMasInfo(tipoMedidaSeleccionado);

    }


    /**
     * Actualiza la cantidad de minerales
     * @param cantidadMinerales
     */
    public void setCantidadMinerales(int cantidadMinerales) {
        this.cantidadMinerales = cantidadMinerales;
    }
    /**
     * Funciones de guardado
     */
    private void cargarpartida(){
        String correoUsuario = obtenerUsuario();
        Mascota_get peticion = new Mascota_get(correoUsuario,(TareasCallbackjuego)this);
        peticion.execute();
    }
    private void cargardetalles(){
        String correoUsuario = obtenerUsuario();
        Mascota_get_detalles peticion = new Mascota_get_detalles(correoUsuario,(TareasCallbackjuego)this);
        peticion.execute();
    }

    @Override
    public void iniciado(int codigoSolicitud) { }

    /**
     * Se lanza la actividad principal
     */
    @Override
    public void terminadoCorrecto(int codigoSolicitud) {
    }

    /**
     * Se muestra un mensaje de error en el inicio de sesión
     */
    @Override
    public void terminadoFallo(int codigoSolicitud) {
        Log.d("ETIQUETA_LOG","Terminado error");
    }

    @Override
    public void terminadoCorrectoConDatos(int codigoSolicitud, JSONArray datos, String tipoDatos) {
        AsignarValores(datos);
    }
    @Override
    public void terminadoCorrectoConDatosDetalle(int codigoSolicitud, JSONArray datos, String tipoDatos) {
        AsignarDetalles(datos);
    }

    private String obtenerUsuario(){
        SharedPreferences preferencias = context.getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        String correo = preferencias.getString(Constantes.PREF_CORREO,"null");
        return correo;
    }
    /**
     * Funcion para cargado desde la base de datos, se realiza en carga
     * en este instante, carga
     * hambre, cansancio, servicio y el nombre
     */

    //esperanza no actualizandose porque genera en ocasiones interacciones raras
    //ya deberia de funcionar, de momento esta comentado por si acaso

    private void AsignarValores(JSONArray datos){

        int minerales = 0;
        float comida = 0;
        int servicio = 0 ;
        int sueno = 0;
        int esperanza = 0;
        String nombre = "";
        try {
            minerales += datos.getJSONObject(0).getInt("minerales");
            comida += datos.getJSONObject(0).getInt("comida");
            servicio += datos.getJSONObject(0).getInt("servicio");
            sueno += datos.getJSONObject(0).getInt("sueno");
            esperanza += datos.getJSONObject(0).getInt("esperanza");
            nombre += datos.getJSONObject(0).getString("nombre");
            Log.d("cargar","minerales"+ minerales + "comida" + comida + "sueno" + sueno + " servicio "+ servicio + "nombre" + nombre + "esperanza" + esperanza);
            SharedPreferences preferencias = context.getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO,MODE_PRIVATE);
            SharedPreferences.Editor miEditor = preferencias.edit();
            miEditor.putInt(Constantes.PREF_MINERALES, minerales);
            this.personaje.setHambre(comida);
            this.personaje.setCansancio(sueno);
            this.personaje.setNecesidadIrServicio(servicio);
            this.personaje.setEsperanzaDeVida(esperanza);
            this.personaje.setNombre(nombre);
            miEditor.commit();
        }
        catch (JSONException e) {
            Log.d("ETIQUETA_LOG",String.valueOf(e));
        }
    }

    private void AsignarDetalles(JSONArray datos){
        String nombre = "";
        try {
            nombre += datos.getJSONObject(0).getString("nombre");
            SharedPreferences preferencias = context.getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO,MODE_PRIVATE);
            SharedPreferences.Editor miEditor = preferencias.edit();
            this.personaje.setNombre(nombre);
            miEditor.commit();
        }
        catch (JSONException e) {
            Log.d("ETIQUETA_LOG",String.valueOf(e));
        }
    }

    /**
     * Funcion para guardado en la base de datos, se realiza en cada accion significativa
     * en este instante, guarda
     * hambre, cansancio, servicio y el nombre
     */
    public void guardarDatosBD(){
        SharedPreferences preferencias = context.getSharedPreferences(Constantes.PREF_NOMBRE_JUEGO,MODE_PRIVATE);
        String usuario =obtenerUsuario();
        //String  minerales= String.valueOf(preferencias.getInt(Constantes.PREF_MINERALES,30)) ;
        //String comida =  String.valueOf(preferencias.getFloat(Constantes.PREF_NECESIDAD_IR_SERVICIO,50));
        //String  servicio = String.valueOf(preferencias.getFloat(Constantes.PREF_HAMBRE,50));
        //String  sueno = String.valueOf(preferencias.getFloat(Constantes.PREF_CANSANCIO,50));
        //String nombre = Constantes.PREF_NOMBRE_JUEGO;
        String nombre = this.personaje.getNombre();
        this.personaje.getCansancio();
        Log.d("nombre", nombre);
        Log.d("servicio", String.valueOf(this.personaje.getCansancio()));
        Log.d("sueno", String.valueOf(this.personaje.getNecesidadIrServicio()));
        Mascota mascota = new Mascota(usuario,preferencias.getInt(Constantes.PREF_MINERALES,30)
                ,preferencias.getFloat(Constantes.PREF_HAMBRE,50)
                ,this.personaje.getNecesidadIrServicio()
                ,this.personaje.getCansancio()
                ,this.personaje.getEsperanzaDeVida()
                ,nombre);
        Mascota_update peticion = new Mascota_update(mascota,this);
        peticion.execute();
    }
}
