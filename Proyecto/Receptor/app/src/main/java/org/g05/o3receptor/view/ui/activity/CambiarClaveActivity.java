package org.g05.o3receptor.view.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.g05.o3receptor.R;
import org.g05.o3receptor.model.entity.Medida;
import org.g05.o3receptor.model.repository.remote.Recuperaciones_put;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;

import java.util.List;

public class CambiarClaveActivity extends AppCompatActivity implements TareaCallback {


    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambiar_clave);
    }


    public void volverAInicioSesion(View view){
        onBackPressed();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }


    /**
     * Se solicita al servidor una recuperacion de contraseña y envia un mail
     * @param v Vista sobre la que se ejerce el manejador de evento al clicar
     */
    public void correoRecuperacion(View v){

        // Generacion de token
        EditText et = findViewById(R.id.et_cambio_clave_correo);
        if(et.length()>0){
            String correo = et.getText().toString();

            Recuperaciones_put recuperacion = new Recuperaciones_put(this,correo);
            recuperacion.execute();
            pd = generarDialogoEspera();
            pd.show();

        } else {
            Log.e("ERROR CREACION TOKEN", "Correo VACIO");
        }


        // Envio de mail

    }

    @Override
    public void iniciado(int codigoSolicitud) {

    }

    @Override
    public void terminadoCorrecto(int codigoSolicitud) {

        if(codigoSolicitud==0){
            Log.d("CORREO", "Enviando correo");
            pd.dismiss();
            finish();
        }


    }

    @Override
    public void terminadoCorrectoConDatos(int codigoSolicitud, JSONArray datos, String tipoDatos) {

    }

    @Override
    public void terminadoCorrectoMapa(int codigoSolicitud, JSONArray datos, Bitmap imagen, String tipoDatos) {

    }

    @Override
    public void terminadoFallo(int codigoSolicitud) {
        pd.dismiss();
        finish();
    }

    @Override
    public void terminadoCorrectoConDatosEstaciones(int codigoSolicitud, JSONArray datos, List<Medida> datosPuntos, String tipoDatos) {

    }



    /**
     * Función que genera un dialogo que se muestra en pantalla
     * para dar información al usuario mientras se realizan tareas
     * en 2º plano
     *
     *
     * @return
     */
    private ProgressDialog generarDialogoEspera(){
        ProgressDialog pd = new ProgressDialog(CambiarClaveActivity.this);
        pd.setTitle(R.string.title_info_registro);
        pd.setMessage(getString(R.string.mensaje_info_registro));
        pd.setCancelable(false);

        return pd;
    } // ()



}