package org.g05.o3receptor.model.entity.juego;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.g05.o3receptor.R;

/**
 * Representa una imagen de fondo y su posición en la pantalla
 * @author Adrián García Andreu
 *
 */
public class Fondo {


    private int x,y; //posicion de la imagen de fondo
    private Bitmap fondo; // Imagen del fondo



    public Fondo(int pantallaX, int pantallaY, Resources recurso, int fondoDrawableId){
        this.x = 0; this.y=0;
        this.fondo = BitmapFactory.decodeResource(recurso, fondoDrawableId);
        this.fondo = Bitmap.createScaledBitmap(this.fondo, pantallaX, pantallaY, false);
       // this.fondo = scaleBitmap(this.fondo);

    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Bitmap getFondo() {
        return fondo;
    }

    public void setFondo(Bitmap fondo) {
        this.fondo = fondo;
    }


}
