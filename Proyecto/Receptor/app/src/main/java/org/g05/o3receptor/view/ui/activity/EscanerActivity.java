package org.g05.o3receptor.view.ui.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.webkit.URLUtil;

import com.budiyev.android.codescanner.CodeScanner;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import org.g05.o3receptor.R;

import java.io.IOException;

public class EscanerActivity extends AppCompatActivity {

    private CodeScanner escaner;

    private final String NODO_CLAVE = "idNodo";

    private CameraSource camaraFisica;
    private SurfaceView camaraVista;
    private final int PERMISIO_CAMARA = 1;

    private String token = "";
    private String tokenanterior = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escaner);

        //Lanzar QR
        camaraVista = (SurfaceView) findViewById(R.id.escaner_vista_camara);
        lanzarEscaner();

    }


/*
    private void lanzarEscaner() {
        CodeScannerView scannerView = findViewById(R.id.escaner_escaner);

        escaner = new CodeScanner(this, scannerView);
        escaner.setDecodeCallback(new DecodeCallback() {

            @Override
            public void onDecoded(@NonNull final Result result) {
                    finalizarEscaner(result.getText());
            }
        });

        // Evento al detectar un click
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });

        // Error en el escaner
        escaner.setErrorCallback(new ErrorCallback() {
            @Override
            public void onError(@NonNull Exception error) {
                error.printStackTrace();
            }
        });

        // Inicia el escaneo con los formatos seleccionados, en nuestro caso solo QR
        //List<BarcodeFormat> formatos = new ArrayList<>(); formatos.add(BarcodeFormat.QR_CODE);
        //mCodeScanner.setFormats(formatos);
        escaner.startPreview();


    } // ()
*/

    /**
     * Devuelve la activdad al registro
     * @param idNodo
     */
    private void finalizarEscaner(String idNodo){

        Intent datosEscaneados = new Intent();
        datosEscaneados.putExtra(NODO_CLAVE, idNodo);

        setResult(RESULT_OK, datosEscaneados);
        finish();

    }


    /**
     * Lanza una intencion a la camara del sistema
     * y lee los codigos QR detectados por esta
     */
    public void lanzarEscaner() {

        // creo el detector qr
        BarcodeDetector barcodeDetector =
                new BarcodeDetector.Builder(this)
                        .setBarcodeFormats(Barcode.ALL_FORMATS)
                        .build();

        // creo la camara
        Point punto = new Point();
        getWindowManager().getDefaultDisplay().getSize(punto);

        camaraFisica = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(punto.y, punto.x)
                .setAutoFocusEnabled(true) //you should add this feature
                .build();

        // listener de ciclo de vida de la camara
        camaraVista.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {

                // verifico si el usuario dio los permisos para la camara
                if (ActivityCompat.checkSelfPermission(EscanerActivity.this, Manifest.permission.CAMERA)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        // verificamos la version de ANdroid que sea al menos la M para mostrar
                        // el dialog de la solicitud de la camara
                        if (shouldShowRequestPermissionRationale(
                                Manifest.permission.CAMERA)) ;
                        requestPermissions(new String[]{Manifest.permission.CAMERA},
                                PERMISIO_CAMARA);
                    }
                    return;
                } else {
                    try {
                        camaraFisica.start(camaraVista.getHolder());
                    } catch (IOException ie) {
                        Log.e("CAMERA SOURCE", ie.getMessage());
                    }
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                camaraFisica.stop();
            }
        });

        // preparo el detector de QR
        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {
            }


            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> barcodes = detections.getDetectedItems();

                if (barcodes.size() > 0) {

                    // obtenemos el token
                    token = barcodes.valueAt(0).displayValue.toString();

                    // verificamos que el token anterior no se igual al actual
                    // esto es util para evitar multiples llamadas empleando el mismo token
                    if (!token.equals(tokenanterior)) {

                        // guardamos el ultimo token proceado
                        tokenanterior = token;
                        Log.i("token", token);

                        if (URLUtil.isValidUrl(token)) {
                            // si es una URL valida abre el navegador
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(token));
                            startActivity(browserIntent);
                        } else {
                            // comparte en otras apps
                            /*
                            Intent shareIntent = new Intent();
                            shareIntent.setAction(Intent.ACTION_SEND);
                            shareIntent.putExtra(Intent.EXTRA_TEXT, token);
                            shareIntent.setType("text/plain");
                            startActivity(shareIntent);

                             */
Log.d("QR", "DETECADO> "+token);
                            finalizarEscaner(token);
                        }

                        new Thread(new Runnable() {
                            public void run() {
                                try {
                                    synchronized (this) {
                                        wait(5000);
                                        // limpiamos el token
                                        tokenanterior = "";
                                    }
                                } catch (InterruptedException e) {
                                    Log.e("Error", "Waiting didnt work!!");
                                    e.printStackTrace();
                                }
                            }
                        }).start();

                    }
                }
            }
        });

    }


}
