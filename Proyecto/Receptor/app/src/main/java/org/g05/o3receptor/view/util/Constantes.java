package org.g05.o3receptor.view.util;

import android.graphics.Bitmap;

/**
 * Clase con recursos estáticos/compartidos por toda la aplicación
 * @author Adrián García Andreu
 */
public class    Constantes {

    // DEBUG
    public static final String API_URI = "http://192.168.1.101/proyecto3A/api/v1.0/";
    public static final String APP_URI = "http://192.168.1.101/proyecto3a/app/";

    // SERVIDOR REAL
    //public static final String API_URI = "https://adgaran1.upv.edu.es/api/v1.0/";

    // SERVIDOR MEDIDAS
    public static final String API_OZONO = "https://webcat-web.gva.es/webcat_web/datosOnlineRvvcca/obtenerEstacionById";
    public static final String API_EXTRAER_OZONO = "https://webcat-web.gva.es/webcat_web/datosOnlineRvvcca/obtenerTablaPestanyaDatosOnline";

    // Claves SHARED PREFERENCES
    public static final String PREF_NOMBRE = "org.g05.o3receptor";
    public static final String PREF_CORREO = "org.g05.o3receptor.CORREO";
    public static final String PREF_CLAVE = "org.g05.o3receptor.CLAVE";
    public static final String PREF_USUARIO = "org.g05.o3receptor.NOMBREUSUARIO";
    public static final String PREF_APELLIDOS = "org.g05.o3receptor.APELLIDOS";
    public static final String PREF_TELEFONO = "org.g05.o3receptor.TELEFONO";
    public static final String PREF_MODO_TESTER = "org.g05.o3receptor.MODOTESTER";
    public static final String PREF_PRIMERA_VEZ_APP = "org.g05.o3receptor.PRIMERAVEZAPP";
    public static final String PREF_MASCOTA = "org.g05.o3receptor.MASCOTA";
    public static final String PREF_SEGUNDO_PLANO = "org.g05.o3receptor.SEGUNDOPLANO";
    public static final String PREF_GPS_ENCENDIDO = "org.g05.o3receptor.GPSENCENDIDO";
    public static final String PREF_NOTIFICACIONES_MEDIDAS = "org.g05.o3receptor.NOTIFICACIONESMEDIDAS";
    public static final String PREF_NOTIFICACIONES_DISTANCIA = "org.g05.o3receptor.NOTIFICACIONESDISTANCIA";
    //--------------------------------------------------------------------------------------------------------------
    public static final String PREF_NOMBRE_JUEGO = "org.g05.o3receptor.JUEGO";
    public static final String PREF_PRIMERA_VEZ_JUEGO = "org.g05.o3receptor.PRIMERAVEZJUEGO";
    public static final String PREF_TIEMPO = "org.g05.o3receptor.TIEMPO";
    public static final String PREF_ESPERANZA_VIDA = "org.g05.o3receptor.ESPERANZAVIDA";
    public static final String PREF_HAMBRE = "org.g05.o3receptor.HAMBRE";
    public static final String PREF_CANSANCIO = "org.g05.o3receptor.CANSANCIO";
    public static final String PREF_NECESIDAD_IR_SERVICIO = "org.g05.o3receptor.NECESIDADIRSERVICIO";
    public static final String PREF_ESTADOS_FISICOS = "org.g05.o3receptor.ESTADOSFISICOS";
    public static final String PREF_ESTADOS_FISICOS_TAM = "org.g05.o3receptor.ESTADOSFISICOSTAM";
    public static final String PREF_ESTADO_ANIMO = "org.g05.o3receptor.ESTADOANIMO";
    public static final String PREF_DORMIDO = "org.g05.o3receptor.DORMIDO";
    public static final String PREF_MINERALES = "org.g05.o3receptor.JUEGOMINERALES";
    public static final String PREF_ALIMENTOS = "org.g05.o3receptor.ALIMENTOS";
    public static final String PREF_ALIMENTOS_VALOR = "org.g05.o3receptor.ALIMENTOSVALOR";
    public static final String PREF_ALIMENTOS_COSTE = "org.g05.o3receptor.ALIMENTOSCOSTE";
    public static final String PREF_ALIMENTOS_NOMBRE = "org.g05.o3receptor.ALIMENTOSNOMBRE";
    public static final String PREF_ALIMENTOS_IMAGEN = "org.g05.o3receptor.ALIMENTOSIMAGEN";
    public static final String PREF_ALIMENTOS_TAM = "org.g05.o3receptor.ALIMENTOSTAM";
    //--------------------------------------------------------------------------------------------------------------
    public static final String PREF_NOMBRE_RECOMPENSAS = "org.g05.o3receptor.RECOMPENSAS";
    public static final String PREF_RECOMPENSA_DIARIA = "org.g05.o3receptor.RECOMENSADIARIAS";
    public static final String PREF_RECOMPENSAS_NUMERO = "org.g05.o3receptor.RECOMPENSASNUMERO";
    public static final String PREF_RECOMPENSA_IMAGEN = "org.g05.o3receptor.RECOMPENSAIMAGEN";
    public static final String PREF_RECOMPENSA_TITULO = "org.g05.o3receptor.RECOMPENSATITULO";
    public static final String PREF_RECOMPENSA_DESCRIPCION = "org.g05.o3receptor.RECOMPENSADESCRIPCION";
    public static final String PREF_RECOMPENSA_TIPO = "org.g05.o3receptor.RECOMPENSATIPO";
    public static final String PREF_RECOMPENSA_ESTA_CUMPLIDA = "org.g05.o3receptor.RECOMPENSAESTACUMPLIDA";
    public static final String PREF_RECOMPENSA_SE_PUEDE_COBRAR = "org.g05.o3receptor.RECOMPENSASEPUEDECOBRAR";
    public static final String PREF_RECOMPENSA_VALOR = "org.g05.o3receptor.RECOMPENSAVALOR";
    public static final String PREF_RECOMPENSA_CANTIDAD_CONSEGUIDA = "org.g05.o3receptor.RECOMPENSACANTIDADCONSEGUIDA";
    public static final String PREF_RECOMPENSA_CANTIDAD_A_CONSEGUIR = "org.g05.o3receptor.RECOMPENSACANTIDADACONSEGUIR";
    public static final String PREF_RECOMPENSA_LATITUD = "org.g05.o3receptor.RECOMPENSALATITUD";
    public static final String PREF_RECOMPENSA_LONGITUD = "org.g05.o3receptor.RECOMPENSALONGITUD";
    //--------------------------------------------------------------------------------------------------------------
    public static final String PREF_LATITUD_USUARIO = "org.g05.o3receptor.LATITUDUSUARIO";
    public static final String PREF_LONGITUD_USUARIO = "org.g05.o3receptor.LONGITUDUSUARIO";
    //--------------------------------------------------------------------------------------------------------------
    public static final String PREF_IDIOMA_APP = "org.g05.o3receptor.PREFERENCIASIDIOMA";
    public static final String PREF_IDIOMA = "org.g05.o3receptor.PREFERENCIASIDIOMA";


    // Codigos de peticiones asíncronas
    public static final int PETICION_INICIO_SESION = 9;
    public static final int PETICION_REGISTRO_USUARIO = 10;
    public static final int PETICION_NODO_ASOCIADO = 11;
    public static final int PETICION_MEDIDAS_TIPO_USUARIO_TIEMPO = 12;
    public static final int PETICION_ACTUALIZAR_PERFIL = 13;
    public static final int PETICION_INTERPOLACION = 14;
    public static final int PETICION_MASCOTA = 15;
    public static final int PETICION_REGISTRO_ESTACION = 15;
    public static final int PETICION_ESTACIONES = 16;
    public static final int PETICION_IMAGENES = 17;
    public static final int PETICION_DATOS_ALREDEDOR_USUARIO = 18;

    public static final int MAXIMO_CONTADOR_MEDIDAS_ERRONEAS = 3;

    // TIPOS MEDIDAS
    public static final String TIPO_GAS_O3 = "O3";
    public static final String TIPO_GAS_SO2 = "SO2";
    public static final String TIPO_GAS_NO2 = "NO2";
    public static final String TIPO_GAS_CO = "CO";
    public static final String TIPO_TEMPERATURA = "TEMPERATURA";
    public static final String TIPO_RUIDO = "RUIDO";
    public static final String TIPO_BATERIA_BAJA = "BATERIA BAJA";

    // TIPOS MEDIDAS ID
    public static final int ID_TIPO_GAS_O3 = 11;
    public static final int ID_TIPO_TEMPERATURA = 12;
    public static final int ID_TIPO_TIPO_RUIDO = 13;
    public static final int ID_TIPO_BATERIA_BAJA = 14;

    // VALORES LIMITE DE LOS GASES
    public static final int VALOR_MINIMO_O3 = 0;
    public static final int VALOR_MEDIO_O3 = 180;
    public static final int VALOR_MAXIMO_O3 = 240;
    public static final int VALOR_MINIMO_NO2 = 200;
    public static final int VALOR_MEDIO_NO2 = 500;
    public static final int VALOR_MAXIMO_NO2 = 1080;
    public static final int VALOR_MINIMO_CO = 10;
    public static final int VALOR_MEDIO_CO = 34;
    public static final int VALOR_MAXIMO_CO = 115;
    public static final int VALOR_MINIMO_SO2 = 20;
    public static final int VALOR_MEDIO_SO2 = 125;
    public static final int VALOR_MAXIMO_SO2 = 350;

    // Tipo mapa modo tester
    public static final String TIPO_MAPA_TEST_RANDOM="aleatorio";
    public static final String TIPO_MAPA_TEST_DEGRADADO="degradado";
    public static final String TIPO_MAPA_TEST_VALORES_BAJOS="valoresBajos";
    public static final String TIPO_MAPA_TEST_VALORES_MEDIOS="valoresMedios";
    public static final String TIPO_MAPA_TEST_VALORES_ALTOS="valoresAltos";
    public static final String TIPO_FAKE="datosFake";
    public static final String TIPO_REALES="datosReales";

    // Constantes del Modo juego
    public static final String NOMBRE_PORDEFECTO = "BioZono";
    public static final int VALOR_MINERAL_PORDEFECTO = 1;
    public static final int VALOR_MINERAL_TESTEO = 100;

    // RECOMPENSAS
    public static final int valorMinimo = 1;

    public static final String TIPO_RECOMPENSA_ZONA = "IRZONA";
    public static final int valorMaximoZona = 500;

    public static final String TIPO_RECOMPENSA_CALIDAD_AIRE = "CALIDADAIRE";
    public static final int valorMaximoCalidadAire = 50;

    public static final String TIPO_RECOMPENSA_RECOGER_MEDIDAS = "RECOGERMEDIDAS";
    public static final int valorMaximoMedidas = 150;
    public static final int valorMaximoCantidadMedidas = 1;
    public static final int valorMinimoCantidadMedidas = 2880; //30seg --> 1 medición. 1h--> 120 mediciones. 24h --> 2880 mediciones
}
