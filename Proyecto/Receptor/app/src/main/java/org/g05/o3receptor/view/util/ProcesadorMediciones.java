package org.g05.o3receptor.view.util;

import android.content.SharedPreferences;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;

import org.g05.o3receptor.model.entity.EnteroEn2Bytes;
import org.g05.o3receptor.model.entity.Incidencia;
import org.g05.o3receptor.model.entity.Medida;
import org.g05.o3receptor.model.repository.remote.Incidencias_post;
import org.g05.o3receptor.model.repository.remote.Medidas_post;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Procesa las medicionas a partir de un beacon de tipo iBeacon y las envía
 * @author Adrián García Andreu
 * @version 1.1
 */
public class ProcesadorMediciones {

    // --------------------------------------------------------------

    private int ultimoContadorVisto;     // Contador ultima medida tomada (para evitar beacons repetidos)
    private HashMap<Integer, String> tipoMedida; // Tipos de medida posibles
    public int ultimoTipoVisto; // Ultimo tipo de medida visto (clave del par clave/valor tipoMedida)


    // --------------------------------------------------------------
    // Parametros para obtener la ubicacion
    private FusedLocationProviderClient manejadorUbicaciones; // Servicio de localizacion del sistema Android
    private LocationRequest solicitudUbicacion; // Peticion de ubicacion
    private LocationCallback callbackUbicacion; // Callback para la peticion de ubicacion
    public Location ultimaUbicacionVista; // Ultima actualizacion conocida del usuario
    public boolean ubicacionActualizada; // Determina si la ubicacion se ha actualizado al tomar la ultima medida, o es anterior.
    private ServicioBTLE servicioBTLE;


     /**
      * --------------------------------------------------------------
      * --------------------------------------------------------------
      * Constructor de clase, se inicializan las variables que lo requieren
      * y a este se le pasa como parametro la actividad desde la que se ejectua
      * para poder acceder a esta y a su contexto
      * -------------------------------------------------------------
      * Activity --> ProcesadorMediciones()
      * -------------------------------------------------------------
     */
    public ProcesadorMediciones(ServicioBTLE servicioBTLE, int ultimoContadorVisto) {
        this.servicioBTLE = servicioBTLE;

        this.ultimoContadorVisto = ultimoContadorVisto;
        this.ubicacionActualizada = false;

        tipoMedida = new HashMap<>();
        tipoMedida.put(Constantes.ID_TIPO_GAS_O3, Constantes.TIPO_GAS_O3);
        tipoMedida.put(Constantes.ID_TIPO_TEMPERATURA, Constantes.TIPO_TEMPERATURA);
        tipoMedida.put(Constantes.ID_TIPO_TIPO_RUIDO, Constantes.TIPO_RUIDO);
        tipoMedida.put(Constantes.ID_TIPO_BATERIA_BAJA, Constantes.TIPO_BATERIA_BAJA);
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Se extraen los valores de un beacon a traves de los bytes
     * de este, y se comprueba si el beacon es valido
     * --------------------------------------------------------------
     *  major:Z, minor:Z --> extraerMediciones() --> ValorDeLaMedida:Z
     * ---------------------------------------------------------------
     * @param major Byte del beacon BLE, contiene el tipo de medida y un contador para la comrpobacion de integridad cada uno en 8 bits de sus 16 totales
     * @param minor Byte del beacon BLE, contiene el valor de la medida tomada
     *
     * @return Si la medida es valida, se retorna el valor del minor, si no lo es, devolvera un nulo
    */
    public Integer extraerMedicion(int major, int minor) {

        EnteroEn2Bytes valoresMajor = descomponerEn2bytes(major);

        int medidaTipo = valoresMajor.getMSB(); // 1 byte del major, ID de la medida
        int contadorBeacon = valoresMajor.getLSB(); // 2o byte del major, contador del beacon


        //Si el ultimo beacon recibido no es el mismo que se habia recibido, se actualiza el contador y se devuelve la medida
        if (contadorBeacon != this.ultimoContadorVisto) {

            Log.e("ERROR", "CONTADOR BE " + contadorBeacon);

            this.ultimoContadorVisto = contadorBeacon;
            this.servicioBTLE.guardarUltimoContadorVisto(this.ultimoContadorVisto);

            this.ultimoTipoVisto = medidaTipo;
            return minor;

        } else {
            return null;
        } //No se retorna nada


    } // ()

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Se envia una medida valida al servidor REST
     * --------------------------------------------------------------
     *  Medida --> enviarMedida()
     * --------------------------------------------------------------
     * @param m Medida obtenida del beacon BLE con tipo y coordenadas GPS asociadas
     */
    public void enviarMedida(Medida m){
        this.ubicacionActualizada = false; // Marco como falso la ubicacion actualizada, para la proxima medida que se tome


        //PARA DEPURAR SOLO, muestra le medida por consola
        Log.i("MEDIDA","Se envía: "+m.getMedida()+" ug/m^3 " +
                "de tipo "+tipoMedida.get(m.getTipo())+" " +
                "en latitud "+m.getUbicacion().getLatitude()+ " longitud "+m.getUbicacion().getLongitude());
        //


        // En funcion de la medida, se obtienen los parametros para la peticion al servidor REST
        String parametros = obtenerParametrosMedidaAPI(m);

        //Se envia la medida a la API REST con los parametros necesarios
        Medidas_post envioDeLaMedida = new Medidas_post(servicioBTLE);
        envioDeLaMedida.execute(Constantes.API_URI+"/medidas/", parametros);

    } // ()


    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Descompone un valor entero de 16 bits en 2 valores
     * simples de 8 bits.
     * --------------------------------------------------------------
     *  Z --> descomponerEn2Bytes() --> EnteroEn2Bytes
     * --------------------------------------------------------------
     * @param valor valor de 16 bits a descomponer en valores de 8 bits
     */
    private EnteroEn2Bytes descomponerEn2bytes(int valor){

        // Mascara de conversion
        int factorConversion = 0b0000000011111111; //0x00FF

        int MSB = valor >> 8; // Desplazamiento de 8 Bits a la derecha para usar como valor solo el MSB
        int LSB = (valor & factorConversion); // Multiplica a nivel de bits por la mascara de conversion para despreciar los primeros 8 bits

        return new EnteroEn2Bytes(MSB, LSB);
    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     *  getTiposMedida() --> Clave/Valor<N, Texto>
     * --------------------------------------------------------------
     * @return HaspMap con los tipos de medidas
     */
    public HashMap<Integer, String> getTiposMedida() {
        return tipoMedida;
    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     *  getUltimoTipoVisto() --> N
     * --------------------------------------------------------------
     * @return Tipo de la ultima medida tomada
     */
    public int getUltimoTipoVisto() {
        return ultimoTipoVisto;
    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     *  getUltimaUbicacionVista() --> Location
     * --------------------------------------------------------------
     * @return Ultima ubicacion conocida
     */
    public Location getUltimaUbicacionVista() {
        return ultimaUbicacionVista;
    }



    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion utilizada para saber si la ubicacion se ha actualizado desde
     * la ultima vez que se solicitio conocer la ubicacion del dispositivo
     * --------------------------------------------------------------
     *  isUbicacionActualizada() --> Booleano
     * --------------------------------------------------------------
     * @return retorna si la ubicacion ha sido actualizada
     */
    public boolean isUbicacionActualizada() {
        return ubicacionActualizada;
    }




    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion para obtener de una Medida un String con el formato requerido
     * con los valores para realizar una peticion al servidor REST
     * --------------------------------------------------------------
     *  Medida --> obtenerParametrosMedidaAPI() --> String
     * --------------------------------------------------------------
     * @param m Medida con un valor, tipo y ubicacion
     * @return retorna un Texto con los valores de la Medida formateados para que la API pueda interpretarlos
     */
    private String obtenerParametrosMedidaAPI(Medida m){

        //Se añaden los parametros del request a la API REST como clave Valor
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("valor", m.getMedida());
        parametros.put("tipoMedida", m.getTipo());
        parametros.put("latitud", m.getUbicacion().getLatitude());
        parametros.put("longitud", m.getUbicacion().getLongitude());
        parametros.put("correo", m.getCorreo());


        // Se recorre el objeto clave valor y se insertan los valores en un Texto
        Iterator it = parametros.entrySet().iterator();
        boolean primero = true;
        String parametrosString = "";
        while (it.hasNext()){
            if(primero){
                primero = false;
            } else{
                parametrosString +="&"; //Se separan los datos con el caracter '&'
            }

            Map.Entry pair = (Map.Entry)it.next();
            parametrosString +=pair.getKey()+"="+pair.getValue(); //A cada dato se le asigna el valor tras el caracter '='
        }

        //Se devuelven los datos con sus valores como Texto
        return parametrosString;

    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Se envia una incidencia al servidor REST
     * --------------------------------------------------------------
     *  Incidencia --> enviarIncidencia()
     * --------------------------------------------------------------
     * @param i Incidencia que se ha producido
     */
    public void enviarIncidencia(Incidencia i){

        //PARA DEPURAR SOLO, muestra la incidencia por consola
        Log.i("INCIDENCIA","Se envía: Tipo de incidencia"+ i.getTipoIncidencia()+" " +
                "del usuario "+i.getCorreoIncidencia()+ " en estado "+i.getEstadoIncidencia());
        //

        //Se envia la medida a la API REST con los parametros necesarios
        Incidencias_post envioDeLaIncidencia = new Incidencias_post(i);
        envioDeLaIncidencia.execute();

    } // ()

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Funcion para obtener de una Incidencia un String con el formato requerido
     * con los valores para realizar una peticion al servidor REST
     * --------------------------------------------------------------
     *  Incidencia --> obtenerParametrosIncidenciaAPI() --> String
     * --------------------------------------------------------------
     * @param i Incidencia con un correo, tipo y estado
     * @return retorna un Texto con los valores de la Incidencia formateados para que la API pueda interpretarlos
     */
    private String obtenerParametrosIncidenciaAPI(Incidencia i){

        //Se añaden los parametros del request a la API REST como clave Valor
        HashMap<String, Object> parametros = new HashMap<>();
        parametros.put("usuario", i.getCorreoIncidencia());
        parametros.put("tipoIncidencia", i.getTipoIncidencia());
        parametros.put("estado", i.getEstadoIncidencia());


        // Se recorre el objeto clave valor y se insertan los valores en un Texto
        Iterator it = parametros.entrySet().iterator();
        boolean primero = true;
        String parametrosString = "";
        while (it.hasNext()){
            if(primero){
                primero = false;
            } else{
                parametrosString +="&"; //Se separan los datos con el caracter '&'
            }

            Map.Entry pair = (Map.Entry)it.next();
            parametrosString +=pair.getKey()+"="+pair.getValue(); //A cada dato se le asigna el valor tras el caracter '='
        }

        //Se devuelven los datos con sus valores como Texto
        return parametrosString;

    }

}
