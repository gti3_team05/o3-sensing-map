package org.g05.o3receptor.view.util.juego;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageView;

import org.g05.o3receptor.model.entity.juego.Fondo;
import org.g05.o3receptor.model.entity.juego.Personaje;

import static android.os.Looper.getMainLooper;

/**
 * Clase que representa la vista del juego como un View de Android
 * en un contenedor, y gestiona las interacciones entre el usuario y el juego.
 *
 * @author  Adrián García
 */
public class JuegoView extends SurfaceView implements Runnable {


    private Thread hiloPrincipal; // Hilo principal del modo juego
    private int pantallaX, pantallaY; // tamaño de la pantalla

    //private float pantallaRatioX, pantallaRatioY;

    // Variable para el control del estado del juego (activo o en pausa)
    private boolean estaJugando;

    // Instancias del motor de juego y de los graficos del juego
    private JuegoMotor jm;
    private JuegoGraficos jg;

    // Variables para gestionar el control de la alimentación de la mascota
    public boolean seHaSoltadoElObjeto = false;
    public int posX, posY;
    public ImageView imagenComida;


    // Visuales
    private Fondo fondoInicio, fondoFin; // Fondos en movimiento


    public JuegoView(Context context, int pantallaX, int pantallaY) {
        super(context);

        // Datos de la pantalla del dispositivo y del renderer
        this.pantallaX = pantallaX; this.pantallaY = pantallaY;

        this.jm = new JuegoMotor(context); // Se crea una instancia del motor de las mecánicas del juego
        this.jg = new JuegoGraficos(this, pantallaX, pantallaY);

    }


    @Override
    public void run() {

        // -------------------
        // Hilo principal de juego
        while (this.estaJugando){

            // En cada iteración, se actualizan los datos, se renderizan los elementos y se espera
            actualizar();
            renderizar();
            esperar();
        }

    }


    /**
     * Función para actualizar valores del juego
     * estados, y captar eventos
     */
    private void actualizar(){

        if (jm.getPersonaje().estoyVivo()){
            actualizarPersonaje();
        }

    }



    /**
     * Funcion para renderizar/dibujar lo visualmente necesario
     * para la vista, en funcion de los datos actualizados
     */
    private void renderizar(){


        this.jg.aplicarGraficosPrincipales();

        // Volvemos el objeto al sitio en caso de que el usuario la haya desplazado y posteriormente soltado
        if(seHaSoltadoElObjeto){
            this.jg.volverObjetoComdiaPosicionInicial(imagenComida, posX, posY);
            seHaSoltadoElObjeto = false;
        }


    }

    /**
     * ---------------------------------------------------------------------------
     * Se mira si la posición del alimento coincide con la de la mascota para saber
     * si está sobre ella o no.
     * ---------------------------------------------------------------------------
     * posActualImagenX: Z, posActualImagenY: Z --> elAlimentoEstaSobreLaMascota()
     * ---------------------------------------------------------------------------
     * @param posActualImagenX
     * @param posActualImagenY
     * @return
     */
    public boolean elAlimentoEstaSobreLaMascota(Float posActualImagenX, Float posActualImagenY){
        Personaje personaje = jg.getPersonaje();
        if(((personaje.getX() < posActualImagenX) && (personaje.getX()+500 > posActualImagenX))&&((personaje.getY() < posActualImagenY) && (personaje.getY()+500 > posActualImagenY))) {
            return true;
        }else{
            return false;
        }
    }


    /**
     * Pausa entre imagen e imagen, para mantener una
     * tasa de imagenes por segundo constante y dar la sensación de
     * movimiento al juego.
     */
    private void esperar(){

        // Espera 17 milisegundos  (la inversa de 0.017 segundos, son 60, que es el nº de imagenes por segundo
        try {
            Thread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    // ---------------------------------
    // Funciones para controlar el flujo del juego desde clases
    // padres
    // ---------------------------------

    public void iniciar(){
        this.estaJugando = true;

        this.hiloPrincipal = new Thread(this);
        this.hiloPrincipal.start();

    }


    public void pausar(){

        try {

            this.estaJugando = false;
            this.hiloPrincipal.join();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    } //()


    /**
     * Ciclo de actualuzacion de el personaje
     * en las iteraciones de juego
     */
    private void actualizarPersonaje(){

        // Cuando ha pasado un factor de tiempo desde la ultima actualizacion, se vuelve a actualizar
        long tiempoActual = System.currentTimeMillis();

        if(tiempoActual >= (jm.getTiempoEntreActualizaciones()+ jm.getUltimaActualizacion())) {

            this.jm.pasarTiempo(); // Psa el tiempo para el personaje
            this.jm.actualizarEstadoPersonaje(); //Se actualizan los estados/valores del personaje

            // Se comprueba el estado del personaje, (vivo, estados o como se muestra)
            this.jm.comprobarEstadoPersonaje();

            // Se actualiza el tiempo en el que se han actualizado los datos del personaje
            this.jm.setUltimaActualizacion(tiempoActual);
            Log.d("pruebadefuego","funcionaporfavor");

        } // end if

    } // ()


    public JuegoMotor getJuegoMotor() {
        return jm;
    }

    public JuegoGraficos getJuegoGraficos() {

        return jg;
    }
}
