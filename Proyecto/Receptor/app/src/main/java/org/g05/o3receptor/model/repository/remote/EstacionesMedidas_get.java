package org.g05.o3receptor.model.repository.remote;

import android.os.AsyncTask;
import android.util.Log;

import org.g05.o3receptor.model.entity.Medida;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Consulta las estaciones de medida de la base de datos
 * @author Carlos Ibanez Lamas
 */
public class EstacionesMedidas_get extends AsyncTask<Void, Void, String> {

    private final String recursoREST = "estacionesMedidas/";
    private final int CODIGO_PETICION = Constantes.PETICION_ESTACIONES;

    private TareaCallback callback;
    private List<Medida> datosPuntos;

    /**
     * Constructor de clase
     * @param callback   Callback para comprobar que se ejecuta correctamente la peticion
     * @param datos      Datos de la anterior peticion de la base (DatosInterpolados_get)
     */
    public EstacionesMedidas_get(TareaCallback callback, List<Medida> datos){
        this.datosPuntos = datos;
        this.callback = callback;
    }

    @Override
    protected String doInBackground(Void... voids) {
        Log.d("Datos estaciones","dentro doInBackground");
        String retorno = null;

        URL url;
        HttpURLConnection connection = null;

        try {
            // ---------------------------------------
            // Peticion GET datos en la URL

            url = new URL(Constantes.API_URI + recursoREST);

            // Crea una conexion HTTP para la url determinada
            connection = (HttpURLConnection) url.openConnection();

            // Se establece el tipo de la conexion
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "*/*");


            // Captura la respuesta
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String linea="", resultado = "";
            while ((linea = reader.readLine()) !=null){
                resultado += linea;
            }
            //Devuelve el resultado
            if(resultado.length()>0){
                retorno = resultado;
            }

            // Establece la conexion
            connection.connect();

            //Muestra por consola la respuesta
            Log.d("Peticion Estaciones", "Response: " + connection.getResponseMessage() + "");

        } catch (Exception e) {
            Log.d("Peticion Estaciones","Response "+e);
            Log.e(e.toString(), "Fallo en la solicitud");
        } finally {
            // Se asegura el cierre de conexion, en caso de haberla establecido
            if(connection!=null){
                connection.disconnect();
            }
        }

        return retorno;
    }


    @Override
    protected void onPostExecute(String resultado) {

        super.onPostExecute(resultado);
        boolean todoCorrecto = false;

        JSONArray datos = null;
        JSONObject resultadoJSON = null;

        if(resultado!=null){
            Log.d("ResultadoEstaciones",resultado);
            try {
                resultadoJSON = new JSONObject(resultado);
                datos = resultadoJSON.getJSONArray("datos");
                if(resultadoJSON.length()>0){ todoCorrecto = true;}
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } // end if

        // Si el resultado es el esperado...
        if(todoCorrecto){
            callback.terminadoCorrectoConDatosEstaciones(CODIGO_PETICION, datos, datosPuntos, "datosEstaciones");
        } else{
            callback.terminadoFallo(CODIGO_PETICION);
        }

    } // ()



}
