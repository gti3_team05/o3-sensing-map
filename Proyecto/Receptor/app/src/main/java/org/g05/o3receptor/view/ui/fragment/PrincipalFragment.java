package org.g05.o3receptor.view.ui.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.github.pengrad.mapscaleview.MapScaleView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.TileOverlay;
import com.google.android.gms.maps.model.TileOverlayOptions;
import com.google.maps.android.heatmaps.Gradient;
import com.google.maps.android.heatmaps.HeatmapTileProvider;
import com.google.maps.android.heatmaps.WeightedLatLng;

import org.g05.o3receptor.R;
import org.g05.o3receptor.model.entity.Medida;
import org.g05.o3receptor.model.repository.remote.DatosInterpolados_get;
import org.g05.o3receptor.model.repository.remote.DescargarImagen_get;
import org.g05.o3receptor.model.repository.remote.EstacionesMedidas_get;
import org.g05.o3receptor.model.repository.remote.EstacionesMedidas_post;
import org.g05.o3receptor.model.repository.remote.InicioSesion_get;
import org.g05.o3receptor.view.util.Constantes;
import org.g05.o3receptor.view.util.ProcesadorMediciones;
import org.g05.o3receptor.view.util.ServicioSegundoPlano;
import org.g05.o3receptor.view.util.TareaCallback;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Console;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class PrincipalFragment extends Fragment implements AdapterView.OnItemSelectedListener, TareaCallback {
    private final String ETIQUETA_LOG = "PRINCIPAL FRAGMENT";

    private final int SOLICITUD_PERMISO_LOCALIZACION = 99;
    private final int SOLICITUD_ENCENDIDO_BL = 98;

    private MapScaleView scaleView;

    private Activity actividadQueEjecuta;
    private View vistaRaiz;
    private Context contextoQueEjecuta;

    private String tipoMedidaSeleccionado = Constantes.TIPO_GAS_O3;
    private String tipoMapaTesteoSeleccionado = Constantes.TIPO_MAPA_TEST_RANDOM;

    private GoogleMap mapa;
    private TileOverlay tileOverlaySO;
    private FloatingActionButton floatingTester;
    private FloatingActionButton floatingView;

    private int mapType = 0;

    private boolean primeraVez = true;
    private boolean haSidoMostradaLaInterpolacion = false;
    private GroundOverlay newarkMap;

    private Marker markerUsuario;
    private List<Marker> markersLogros;
    private boolean hiloActivado = true;

    /**
     * Inflacion de la vista que representa el fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_principal, container, false);
        this.vistaRaiz = root;

        return root;
    }

    /**
     * Inicio de comprobaciones al crear la actividad y estar
     * infladas las vistas
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(estaElUsuarioLogueado()){
            // Se comprueba el bluetooth
            comprobarBlueetooth();

            // Se solicita el permiso de localizacion al usuario en caso de no estar concedido
            // Si se tiene permiso de localizacion, comienza el escaneo, sino se vuelve a solicitar
            boolean concedido = false;
            concedido = solicitarPermisos();
            if(concedido){
                iniciarServicio(ServicioSegundoPlano.class);
            }
        }

        inicilializarSpinnerTiposMedidas();
        inicializarBotonesDesplegables();
    }

    private String obtenerFechaActual(){
        DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd");
        Date date = new Date();

        return dateFormat.format(date);
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Comprobamos si el usuario está logueado y si tiene o no mascota
     * con la finalidad de mostrar unas u otras opciones
     * --------------------------------------------------------------
     * estaElUsuarioLogueado() --> VoF
     * --------------------------------------------------------------
     * @return VoF
     */
    private boolean estaElUsuarioLogueado(){
        SharedPreferences sharedPref = getActivity().getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        String correo  = sharedPref.getString(Constantes.PREF_CORREO, null); // Indica que está logeado
        String clave  = sharedPref.getString(Constantes.PREF_CLAVE, null); // Indica que está logeado
        boolean mascota  = sharedPref.getBoolean(Constantes.PREF_MASCOTA, true); // Indica que está logeado

        if(correo == null && clave == null){ // Está logeado
            return false;
        }else{
            return true;
        }
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Miramos mediante el sharedPreferences si el usuario quiere tener
     * mascota o no
     * --------------------------------------------------------------
     * estaActivadaLaMacota()
     * --------------------------------------------------------------
     * @return boolean
     */
    private boolean estaActivadaLaMacota(){
        SharedPreferences sharedPref = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE, MODE_PRIVATE);
        boolean mascota  = sharedPref.getBoolean(Constantes.PREF_MASCOTA, true); // Indica que está logeado
        if(mascota){
            return true;
        }else{
            return false;
        }
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Ponemos los correspondientes listeneres a los botones desplegables
     * --------------------------------------------------------------
     * inicializarBotonesDesplegables()
     * --------------------------------------------------------------
     */
    private void inicializarBotonesDesplegables(){
        FloatingActionButton floatingMasInfo = vistaRaiz.findViewById(R.id.floatingMasInfo);
        floatingMasInfo.setVisibility(View.VISIBLE);
        floatingMasInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarMasInfo(tipoMedidaSeleccionado);
            }
        });

        floatingTester = vistaRaiz.findViewById(R.id.floatingTester);
        inicializarFloatingTester(estaActivadoModoTester());

        floatingView = getActivity().findViewById(R.id.floatingView);
        floatingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mapType == 0) {
                    mapa.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    floatingView.setImageResource(R.drawable.ic_road_white_24dp);
                    mapType++;
                } else {
                    mapa.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    floatingView.setImageResource(R.drawable.ic_satellite_white_24dp);
                    mapType = 0;
                }
            }
        });
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Inicializamos el modo tester
     * --------------------------------------------------------------
     * estaActivado:VoF --> inicializarFloatingTester()
     * --------------------------------------------------------------
     * @param estaActivado
     */
    private void inicializarFloatingTester(boolean estaActivado){
        if(estaActivado){
            if(primeraVez){
                floatingTester.setVisibility(View.VISIBLE);
                primeraVez =  false;
            }

            floatingTester.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cambiarTipoMapaFake(floatingTester);
                    inicializarMapa(tipoMedidaSeleccionado,tipoMapaTesteoSeleccionado,Constantes.TIPO_FAKE);
                }
            });
        }else{
            Log.d(ETIQUETA_LOG,"el modo tester está desactivado");
            floatingTester.setVisibility(View.GONE);
        }
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     *  Solicita al usuario por pantalla la convesion de los permisos
     *  de ubicacion, en caso de no tenerlos
     * --------------------------------------------------------------
     * solicitarPermisos() --> Booleano
     * --------------------------------------------------------------
     * @return Devuelve si los permisos han sido concedidos o no
     */
    private boolean solicitarPermisos(){

        boolean resultado;

        // Si no se tienen condedidos los permisos de localizacion
        if (ContextCompat.checkSelfPermission(contextoQueEjecuta,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Si se requiere que el usuario acepte o no manualmente o no (depende del sistema)
            if (ActivityCompat.shouldShowRequestPermissionRationale(actividadQueEjecuta,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                //Crea un dialogo para solicitar el permiso al usuario
                new AlertDialog.Builder(contextoQueEjecuta)
                        .setTitle(R.string.titulo_permiso_localizacion)
                        .setMessage(R.string.mensaje_permiso_localizacion)
                        .setPositiveButton(R.string.conceder, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(actividadQueEjecuta,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        SOLICITUD_PERMISO_LOCALIZACION);
                            }
                        })
                        .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //No hacer nada, en caso de no obtener el permiso, se vuelve a solciitar,
                                // pues no se puede usar la aplicacion sin este permiso
                                solicitarPermisos();
                            }
                        })
                        .create()
                        .show(); //Muestra el dialogo


            } else {
                // Si no se requiere de la interaccion del usuario, se solicitan los permisos al sistema directamente.
                ActivityCompat.requestPermissions(actividadQueEjecuta,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        SOLICITUD_PERMISO_LOCALIZACION);
            } // end if


            resultado = false;
        } else {
            // Si los permisos ya estaban concedidos
            resultado = true;
        }

        return resultado;

    } // ()


    /**
     *
     * @return
     */
    private void comprobarBlueetooth(){

        BluetoothAdapter bla = BluetoothAdapter.getDefaultAdapter();

        if(bla!=null){
            if (!bla.isEnabled()) {
                cerrarServicio(ServicioSegundoPlano.class);

                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, SOLICITUD_ENCENDIDO_BL);

            }
        } // en caso de que sea nulo, el dispositivo no tiene Bluetooth (no es compatible)

    }

    // *************************************************************************************************************************************************************************** //
    // **************************************************************** SERVICIOS *********************************************************************************************** //

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * cerramos un servicio dado si está iniciado
     * --------------------------------------------------------------
     * serviceClass:Class--> solicitarPermisos()
     * --------------------------------------------------------------
     * @param serviceClass
     */
    private void cerrarServicio(Class<?> serviceClass){
        if(estaSiendoEjecutadoElServicio(serviceClass)){
            Intent intentBuscarDispositivosBTLE = new Intent(actividadQueEjecuta.getApplicationContext(), serviceClass);
            contextoQueEjecuta.stopService(intentBuscarDispositivosBTLE);
        }
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * iniciamos un servicio dado si no lo está
     * --------------------------------------------------------------
     * iniciarServicio:Class--> solicitarPermisos()
     * --------------------------------------------------------------
     * @param serviceClass
     */
    private void iniciarServicio(Class<?> serviceClass){
        if(!estaSiendoEjecutadoElServicio(serviceClass)){
            Intent intentBuscarDispositivosBTLE = new Intent(actividadQueEjecuta.getApplicationContext(), serviceClass);
            contextoQueEjecuta.startService(intentBuscarDispositivosBTLE);
        }
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * dado un servicio nos dice si ya está activado o no.
     * Es una clase auxiliar empleada para cosas como abrir y cerrar servicios
     * --------------------------------------------------------------
     * iniciarServicio:Class--> estaSiendoEjecutadoElServicio() --> VoF
     * --------------------------------------------------------------
     * @param serviceClass
     * @return
     */
    private boolean estaSiendoEjecutadoElServicio(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) actividadQueEjecuta.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    // *************************************************************************************************************************************************************************** //
    // **************************************************************** CICLO VIDA *********************************************************************************************** //


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.actividadQueEjecuta = getActivity(); // Obtengo la actividad desde la que se ejecuta el fragment
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.contextoQueEjecuta = context; // Obtengo el contexto desde la que se ejecuta el fragment

    }

    /**
     * -------------------------------------------------------------------------------------
     * -------------------------------------------------------------------------------------
     *  Callback para procesar la resolucion de la convesion de permisos
     * -------------------------------------------------------------------------------------
     * requestCode:Z, permissions:Texto, grantResults:[Z] --> onRequestPermissionsResult()
     * -------------------------------------------------------------------------------------
     * @param requestCode codigo de la solicitud
     * @param permissions vector de permisos
     * @param grantResults vector que contiene la resolucion de los permisos
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            // ----------------------------------------------------------
            // Permiso de localizacion
            case SOLICITUD_PERMISO_LOCALIZACION: {

                // Si hay algun permiso concedido para la app
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // Se comprueba si son los permisos de localizacion
                    if (ContextCompat.checkSelfPermission(contextoQueEjecuta,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        // -------------------------------------------
                        // Se inicia el escaneo de beacons
                        iniciarServicio(ServicioSegundoPlano.class);
                    }

                } else {
                    // Si el permiso de deniega, no se hace nada
                    // se requeire del permiso para ejecutar la aplicacion,
                    Log.e("ERROR", "Permiso de localizacion denegado...");
                    solicitarPermisos();

                }
                return;
            } // Fin permiso de localizacion

        } // end switch


    } // ()



    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Si es el encendido del bluetooth
        if(requestCode == SOLICITUD_ENCENDIDO_BL){
            if(resultCode == RESULT_OK){ //Bluetooth encendido
                iniciarServicio(ServicioSegundoPlano.class);
            } else{
                // Bluetooth NO encendido
                comprobarBlueetooth(); // Se vuelve a solicitar el encendido del Blueetooth
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        inicializarFloatingTester(estaActivadoModoTester());
        if(mapa!=null){
            if(estaActivadoModoTester()){
                inicializarMapa(tipoMedidaSeleccionado,tipoMapaTesteoSeleccionado,Constantes.TIPO_FAKE);
            }else{
                inicializarMapa(tipoMedidaSeleccionado,tipoMapaTesteoSeleccionado,Constantes.TIPO_REALES);
            }

            if(estaActivadaLaMacota() && estaElUsuarioLogueado()){
                hiloActivado = true;

                // Mostramos los markers de las recompensas
                mostrarMarkersRecompensas();

                // Mostramos el marker del usuario
                actualizarPosicionJugador();
            }else{
                hiloActivado = false;
                limpiarMarkersRecompensaYUsuario();
            }
        }
    }

    // *************************************************************************************************************************************************************************** //
    // **************************************************************** POPUP MÁS INFO ******************************************************************************************* //

    /**
     * --------------------------------------------------------------------------
     * --------------------------------------------------------------------------
     *  Dependiendo del gas que esté seleccionado, pasamos unos datos u otros
     *  al pop-up para que muestre la información según el gas que toque.
     * --------------------------------------------------------------------------
     * tipo:Texto
     * --------------------------------------------------------------------------
     * @param tipo
     */
    private void mostrarMasInfo(String tipo){
        Log.d("estadentro", tipo);
        switch (tipo){
            case Constantes.TIPO_GAS_O3:
                mostrarPopUp(getString(R.string.O3_masinfo_popup_titulo), getString(R.string.O3_masinfo_popup_contenido_reducido), getString(R.string.O3_masinfo_popup_contenido), getResources().getDrawable(R.drawable.semaforo_o3));
                break;
            case Constantes.TIPO_GAS_CO:
                mostrarPopUp(getString(R.string.CO_masinfo_popup_titulo), getString(R.string.CO_masinfo_popup_contenido_reducido), getString(R.string.CO_masinfo_popup_contenido), getResources().getDrawable(R.drawable.semaforo_co));
                break;
            case Constantes.TIPO_GAS_NO2:
                mostrarPopUp( getString(R.string.NO2_masinfo_popup_titulo), getString(R.string.NO2_masinfo_popup_contenido_reducido), getString(R.string.NO2_masinfo_popup_contenido), getResources().getDrawable(R.drawable.semaforo_no2));
                break;
            case Constantes.TIPO_GAS_SO2:
                mostrarPopUp(getString(R.string.SO2_masinfo_popup_titulo), getString(R.string.SO2_masinfo_popup_contenido_reducido), getString(R.string.SO2_masinfo_popup_contenido), getResources().getDrawable(R.drawable.semaforo_so2));
                break;
        }
    }

    /**
     * --------------------------------------------------------------------------
     * --------------------------------------------------------------------------
     *  Mostramos el popUp de más info, cambiando los datos y la
     *  información dependiendo del tipo de gas que esté seleccionado
     * --------------------------------------------------------------------------
     * titulo:Texto, contenido_reducido:Texto, contenido:Texto, imagen:Texto
     * --------------------------------------------------------------------------
     * @param titulo título para poner en el pop-up según el sensor seleccionado
     * @param contenido_reducido vector de permisos
     * @param contenido vector que contiene la resolucion de los permisos
     * @param imagen vector que contiene la resolucion de los permisos
     */
    private void mostrarPopUp(String titulo, final String contenido_reducido, final String contenido, Drawable imagen){
        // llamamos al pop up
        final Dialog dialog = new Dialog(actividadQueEjecuta);
        dialog.setContentView(R.layout.fragment_principal_masinfo_popup);

        // Llamamos a los componentes del pop-up que vamos a editar
        final LinearLayout mostrarMas = (LinearLayout) dialog.findViewById(R.id.btn_MostrarMas);
        final LinearLayout mostrarMenos = (LinearLayout) dialog.findViewById(R.id.btn_MostrarMenos);
        ImageView imgSemaforo = (ImageView) dialog.findViewById(R.id.img_Semaforo);
        TextView txtTitulo = (TextView) dialog.findViewById(R.id.txt_Titulo);
        final TextView txtContenido = (TextView) dialog.findViewById(R.id.txt_Contenido);

        // Rellenar texto e imagen según el sensor
        imgSemaforo.setBackground(imagen);
        txtTitulo.setText(titulo);
        txtContenido.setText(contenido_reducido);

        // mostrarMas Button
        mostrarMas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtContenido.setText(contenido);
                mostrarMas.setVisibility(View.GONE);
                mostrarMenos.setVisibility(View.VISIBLE);
            }
        });

        // mostrarMenos Button
        mostrarMenos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtContenido.setText(contenido_reducido);
                mostrarMenos.setVisibility(View.GONE);
                mostrarMas.setVisibility(View.VISIBLE);
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();
    }

    // *************************************************************************************************************************************************************************** //
    // ********************************************************************** SPINNERS ******************************************************************************************* //

    /**
     * --------------------------------------------------------------------------
     * --------------------------------------------------------------------------
     * Inicializamos el spinner del tipo de medidas con las distintas opciones
     * --------------------------------------------------------------------------
     * inicilializarSpinnerTiposMedidas()
     * --------------------------------------------------------------------------
     */
    private void inicilializarSpinnerTiposMedidas(){
        Spinner spinner = (Spinner) vistaRaiz.findViewById(R.id.spinner_tipos_gases);
        // Creamos el ArrayAdaptar utilizando el array de strings y el tipo de item del spinner layout que hemos creado
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(contextoQueEjecuta, R.array.tipos_medidas, R.layout.color_spinner_layout);
        // Especificamos el layut sobre el que van a aparecer todas als opciones al desplegarlo
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_layout);
        // Ponemos el escuchador
        spinner.setOnItemSelectedListener(this);
        // Aplicamos el spinner
        spinner.setAdapter(adapter);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
        String titulo = parent.getItemAtPosition(pos).toString();
        if(titulo.contains(Constantes.TIPO_GAS_O3)){

            tipoMedidaSeleccionado =  Constantes.TIPO_GAS_O3;

        }else if(titulo.contains(Constantes.TIPO_GAS_CO)){

            tipoMedidaSeleccionado =  Constantes.TIPO_GAS_CO;

        }else if(titulo.contains(Constantes.TIPO_GAS_NO2)){

            tipoMedidaSeleccionado =  Constantes.TIPO_GAS_NO2;

        }else if(titulo.contains(Constantes.TIPO_GAS_SO2)){

            tipoMedidaSeleccionado =  Constantes.TIPO_GAS_SO2;

        }

        if(estaActivadoModoTester()){
            inicializarMapa(tipoMedidaSeleccionado,tipoMapaTesteoSeleccionado,Constantes.TIPO_FAKE);
        }else{
            inicializarMapa(tipoMedidaSeleccionado,tipoMapaTesteoSeleccionado,Constantes.TIPO_REALES);
        }
    }

    public void onNothingSelected(AdapterView<?> parent) { }

    // *************************************************************************************************************************************************************************** //
    // ************************************************************************** MAPA ******************************************************************************************* //

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Cuando se haya creado la vista se inicializa el mapa
     * --------------------------------------------------------------
     * view:View, savedInstanceState: Bundle --> onViewCreated()
     * --------------------------------------------------------------
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        scaleView = vistaRaiz.findViewById(R.id.scaleView);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(callback);
        }
    }

    /**
     * ----------------------------------------------------------------
     * ----------------------------------------------------------------
     * Callback que nos avisa cuando el mapa está listo para ser usado
     * ----------------------------------------------------------------
     * googleMap: GoogleMap --> onMapReady()
     * ----------------------------------------------------------------
     */
    private OnMapReadyCallback callback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap googleMap) {
            mapa = googleMap;

            if(estaActivadaLaMacota() && estaElUsuarioLogueado()){
                // Mostramos los markers de las recompensas
                mostrarMarkersRecompensas();
                // Mostramos el marker del usuario
                actualizarPosicionJugador();
            }else{
                limpiarMarkersRecompensaYUsuario();
            }

            if(estaActivadoModoTester()){
                inicializarMapa(tipoMedidaSeleccionado,tipoMapaTesteoSeleccionado,Constantes.TIPO_FAKE);
            }else{
                inicializarMapa(tipoMedidaSeleccionado,tipoMapaTesteoSeleccionado,Constantes.TIPO_REALES);
            }

            mapa.setOnCameraMoveStartedListener(new GoogleMap.OnCameraMoveStartedListener() {
                @Override
                public void onCameraMoveStarted(int i) {
                    scaleView.update(mapa.getCameraPosition().zoom, mapa.getCameraPosition().target.latitude);
                }
            });

            mapa.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
                @Override
                public void onCameraMove() {
                    scaleView.update(mapa.getCameraPosition().zoom, mapa.getCameraPosition().target.latitude);
                }
            });

            mapa.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    scaleView.update(mapa.getCameraPosition().zoom, mapa.getCameraPosition().target.latitude);
                }
            });

        }
    };

    private void limpiarMarkersRecompensaYUsuario(){
        try{

            for(int i=0; i<markersLogros.size(); i++){
                markersLogros.get(i).remove();
                markersLogros.set(i,null);
            }
        }catch (Exception e){
            Log.d(ETIQUETA_LOG, "error "+e);
        }
        try{
            markerUsuario.remove();
            markerUsuario = null;
        }catch (Exception e){
            Log.d(ETIQUETA_LOG, "error "+e);
        }
    }

    /**
     * ----------------------------------------------------------------
     * ----------------------------------------------------------------
     * Mira en el sharedPreferences si el modo de testeo esta activado
     * ----------------------------------------------------------------
     * estaActivadoModoTester() -->VoF
     * ----------------------------------------------------------------
     * @return VoF
     */
    private boolean estaActivadoModoTester(){
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        return preferencias.getBoolean(Constantes.PREF_MODO_TESTER,false);
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Al hacer click sobre el botón de tipo de mapa fake, se cambia
     * el icono del botón y el respectivo mapa
     * --------------------------------------------------------------
     * boton:FloatingActionButton --> cambiarTipoMapaFake()
     * --------------------------------------------------------------
     */
    private void cambiarTipoMapaFake(FloatingActionButton boton){
        switch (tipoMapaTesteoSeleccionado){
            case Constantes.TIPO_MAPA_TEST_RANDOM:
                tipoMapaTesteoSeleccionado = Constantes.TIPO_MAPA_TEST_DEGRADADO;
                boton.setImageDrawable(getResources().getDrawable(R.drawable.ic_letra_d));
                break;
            case Constantes.TIPO_MAPA_TEST_DEGRADADO:
                tipoMapaTesteoSeleccionado = Constantes.TIPO_MAPA_TEST_VALORES_BAJOS;
                boton.setImageDrawable(getResources().getDrawable(R.drawable.ic_letra_b));
                break;
            case Constantes.TIPO_MAPA_TEST_VALORES_BAJOS:
                tipoMapaTesteoSeleccionado = Constantes.TIPO_MAPA_TEST_VALORES_MEDIOS;
                boton.setImageDrawable(getResources().getDrawable(R.drawable.ic_letra_m));
                break;
            case Constantes.TIPO_MAPA_TEST_VALORES_MEDIOS:
                tipoMapaTesteoSeleccionado = Constantes.TIPO_MAPA_TEST_VALORES_ALTOS;
                boton.setImageDrawable(getResources().getDrawable(R.drawable.ic_letra_a));
                break;
            case Constantes.TIPO_MAPA_TEST_VALORES_ALTOS:
                tipoMapaTesteoSeleccionado = Constantes.TIPO_MAPA_TEST_RANDOM;
                boton.setImageDrawable(getResources().getDrawable(R.drawable.ic_letra_r));
                break;
        }
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Esta funcion limpia por completo el mapa
     * --------------------------------------------------------------
     * limpiarMapa()
     * --------------------------------------------------------------
     */
    private void limpiarMapa(){
        try { tileOverlaySO.remove(); }catch (Exception e){ }
        try { newarkMap.remove(); }catch (Exception e){ }
    }

    /**
     * ---------------------------------------------------------------------
     * ---------------------------------------------------------------------
     * Inicializa el mapa, y segun si es de datos fake o de datos reales,
     * se crea el mapa o se realiza una petición al servidor para obtener
     * el mapa interpolado del dia
     * ---------------------------------------------------------------------
     * tipoGas:Txt, tipoDegradado:Txt, tipoDatos:Txt --> inicializarMapa()
     * ---------------------------------------------------------------------
     * @param tipoGas
     * @param tipoDegradado
     * @param tipoDatos
     */
    private void inicializarMapa(String tipoGas, String tipoDegradado, String tipoDatos){
        // En caso de que esté ya pintado el mapa se elimina para no crear unas capas encima de las otras
        limpiarMapa();

        // Obtenemos los datos reales o fake
        List<Medida> datos = new ArrayList<>();
        if(tipoDatos == Constantes.TIPO_FAKE){
            datos = generarMapaDatosFake(tipoGas,tipoDegradado,39.052614890343506,38.94460007805937,-0.14469997740041482,-0.226978537452108, 1000);
            cargarMarkers(datos, tipoGas);
        }else{
            String fecha = obtenerFechaActual();
            DatosInterpolados_get peticionDatos = new DatosInterpolados_get((TareaCallback)this, tipoMedidaSeleccionado, fecha);
            peticionDatos.execute();
        }

    }

    /**
     * ----------------------------------------------------------------
     * ----------------------------------------------------------------
     * Esta función se llama al finalizar la obtención (tanto de datos
     * reales como fakes) y consulta la base de datos para conseguir las
     * estaciones de medida de la BBDD
     * ----------------------------------------------------------------
     * tipoGas:Txt, datos:[Medida] --> cargarMarkers()
     * ----------------------------------------------------------------
     * @param tipoGas
     * @param datos
     */
    private void cargarMarkers(List<Medida> datos, String tipoGas){
        //Obtenemos estaciones para hacer markers
        EstacionesMedidas_get peticionEstaciones = new EstacionesMedidas_get((TareaCallback)this, datos);
        peticionEstaciones.execute();
    }

    /**
     * ----------------------------------------------------------------
     * ----------------------------------------------------------------
     * Esta función se llama al finalizar la obtención (tanto de datos
     * reales como fakes) y se representa en el mapa de la app el mapa
     * de google con una capa HeatMap que representa la concentración
     * ----------------------------------------------------------------
     * tipoGas:Txt, datos:[Medida] --> mostrarMapa()
     * ----------------------------------------------------------------
     * @param tipoGas
     * @param datos
     */
    private void mostrarMapa(String tipoGas, List<Medida> datos, List<JSONObject> estaciones){
        if(datos.size() != 0){

            Log.e("DATO",datos.get(0).getUbicacion().getLatitude() + "," + datos.get(0).getUbicacion().getLongitude());

            // Obtenemos el punto medio de los datos y centramos la cámara sobre él
            Location puntoMedio = obtenerPuntoMedio(datos);
            centrarCamara(12.8f, puntoMedio);

            // Convertimos la lista de medidas en el tipo necesario para poder representar los puntos
            List<WeightedLatLng> latLngs = convertirMedidasAHeatMapData(datos);

            // Generamos el gradiente en función de los valores bajos, medios y altos
            float valorBajo = 0.01f;
            float valorMedio = obtenerValorMedioEscala(tipoGas);
            float valorAlto = 1f;
            float[] puntosInicialesIntervalo3 = {valorBajo,valorMedio,valorAlto};

            int[] colores3Rojo = {Color.rgb(102, 225, 0), Color.rgb(255, 225, 0), Color.rgb(255, 0, 0)};

            Gradient gradiente = new Gradient(colores3Rojo, puntosInicialesIntervalo3);

            // Creamos la capa de puntos interpolados sobre el mapa
            HeatmapTileProvider provider = new HeatmapTileProvider.Builder()
                    .weightedData(latLngs)
                    .gradient(gradiente)
                    .radius(40)
                    .opacity(0.4)
                    .maxIntensity(1)
                    .build();

            // Añadimos la capa
            tileOverlaySO = mapa.addTileOverlay(new TileOverlayOptions().tileProvider(provider));
            tileOverlaySO.clearTileCache();
        }else if(!haSidoMostradaLaInterpolacion){
            Toast.makeText(contextoQueEjecuta, getResources().getString(R.string.titulo_error_datos), Toast.LENGTH_LONG).show();
        }

        if(estaciones.size() != 0){
            introducirMarkers(estaciones);
        }
    }

    /**
     * -------------------------------------------------------------------------------------------
     * -------------------------------------------------------------------------------------------
     * Devuelve una lista de medidas generadas de forma automática dadas una serie de condiciones
     * -------------------------------------------------------------------------------------------
     * tipoGas:Txt, tipoDegradado:Txt, latMin:R, latMax:R, lonMin:R, lonMax:R, numPuntos:N
     *      --> generarMapaDatosFake() --> [Medida]
     * -------------------------------------------------------------------------------------------
     * @param tipoGas
     * @param tipoDegradado
     * @param latMin
     * @param latMax
     * @param lonMin
     * @param lonMax
     * @param numPuntos
     * @return
     */
    private List<Medida> generarMapaDatosFake(String tipoGas, String tipoDegradado, double latMin, double latMax, double lonMin, double lonMax, int numPuntos){
        List<Medida> datos = new ArrayList<>();
        switch (tipoGas){
            case Constantes.TIPO_GAS_CO:
                datos = generarPuntosRandom(latMin,latMax,lonMin,lonMax,Constantes.VALOR_MINIMO_CO,Constantes.VALOR_MAXIMO_CO, Constantes.VALOR_MEDIO_CO,numPuntos,tipoDegradado);
                break;
            case Constantes.TIPO_GAS_NO2:
                datos = generarPuntosRandom(latMin,latMax,lonMin,lonMax,Constantes.VALOR_MINIMO_NO2,Constantes.VALOR_MAXIMO_NO2,Constantes.VALOR_MEDIO_NO2,numPuntos,tipoDegradado);
                break;
            case Constantes.TIPO_GAS_O3:
                datos = generarPuntosRandom(latMin,latMax,lonMin,lonMax,Constantes.VALOR_MINIMO_O3,Constantes.VALOR_MAXIMO_O3,Constantes.VALOR_MEDIO_O3,numPuntos,tipoDegradado);
                break;
            case Constantes.TIPO_GAS_SO2:
                datos = generarPuntosRandom(latMin,latMax,lonMin,lonMax,Constantes.VALOR_MINIMO_SO2,Constantes.VALOR_MAXIMO_SO2,Constantes.VALOR_MEDIO_SO2,numPuntos,tipoDegradado);
                break;
        }
        return datos;
    }

    /**
     * ------------------------------------------------------------------------------------------------------------
     * ------------------------------------------------------------------------------------------------------------
     * Dadas las latitudes y longitudes máximas y mínimas así como los pesos del gas a representar
     * , el número de puntos a representar y el tipo de degradado que queremos visualizar, se generará
     * de forma automática una serie de puntos-
     * ------------------------------------------------------------------------------------------------------------
     * latMin:R, latMax:R, lonMin:R, lonMax:R, pesoMin:R, pesoMax:R, pesoMedio:R, numPuntos:N, tipoDegradado:Txt
     *          --> ordenarPuntosMayorAMenor() --> [Medida]
     * ------------------------------------------------------------------------------------------------------------
     * @param latMin
     * @param latMax
     * @param lonMin
     * @param lonMax
     * @param pesoMin
     * @param pesoMax
     * @param pesoMedio
     * @param numPuntos
     * @param tipoDegradado
     * @return
     */
    private List<Medida> generarPuntosRandom(double latMin, double latMax, double lonMin, double lonMax, double pesoMin, double pesoMax, double pesoMedio, int numPuntos, String tipoDegradado ){
        List<Medida> datos = new ArrayList<>();
        for(int i=0; i<numPuntos; i++){
            // Obtenemos una longitud y latitud aleatoria comprendida entre los puntos máximos y mínimos
            double lat = (double) (Math.random() * (latMax - latMin)) + latMin;
            double longi = (double) (Math.random() * (lonMax - lonMin)) + lonMin;

            // Generamos un peso en función del tipo de degradado para cada medida
            double peso = 0;
            double pesoNormalizado = 0;
            switch(tipoDegradado){
                case "aleatorio":
                    peso = (double) (Math.random() * (pesoMax - pesoMin)) + pesoMin;
                    pesoNormalizado = (((peso - pesoMin) * (1f - 0.01f)) / (pesoMax - pesoMin)) + 0.01f;
                    break;
                case "degradado":
                    peso = 0;
                    break;
                case "valoresBajos":
                    pesoNormalizado = (((pesoMin - pesoMin) * (1f - 0.1f)) / (pesoMax - pesoMin)) + 0.1f;
                    break;
                case "valoresMedios":
                    pesoNormalizado = (((pesoMedio - pesoMin) * (1f - 0.01f)) / (pesoMax - pesoMin)) + 0.01f;
                    break;
                case "valoresAltos":
                    pesoNormalizado = (((pesoMax - pesoMin) * (1f - 0.1f)) / (pesoMax - pesoMin)) + 0.1f;
                    break;

            }

            // Creamos un objeto medida con los datos anteriores y lo almacenamos en la lista
            Location localizacion = new Location("");
            localizacion.setLatitude(lat);
            localizacion.setLongitude(longi);
            datos.add(new Medida(localizacion, (float)pesoNormalizado));
        }

        // Si se ha de mostrar un degradado llamamos a la función que los va a ordenar de mayor a menor y les va a asignar un peso ascendente
        if(tipoDegradado == "degradado"){
            datos = ordenarPuntosMayorAMenor(numPuntos, pesoMax, pesoMin, datos);
        }

        return datos;
    }

    /**
     * --------------------------------------------------------------------------------------------
     * --------------------------------------------------------------------------------------------
     * Dada una lista de medidas, las ordena de mayor a menor y les asigna un peso ascendente
     * con la finalidad de generar un degradado
     * --------------------------------------------------------------------------------------------
     * numPuntos:N, pesoFin:R, pesoIn:R, datos:[Medida]--> ordenarPuntosMayorAMenor() --> [Medida]
     * --------------------------------------------------------------------------------------------
     * @param numPuntos
     * @param pesoFin
     * @param pesoIn
     * @param datos
     * @return
     */
    private List<Medida> ordenarPuntosMayorAMenor(int numPuntos, double pesoFin, double pesoIn, List<Medida> datos){
        ArrayList<Double> puntosX = new ArrayList<>();
        // Hacemos una copia de la lista para no sobreescribirla
        List<Medida> listaFakeCopia = new ArrayList<>();
        listaFakeCopia.addAll(datos);

        // Recorremos la lista para guardar en la otra lista solo las longitudes que posteriormente ordenaremos de mayor a menor
        for(int i=0; i<listaFakeCopia.size(); i++){
            puntosX.add(listaFakeCopia.get(i).getUbicacion().getLongitude());
        }

        // Ordenanmos la lista de mayor a menor
        Comparator<Double> comparador = Collections.reverseOrder();
        Collections.sort(puntosX, comparador);

        // Borramos la lista
        datos.clear();

        for(int i=0; i<puntosX.size(); i++){
            for(int n=0; n<listaFakeCopia.size(); n++){
                if(listaFakeCopia.get(n).getUbicacion().getLongitude() == puntosX.get(i)){
                    // Asignamos un peso normalizado de menor a mayor para poder generar un degradado
                    datos.add(listaFakeCopia.get(n));
                    double peso = (float) (pesoFin/numPuntos)*i;
                    double pesoNormalizado = (((peso - pesoIn) * (1f - 0f)) / (pesoFin - pesoIn)) + 0.1f;
                    if(pesoNormalizado < 0){
                        pesoNormalizado = 0;
                    }
                    // Añadimos los puntos ordenados con sus respectivos pesos a la lista
                    datos.set(i,new Medida(listaFakeCopia.get(n).getUbicacion(),(float) pesoNormalizado));
                }
            }
        }

        return datos;
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Dada una lista de medidas, obtiene el punto medio
     * --------------------------------------------------------------
     * datos:[Medida]--> obtenerPuntoMedio() --> Location
     * --------------------------------------------------------------
     * @param datos
     * @return
     */
    private Location obtenerPuntoMedio(List<Medida> datos){
        Location localizacion = new Location("");
        double latMedia = 0;
        double lonMedia = 0;

        for(int i=0; i<datos.size(); i++){
            lonMedia += datos.get(i).getUbicacion().getLongitude();
            latMedia += datos.get(i).getUbicacion().getLatitude();
        }

        latMedia = latMedia/datos.size();
        localizacion.setLatitude(latMedia);
        lonMedia = lonMedia/datos.size();
        localizacion.setLongitude(lonMedia);

        return localizacion;
    }

    /**
     * ----------------------------------------------------------------------
     * ----------------------------------------------------------------------
     * Dada una lista de medidas, las convertimos en el formato necesario
     * para poder representalo en el mapa
     * ----------------------------------------------------------------------
     * datos:[Medida]--> convertirMedidasAHeatMapData() --> [WeightedLatLng]
     * ----------------------------------------------------------------------
     * @param datos
     * @return
     */
    private List<WeightedLatLng> convertirMedidasAHeatMapData(List<Medida> datos){
        List<WeightedLatLng> latLngs = new ArrayList<>();
        for(int i=0; i<datos.size(); i++){
            latLngs.add(new WeightedLatLng(new LatLng(datos.get(i).getUbicacion().getLatitude(), datos.get(i).getUbicacion().getLongitude()),datos.get(i).getMedidaNormalizada()));
        }

        return latLngs;
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Dado el punto central del mapa, redirigimos el mapa a él y
     * hacemos zoom
     * --------------------------------------------------------------
     * nivelZoom: Z, punto: Location --> centrarCamara()
     * --------------------------------------------------------------
     * @param nivelZoom
     * @param punto
     */
    private void centrarCamara(float nivelZoom, Location punto){
        mapa.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(punto.getLatitude(), punto.getLongitude())));
        mapa.setMinZoomPreference(nivelZoom);
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Obtenemos el valor medio para la escala en función del gas
     * ya que no es lo mismo para O3 (medio -->180, alto --> 240)
     * que para SO2 (medio -->125, alto --> 350)
     * --------------------------------------------------------------
     * tipoGas: Txt--> obtenerValorMedioEscala()
     * --------------------------------------------------------------
     * @param tipoGas
     * @return
     */
    private float obtenerValorMedioEscala(String tipoGas){
        float valorMedio = 0;

        switch (tipoGas){
            case Constantes.TIPO_GAS_CO:
                valorMedio = (float) Constantes.VALOR_MEDIO_CO / Constantes.VALOR_MAXIMO_CO;
                break;
            case Constantes.TIPO_GAS_NO2:
                valorMedio = (float) Constantes.VALOR_MEDIO_NO2 / Constantes.VALOR_MAXIMO_NO2;
                break;
            case Constantes.TIPO_GAS_O3:
                valorMedio = (float) Constantes.VALOR_MEDIO_O3 / Constantes.VALOR_MAXIMO_O3;
                break;
            case Constantes.TIPO_GAS_SO2:
                valorMedio = (float) Constantes.VALOR_MEDIO_SO2 / Constantes.VALOR_MAXIMO_SO2;
                break;
        }

        return valorMedio;
    }

    /**
     * -----------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------
     * Convierte una imagen png en un bitmap para usarlo como icono
     * personalizado
     * -----------------------------------------------------------------------------------
     * context: contextoActividad, vectorResId: drawable --> bitmapDescriptorFromVector()
     * -----------------------------------------------------------------------------------
     * @param context
     * @param vectorResId
     * @return
     */
    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void iniciado(int codigoSolicitud) {

    }

    @Override
    public void terminadoCorrecto(int codigoSolicitud) {

    }
    @Override
    public void terminadoCorrectoConDatos(int codigoSolicitud, JSONArray datos, String tipoDatos) {
    }


    /**
     * ----------------------------------------------------------------------------------------------------
     * ----------------------------------------------------------------------------------------------------
     * Despues de realizar una peticion a la api para conseguir valores
     * interpolados, se obtiene el punto medio resultante y se inserta la imagen
     * ----------------------------------------------------------------------------------------------------
     * codigoSolicitud: N, datos:[{lat:R, lon:R, valor:R}], tipoDatos: Txt--> obtenerValorMedioEscala()
     * ----------------------------------------------------------------------------------------------------
     * @param codigoSolicitud
     * @param datos
     * @param imagen
     * @param tipoDatos
     * @return
     */
    @Override
    public void terminadoCorrectoMapa(int codigoSolicitud, JSONArray datos, Bitmap imagen, String tipoDatos) {
        if(tipoDatos == "mapa"){
            try{
                // Obtenemos la latitud y longitud media
                double latitudMedia = 0;
                double longitudMedia = 0;
                int contador = 0;
                for(int i=0; i<datos.length(); i++){
                    JSONObject dato = datos.getJSONObject(i);
                    if(dato.getString("valor") != "NaN"){
                        contador++;
                        latitudMedia += (double)dato.getDouble("latitud");
                        longitudMedia += (double)dato.getDouble("longitud");
                    }
                }
                latitudMedia = latitudMedia/contador;
                longitudMedia = longitudMedia/contador;

                // Seteamos el punto medio
                LatLng newarkLatLng = new LatLng(latitudMedia, longitudMedia);
                Location puntoMedio = new Location("");
                puntoMedio.setLatitude(latitudMedia);
                puntoMedio.setLongitude(longitudMedia);

                // Mostramos la capa sobre el mapa, dandole la imagen correspondiente, con un nivel de opacidad y su posición en el mapa.
                newarkMap = mapa.addGroundOverlay(new GroundOverlayOptions()
                        .image(BitmapDescriptorFactory.fromBitmap(imagen))
                        .transparency(0.7f)
                        .position(newarkLatLng, 41600f, 33700f));

                // Centramos la cámara
                centrarCamara(12.8f, puntoMedio);
                // Ponemos a true la variable haSidoMostradaLaInterpolacion para que no haya conflictos con las llamadas a la visualización de mapas
                haSidoMostradaLaInterpolacion = true;

                // Al mostrar los markers le pasamos una lista vacia de puntos (ya que no hace falta mostrarlos, ya que mostramos la capa con la imagen)
                List<Medida> resultado = new ArrayList<>();
                cargarMarkers(resultado, tipoMedidaSeleccionado);
            }catch (Exception e){
                Log.d(ETIQUETA_LOG, "ERROR AL MOSTRAR LA IMAGEN");
                haSidoMostradaLaInterpolacion = false;
            }
        }
    }

    @Override
    public void terminadoFallo(int codigoSolicitud) {

    }

    // *************************************************************************************************************************************************************************** //
    // ****************************************************************** MARKERS ESTACIONES ************************************************************************************* //

    /**
     * -----------------------------------------------------------------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------------------------------------------------------------
     * Despues de realizar una peticion a la api para conseguir las estaciones de medida,
     * lo que se realiza aqui es un tratamiento de los datos
     * obtenidos de la petición asíncrona al servidor para luego inicializar el mapa
     * -----------------------------------------------------------------------------------------------------------------------------------------
     * codigoSolicitud: N, datos:[{lat:R, lon:R, nombre: Txt}], datosPuntos: List<Medidas>, tipoDatos: Txt--> terminadoCorrectoConDatosEstaciones()
     * -----------------------------------------------------------------------------------------------------------------------------------------
     * @param codigoSolicitud
     * @param datos
     * @param datosPuntos
     * @param tipoDatos
     * @return
     */
    @Override
    public void terminadoCorrectoConDatosEstaciones(int codigoSolicitud, JSONArray datos, List<Medida> datosPuntos, String tipoDatos){
        List<JSONObject> estaciones = new ArrayList<>();
        try{
            for(int i=0; i<datos.length(); i++){
                JSONObject dato = datos.getJSONObject(i);
                estaciones.add(dato);
            }
        }catch (Exception e){ }
        mostrarMapa(tipoMedidaSeleccionado,datosPuntos,estaciones);
    }

    /**
     * --------------------------------------------------------------
     * --------------------------------------------------------------
     * Introduce marcadores en el mapa de las estaciones de medida
     * obtenidas de la peticion de la BBDD
     * --------------------------------------------------------------
     * estaciones: JSON[lat:Z, lng:Z, nombre:Texto]--> introducirMarkers()
     * --------------------------------------------------------------
     * @param estaciones
     * @return
     */
    private void introducirMarkers (List<JSONObject> estaciones){
        for(int i = 0; i < estaciones.size(); i++){
            try {
                double latitud = estaciones.get(i).getDouble("lat");
                double longitud = estaciones.get(i).getDouble("lng");
                String nombre = estaciones.get(i).getString("nombre");
                mapa.addMarker(new MarkerOptions()
                        .position(new LatLng(latitud, longitud))
                        .title(nombre)
                        .icon(bitmapDescriptorFromVector(getActivity(), R.drawable.marker))
                );
            } catch (Exception e){ }
        }
    }

    // *************************************************************************************************************************************************************************** //
    // ****************************************************************** RECOMPENSAS ZONAS ************************************************************************************** //

    /**
     * ----------------------------------------------------------------
     * ----------------------------------------------------------------
     * Mostramos los markers con las zonas donde si acude el usuario
     * podrá obtener recompensas
     * ----------------------------------------------------------------
     * mostrarMarkersRecompensas()
     * ----------------------------------------------------------------
     */
    private void mostrarMarkersRecompensas(){
        markersLogros = new ArrayList<>();

        // Leemos en el shared preferences cuantas recompensas hay
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE_RECOMPENSAS,MODE_PRIVATE);

        int numeroRecompensas = preferencias.getInt(Constantes.PREF_RECOMPENSAS_NUMERO, 0);

        double latMedia = 0;
        double lonMedia = 0;
        int numeroZonas = 0;

        for(int i = 0; i < numeroRecompensas; i++){
            // Si la recompensa es de tipo zona
            if(preferencias.getString(Constantes.PREF_RECOMPENSA_TIPO+i,"").equals(Constantes.TIPO_RECOMPENSA_ZONA)){
                // Incrementamos el numero de zonas y la latitud y longitud media
                numeroZonas++;

                double latitud = Double.longBitsToDouble(preferencias.getLong(Constantes.PREF_RECOMPENSA_LATITUD+i, 0));
                double longitud = Double.longBitsToDouble(preferencias.getLong(Constantes.PREF_RECOMPENSA_LONGITUD+i, 0));
                latMedia += latitud;
                lonMedia += longitud;

                // Creamos un marker en función de si la recompensa ya ha sido obtenida o no
                Marker m = null;

                // Miramos si esa ubicación ya está obtenida
                if(preferencias.getBoolean(Constantes.PREF_RECOMPENSA_SE_PUEDE_COBRAR+i,false)){
                    m = mapa.addMarker(new MarkerOptions()
                            .position(new LatLng(latitud, longitud))
                            .title(getString(R.string.titulo_zona_obtenida))
                            .icon(bitmapDescriptorFromVector(getActivity(), R.drawable.marker_recompensa_disabled))
                    );
                }else{
                    m = mapa.addMarker(new MarkerOptions()
                            .position(new LatLng(latitud, longitud))
                            .title(preferencias.getString(Constantes.PREF_RECOMPENSA_TITULO+i,""))
                            .icon(bitmapDescriptorFromVector(getActivity(), R.drawable.marker_recompensa))
                    );
                }

                // Añadimos la zona a la lista para posteriormente is mirando si el usuario ha entrado en alguna y en consecuencia cambiar su marker
                markersLogros.add(m);
            }
        }

        // Obtenemos la latitud y longitud media de todas las estaciones
        latMedia = latMedia/numeroZonas;
        lonMedia = lonMedia/numeroZonas;

        // Creamos un objeto location con las latitudes y longitudes medias
        Location location = new Location("");
        location.setLongitude(lonMedia);
        location.setLatitude(latMedia);
        // Crentramos la cámara en ese punto con un nivel de zoom
        centrarCamara(13.2f, location);
    }

    /**
     * -----------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------
     * Cada x tiempo (5segundos) vamos actualizando la posición del usuario en el mapa.
     * NOTA: Para ello es necesario el uso de un hilo adicional y editar la ui desde su
     * propio hilo.
     * -----------------------------------------------------------------------------------
     * actualizarPosicionJugador()
     * -----------------------------------------------------------------------------------
     */
    private void actualizarPosicionJugador(){
        new Thread(new Runnable() {
            public void run() {
                // Tiempo en segundos que va a esperar
                int tiempoEnSegundos = 5;

                // Mientras que el hilo este activado actualizamos tanto la posición del usuario
                // como los markers. Esta variable pasa a ser false cuando la opción de la mascota esté desactivada en ajustes
                while(hiloActivado){
                    try {
                        // Esperamos 5 segundos
                        Thread.sleep(tiempoEnSegundos * 1000);
                        LatLng latLng = obtenerPosicionUsuario();

                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                // Si está el GPS encendido
                                if(estaElGPSEncendido()){
                                    try{
                                        // Si ya hay un marker de usuario lo quitamos
                                        if(markerUsuario != null){
                                            markerUsuario.remove();
                                            markerUsuario = null;
                                        }

                                        // Actualizamos el marker del usuario
                                        markerUsuario = mapa.addMarker(new MarkerOptions()
                                                .position(latLng)
                                                .title(obtenerNombreUsuario())
                                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_usuario))
                                        );
                                    }catch (Exception e){
                                        Log.d(ETIQUETA_LOG, "error al mostrar el marker");
                                    }
                                }else{
                                    // Quitamos el marker de la pantalla ya que entonces la posición del usuario no sería la correcta
                                    if(markerUsuario != null){
                                        markerUsuario.remove();
                                        markerUsuario = null;
                                    }
                                }

                                // Comprobamos si se ha conseguido algún logro
                                comprobarSiSeHaConseguidoUnLogro();
                            }
                        });

                    } catch (InterruptedException ie) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }).start();
    }

    /**
     * -----------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------
     * Comprobamos cada x periodo de tiempo (5 segundos) si el usuario ha conseguido
     * algún logro ya que se ha acercado mucho (< de 3m) a una zona con recompensa
     * -----------------------------------------------------------------------------------
     * comprobarSiSeHaConseguidoUnLogro()
     * -----------------------------------------------------------------------------------
     */
    private void comprobarSiSeHaConseguidoUnLogro(){
        // Miramos cuantas recompensas hay
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE_RECOMPENSAS,MODE_PRIVATE);

        int numeroRecompensas = preferencias.getInt(Constantes.PREF_RECOMPENSAS_NUMERO, 0);

        // Variable para saber posteriormente que marker de recompensa es
        int numeroContador = -1;
        for(int i = 0; i < numeroRecompensas; i++){
            // Si el tipo de recompensa es de zona
            if(preferencias.getString(Constantes.PREF_RECOMPENSA_TIPO+i,"").equals(Constantes.TIPO_RECOMPENSA_ZONA)){
                // Incrementamos el contador del marker
                numeroContador++;

                // Obtenemos la posicion del usuario
                LatLng usuarioLatLng = obtenerPosicionUsuario();

                // Obtenemos la latitud y longitud máxima y mínima de cada punto en función de un determinado radio para ver si el usuario ha entrado en la zona
                double latitud = Double.longBitsToDouble(preferencias.getLong(Constantes.PREF_RECOMPENSA_LATITUD+i, 0));
                double latMax = latitud+0.00002;
                double latMin = latitud-0.00002;
                double longitud = Double.longBitsToDouble(preferencias.getLong(Constantes.PREF_RECOMPENSA_LONGITUD+i, 0));
                double lonMax = longitud+0.00003;
                double lonMin = longitud-0.00003;

                // Si el usuario ha entrado en la zona
                if((usuarioLatLng.latitude>latMin && usuarioLatLng.latitude<latMax)&&(usuarioLatLng.longitude>lonMax && usuarioLatLng.longitude<lonMin)){
                    // Cambiamos el estado de la recompensa a: Conseguida
                    cambiarEstadoRecompensa(true, i);
                    // Cambiamos el marker
                    cambiarMarker(R.drawable.marker_recompensa_disabled,getString(R.string.titulo_zona_obtenida), numeroContador, new LatLng(latitud,longitud));
                }
            }
        }
    }

    /**
     * -----------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------
     * Cambiamos el marker de una recompensa para que aparezca como ya obtenida
     * -----------------------------------------------------------------------------------
     * comprobarSiSeHaConseguidoUnLogro()
     * -----------------------------------------------------------------------------------
     * @param icono
     * @param texto
     * @param pos
     * @param location
     */
    private void cambiarMarker(int icono, String texto, int pos, LatLng location){
        markersLogros.set(pos, mapa.addMarker(new MarkerOptions()
                .position(location)
                .title(obtenerNombreUsuario())
                .icon(BitmapDescriptorFactory.fromResource(icono))
        ));
    }

    /**
     * -----------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------
     * Modificamos en el sharedPreferences el estado de la recompensa a conseguida para
     * que el usuario pueda ir a cobrarla.
     * -----------------------------------------------------------------------------------
     * conseguido: VoF, posLista: N --> cambiarEstadoRecompensa()
     * -----------------------------------------------------------------------------------
     * @param conseguido
     * @param posLista
     */
    private void cambiarEstadoRecompensa(boolean conseguido, int posLista){
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE_RECOMPENSAS,MODE_PRIVATE);
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putBoolean(Constantes.PREF_RECOMPENSA_SE_PUEDE_COBRAR+posLista, conseguido);
        editor.commit();
    }

    /**
     * -----------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------
     * Miramos si está el GPS encendido
     * -----------------------------------------------------------------------------------
     * cambiarEstadoRecompensa() --> VoF
     * -----------------------------------------------------------------------------------
     * @return
     */
    private boolean estaElGPSEncendido(){
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        return preferencias.getBoolean(Constantes.PREF_GPS_ENCENDIDO,false);
    }

    /**
     * -----------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------
     * Obtenemos el nombre del usuario para:
     *  1. Ponerlo en el marker
     * -----------------------------------------------------------------------------------
     * obtenerNombreUsuario() --> Texto
     * -----------------------------------------------------------------------------------
     * @return
     */
    private String obtenerNombreUsuario(){
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        return preferencias.getString(Constantes.PREF_USUARIO,"Usuario");
    }

    /**
     * -----------------------------------------------------------------------------------
     * -----------------------------------------------------------------------------------
     * Obtenemos la última ubicación obtenida del usuario
     * -----------------------------------------------------------------------------------
     * obtenerPosicionUsuario() --> LatLng
     * -----------------------------------------------------------------------------------
     * @return
     */
    private LatLng obtenerPosicionUsuario(){
        SharedPreferences preferencias = contextoQueEjecuta.getSharedPreferences(Constantes.PREF_NOMBRE,MODE_PRIVATE);
        double lat = Double.longBitsToDouble(preferencias.getLong(Constantes.PREF_LATITUD_USUARIO, Double.doubleToLongBits(38.97462)));
        double lon = Double.longBitsToDouble(preferencias.getLong(Constantes.PREF_LONGITUD_USUARIO, Double.doubleToLongBits(-0.17990)));
        LatLng posicion = new LatLng(lat, lon);

        return posicion;
    }
}