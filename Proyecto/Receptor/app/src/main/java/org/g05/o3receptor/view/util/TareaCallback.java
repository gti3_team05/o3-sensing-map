package org.g05.o3receptor.view.util;


import android.graphics.Bitmap;

import org.g05.o3receptor.model.entity.Medida;
import org.json.JSONArray;

import java.util.List;

/**
 * Interfaz para ser implementada en tareas asincronas y poder realizar acciones
 * cuando se inicien o se terminen desde el contexto en el que se ejecutan dichas tareas
 *
 * @author Adrián García Andreu
 */
public interface TareaCallback {

    void iniciado(int codigoSolicitud); // Cuando una tarea comience
    void terminadoCorrecto(int codigoSolicitud); // Cuando una tarea termine satisfactoriamente
    void terminadoCorrectoConDatos(int codigoSolicitud, JSONArray datos, String tipoDatos); // Cuando una tarea termine satisfactoriamente
    void terminadoCorrectoMapa(int codigoSolicitud, JSONArray datos, Bitmap imagen, String tipoDatos); // Cuando una tarea termine satisfactoriamente
    void terminadoFallo(int codigoSolicitud); // Cuando una tarea termine con un fallo
    void terminadoCorrectoConDatosEstaciones(int codigoSolicitud, JSONArray datos, List<Medida> datosPuntos, String tipoDatos);

}
